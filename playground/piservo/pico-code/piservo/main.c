#include <stdio.h>
#include "pico/stdlib.h"
#include "pico/binary_info.h"
#include "pico/i2c_slave.h"
#include "bsp/board.h"
#include "hardware/gpio.h"
#include "hardware/adc.h"
#include "hardware/pwm.h"
#include "hardware/i2c.h"

#define PWM_CYCLES 10000
#define PWM_CYCLES_FACTOR PWM_CYCLES / 100

static const uint I2C_SLAVE_ADDRESS = 0x37;
static const uint I2C_BAUDRATE = 100000; // 100 kHz

// For this example, we run both the master and slave from the same board.
// You'll need to wire pin GP4 to GP6 (SDA), and pin GP5 to GP7 (SCL).
static const uint I2C_SLAVE_SDA_PIN = 10; 
static const uint I2C_SLAVE_SCL_PIN = 11;
static const uint I2C_MASTER_SDA_PIN = 12;
static const uint I2C_MASTER_SCL_PIN = 13;

uint8_t connected = 0;
int8_t power = 0;
uint slice_0;
uint slice_1;
uint slice_2;
i2c_slave_event_t last_i2c_event = 7;


static struct
{
    uint8_t mem[256];
    uint8_t mem_address;
    bool mem_address_written;
} context;

// Our handler is called from the I2C ISR, so it must complete quickly. Blocking calls /
// printing to stdio may interfere with interrupt handling.
static void i2c_slave_handler(i2c_inst_t *i2c, i2c_slave_event_t event) {
    last_i2c_event = event;
    switch (event) {
    case I2C_SLAVE_RECEIVE: // master has written some data
        if (!context.mem_address_written) {
            // writes always start with the memory address
            context.mem_address = i2c_read_byte_raw(i2c);
            context.mem_address_written = true;
        } else {
            // save into memory
            context.mem[context.mem_address] = i2c_read_byte_raw(i2c);
            context.mem_address++;
        }
        break;
    case I2C_SLAVE_REQUEST: // master is requesting data
        // load from memory
        i2c_write_byte_raw(i2c, context.mem[context.mem_address]);
        context.mem_address++;
        break;
    case I2C_SLAVE_FINISH: // master has signalled Stop / Restart
        context.mem_address_written = false;
        break;
    default:
        break;
    }
}

static void setup_slave() {
    gpio_init(I2C_SLAVE_SDA_PIN);
    gpio_set_function(I2C_SLAVE_SDA_PIN, GPIO_FUNC_I2C);
    gpio_pull_up(I2C_SLAVE_SDA_PIN);

    gpio_init(I2C_SLAVE_SCL_PIN);
    gpio_set_function(I2C_SLAVE_SCL_PIN, GPIO_FUNC_I2C);
    gpio_pull_up(I2C_SLAVE_SCL_PIN);

    i2c_init(i2c1, I2C_BAUDRATE);
    // configure I2C1 for slave mode
    i2c_slave_init(i2c1, I2C_SLAVE_ADDRESS, &i2c_slave_handler);
}

void print_help() {
    puts("\e[;H\nCommands:");
    puts("0, 1, 2\t: Select ADC channel n");
    puts("r\t: Sample once");
    puts("f\t: Freeing break");
    puts("<\t: Left");
    puts(">\t: Right");
}

void __not_in_flash_func(adc_capture)(uint16_t *buf, size_t count) {
    adc_fifo_setup(true, false, 0, false, false);
    adc_run(true);
    for (int i = 0; i < count; i = i + 1)
        buf[i] = adc_fifo_get_blocking();
    adc_run(false);
    adc_fifo_drain();
}

void change_motor_speed_high(int8_t power) {
    int pwm = 0;
    if (power == 0) {
        pwm_set_chan_level(slice_1, PWM_CHAN_A, PWM_CYCLES - 1);
        pwm_set_chan_level(slice_1, PWM_CHAN_B, PWM_CYCLES - 1);
    } else if (power < 0) {
        pwm = (100 + power) * PWM_CYCLES_FACTOR;
        pwm_set_chan_level(slice_1, PWM_CHAN_A, PWM_CYCLES);
        pwm_set_chan_level(slice_1, PWM_CHAN_B, pwm);
    } else {
        pwm = (100 - power) * PWM_CYCLES_FACTOR;
        pwm_set_chan_level(slice_1, PWM_CHAN_A, pwm);
        pwm_set_chan_level(slice_1, PWM_CHAN_B, PWM_CYCLES);
    }
    printf("New speed high %d, pwm %d\n", power, pwm);
}

void change_motor_speed_low(int8_t power) {
    int pwm = 0;
    if (power == 0) {
        pwm_set_chan_level(slice_1, PWM_CHAN_A, 0);
        pwm_set_chan_level(slice_1, PWM_CHAN_B, 0);
    } else if (power < 0) {
        pwm = -power * PWM_CYCLES_FACTOR;
        pwm_set_chan_level(slice_1, PWM_CHAN_A, 0);
        pwm_set_chan_level(slice_1, PWM_CHAN_B, pwm);
    } else {
        pwm = power * PWM_CYCLES_FACTOR;
        pwm_set_chan_level(slice_1, PWM_CHAN_A, pwm);
        pwm_set_chan_level(slice_1, PWM_CHAN_B, 0);
    }
    printf("New speed high %d, pwm %d\n", power, pwm);
}

static void run_master() {
    gpio_init(I2C_MASTER_SDA_PIN);
    gpio_set_function(I2C_MASTER_SDA_PIN, GPIO_FUNC_I2C);
    // pull-ups are already active on slave side, this is just a fail-safe in case the wiring is faulty
    gpio_pull_up(I2C_MASTER_SDA_PIN);

    gpio_init(I2C_MASTER_SCL_PIN);
    gpio_set_function(I2C_MASTER_SCL_PIN, GPIO_FUNC_I2C);
    gpio_pull_up(I2C_MASTER_SCL_PIN);

    i2c_init(i2c0, I2C_BAUDRATE);

    uint8_t mem_address = 0;
    // for (uint8_t mem_address = 0;; mem_address = (mem_address + 32) % 256) {
        char msg[32];
        snprintf(msg, sizeof(msg), "Hello, I2C slave! - 0x%02X", mem_address);
        uint8_t msg_len = strlen(msg);

        uint8_t buf[32];
        buf[0] = mem_address;
        memcpy(buf + 1, msg, msg_len);
        // write message at mem_address
        printf("Write at 0x%02X: '%s'\n", mem_address, msg);
        int count = i2c_write_blocking(i2c0, I2C_SLAVE_ADDRESS, buf, 1 + msg_len, false);
        if (count < 0) {
            puts("Couldn't write to slave, please check your wiring!");
            return;
        }
        hard_assert(count == 1 + msg_len);

        // seek to mem_address
        count = i2c_write_blocking(i2c0, I2C_SLAVE_ADDRESS, buf, 1, true);
        hard_assert(count == 1);
        // partial read
        uint8_t split = 5;
        count = i2c_read_blocking(i2c0, I2C_SLAVE_ADDRESS, buf, split, true);
        hard_assert(count == split);
        buf[count] = '\0';
        printf("Read  at 0x%02X: '%s'\n", mem_address, buf);
        hard_assert(memcmp(buf, msg, split) == 0);
        // read the remaining bytes, continuing from last address
        count = i2c_read_blocking(i2c0, I2C_SLAVE_ADDRESS, buf, msg_len - split, false);
        hard_assert(count == msg_len - split);
        buf[count] = '\0';
        printf("Read  at 0x%02X: '%s'\n", mem_address + split, buf);
        hard_assert(memcmp(buf, msg + split, msg_len - split) == 0);

        puts("");
        // sleep_ms(2000);
    // }
}

int main(void) {
    setup_slave();
    tusb_init();
    stdio_init_all();
    adc_init();
    adc_set_temp_sensor_enabled(true);

    gpio_set_dir_all_bits(0);
    for (int i = 26; i <= 28; ++i) {
        gpio_set_function(i, GPIO_FUNC_SIO);
        gpio_disable_pulls(i);
        gpio_set_input_enabled(i, false);
    }

    for (int i = 0; i < 4; i++) {
        gpio_set_function(i, GPIO_FUNC_PWM);
    }

    slice_0 = pwm_gpio_to_slice_num(0);
    slice_1 = pwm_gpio_to_slice_num(2);
    // slice_2 = pwm_gpio_to_slice_num(4);

    pwm_set_wrap(slice_0, PWM_CYCLES);
    pwm_set_wrap(slice_1, PWM_CYCLES);
    pwm_set_wrap(slice_2, PWM_CYCLES);
    pwm_set_chan_level(slice_0, PWM_CHAN_A, 0);
    pwm_set_chan_level(slice_0, PWM_CHAN_B, 0);
    pwm_set_chan_level(slice_1, PWM_CHAN_A, PWM_CYCLES - 1);
    pwm_set_chan_level(slice_1, PWM_CHAN_B, PWM_CYCLES - 1);
    // pwm_set_chan_level(slice_2, PWM_CHAN_A, 0);
    // pwm_set_chan_level(slice_2, PWM_CHAN_B, 0);
    pwm_set_enabled(slice_0, true);
    pwm_set_enabled(slice_1, true);
    // pwm_set_enabled(slice_2, true);


    while (1) {
        tud_task(); // tinyusb device task
        if (!connected && tud_cdc_connected()) {
            print_help();
            printf("RP2040 Connected\n");
            connected = 1;
        }

        if (last_i2c_event != 7) {
            printf("Received i2c event %d", last_i2c_event);
            last_i2c_event = 7;
        }

        if (tud_cdc_available()) {
            char c = getchar();
            // printf("%c", c);
            switch (c) {
                case '<':
                    if (power > -100) {
                        power -= 10;
                        if (power < -100) {
                            power = -100;
                        }
                    }
                    change_motor_speed_high(power);
                    break;
                case '>':
                    if (power < 100) {
                        power += 10;
                        if (power > 100) {
                            power = 100;
                        }
                    }
                    change_motor_speed_high(power);
                    break;
                case ',':
                    if (power > -100) {
                        power -= 2;
                        if (power < -100) {
                            power = -100;
                        }
                    }
                    change_motor_speed_low(power);
                    break;
                case '.':
                    if (power < 100) {
                        power += 2;
                        if (power > 100) {
                            power = 100;
                        }
                    }
                    change_motor_speed_low(power);
                    break;
                case 'f':
                    pwm_set_chan_level(slice_1, PWM_CHAN_A, 0);
                    pwm_set_chan_level(slice_1, PWM_CHAN_B, 0);
                    printf("Releasing break");
                    break;
                case '0':
                case '1':
                case '2':
                    printf("%c\n", c);
                    if (c < '0' || c > '7') {
                        printf("Unknown input channel\n");
                        print_help();
                    } else {
                        adc_select_input(c - '0');
                        printf("Switched to channel %c\n", c);
                    }
                    break;
                case 'r': {
                    uint32_t result = adc_read();
                    const float conversion_factor = 3.3f / (1 << 12);
                    printf("\n0x%03x -> %f V\n", result, result * conversion_factor);
                    break;
                }
                case '\n':
                case '\r':
                    break;
                case 'h':
                    print_help();
                    break;
                case 'i':
                    run_master();
                    break;
                default:
                    printf("\nUnrecognised command: %c\n", c);
                    print_help();
                    break;
            }
        }
    }
}
