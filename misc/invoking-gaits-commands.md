


## Relax

```bash
mosquitto_pub -h 172.24.1.175 -t gait/invoke/relax -m "height=90,x=-5"
```


## Position (stop, stand...)

```bash
mosquitto_pub -h 172.24.1.175 -t gait/invoke/position -m "height=90,x=0,y=0,ox=0,oy=-10,cx=0,cy=0,speed=0.5"
```

## Working walk gait:

```bash
mosquitto_pub -h 172.24.1.175 -t gait/invoke/cat_walk -m "speed=4,ox=-2,cy=-5,tx=0.5,ty=0.5,stride=20,height=85"

mosquitto_pub -h 172.24.1.175 -t gait/invoke/cat_walk -m "speed=2,ox=-2,cy=-5,tx=0.5,ty=0.5,stride=100,height=80"

#mosquitto_pub -h 172.24.1.175 -t gait/invoke/cat_walk -m "speed=1,ox=-2,cy=-5,tx=0.5,ty=0.5,stride=70,height=80"
mosquitto_pub -h 172.24.1.175 -t gait/invoke/cat_walk -m "speed=4,height=80,ox=-5,cx=-10,ty=0.2,tx=0.0,stride=80,cog_speed=.4"


mosquitto_pub -h 172.24.1.175 -t gait/invoke/cat_walk -m "speed=4,height=80,cx=0,ty=0,tx=0.4,stride=70"
mosquitto_pub -h 172.24.1.175 -t gait/invoke/cat_walk -m "speed=4,height=80,cx=0,ty=0,tx=0.4,stride=30"

---
mosquitto_pub -h 172.24.1.176 -t gait/invoke/cat_walk_dp4 -m "speed=2,height=75,cy=20,leg_raise=30,stride=80,ox=-15"

mosquitto_pub -h 172.24.1.176 -t gait/invoke/cat_walk_dp4 -m "speed=2,height=80,cy=20,leg_raise=30,stride=75,ox=-15"


```


## Working trot gait:

```bash
mosquitto_pub -h 172.24.1.175 -t gait/invoke/trot -m "height=90,stride=20,speed=0.8"
```

