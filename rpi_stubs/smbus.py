
#
# Copyright 2016-2017 Games Creators Club
#
# MIT License
#


class SMBus:
    def __init__(self, _bus_no):
        # raise NotImplemented()
        pass

    def write_byte(self, _i2c_address, _byte):
        raise NotImplemented()

    def write_byte_data(self, _i2c_address, _local_address, _data):
        raise NotImplemented()

    def write_block_data(self, _i2c_address, _local_address, _data_array):
        raise NotImplemented()

    def read_byte(self, _i2c_address):
        raise NotImplemented()

    def read_byte_data(self, _i2c_address, _local_address):
        raise NotImplemented()

    def read_block_data(self, _i2c_address, _local_address):
        raise NotImplemented()

    def write_i2c_block_data(self, _i2c_address, _local_address, _data):
        raise NotImplemented()

    def read_i2c_block_data(self, _i2c_address, _local_address, _count):
        raise NotImplemented()

    def read_word_data(self, _i2c_address, _local_address):
        raise NotImplemented()

    def write_word_data(self, _i2c_address, _local_address, _word):
        raise NotImplemented()
