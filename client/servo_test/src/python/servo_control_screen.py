import pyros

from pygame import Rect

from monitor.current_graphs_panel import GraphsPanel
from pyros_support_ui.text_toggle_button import TextToggleButton
from pyros_support_ui import PyrosClientApp
from pyros_support_ui.components import Collection, LeftRightLayout, BaseUIFactory, Component, UiHint
from servo_status_panel import ServoStatusPanel

from telemetry_utils.telemetry_controller import TelemetryController
from ui.screen import Screen


class ServoControlScreen(Screen):

    CURRENT_SENSOR_PANEL_WIDTH = 400
    BATTERY_PANEL_WIDTH = 300

    def __init__(self, main_app: PyrosClientApp, ui_factory: BaseUIFactory,
                 telemetry_controller: TelemetryController):
        super().__init__(main_app)
        self.telemetry_controller = telemetry_controller

        self.commands_panel = ui_factory.panel(None, layout=LeftRightLayout(margin=10), hint=UiHint.NO_DECORATION)

        self.sensors_graphs_panel = GraphsPanel(None, ui_factory)

        self.add_component(self.sensors_graphs_panel)
        self.servo_status_panel = ServoStatusPanel(None, ui_factory)
        self.add_component(self.servo_status_panel)

        self.commands_panel.add_component(Component(Rect(0, 0, 10, 20)))

        self.servos_buttons = TextToggleButton(Rect(0, 0, 160, 20),
                                               ui_factory,
                                               on_text="Start Servos", on_click_on=self.start_servos,
                                               off_text="Stop Servos", on_click_off=self.stop_servos)
        self.commands_panel.add_component(self.servos_buttons)

        # self.commands_panel.add_component(ui_factory.text_button(Rect(0, 0, 160, 20), "Stop", on_click=self.stop_servo))
        # self.commands_panel.add_component(ui_factory.text_button(Rect(0, 0, 160, 20), "Middle", on_click=self.middle_servo))
        # self.commands_panel.add_component(ui_factory.text_button(Rect(0, 0, 100, 20), " < ", on_click=self.left_servo))
        # self.commands_panel.add_component(ui_factory.text_button(Rect(0, 0, 100, 20), " > ", on_click=self.right_servo))

        self.control_panel = ui_factory.panel(Rect(0, 0, 200, 200))
        self.add_component(self.control_panel)

    def on_show(self):
        self.telemetry_controller.set_on_connection_callback(self._on_telemetry_connection)

    def on_hide(self):
        self.telemetry_controller.clear_on_connection_callback()

    def _on_telemetry_connection(self, connected: bool) -> None:
        if connected:
            self._setup_telemetry_client(self.telemetry_controller)

    def start_servos(self, *_args):
        self.servos_buttons.toggle_off()

    def stop_servos(self, *_args):
        self.servos_buttons.toggle_on()

    def set_contributing_panel(self, contributing_command_panel: Collection):
        contributing_command_panel.add_component(self.commands_panel)

    def _setup_telemetry_client(self, telemetry_controller: TelemetryController):
        self.sensors_graphs_panel.setup_telemetry_client(telemetry_controller)

    def draw(self, surface):
        super().draw(surface)

    def redefine_rect(self, rect):
        self.rect = rect
        self.sensors_graphs_panel.redefine_rect(
            Rect(5, rect.y, rect.right - self.BATTERY_PANEL_WIDTH - 10, rect.height))
        self.servo_status_panel.redefine_rect(
            Rect(self.sensors_graphs_panel.rect.right + 5, rect.y, self.BATTERY_PANEL_WIDTH, rect.height)
        )
        self.control_panel.redefine_rect(
            Rect(self.sensors_graphs_panel.rect.right + 5, rect.bottom - 200, self.BATTERY_PANEL_WIDTH, rect.height)
        )

    @staticmethod
    def calibrate(*_args):
        pyros.publish("balancing/calibrate", "all")
