
import sys

if len(sys.argv) < 2:
    print(f"Missing filename argument")
    sys.exit(1)

filename = sys.argv[1]

with open(filename, "rt") as f:
    lines = f.readlines()

headers = {k: i for i, k in enumerate(lines[0].split(","))}
lines = lines[1:]

pos_column = headers["position"]
angle_column = headers["angle"]

up = []
down = []

going_up = True
middle = False
prev_angle = 0
prev_pos = 0
done = False

for line in lines:
    s = line.split(",")
    pos = int(s[pos_column])
    angle = float(s[angle_column])
    if not done:
        if middle:
            if pos <= 2840:
                middle = False
                going_up = False
        if 510 <= pos <= 2840:
            if pos != prev_pos and prev_pos != 0:
                if going_up:
                    up.append((prev_pos, prev_angle))
                else:
                    down.append((prev_pos, prev_angle))
            prev_pos = pos
            prev_angle = angle
        else:
            if going_up and pos > 2840:
                if pos == 2841:
                    up.append((prev_pos, prev_angle))
                middle = True
                prev_pos = 0
                prev_angle = 0
            elif not going_up and pos < 510:
                if pos == 509:
                    down.append((prev_pos, prev_angle))
                done = True


up_filename = filename[:-4] + "-up.csv"
down_filename = filename[:-4] + "-down.csv"

with open(up_filename, "wt") as f:
    for pos_angle in up:
        f.write(str(pos_angle[0]) + "," + str(pos_angle[1]) + "\n")

with open(down_filename, "wt") as f:
    for pos_angle in down:
        f.write(str(pos_angle[0]) + "," + str(pos_angle[1]) + "\n")
