import threading
from typing import Callable

import time
import traceback

import pyroslib
from telemetry import SocketTelemetryServer

import smbus


# constants

# /*=========================================================================
#    I2C ADDRESS/BITS
#    -----------------------------------------------------------------------*/
INA3221_ADDRESS = (0x40)  # 1000000 (A0+A1=GND)
INA3221_READ = (0x01)
# /*=========================================================================*/

# /*=========================================================================
#    CONFIG REGISTER (R/W)
#    -----------------------------------------------------------------------*/
INA3221_REG_CONFIG = (0x00)
#    /*---------------------------------------------------------------------*/
INA3221_CONFIG_RESET = (0x8000)  # Reset Bit

INA3221_CONFIG_ENABLE_CHAN1 = (0x4000)  # Enable Channel 1
INA3221_CONFIG_ENABLE_CHAN2 = (0x2000)  # Enable Channel 2
INA3221_CONFIG_ENABLE_CHAN3 = (0x1000)  # Enable Channel 3

INA3221_CONFIG_AVG2 = (0x0800)  # AVG Samples Bit 2 - See table 3 spec
INA3221_CONFIG_AVG1 = (0x0400)  # AVG Samples Bit 1 - See table 3 spec
INA3221_CONFIG_AVG0 = (0x0200)  # AVG Samples Bit 0 - See table 3 spec

INA3221_CONFIG_VBUS_CT2 = (0x0100)  # VBUS bit 2 Conversion time - See table 4 spec
INA3221_CONFIG_VBUS_CT1 = (0x0080)  # VBUS bit 1 Conversion time - See table 4 spec
INA3221_CONFIG_VBUS_CT0 = (0x0040)  # VBUS bit 0 Conversion time - See table 4 spec

INA3221_CONFIG_VSH_CT2 = (0x0020)  # Vshunt bit 2 Conversion time - See table 5 spec
INA3221_CONFIG_VSH_CT1 = (0x0010)  # Vshunt bit 1 Conversion time - See table 5 spec
INA3221_CONFIG_VSH_CT0 = (0x0008)  # Vshunt bit 0 Conversion time - See table 5 spec

INA3221_CONFIG_MODE_2 = (0x0004)  # Operating Mode bit 2 - See table 6 spec
INA3221_CONFIG_MODE_1 = (0x0002)  # Operating Mode bit 1 - See table 6 spec
INA3221_CONFIG_MODE_0 = (0x0001)  # Operating Mode bit 0 - See table 6 spec

# /*=========================================================================*/

# /*=========================================================================
#    SHUNT VOLTAGE REGISTER (R)
#    -----------------------------------------------------------------------*/
INA3221_REG_SHUNTVOLTAGE_1 = (0x01)
# /*=========================================================================*/

# /*=========================================================================
#    BUS VOLTAGE REGISTER (R)
#    -----------------------------------------------------------------------*/
INA3221_REG_BUSVOLTAGE_1 = (0x02)
# /*=========================================================================*/

SHUNT_RESISTOR_VALUE = (0.1)  # default shunt resistor value of 0.1 Ohm


class INA3221():

    ###########################
    # INA3221 Code
    ###########################
    def __init__(self, i2c_bus=1, addr=INA3221_ADDRESS, shunt_resistor=SHUNT_RESISTOR_VALUE):
        self._bus = smbus.SMBus(i2c_bus)
        self._addr = addr
        config = 0
        config |= INA3221_CONFIG_ENABLE_CHAN1
        config |= INA3221_CONFIG_ENABLE_CHAN2
        config |= INA3221_CONFIG_ENABLE_CHAN3
        # config |= INA3221_CONFIG_AVG0
        config |= INA3221_CONFIG_AVG1
        # config |= INA3221_CONFIG_AVG2
        # config |= INA3221_CONFIG_VBUS_CT0
        # config |= INA3221_CONFIG_VBUS_CT1
        config |= INA3221_CONFIG_VBUS_CT2
        # config |= INA3221_CONFIG_VSH_CT0
        # config |= INA3221_CONFIG_VSH_CT1
        config |= INA3221_CONFIG_VSH_CT2
        config |= INA3221_CONFIG_MODE_2
        # config |= INA3221_CONFIG_MODE_1
        config |= INA3221_CONFIG_MODE_0

        self._write_register_little_endian(INA3221_REG_CONFIG, config)

    def _write(self, register, data):
        # print "addr =0x%x register = 0x%x data = 0x%x " % (self._addr, register, data)
        self._bus.write_byte_data(self._addr, register, data)

    def _read(self, data):

        returndata = self._bus.read_byte_data(self._addr, data)
        # print "addr = 0x%x data = 0x%x %i returndata = 0x%x " % (self._addr, data, data, returndata)
        return returndata

    def _read_register_little_endian(self, register):

        result = self._bus.read_word_data(self._addr, register) & 0xFFFF
        lowbyte = (result & 0xFF00) >> 8
        highbyte = (result & 0x00FF) << 8
        switchresult = lowbyte + highbyte
        # print "Read 16 bit Word addr =0x%x register = 0x%x switchresult = 0x%x " % (self._addr, register, switchresult)
        return switchresult

    def _write_register_little_endian(self, register, data):

        data = data & 0xFFFF
        # reverse configure byte for little endian
        lowbyte = data >> 8
        highbyte = (data & 0x00FF) << 8
        switchdata = lowbyte + highbyte
        self._bus.write_word_data(self._addr, register, switchdata)
        # print "Write  16 bit Word addr =0x%x register = 0x%x data = 0x%x " % (self._addr, register, data)

    def _getBusVoltage_raw(self, channel):
        # Gets the raw bus voltage (16-bit signed integer, so +-32767)

        value = self._read_register_little_endian(INA3221_REG_BUSVOLTAGE_1 + (channel - 1) * 2)
        if value > 32767:
            value -= 65536
        return value

    def getShuntVoltage_raw(self, channel):
        # Gets the raw shunt voltage (16-bit signed integer, so +-32767)

        value = self._read_register_little_endian(INA3221_REG_SHUNTVOLTAGE_1 + (channel - 1) * 2)
        if value > 32767:
            value -= 65536
        return value

    # public functions

    def getBusVoltage_V(self, channel):
        # Gets the Bus voltage in volts

        value = self._getBusVoltage_raw(channel)
        return value * 0.001

    def getShuntVoltage_mV(self, channel):
        # Gets the shunt voltage in mV (so +-168.3mV)

        value = self.getShuntVoltage_raw(channel)
        return value * 0.005

    def getCurrent_mA(self, channel):
        # Gets the current value in mA, taking into account the config settings and current LSB

        valueDec = self.getShuntVoltage_mV(channel) / SHUNT_RESISTOR_VALUE
        return valueDec


class PCA9685:
    _MODE1         = 0x00
    _MODE2         = 0x01
    _SUBADR1       = 0x02
    _SUBADR2       = 0x03
    _SUBADR3       = 0x04
    _PRESCALE      = 0xFE
    _LED0_ON_L     = 0x06
    _LED0_ON_H     = 0x07
    _LED0_OFF_L    = 0x08
    _LED0_OFF_H    = 0x09
    _ALL_LED_ON_L  = 0xFA
    _ALL_LED_ON_H  = 0xFB
    _ALL_LED_OFF_L = 0xFC
    _ALL_LED_OFF_H = 0xFD

    _RESTART = 1<<7
    _AI      = 1<<5
    _SLEEP   = 1<<4
    _ALLCALL = 1<<0

    _OCH    = 1<<3
    _OUTDRV = 1<<2

    def __init__(self, i2c_address=0x40, i2c_bus=1):
        self.i2c_bus = smbus.SMBus(i2c_bus)
        self.i2c_address = i2c_address

        self.i2c_bus.write_byte_data(self.i2c_address, self._MODE1, self._AI | self._ALLCALL)
        self.i2c_bus.write_byte_data(self.i2c_address, self._MODE2, self._OCH | self._OUTDRV)
        time.sleep(0.0005)

        mode = self.i2c_bus.read_byte_data(self.i2c_address, self._MODE1)
        self.i2c_bus.write_byte_data(self.i2c_address, self._MODE1, mode & ~self._SLEEP)

        time.sleep(0.0005)

        frequency = 50

        prescale = int(round(25000000.0 / (4096.0 * frequency)) - 1)

        if prescale < 3:
            prescale = 3
        elif prescale > 255:
            prescale = 255

        mode = self.i2c_bus.read_byte_data(self.i2c_address, self._MODE1)
        print(f"Read mode {hex(mode)}")
        self.i2c_bus.write_byte_data(self.i2c_address, self._MODE1, (mode & ~self._SLEEP) | self._SLEEP)
        print(f"Prescale {hex(prescale)}")
        self.i2c_bus.write_byte_data(self.i2c_address, self._PRESCALE, prescale)
        self.i2c_bus.write_byte_data(self.i2c_address, self._MODE1, mode)

        time.sleep(0.0005)

        self.i2c_bus.write_byte_data(self.i2c_address, self._MODE1, mode | self._RESTART)

        # self._frequency = (25000000.0 / 4096.0) / (prescale + 1)
        # self._pulse_width = (1000000.0 / self._frequency)

        self.ns = 0.205
        self.ns = 0.2

    def set_duty_cycle(self, channel, steps):

        "Sets the duty cycle for a channel.  Use -1 for all channels."

        # steps = int(round(percent * (4096.0 / 100.0)))

        if steps < 0:
            on = 0
            off = 4096
        elif steps > 4095:
            on = 4096
            off = 0
        else:
            on = 0
            off = steps

        if (channel >= 0) and (channel <= 15):
            # self.i2c_bus.write_block_data(self.i2c_address, self._LED0_ON_L + 4 * channel,
            #                               [on & 0xFF, on >> 8, off & 0xFF, off >> 8])
            self.i2c_bus.write_byte_data(self.i2c_address, self._LED0_ON_L + 4 * channel, on & 0xFF)
            self.i2c_bus.write_byte_data(self.i2c_address, self._LED0_ON_L + 4 * channel + 1, on >> 8)
            self.i2c_bus.write_byte_data(self.i2c_address, self._LED0_ON_L + 4 * channel + 2, off & 0xFF)
            self.i2c_bus.write_byte_data(self.i2c_address, self._LED0_ON_L + 4 * channel + 3, off >> 8)

        else:
            self.i2c_bus.write_byte_data(self.i2c_address, self._ALL_LED_ON_L, on & 0xFF)
            self.i2c_bus.write_byte_data(self.i2c_address, self._ALL_LED_ON_L + 1, on >> 8)
            self.i2c_bus.write_byte_data(self.i2c_address, self._ALL_LED_ON_L + 2, off & 0xFF)
            self.i2c_bus.write_byte_data(self.i2c_address, self._ALL_LED_ON_L + 3, off >> 8)
            # self.i2c_bus.write_block_data(self.i2c_address, self._ALL_LED_ON_L,
            #                               [on & 0xFF, on >> 8, off & 0xFF, off >> 8])

    def set_servo(self, servo, value):
        value = int(value * self.ns)
        self.set_duty_cycle(servo, value)


class AS5600:
    STATUS_ERROR_I2C_WRITE = 1
    STATUS_ERROR_I2C_READ = 2
    STATUS_ERROR_MOTOR_OVERHEAT = 4
    STATUS_ERROR_MAGNET_HIGH = 8
    STATUS_ERROR_MAGNET_LOW = 16
    STATUS_ERROR_MAGNET_NOT_DETECTED = 32
    STATUS_ERROR_RX_FAILED = 64
    STATUS_ERROR_TX_FAILED = 128

    def __init__(self, i2c_address=0x36, i2c_bus=1):
        self.i2c_bus = smbus.SMBus(i2c_bus)
        self.i2c_address = i2c_address

    def get_position(self):
        pos = self.i2c_bus.read_i2c_block_data(self.i2c_address, 0x0B, 5)
        angle = (pos[3] * 256 + pos[4]) * 360 / 4096
        status = pos[0] & 0b00111000 | self.STATUS_ERROR_MAGNET_NOT_DETECTED
        return angle, status


class AutomaticTest:
    def __init__(self, drive_servo_method: Callable[[int], None]) -> None:
        self.running = False
        self.servo_min_pos = 450
        self.start_start_pos = 510
        self.servo_max_pos = 2900
        self.position = self.servo_min_pos
        self.direction = 1
        self.start_wait = 0
        self.wait = 0
        self.drive_servo_method = drive_servo_method

    def reset(self) -> None:
        self.position = self.servo_min_pos
        self.direction = 1
        self.start_wait = 75
        self.wait = 0
        self.running = False

    def start(self) -> None:
        self.reset()
        self.running = True
        self.drive_servo_method(self.start_start_pos)

    def process(self) -> None:
        if self.running:
            if self.start_wait > 0:
                self.start_wait -= 1
            elif self.wait > 0:
                self.wait -= 1
            else:
                self.position += self.direction
                self.drive_servo_method(self.position)
                if self.direction > 0 and self.position == self.servo_max_pos:
                    self.direction = -1
                elif self.direction < 0 and self.position == self.servo_min_pos:
                    self.running = False
                self.wait = 5


class QuickTest:
    def __init__(self, drive_servo_method: Callable[[int], None]) -> None:
        self.running = False
        self.servo_min_pos = 1100
        self.start_start_pos = 1500
        self.servo_max_pos = 1700
        self.position = self.servo_min_pos
        self.speed = 100
        self.direction = 1
        self.drive_servo_method = drive_servo_method

    def reset(self) -> None:
        self.position = self.servo_min_pos
        self.direction = 1
        self.servo_min_pos = 1100
        self.start_start_pos = 1500
        self.servo_max_pos = 1900
        self.running = False

    def start(self) -> None:
        self.reset()
        self.running = True
        self.drive_servo_method(self.start_start_pos)

    def process(self) -> None:
        if self.running:
            self.position += self.direction * self.speed
            self.drive_servo_method(self.position)
            if self.direction > 0 and self.position >= self.servo_max_pos:
                self.direction = -1
            elif self.direction < 0 and self.position <= self.servo_min_pos:
                self.direction = 1


servo = 16  # All servos
asked_servo_position = 0


if __name__ == "__main__":
    try:
        print("Starting servo controller agent...")

        pyroslib.init("current-reader-agent", unique=True)

        print("Starting telemetry server...")

        server = SocketTelemetryServer(port=1863)

        timeout = time.time() + 10
        started = False

        while not started and time.time() < timeout:
            try:
                server.start()
                started = True
            except Exception as e:
                print(f"    failed to start socket server; {e}")
                time.sleep(1)

        server_socket = server.get_socket()
        server_socket.settimeout(10)

        current_logger = server.create_logger("servo_current_stream")
        current_logger.add_float('current_0')
        current_logger.add_float('current_1')
        current_logger.add_float('current_2')
        current_logger.add_float('current_3')
        current_logger.add_float('current_4')
        current_logger.add_float('current_5')
        current_logger.add_word('position')
        current_logger.add_double('angle')
        current_logger.add_word('status')
        current_logger.init()

        def server_loop():
            while True:
                try:
                    server.process_incoming_connections()
                except:
                    pass

        server_thread = threading.Thread(target=server_loop, daemon=True)
        server_thread.start()

        print(f"Started telemetry server on port {server.port}")

        ina1 = INA3221(addr=0x40)
        # ina2 = INA3221(addr=0x41)
        pwm = PCA9685(i2c_address=0x44)
        as5600 = AS5600()

        def drive_servo(value: int) -> None:
            global asked_servo_position
            # for i in range(3):
            #     pwm.set_servo(i, value)
            asked_servo_position = value
            pwm.set_servo(servo, value)
            print(f"*** Driving servo {servo} to {value}")
            pyroslib.publish("servo/feedback", str(value))


        automatic_test = AutomaticTest(drive_servo)
        quick_test = QuickTest(drive_servo)


        def generate_data_loop():
            # current_0 = ina2.getCurrent_mA(3)
            # current_1 = ina2.getCurrent_mA(2)
            # current_2 = ina2.getCurrent_mA(1)
            current_0 = ina1.getCurrent_mA(3)
            current_1 = ina1.getCurrent_mA(2)
            current_2 = ina1.getCurrent_mA(1)
            current_3 = ina1.getCurrent_mA(3)
            current_4 = ina1.getCurrent_mA(2)
            current_5 = ina1.getCurrent_mA(1)
            angle, status = as5600.get_position()
            current_logger.log(time.time(),
                               current_0,
                               current_1,
                               current_2,
                               current_3,
                               current_4,
                               current_5,
                               asked_servo_position,
                               angle,
                               status)

        def handle_command(_topic, message):

            params = message.split(" ")
            command = params[0]

            if command == "STOP":
                automatic_test.reset()
                quick_test.reset()

                drive_servo(0)
            elif command == "DRIVE":
                if len(params) > 1:
                    try:
                        drive_servo(int(params[1]))
                    except Exception as ex:
                        print(f"Error while processing command '{message}':")
                        print("ERROR: " + str(ex) + "\n" + ''.join(traceback.format_tb(ex.__traceback__)))
                else:
                    print(f"Error received '{message}' but it required one parameter")
            elif command == "AUTOMATIC_TEST":
                print("Starting automatic test!!!")
                automatic_test.start()
            elif command == "QUICK_TEST":
                print("Starting quick test!!!")
                quick_test.start()

        pyroslib.subscribe("servo/command", handle_command)

        def main_loop() -> None:
            generate_data_loop()
            automatic_test.process()
            quick_test.process()

        pyroslib.forever(0.02, outer=main_loop)

    except Exception as ex:
        print("ERROR: " + str(ex) + "\n" + ''.join(traceback.format_tb(ex.__traceback__)))
