################################################################################
# Copyright (C) 2021 Abstract Horizon
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Apache License v2.0
# which accompanies this distribution, and is available at
# https://www.apache.org/licenses/LICENSE-2.0
#
#  Contributors:
#    Daniel Sendula - initial API and implementation
#
#################################################################################

import sys
import pygame
import pyros

from pygame import Rect

from servo_control_screen import ServoControlScreen
from pyros_support_ui import PyrosClientApp, agent
from pyros_support_ui.components import UIAdapter, Collection, LeftRightLayout, UiHint, BaseUIFactory, CardsCollection
from pyros_support_ui.drop_down import DropDown
from pyros_support_ui.pygamehelper import load_font, load_image

from pyros_support_ui.box_blue_sf_theme import BoxBlueSFThemeFactory

from telemetry_utils.telemetry_controller import TelemetryController
from ui.screen import Screen

ui_adapter = UIAdapter(screen_size=(1400, 848))
ui_factory = BoxBlueSFThemeFactory(ui_adapter, font=load_font("Exo-Regular.otf", 20), small_font=load_font("Exo-Regular.otf", 14), antialias=True)

pyros_client_app = PyrosClientApp(ui_factory,
                                  logo_image=load_image("flig_coloured_small.png"),
                                  logo_alt_image=load_image("flig_green_small.png"),
                                  connect_to_first=True,
                                  connect_to_only=False,
                                  rover_filter=lambda response, _: "SERVO" in response)


pyros_client_app.pyros_init("servo-controller-#")
if len(sys.argv) > 1:
    pyros_client_app.add_from_arguments(sys.argv[1], {"ROVER": "FLIG", "TYPE": "PYROS", "NAME": "FLIG"})

ui_adapter.top_component = pyros_client_app


class MainScreen(Collection):
    DECORATION_OFFSET = 0

    def __init__(self, ui_factory: BaseUIFactory, telemetry_controller: TelemetryController) -> None:
        super().__init__(None)
        self.telemetry_controller = telemetry_controller
        self.commands_panel = ui_factory.panel(None, layout=LeftRightLayout(margin=10), hint=UiHint.NO_DECORATION)

        self.add_component(self.commands_panel)
        self.main_panel = CardsCollection(None)
        self.add_component(self.main_panel)

        self.main_menu_drop_down = DropDown(
            Rect(0, 0, 120, 30),
            ui_factory,
            select_method=self.select_screen,
            update_activation_button_text=False,
            text="Menu",
            menu_width=170
        )
        self.add_component(self.main_menu_drop_down.menu)
        self.commands_panel.add_component(self.main_menu_drop_down)

        self.telemetry_menu_drop_down = DropDown(
            Rect(0, 0, 120, 30),
            ui_factory,
            select_method=self.telemetry_actions,
            update_activation_button_text=False,
            text="Telemetry",
            menu_width=120
        )
        self.telemetry_menu_drop_down.add_item("start", "Start")
        self.telemetry_menu_drop_down.add_item("stop", "Stop")
        self.telemetry_menu_drop_down.add_item("save", "Save")
        self.telemetry_menu_drop_down.add_item("clear", "Clear")
        self.add_component(self.telemetry_menu_drop_down.menu)
        self.commands_panel.add_component(self.telemetry_menu_drop_down)

        self.contrubuting_panel = CardsCollection(None)
        self.commands_panel.add_component(self.contrubuting_panel)

    def redefine_rect(self, rect) -> None:
        self.rect = rect
        self.commands_panel.redefine_rect(pygame.Rect(rect.x + 5, rect.y - MainScreen.DECORATION_OFFSET, rect.width - 10, 30))
        self.main_panel.redefine_rect(pygame.Rect(rect.x + 5, self.commands_panel.rect.bottom + 5, rect.width - 10, rect.height - 5 - self.commands_panel.rect.height + MainScreen.DECORATION_OFFSET))

    def key_down(self, code: str) -> bool:
        return self.main_panel.key_down(code)

    def key_up(self, code: str) -> bool:
        return self.main_panel.key_up(code)

    def add_screen(self, screen_id: str, screen_name: str, component: Screen) -> None:
        self.main_panel.add_card(screen_id, component)
        self.main_menu_drop_down.add_item(screen_id, screen_name)
        contributing_command_panel = Collection(None)
        self.contrubuting_panel.add_card(screen_id, contributing_command_panel)
        component.set_contributing_panel(contributing_command_panel)

    def select_screen(self, screen_id: str) -> None:
        self.main_panel.select_card(screen_id)
        self.contrubuting_panel.select_card(screen_id)

    def telemetry_actions(self, action: str) -> None:
        print(f"Selected action {action}")
        if action == "start":
            self.telemetry_controller.trigger_connecting_telemetry()
            # button = cast(Button, self.telemetry_buttons.card_component("on"))
            # button.enabled = False
        elif action == "stop":
            # self.telemetry_buttons.toggle_on()
            self.telemetry_controller.safely_close_telemetry_client()
        elif action == "save":
            self.telemetry_controller.save_telemetry()
        elif action == "clear":
            self.telemetry_controller.clear_telemetry()


telemetry_controller = TelemetryController(pyros_client_app)
telemetry_controller.setup_connect_telemetry_thread()

main_screen = MainScreen(ui_factory, telemetry_controller)
monitor_screen = ServoControlScreen(pyros_client_app, ui_factory, telemetry_controller)

main_screen.add_screen("monitor", "Monitor", monitor_screen)
main_screen.select_screen("monitor")

pyros_client_app.set_content(main_screen)
pyros_client_app.start_discovery_in_background()


def servo_feedback_callback(_topic, message) -> None:
    try:
        monitor_screen.servo_status_panel.update_servo_pos_text(int(message))
    except Exception as e:
        print(f"Got exception {e}")


def _on_connected() -> None:
    agent.init(None, "servo_controller_agent.py", agent_id="servo_controller_agent")
    pyros.subscribe("servo/feedback", servo_feedback_callback)


pyros_client_app.add_on_connected_subscriber(_on_connected)


while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()

        ui_adapter.process_event(event)
        if event.type == pygame.KEYDOWN:
            main_screen.key_down(event.key)
        if event.type == pygame.KEYUP:
            main_screen.key_up(event.key)

    if telemetry_controller.telemetry_client is not None:
        client = telemetry_controller.telemetry_client
        if "servo_current_stream" in client.streams:
            stream = client.streams['servo_current_stream']
            values = client.storage._values(stream)

            if len(values) > 0:
                monitor_screen.servo_status_panel.servo_angle_label.text = f"{values[-1][1][8]:.2f}"

    pyros.loop(0.03)
    agent.keep_agents()

    ui_adapter.draw(ui_adapter.screen)

    ui_adapter.frame_end()
