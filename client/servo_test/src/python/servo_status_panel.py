import pyros

from pyros_support_ui.components import Collection, TopDownLayout, BaseUIFactory, ALIGNMENT, LeftRightLayout
from pygame import Rect


class ServoStatusPanel(Collection):
    def __init__(self, rect, ui_factory: BaseUIFactory):
        super().__init__(rect, layout=TopDownLayout(margin=10))

        self.last_time = 0
        self.servo_angle_text_label = ui_factory.label(Rect(0, 0, 200, 30), "Servo Angle", h_alignment=ALIGNMENT.CENTER, v_alignment=ALIGNMENT.MIDDLE)
        self.servo_angle_label = ui_factory.label(Rect(0, 0, 200, 30), "0", h_alignment=ALIGNMENT.CENTER, v_alignment=ALIGNMENT.MIDDLE)

        self.servo_pos_text_label = ui_factory.label(Rect(0, 0, 200, 30), "Servo Position", h_alignment=ALIGNMENT.CENTER, v_alignment=ALIGNMENT.MIDDLE)
        self._servo_pos = 1500
        self.servo_pos_label = ui_factory.label(Rect(0, 0, 200, 30), "0", h_alignment=ALIGNMENT.CENTER, v_alignment=ALIGNMENT.MIDDLE)

        self.add_component(self.servo_angle_text_label)
        self.add_component(self.servo_angle_label)
        self.add_component(self.servo_pos_text_label)
        self.add_component(self.servo_pos_label)
        self.add_component(ui_factory.text_button(Rect(0, 0, 160, 20), "Stop", on_click=self.stop_servo))
        self.add_component(ui_factory.text_button(Rect(0, 0, 160, 20), "Middle", on_click=self.middle_servo))
        self.left_right_panel = Collection(Rect(0, 0, 330, 20), layout=LeftRightLayout(h_alignment=ALIGNMENT.CENTER, margin=10))
        self.add_component(self.left_right_panel)
        self.left_right_panel.add_component(ui_factory.text_button(Rect(0, 0, 50, 20), " <<< ", on_click=self.max_left_servo))
        self.left_right_panel.add_component(ui_factory.text_button(Rect(0, 0, 40, 20), " << ", on_click=self.more_left_servo))
        self.left_right_panel.add_component(ui_factory.text_button(Rect(0, 0, 30, 20), " < ", on_click=self.left_servo))
        self.left_right_panel.add_component(ui_factory.text_button(Rect(0, 0, 30, 20), " > ", on_click=self.right_servo))
        self.left_right_panel.add_component(ui_factory.text_button(Rect(0, 0, 40, 20), " >> ", on_click=self.more_right_servo))
        self.left_right_panel.add_component(ui_factory.text_button(Rect(0, 0, 50, 20), " >>> ", on_click=self.max_right_servo))

        self.add_component(ui_factory.text_button(Rect(0, 0, 160, 20), "Automatic test", on_click=self.automatic_test))
        self.add_component(ui_factory.text_button(Rect(0, 0, 160, 20), "Quick test", on_click=self.quick_test))

    def update_servo_pos_text(self, servo_pos: int) -> None:
        self._servo_pos = servo_pos
        self.servo_pos_label.text = str(self._servo_pos)

    def set_servo_pos(self, servo_pos: int) -> None:
        if self._servo_pos != servo_pos:
            pyros.publish("servo/command", f"DRIVE {servo_pos}")
        self.update_servo_pos_text(servo_pos)

    def stop_servo(self, *_args) -> None:
        pyros.publish("servo/command", "STOP")
        # self.servo_pos_label.text = "0"

    def middle_servo(self, *_args) -> None:
        self.set_servo_pos(1500)

    def automatic_test(self, *_args) -> None:
        pyros.publish("servo/command", "AUTOMATIC_TEST")

    def quick_test(self, *_args) -> None:
        pyros.publish("servo/command", "QUICK_TEST")

    def left_servo(self, *_args) -> None:
        self.set_servo_pos(self._servo_pos - 10)

    def more_left_servo(self, *_args) -> None:
        self.set_servo_pos(self._servo_pos - 100)

    def max_left_servo(self, *_args) -> None:
        self.set_servo_pos(510)

    def right_servo(self, *_args) -> None:
        self.set_servo_pos(self._servo_pos + 10)

    def more_right_servo(self, *_args) -> None:
        self.set_servo_pos(self._servo_pos + 100)

    def max_right_servo(self, *_args) -> None:
        self.set_servo_pos(2830)
