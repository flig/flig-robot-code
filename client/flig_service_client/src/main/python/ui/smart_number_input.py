from typing import Optional, Callable

from pygame import Rect
from pygame.font import Font

from servo_lib.leg_servos import StorageValue
from pyros_support_ui.components import BaseUIFactory
from pyros_support_ui.number_input import NumberInputComponent, Number, GetterSetterPair


class SmartNumberInputComponent(NumberInputComponent):
    MODIFIED_COLOUR = (255, 160, 160)
    NOT_RECEIVED_COLOUR = (160, 160, 160)
    CLEAR_COLOUR = (220, 255, 220)

    def __init__(self, rect: Rect, ui_factory: BaseUIFactory,
                 getter_setter_pair: GetterSetterPair, storage_value_getter: Callable[[], StorageValue],
                 minimal_value: Number, maximum_value: Number,
                 name: str,
                 button_font: Optional[Font] = None, value_font: Optional[Font] = None,
                 top_scale: int = 7, bottom_scale: int = 4):
        super().__init__(rect, ui_factory,
                         getter_setter_pair,
                         name,
                         minimal_value=minimal_value, maximum_value=maximum_value,
                         button_font=button_font, value_font=value_font, top_scale=top_scale, bottom_scale=bottom_scale)
        self.storage_value_getter = storage_value_getter

    def draw(self, surface):
        def set_label_colour(label, colour):
            if label.colour != colour:
                label.colour = colour

        storage_value = self.storage_value_getter()
        if storage_value is None:
            set_label_colour(self.left, SmartNumberInputComponent.NOT_RECEIVED_COLOUR)
            set_label_colour(self.right, SmartNumberInputComponent.NOT_RECEIVED_COLOUR)
        elif storage_value.modified:
            set_label_colour(self.left, SmartNumberInputComponent.MODIFIED_COLOUR)
            set_label_colour(self.right, SmartNumberInputComponent.MODIFIED_COLOUR)
        elif not storage_value.received:
            set_label_colour(self.left, SmartNumberInputComponent.NOT_RECEIVED_COLOUR)
            set_label_colour(self.right, SmartNumberInputComponent.NOT_RECEIVED_COLOUR)
        else:
            set_label_colour(self.left, SmartNumberInputComponent.CLEAR_COLOUR)
            set_label_colour(self.right, SmartNumberInputComponent.CLEAR_COLOUR)
        super().draw(surface)
