from functools import partial
from typing import List, Tuple, Callable, Optional

import pygame
from pygame import Rect

from pyros_support_ui.components import BaseUIFactory, Menu, Button


def define_drop_down(ui_factory: BaseUIFactory,
                     values: List[Tuple[str, str]],
                     select_method: Callable,
                     default_id: Optional[str]) -> Tuple[Menu, Button]:
    menu = ui_factory.menu(Rect(0, 0, 150, 200), background_colour=pygame.color.THECOLORS['black'])
    # self.add_component(menu)
    menu.hide()

    def select_value(_id, _b=None, _p=None):
        menu.hide()
        select_method(_id)
        drop_down_activation_button.label.text = [v for v in values if v[0] == _id][0][1]

    for ident, text in values:
        menu.add_menu_item(text, callback=partial(select_value, ident))

    def start_drop_down(button, _=None):
        if menu.rect.x != button.rect.x or menu.rect.y != button.rect.y:
            menu.redefine_rect(Rect(button.rect.x, button.rect.bottom, 200, 200))
        menu.show()

    drop_down_activation_button = ui_factory.text_button(Rect(0, 0, 150, 20),
                                                         values[0][1],
                                                         on_click=start_drop_down)

    # self.commands_panel.add_component(drop_down_activation_button)

    if default_id is not None:
        select_value(default_id)
    return drop_down_activation_button, menu
