import pygame

from jcontroller_client import JoystickClient
from pyros_support_ui.components import Component, BaseUIFactory


class JoystickComponent(Component):
    def __init__(self, ui_factory: BaseUIFactory, joystick: JoystickClient):
        super(JoystickComponent, self).__init__(None)
        self.joystick = joystick
        self.xo = 0
        self.yo = 0
        self.font = ui_factory.font
        self.font_colour = ui_factory.colour if ui_factory.colour is not None else pygame.color.THECOLORS['white']

    def redefine_rect(self, rect: pygame.Rect) -> None:
        super().redefine_rect(rect)
        self.xo = rect.x + 20
        self.yo = rect.y + 20

    def draw(self, surface) -> None:
        x1 = float(self.joystick.axis_states["x"] * 20)
        y1 = float(self.joystick.axis_states["y"] * 20)
        x2 = float(self.joystick.axis_states["rx"] * 20)
        y2 = float(self.joystick.axis_states["ry"] * 20)

        self.draw_line(surface, (x1 + 20, 0), (x1 + 20, 40))
        self.draw_line(surface, (0, y1 + 20),(40, y1 + 20))
        self.draw_line(surface, (87 + x2 + 20, 0), (87 + x2 + 20, 40))
        self.draw_line(surface, (87, y2 + 20), (87 + 40, y2 + 20))

        x3 = float(self.joystick.axis_states["hat0x"])
        y3 = float(self.joystick.axis_states["hat0y"])

        # white = (255,255,255)
        if x3 < 0:
            self.draw_line(surface, (0, 50), (7, 57))
            self.draw_line(surface, (16, 50), (23, 57))
        elif x3 > 0:
            self.draw_rect(surface, (0, 50), (7, 57))
            self.draw_rect(surface, (16, 50), (23, 57))
        else:
            self.draw_rect(surface, (0, 50), (7, 57))
            self.draw_rect(surface, (16, 50), (23, 57))

        if y3 < 0:
            self.draw_rect(surface, (8, 42), (15, 49))
            self.draw_rect(surface, (8, 58), (15, 63))
        elif y3 > 0:
            self.draw_rect(surface, (8, 42), (15, 49))
            self.draw_rect(surface, (8, 58), (15, 63))
        else:
            self.draw_rect(surface, (8, 42), (15, 49))
            self.draw_rect(surface, (8, 58), (15, 63))

        self.draw_rover(surface)
        self.draw_top_speed(surface, self.rect.width - 120, 100)

    def draw_text(self, surface, xy, t):
        surface.blit(self.font.render(str(t), 1, self.font_colour), (self.xo + xy[0], self.yo + xy[1]))

    def draw_line(self, surface, start, end):
        pygame.draw.line(surface, (255, 255, 255), (self.xo + start[0], self.yo + start[1]), (self.xo + end[0], self.yo + end[1]))

    def draw_rect(self, surface, start, end):
        pass # pygame.draw.rect(screen, (255, 255, 255), (start[0], start[1] + 50, end[0], end[1] + 50))

    def draw_rover(self, surface):
        self.draw_text(surface, (0, 150), "R:")
        self.draw_text(surface, (55, 150), "S:")
        self.draw_text(surface, (0, 100), "D:")

    def draw_top_speed(self, surface, x, y):
        spd = self.joystick.calc_rover_speed(1)

        x = x - 16
        self.draw_text(surface, (x, y), f"Speed: {spd}")
