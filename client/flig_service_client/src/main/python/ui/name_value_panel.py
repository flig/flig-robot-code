from pygame import Rect

from typing import Optional, Generic, TypeVar, Callable, Union, Dict
from pyros_support_ui.components import Component, Collection, BaseUIFactory, LeftRightLayout, ALIGNMENT, TopDownLayout


T = TypeVar('T')


class NameValueComponent(Collection, Generic[T]):
    def __init__(self, rect: Optional[Rect], ui_factory: BaseUIFactory,
                 key_width: int, value_width: int, height: int,
                 formatter: Callable[[T], str]) -> None:
        super().__init__(rect, layout=LeftRightLayout(margin=5))
        self.key_width = key_width
        self.value_width = value_width
        self.key_label = ui_factory.label(Rect(0, 0, key_width, height), "", h_alignment=ALIGNMENT.LEFT, v_alignment=ALIGNMENT.TOP)
        self.value_label = ui_factory.label(Rect(0, 0, value_width, height), "", h_alignment=ALIGNMENT.RIGHT, v_alignment=ALIGNMENT.TOP)
        self.formatter = formatter
        self.add_component(self.key_label)
        self.add_component(self.value_label)

    def redefine_rect(self, rect) -> None:
        super().redefine_rect(rect)

    @property
    def key(self) -> str:
        return self.key_label.text

    @key.setter
    def key(self, key: str) -> None:
        self.key_label.text = key

    @property
    def value(self) -> T:
        return self.formatter(self.value_label.text)

    @value.setter
    def value(self, value: T) -> None:
        self.value_label.text = self.formatter(value)


class NameValueListComponent(Collection):
    def __init__(self, rect: Optional[Rect], ui_factory: BaseUIFactory, key_width: int, value_width: int, entry_height: int) -> None:
        super().__init__(rect, layout=TopDownLayout(margin=5))
        self.ui_factory = ui_factory
        self.key_width = key_width
        self.value_width = value_width
        self.entry_height = entry_height
        self.dict: Dict[str, NameValueComponent] = {}
        self.values: Dict[str, Union[str, float, int]] = {}

    def _add_key_value(self, key_id: str, key_name: str, value: str, formatter: Callable[[T], str]) -> None:
        self.dict[key_id] = NameValueComponent(
            Rect(0, 0, self.key_width + self.value_width + 5, self.entry_height),
            self.ui_factory, key_width=self.key_width, value_width=self.value_width, height=self.entry_height, formatter=formatter)

        self.dict[key_id].key = key_name
        self.add_component(self.dict[key_id])

    def add_str_value(self, key_id: str, key_name: str, value: str) -> None:
        def formatter(s: str) -> str:
            return f"{s}"
        self._add_key_value(key_id, key_name, formatter(value), formatter)
        self.values[key_id] = value

    def add_float_value(self, key_id: str, key_name: str, value: float) -> None:
        def formatter(value: float) -> str:
            return f"{value:.1f}"
        self._add_key_value(key_id, key_name, formatter(value), formatter)
        self.values[key_id] = value

    def add_int_value(self, key_id: str, key_name: str, value: int) -> None:
        def formatter(i: int) -> str:
            return f"{i}"
        self._add_key_value(key_id, key_name, formatter(value), formatter)
        self.values[key_id] = value

    def set_value(self, key_id: str, value: Union[str, float, int]) -> None:
        self.values[key_id] = value
        self.dict[key_id].value = value

    def get_value(self, key_id: str) -> Union[str, float, int]:
        return self.values[key_id]

    def add_spacer(self, height: int) -> None:
        self.add_component(Component(Rect(0, 0, 0, height)))