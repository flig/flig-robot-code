from typing import Optional

from pyros_support_ui import PyrosClientApp
from pyros_support_ui.components import Collection, Component


class Screen(Collection):
    def __init__(self, main_app: PyrosClientApp):
        super().__init__(None)
        self.main_app = main_app

    def set_contributing_panel(self, contributing_command_panel: Collection):
        pass

    @Component.visible.setter
    def visible(self, is_visible: bool):
        Component.visible.fset(self, is_visible)

        if is_visible:
            self.on_show()
        else:
            self.on_hide()

    def on_show(self):
        pass

    def on_hide(self):
        pass

    def set_modal(self, modal: Component) -> None:
        self.main_app.set_modal(modal)

    def clear_modal(self) -> None:
        self.main_app.clear_modal()
