from pyros_support_ui import PyrosClientApp
from pyros_support_ui.graph_component import TelemetryGraphData
from telemetry_utils.telemetry_receiver_adapter import TelemetryReceiverAdapter


class TelemetryController(TelemetryReceiverAdapter):
    def __init__(self, main_app: PyrosClientApp):
        super().__init__("servo_current", "servo_current_stream")
        self.main_app = main_app

        self.graph_data = {}

    def on_telemetry_connection(self, connected: bool) -> None:
        if connected:
            stream = self.telemetry_client.streams[self.stream_name]
            for i in range(len(stream.fields)):
                self.graph_data[f"current_{i}"] = TelemetryGraphData(
                    self.telemetry_client,
                    stream, stream.fields[i].name, 500 if i < 7 else 10, -500 if i < 7 else 0, auto_scale=True)
        else:
            self.graph_data.clear()
