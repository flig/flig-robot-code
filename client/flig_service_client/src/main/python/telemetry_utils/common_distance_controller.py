from abc import ABC

from typing import Optional

from telemetry import TelemetryStreamDefinition

from telemetry_utils.telemetry_receiver_adapter import TelemetryReceiverAdapter


class CommonDistancesTelemetryController(TelemetryReceiverAdapter, ABC):
    def __init__(self, name: str, stream_name: str, telemetry_host: Optional[str] = None, telemetry_port: Optional[int] = 6400):
        super().__init__(name, stream_name, telemetry_host=telemetry_host, telemetry_port=telemetry_port)

        self.stream: Optional[TelemetryStreamDefinition] = None

        self.statuses = [0] * 16
        self.distances = [1500] * 16
        self.received_distances = [1500] * 16

        self.wall_angle = 0
        self.wall_angle_avg = 0
        self.wall_points = 0
        self.wall_distance = 0

        self.floor_angles = [0.0] * 4
        self.floor_points = [0] * 4
        self.object_distance = [0.0] * 4

        self.gyro_bearing = 0
