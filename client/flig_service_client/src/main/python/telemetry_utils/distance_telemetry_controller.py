import math
import time

from typing import Tuple

from pyros_support_ui import PyrosClientApp
from telemetry_utils.common_distance_controller import CommonDistancesTelemetryController


ANGLE_90 = math.pi / 2
ANGLE_45 = math.pi / 4
ANGLE_22_5 = math.pi / 8
ANGLE_12_25 = math.pi / 16
ANGLE_9_5 = math.pi / 19
ANGLE_6_125 = math.pi / 32

FLOOR_OUTER_ANGLE = ANGLE_22_5  # - ANGLE_12_25
# FLOOR_INNER_ANGLE = ANGLE_9_5  # - ANGLE_12_25
FLOOR_INNER_ANGLE = ANGLE_12_25  # - ANGLE_12_25
WALL_OUTER_ANGLE = ANGLE_22_5 - ANGLE_6_125
WALL_INNER_ANGLE = ANGLE_12_25 - ANGLE_6_125

# ANGLES = [ANGLE_90 - OUTER_ANGLE, ANGLE_90 - INNER_ANGLE, ANGLE_90 + INNER_ANGLE, ANGLE_90  +OUTER_ANGLE]
FLOOR_ANGLES = [-FLOOR_OUTER_ANGLE, -FLOOR_INNER_ANGLE, FLOOR_INNER_ANGLE, FLOOR_OUTER_ANGLE]
WALL_ANGLES = [ANGLE_90 - -WALL_OUTER_ANGLE, ANGLE_90 - -WALL_INNER_ANGLE, ANGLE_90 - WALL_INNER_ANGLE, ANGLE_90 - WALL_OUTER_ANGLE]


class DistancesTelemetryController(CommonDistancesTelemetryController):
    def __init__(self, main_app: PyrosClientApp):
        super().__init__("distances", "vl53lcx", telemetry_host="172.24.1.177", telemetry_port=6401)
        self.main_app = main_app
        self.wall_angle_avg_k = 0.1

    def on_telemetry_connection(self, connected: bool) -> None:
        if connected:
            self.stream = self.telemetry_client.streams[self.stream_name]
        else:
            self.stream = None

    def on_telemetry_received(self) -> None:
        current_time = time.time()

        def write_data(records):
            if len(records) > 0:
                # print(f"    receiving {len(records)} record(s): ", end="")
                for record in records:
                    # print(f"    got distances record {record}")
                    for i in range(0, 32):
                        if i % 2 == 0:
                            self.statuses[i // 2] = record[i + 1]
                        else:
                            self.received_distances[i // 2] = record[i + 1]

                for i in range(len(self.received_distances)):
                    if self.statuses[i] == 5:
                        self.distances[i] = self.received_distances[i]
                self.telemetry_client.trim(self.stream, current_time)
                self.process_response()

        def fetch_stream(stream):
            self.stream = stream
            self.telemetry_client.retrieve(stream, 0, time.time(), write_data)

        if self.telemetry_client is not None and self.telemetry_client.socket is not None:
            self.telemetry_client.get_stream_definition(self.stream_name, fetch_stream)

    def process_response(self) -> None:
        def points_distance(p1: Tuple[float, float], p2: Tuple[float, float]) -> float:
            return math.sqrt((p1[0] - p2[0]) * (p1[0] - p2[0]) + (p1[1] - p2[1]) * (p1[1] - p2[1]))

        def is_close(d1: float, d2: float, tol: float = 0.998) -> bool:
            return ((d1 > d2 and d2 / d1 > tol)
                    or (d1 < d2 and d1 / d2 > tol))

        top_points = [(0.0, 0.0)] * 4

        wall_distance = 9999
        for i in range(4):
            d = self.distances[i]
            if d < wall_distance:
                wall_distance = d
            x = d
            y = d / math.tan(WALL_ANGLES[i])
            top_points[i] = (x, y)

        d12 = points_distance(top_points[0], top_points[1])
        d23 = points_distance(top_points[1], top_points[2])
        d34 = points_distance(top_points[2], top_points[3])
        d13 = points_distance(top_points[0], top_points[2])
        d24 = points_distance(top_points[1], top_points[3])

        top_wall_right = True
        if is_close(d12 + d23, d13, 0.975):
            self.wall_points = 3
            wall_distance = self.distances[1]

            if is_close(d23 + d34, d24):
                self.wall_points = 4
        elif is_close(d23 + d34, d24, 0.975):
            wall_distance = self.distances[2]
            top_wall_right = False
            self.wall_points = -3
        else:
            d2 = self.distances[1]
            d3 = self.distances[2]
            wall_distance = d2 if d2 < d3 else d3
            self.wall_points = 2

        if top_wall_right:
            a = top_points[0][0] - top_points[1][0]
            c = d12
        else:
            a = top_points[2][0] - top_points[3][0]
            c = d34

        self.wall_angle = math.degrees(-math.asin(a / c))
        self.wall_angle_avg = self.wall_angle_avg * (1 - self.wall_angle_avg_k) + self.wall_angle * self.wall_angle_avg_k
        self.wall_distance = wall_distance

        expected_lower_distance = 100 / math.cos(math.radians(55))
        for l in range(4):
            if expected_lower_distance / self.distances[3 * 4 + l] < 0.80 or expected_lower_distance / self.distances[3 * 4 + l] > 1.2:
                self.object_distance[l] = self.distances[3 * 4 + l]
                self.floor_points[l] = -1
                self.floor_angles[l] = 0
            elif self.distances[3 * 4 + l] > self.distances[2 * 4 + l]:
                self.object_distance[l] = self.distances[2 * 4 + l]
                self.floor_points[l] = -2
                self.floor_angles[l] = 0
            else:
                points = [(0.0, 0.0)] * 4
                for i in range(1, 4):
                    d = self.distances[i * 4 + l]
                    angle = FLOOR_ANGLES[i]
                    x = d * math.cos(angle)
                    y = d * math.sin(angle)
                    points[i - 1] = (x, y)

                d12 = points_distance(points[0], points[1])
                d23 = points_distance(points[1], points[2])
                d13 = points_distance(points[0], points[2])

                if is_close(d12 + d23, d13):
                    self.floor_points[l] = 3
                    self.object_distance[l] = 9999
                else:
                    self.floor_points[l] = 2
                    self.object_distance[l] = self.distances[1 * 4 + l]

                a = points[2][0] - points[1][0]
                c = d23
                try:
                    self.floor_angles[l] = math.degrees(ANGLE_90 + math.asin(a / c) - ANGLE_12_25)
                except ValueError:
                    print(f"Math domain error a={a:.2f}, c={c:.2f} a/c={a/c:.2f}")

                if self.floor_angles[l] > 12:
                    self.floor_points[l] = -3
                    self.object_distance[l] = self.distances[2 * 4 + l]
