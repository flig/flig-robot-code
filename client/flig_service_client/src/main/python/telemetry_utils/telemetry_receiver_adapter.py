

import os
from abc import ABC, abstractmethod

import time
import traceback

from threading import Thread
from typing import cast, Optional, Callable

import pyros
from telemetry import CachingSocketTelemetryClient, TelemetryClient


class TelemetryReceiverAdapter(ABC):
    def __init__(self, name: str, stream_name: str, telemetry_host: Optional[str] = None, telemetry_port: Optional[int] = 6400):
        self.name = name
        self.stream_name = stream_name
        self.telemetry_client: Optional[CachingSocketTelemetryClient] = None
        self.telemetry_host: Optional[str] = telemetry_host
        self.telemetry_port: Optional[int] = telemetry_port
        self.telemetry_stopped = True
        self.telemetry_connected = False

        self._collect_data = True

        self.connect_thread = None

        self._telemetry_receiving_thread = Thread(target=self._receive_telemetry, name=f"{name}_receiving_telemetry", daemon=True)
        self._telemetry_receiving_thread.start()

        self.do_connect_telemetry = False

        self.on_connection_callback: Optional[Callable[[bool], None]] = None
        pyros.subscribe(f"telemetry/stream/{self.stream_name}/details", self.telemetry_details_topic)

        self._setup_connect_telemetry_thread()

    def telemetry_details_topic(self, _topic, payload) -> None:
        host_port = payload.split(":")
        if len(host_port) == 2:
            try:
                self.telemetry_port = int(host_port[1])
            except Exception as e:
                print(f"Invalid host_port details '{payload}'; {e}")

            host = host_port[0]

            if f"REPLACE_{host.replace('.', '_')}" in os.environ:
                host = os.environ[f"REPLACE_{host.replace('.', '_')}"]
            self.telemetry_host = host
        else:
            print(f"Invalid host_port details '{payload}'")

    def set_on_connection_callback(self, callback: Callable[[bool], None]) -> None:
        self.on_connection_callback = callback

    def clear_on_connection_callback(self) -> None:
        self.on_connection_callback = None

    def _setup_connect_telemetry_thread(self):
        def connect_telemetry_thread():
            while True:
                # noinspection PyBroadException
                try:
                    while not self.do_connect_telemetry:
                        time.sleep(0.5)

                    self.do_connect_telemetry = False

                    connected = False
                    timeout = time.time() + 15

                    while not connected and time.time() < timeout:
                        host = self.telemetry_host
                        port = self.telemetry_port
                        if host is None:
                            host, _ = pyros.get_connection_details()
                        if host is not None and port is not None:
                            try:
                                print(f"Connect thread {self.name}: locally connecting to remote server agent at {host}:{port} ...")
                                if self.telemetry_client is None or cast(CachingSocketTelemetryClient, self.telemetry_client).host != host:
                                    self._setup_telemetry_client(host, port)

                                self.telemetry_connected = True
                                connected = True
                                if self.on_connection_callback is not None:
                                    self.on_telemetry_connection(True)
                                    self.on_connection_callback(True)

                                print(f"Connect thread {self.name}: connected to remote telemetry server at {host}:{port}.")
                            except Exception as e:
                                print(f"Connect thread: exception connecting to remote telemetry server at {host}:{port}; {e}")
                                self.safely_close_telemetry_client()
                                time.sleep(1)
                        else:
                            time.sleep(1)
                    if not connected:
                        if self.on_connection_callback is not None:
                            self.on_telemetry_connection(False)
                            self.on_connection_callback(False)
                        print(f"Connect thread: failed to connect to remote telemetry stream at {self.telemetry_host}:{self.telemetry_port}.")
                except Exception as _:
                    pass

        self.connect_thread = Thread(target=connect_telemetry_thread, name="connect_telemetry", daemon=True)
        self.connect_thread.start()

    def _setup_telemetry_client(self, host, port) -> TelemetryClient:
        self.safely_close_telemetry_client()

        print(f"... starting telemetry client at {host}:{port}...")
        self.telemetry_client = CachingSocketTelemetryClient(host=host, port=port)
        self.telemetry_client.start()
        print(f"... started telemetry client at {host}:{port}.")
        self.telemetry_client.socket.settimeout(2)

        return self.telemetry_client

    def safely_close_telemetry_client(self):
        if self.telemetry_client is not None:
            # noinspection PyBroadException
            try:
                self.telemetry_client.stop()
            except Exception as _:
                pass
        self.telemetry_client = None

    def start_connecting_telemetry(self):
        self.do_connect_telemetry = True

    def _receive_telemetry(self):
        had_exception = False
        while True:
            try:
                if self.telemetry_connected and self.telemetry_client is not None and self._collect_data:
                    self.telemetry_client.process_incoming_data()
                    self.on_telemetry_received()
                had_exception = False
            except Exception as ex:
                self.safely_close_telemetry_client()
                if not had_exception:
                    print(f"ERROR {self.name}: {ex}\n{''.join(traceback.format_tb(ex.__traceback__))}")
                    had_exception = True
                    if self._collect_data:
                        self.start_connecting_telemetry()

    def save_telemetry(self, *_args):
        def create_filename(i: int) -> str:
            path_prefix = os.environ["TELEMETRY_SAVE_PATH"] if "TELEMETRY_SAVE_PATH" in os.environ else ""
            if i <= 0:
                return os.path.join(path_prefix, f"{self.stream_name}_log.csv")
            return os.path.join(path_prefix, f"{self.stream_name}_log.{i}.csv")

        def find_filename() -> str:
            i = 0
            filename = create_filename(i)
            while os.path.exists(filename):
                i += 1
                filename = create_filename(i)
            return filename

        with open(find_filename(), "wt") as file:
            def write_data(records):
                for record in records:
                    file.write(",".join([str(f) for f in record]) + "\n")

            def write_header(stream):
                file.write("timestamp," + ",".join(f.name for f in stream.fields) + "\n")
                self.telemetry_client.retrieve(stream, 0, time.time(), write_data)

            self.telemetry_client.get_stream_definition(self.stream_name, write_header)

    def clear_telemetry(self, *_args):
        self.telemetry_client.get_stream_definition(self.stream_name, lambda stream: self.telemetry_client.trim(stream, time.time()))

    def on_telemetry_received(self) -> None:
        pass

    @abstractmethod
    def on_telemetry_connection(self, connected: bool) -> None:
        pass
