import math
import time

from typing import Optional

import pyros
from telemetry import TelemetryStreamDefinition

from pyros_support_ui import PyrosClientApp
from telemetry_utils.telemetry_receiver_adapter import TelemetryReceiverAdapter


class NineDoFTelemetryController(TelemetryReceiverAdapter):
    def __init__(self, main_app: PyrosClientApp):
        super().__init__("9dof", "icm20948", telemetry_host="172.24.1.177", telemetry_port=6402)
        self.main_app = main_app

        self.stream: Optional[TelemetryStreamDefinition] = None
        self.gyro_x = 0
        self.gyro_y = 0
        self.gyro_z = 0
        self.accel_x = 0
        self.accel_y = 0
        self.accel_z = 0
        self.compass_x = 0
        self.compass_y = 0
        self.compass_z = 0

        self.max_mag_x = -999999
        self.min_mag_x = 999999

        self.max_mag_y = -999999
        self.min_mag_y = 999999

        self.max_mag_z = -999999
        self.min_mag_z = 999999

        self.pitch = 0.0
        self.roll = 0.0
        
        self.bearing = 0.0

        self.calibration_is_on = False

        self._expected_int_config_values = {
            "icm20948/min_mag_x", "icm20948/max_mag_x",
            "icm20948/min_mag_y", "icm20948/max_mag_y",
            "icm20948/min_mag_z", "icm20948/max_mag_z",
        }
        self._received_int_config_values = set()
        pyros.subscribe("storage/write/icm20948/#", self._handle_configuration)

    def request_stored_calibration(self) -> None:
        print("    fetching previously calibrated values")
        for k in self._expected_int_config_values:
            print(f"        asking for {k}")
            pyros.publish(f"storage/read/{k}", "")

    def _handle_configuration(self, topic, message: str) -> None:
        split_path = topic.split("/")
        del split_path[0]
        del split_path[0]

        topic = "/".join(split_path)
        if message != "" and topic in self._expected_int_config_values:
            self._received_int_config_values.add(topic)
            setattr(self, split_path[-1], float(message))
            print(f"    ... received {topic} '{message}'")

    def clear_calibration(self) -> None:
        self.max_mag_x = -999999
        self.min_mag_x = 999999

        self.max_mag_y = -999999
        self.min_mag_y = 999999

        self.max_mag_z = -999999
        self.min_mag_z = 999999

    @property
    def calibrating(self) -> bool:
        return self.calibration_is_on

    @calibrating.setter
    def calibrating(self, calibration_is_on) -> None:
        self.calibration_is_on = calibration_is_on

    def on_telemetry_connection(self, connected: bool) -> None:
        if connected:
            self.stream = self.telemetry_client.streams[self.stream_name]
        else:
            self.stream = None

    def on_telemetry_received(self) -> None:
        current_time = time.time()

        def write_data(records):
            if len(records) > 0:
                # print(f"    receiving {len(records)} record(s): ", end="")
                for record in records:
                    # print(f"    got 9dof record {record}")
                    self.gyro_x = record[1]
                    self.gyro_y = record[2]
                    self.gyro_z = record[3]
                    self.accel_x = record[4]
                    self.accel_y = record[5]
                    self.accel_z = record[6]
                    self.compass_x = record[7]
                    self.compass_y = record[8]
                    self.compass_z = record[9]

                    self.process_results()

                self.telemetry_client.trim(self.stream, current_time)

        def fetch_stream(stream):
            self.stream = stream
            self.telemetry_client.retrieve(stream, 0, time.time(), write_data)

        if self.telemetry_client is not None and self.telemetry_client.socket is not None:
            self.telemetry_client.get_stream_definition(self.stream_name, fetch_stream)

    def process_results(self) -> None:
        if self.calibration_is_on:
            if self.compass_x > self.max_mag_x:
                self.max_mag_x = self.compass_x
                print(f"Got new max_mag_x {self.max_mag_x:.1f}")
                pyros.publish("storage/write/icm20948/max_mag_x", f"{self.max_mag_x:.1f}")
            if self.compass_x < self.min_mag_x:
                self.min_mag_x = self.compass_x
                pyros.publish("storage/write/icm20948/min_mag_x", f"{self.min_mag_x:.1f}")

            if self.compass_y > self.max_mag_y:
                self.max_mag_y = self.compass_y
                pyros.publish("storage/write/icm20948/max_mag_y", f"{self.max_mag_y:.1f}")
            if self.compass_y < self.min_mag_y:
                self.min_mag_y = self.compass_y
                pyros.publish("storage/write/icm20948/min_mag_y", f"{self.min_mag_y:.1f}")

            if self.compass_z > self.max_mag_z:
                self.max_mag_z = self.compass_z
                pyros.publish("storage/write/icm20948/max_mag_z", f"{self.max_mag_z:.1f}")
            if self.compass_z < self.min_mag_z:
                self.min_mag_z = self.compass_z
                pyros.publish("storage/write/icm20948/min_mag_z", f"{self.min_mag_z:.1f}")

        mag_x = (self.compass_x - self.min_mag_x) / (self.max_mag_x - self.min_mag_x) if self.max_mag_x != self.min_mag_x else 0
        mag_y = (self.compass_y - self.min_mag_y) / (self.max_mag_y - self.min_mag_y) if self.max_mag_y != self.min_mag_y else 0
        mag_z = (self.compass_z - self.min_mag_z) / (self.max_mag_z - self.min_mag_z) if self.max_mag_z != self.min_mag_z else 0

        mag_x -= 0.5
        mag_y -= 0.5
        mag_z -= 0.5

        bearing = math.atan2(mag_x, mag_y)
        if bearing < 0:
            bearing += 2 * math.pi
        self.bearing = bearing

        self.pitch = math.atan2(self.accel_x, self.accel_z)
        self.roll = math.atan2(self.accel_y, self.accel_z)
        # print("    processed results from 9dof sensor")
