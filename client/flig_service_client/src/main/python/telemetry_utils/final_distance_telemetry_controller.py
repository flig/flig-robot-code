import math
import time

from pyros_support_ui import PyrosClientApp
from telemetry_utils.common_distance_controller import CommonDistancesTelemetryController


ANGLE_90 = math.pi / 2
ANGLE_45 = math.pi / 4
ANGLE_22_5 = math.pi / 8
ANGLE_12_25 = math.pi / 16
ANGLE_9_5 = math.pi / 19
ANGLE_6_125 = math.pi / 32

FLOOR_OUTER_ANGLE = ANGLE_22_5  # - ANGLE_12_25
# FLOOR_INNER_ANGLE = ANGLE_9_5  # - ANGLE_12_25
FLOOR_INNER_ANGLE = ANGLE_12_25  # - ANGLE_12_25
WALL_OUTER_ANGLE = ANGLE_22_5 - ANGLE_6_125
WALL_INNER_ANGLE = ANGLE_12_25 - ANGLE_6_125

# ANGLES = [ANGLE_90 - OUTER_ANGLE, ANGLE_90 - INNER_ANGLE, ANGLE_90 + INNER_ANGLE, ANGLE_90  +OUTER_ANGLE]
FLOOR_ANGLES = [-FLOOR_OUTER_ANGLE, -FLOOR_INNER_ANGLE, FLOOR_INNER_ANGLE, FLOOR_OUTER_ANGLE]
WALL_ANGLES = [ANGLE_90 - -WALL_OUTER_ANGLE, ANGLE_90 - -WALL_INNER_ANGLE, ANGLE_90 - WALL_INNER_ANGLE, ANGLE_90 - WALL_OUTER_ANGLE]


class FinalDistancesTelemetryController(CommonDistancesTelemetryController):
    def __init__(self, main_app: PyrosClientApp):
        super().__init__("distances", "positioning_stream", telemetry_port=6403)
        self.main_app = main_app

    def on_telemetry_connection(self, connected: bool) -> None:
        if connected:
            self.stream = self.telemetry_client.streams[self.stream_name]
        else:
            self.stream = None

    def on_telemetry_received(self) -> None:
        current_time = time.time()

        def write_data(records):
            if len(records) > 0:
                # print(f"    receiving {len(records)} record(s): ", end="")
                for record in records:
                    self.gyro_bearing = record[1]
                    self.wall_angle = record[2]
                    self.wall_angle_avg = record[3]
                    self.wall_points = record[4]
                    self.wall_distance = record[5]
                    for i in range(4):
                        self.object_distance[i] = record[6 + i]
                    for i in range(4):
                        self.floor_angles[i] = record[10 + i * 2 + 0]
                        self.floor_points[i] = record[10 + i * 2 + 1]

                self.telemetry_client.trim(self.stream, current_time)

        def fetch_stream(stream):
            self.stream = stream
            self.telemetry_client.retrieve(stream, 0, time.time(), write_data)

        if self.telemetry_client is not None and self.telemetry_client.socket is not None:
            self.telemetry_client.get_stream_definition(self.stream_name, fetch_stream)
