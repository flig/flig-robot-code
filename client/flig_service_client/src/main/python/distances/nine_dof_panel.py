import math
from typing import Optional, Generic, TypeVar, Callable, Union, Dict

from pygame import Rect

from pyros_support_ui.components import BaseUIFactory, Collection, TopDownLayout, LeftRightLayout, ALIGNMENT, Component, CardsCollection
from telemetry_utils.nine_dof_telemetry_controller import NineDoFTelemetryController
from ui.name_value_panel import NameValueListComponent

WIDTH = 20
HEIGHT = 20
MARGIN = 10

T = TypeVar('T')

#
# class NameValueComponent(Collection, Generic[T]):
#     def __init__(self, rect: Optional[Rect], ui_factory: BaseUIFactory,
#                  key_width: int, value_width: int, height: int,
#                  formatter: Callable[[T], str]) -> None:
#         super().__init__(rect, layout=LeftRightLayout(margin=5))
#         self.key_width = key_width
#         self.value_width = value_width
#         self.key_label = ui_factory.label(Rect(0, 0, key_width, height), "", h_alignment=ALIGNMENT.LEFT, v_alignment=ALIGNMENT.TOP)
#         self.value_label = ui_factory.label(Rect(0, 0, value_width, height), "", h_alignment=ALIGNMENT.RIGHT, v_alignment=ALIGNMENT.TOP)
#         self.formatter = formatter
#         self.add_component(self.key_label)
#         self.add_component(self.value_label)
#
#     def redefine_rect(self, rect) -> None:
#         super().redefine_rect(rect)
#
#     @property
#     def key(self) -> str:
#         return self.key_label.text
#
#     @key.setter
#     def key(self, key: str) -> None:
#         self.key_label.text = key
#
#     @property
#     def value(self) -> T:
#         return self.formatter(self.value_label.text)
#
#     @value.setter
#     def value(self, value: T) -> None:
#         self.value_label.text = self.formatter(value)
#
#
# class NameValueListComponent(Collection):
#     def __init__(self, rect: Optional[Rect], ui_factory: BaseUIFactory, key_width: int, value_width: int, entry_height: int) -> None:
#         super().__init__(rect, layout=TopDownLayout(margin=5))
#         self.ui_factory = ui_factory
#         self.key_width = key_width
#         self.value_width = value_width
#         self.entry_height = entry_height
#         self.dict: Dict[str, NameValueComponent] = {}
#         self.values: Dict[str, Union[str, float, int]] = {}
#
#     def _add_key_value(self, key_id: str, key_name: str, value: str, formatter: Callable[[T], str]) -> None:
#         self.dict[key_id] = NameValueComponent(
#             Rect(0, 0, self.key_width + self.value_width + 5, self.entry_height),
#             self.ui_factory, key_width=self.key_width, value_width=self.value_width, height=self.entry_height, formatter=formatter)
#
#         self.dict[key_id].key = key_name
#         self.add_component(self.dict[key_id])
#
#     def add_str_value(self, key_id: str, key_name: str, value: str) -> None:
#         def formatter(s: str) -> str:
#             return f"{s}"
#         self._add_key_value(key_id, key_name, formatter(value), formatter)
#         self.values[key_id] = value
#
#     def add_float_value(self, key_id: str, key_name: str, value: float) -> None:
#         def formatter(value: float) -> str:
#             return f"{value:.1f}"
#         self._add_key_value(key_id, key_name, formatter(value), formatter)
#         self.values[key_id] = value
#
#     def add_int_value(self, key_id: str, key_name: str, value: int) -> None:
#         def formatter(i: int) -> str:
#             return f"{i}"
#         self._add_key_value(key_id, key_name, formatter(value), formatter)
#         self.values[key_id] = value
#
#     def set_value(self, key_id: str, value: Union[str, float, int]) -> None:
#         self.values[key_id] = value
#         self.dict[key_id].value = value
#
#     def get_value(self, key_id: str) -> Union[str, float, int]:
#         return self.values[key_id]
#
#     def add_spacer(self, height: int) -> None:
#         self.add_component(Component(Rect(0, 0, 0, height)))


class NineDofPanel(NameValueListComponent):
    def __init__(self, rect: Optional[Rect], ui_factory: BaseUIFactory) -> None:
        super().__init__(rect, ui_factory, 160, 120, 20)
        self.nine_dof_telemetry_controller: Optional[NineDoFTelemetryController] = None
        self.add_float_value("roll", "Roll", 0.0)
        self.add_float_value("pitch", "Pitch", 0.0)
        self.add_float_value("bearing", "Bearing", 0.0)
        self.add_spacer(10)
        self.add_float_value("compass_x", "mag x", 0)
        self.add_float_value("compass_y", "mag y", 0)
        self.add_float_value("compass_z", "mag z", 0)
        self.add_spacer(10)
        self.add_float_value("gyro_x", "gyro x", 0)
        self.add_float_value("gyro_y", "gyro y", 0)
        self.add_float_value("gyro_z", "gyro z", 0)
        self.add_spacer(10)

        self.calibration_panel = CardsCollection(Rect(0, 0, 100, 18))
        self.calibration_on_button = ui_factory.text_button(Rect(0, 0, 100, 18), "On", on_click=self.calibration_button_on)
        self.calibration_off_button = ui_factory.text_button(Rect(0, 0, 100, 18), "Off", on_click=self.calibration_button_off)
        self.calibration_panel.add_card("on", self.calibration_on_button)
        self.calibration_panel.add_card("off", self.calibration_off_button)
        self.calibration_panel.select_card("on")
        self.add_component(self.calibration_panel)

        self.calibration_clear_button = ui_factory.text_button(Rect(0, 0, 100, 18), "Clear cal", on_click=self.calibration_clear)
        self.add_component(self.calibration_clear_button)

        self.add_spacer(10)
        self.add_float_value("max_compass_x", "Max Cx", 0)
        self.add_float_value("min_compass_x", "Min Cx", 0)
        self.add_float_value("max_compass_y", "Max Cy", 0)
        self.add_float_value("min_compass_y", "Min Cy", 0)
        self.add_float_value("max_compass_z", "Max Cz", 0)
        self.add_float_value("min_compass_z", "Min Cz", 0)
        self.add_spacer(10)

    def setup_nine_dof_telemetry_client(self, nine_dof_telemetry_controller: NineDoFTelemetryController):
        self.nine_dof_telemetry_controller = nine_dof_telemetry_controller

    def draw(self, surface) -> None:
        if self.nine_dof_telemetry_controller is not None:
            self.set_value("roll", math.degrees(self.nine_dof_telemetry_controller.roll))
            self.set_value("pitch", math.degrees(self.nine_dof_telemetry_controller.pitch))
            self.set_value("bearing", math.degrees(self.nine_dof_telemetry_controller.bearing))

            self.set_value("compass_x", self.nine_dof_telemetry_controller.compass_x)
            self.set_value("compass_y", self.nine_dof_telemetry_controller.compass_y)
            self.set_value("compass_z", self.nine_dof_telemetry_controller.compass_z)

            self.set_value("gyro_x", self.nine_dof_telemetry_controller.gyro_x)
            self.set_value("gyro_y", self.nine_dof_telemetry_controller.gyro_y)
            self.set_value("gyro_z", self.nine_dof_telemetry_controller.gyro_z)

            self.set_value("max_compass_x", self.nine_dof_telemetry_controller.max_mag_x)
            self.set_value("min_compass_x", self.nine_dof_telemetry_controller.min_mag_x)

            self.set_value("max_compass_y", self.nine_dof_telemetry_controller.max_mag_y)
            self.set_value("min_compass_y", self.nine_dof_telemetry_controller.min_mag_y)

            self.set_value("max_compass_z", self.nine_dof_telemetry_controller.max_mag_z)
            self.set_value("min_compass_z", self.nine_dof_telemetry_controller.min_mag_z)

        super().draw(surface)

    def calibration_button_on(self, *args) -> None:
        print("Starting calibration")
        self.calibration_panel.select_card("off")
        self.nine_dof_telemetry_controller.calibrating = True

    def calibration_button_off(self, *args) -> None:
        print("Stopping calibration")
        self.calibration_panel.select_card("on")
        self.nine_dof_telemetry_controller.calibrating = False

    def calibration_clear(self, *args) -> None:
        self.nine_dof_telemetry_controller.clear_calibration()
