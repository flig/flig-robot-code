from typing import Callable

from pygame import Rect

from distances.distances_panel import DistancesPanel
from distances.nine_dof_panel import NineDofPanel
from pyros_support_ui import PyrosClientApp
from pyros_support_ui.components import Collection, LeftRightLayout, BaseUIFactory, Component, UiHint
from telemetry_utils.common_distance_controller import CommonDistancesTelemetryController
from telemetry_utils.distance_telemetry_controller import DistancesTelemetryController
from telemetry_utils.nine_dof_telemetry_controller import NineDoFTelemetryController

from ui.screen import Screen


class DistanceControllerSelection:
    selected_distances_telemetry_controller: CommonDistancesTelemetryController
    selection_changed_callback: Callable[[], None]

    def set_selection_changed_callback(self, selection_changed_callback: Callable[[], None]):
        self.selection_changed_callback = selection_changed_callback


class DistancesScreen(Screen):

    CURRENT_SENSOR_PANEL_WIDTH = 300
    NINE_DOF_PANEL_WIDTH = 300

    def __init__(self, main_app: PyrosClientApp, ui_factory: BaseUIFactory,
                 main_screen: DistanceControllerSelection,
                 nine_dof_telemetry_controler: NineDoFTelemetryController):
        super().__init__(main_app)
        self.main_screen = main_screen
        self.nine_dof_telemetry_controler = nine_dof_telemetry_controler

        self.commands_panel = ui_factory.panel(None, layout=LeftRightLayout(margin=10), hint=UiHint.NO_DECORATION)

        self.distances_panel = DistancesPanel(None, ui_factory)
        self.add_component(self.distances_panel)

        self.nine_dof_panel = NineDofPanel(None, ui_factory)
        self.add_component(self.nine_dof_panel)

        self.commands_panel.add_component(Component(Rect(0, 0, 10, 20)))
        self.main_screen.set_selection_changed_callback(self._selection_changed_callback)

        self.nine_dof_telemetry_controler.set_on_connection_callback(self._on_nine_dof_telemetry_connection)

    def _selection_changed_callback(self) -> None:
        self.main_screen.selected_distances_telemetry_controller.set_on_connection_callback(self._on_distance_telemetry_connection)

    def _on_distance_telemetry_connection(self, connected: bool) -> None:
        if connected:
            self.distances_panel.setup_distance_telemetry_client(self.main_screen.selected_distances_telemetry_controller)

    def _on_nine_dof_telemetry_connection(self, connected: bool) -> None:
        if connected:
            self.nine_dof_panel.setup_nine_dof_telemetry_client(self.nine_dof_telemetry_controler)

    def set_contributing_panel(self, contributing_command_panel: Collection):
        contributing_command_panel.add_component(self.commands_panel)

    def draw(self, surface):
        super().draw(surface)

    def redefine_rect(self, rect):
        self.rect = rect
        self.distances_panel.redefine_rect(
            Rect(5, rect.y, rect.right - self.NINE_DOF_PANEL_WIDTH - 10, rect.height))
        self.nine_dof_panel.redefine_rect(
            Rect(self.distances_panel.rect.right + 5, rect.top + 5, self.NINE_DOF_PANEL_WIDTH, rect.height)
        )
