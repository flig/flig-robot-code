import math
from typing import Optional

import pygame.draw
from pygame.rect import Rect

from pyros_support_ui.components import Panel, BaseUIFactory
from telemetry_utils.common_distance_controller import CommonDistancesTelemetryController
from telemetry_utils.distance_telemetry_controller import FLOOR_ANGLES, ANGLE_90
from ui.name_value_panel import NameValueListComponent

WIDTH = 50
HEIGHT = 50
MARGIN = 10

WALL_POINT_DISTANCE = 20
WALL_X = 300
WALL_Y = 180

FLOOR_OFFSET = 3
FLOOR_X = 400
FLOOR_Y = 180


class DistancesPanel(Panel):
    def __init__(self, _: Optional[Rect], ui_factory: BaseUIFactory) -> None:
        super().__init__(Rect(0, 0, 220, 220))
        self.distances_telemetry_controller: Optional[CommonDistancesTelemetryController] = None
        self.ui_factory = ui_factory
        self.details = NameValueListComponent(Rect(0, 0, 300, 220), ui_factory, 160, 120, 20)
        self.add_component(self.details)

        self.details.add_float_value("gyro_bearing", "Gyro ˚", 0.0)
        self.details.add_float_value("wall_angle", "Wall ˚", 0.0)
        self.details.add_float_value("wall_distance", "Wall", 0.0)
        self.details.add_int_value("wall_points", "Points", 0)
        self.details.add_spacer(10)

        self.details.add_float_value("object_distance_1", "D@-45˚", 0)
        self.details.add_float_value("object_distance_2", "D@-22˚", 0)
        self.details.add_float_value("object_distance_3", "D@+22˚", 0)
        self.details.add_float_value("object_distance_4", "D@+45˚", 0)
        self.details.add_spacer(10)

        self.details.add_float_value("floor_angle_1", "FA 1 ˚", 0)
        self.details.add_int_value("floor_points_1", "FP 1", 0)
        self.details.add_float_value("floor_angle_2", "FA 2 ˚", 0)
        self.details.add_int_value("floor_points_2", "FP 2", 0)
        self.details.add_float_value("floor_angle_3", "FA 3 ˚", 0)
        self.details.add_int_value("floor_points_3", "FP 3", 0)
        self.details.add_float_value("floor_angle_4", "FA 4 ˚", 0)
        self.details.add_int_value("floor_points_4", "FP 4", 0)

        self._rover_wall_polygon = []
        self._rover_floor_polygon = []

    def redefine_rect(self, rect) -> None:
        details_rect = Rect(rect.x, rect.y + 250, rect.width, rect.height)
        super().redefine_rect(details_rect)
        self.rect = rect
        self._rover_wall_polygon = [(self.rect.x + WALL_X - WALL_POINT_DISTANCE // 2, self.rect.y + WALL_Y),
                                    (self.rect.x + WALL_X - WALL_POINT_DISTANCE // 2 - 10, self.rect.y + WALL_Y + 20),
                                    (self.rect.x + WALL_X - WALL_POINT_DISTANCE // 2 + 10, self.rect.y + WALL_Y + 20)]
        # self._rover_floor_polygon = [(self.rect.x + FLOOR_X + FLOOR_OFFSET * 1.5, self.rect.y + FLOOR_Y - 7 + FLOOR_OFFSET),
        #                              (self.rect.x + FLOOR_X + FLOOR_OFFSET * 1.5 - 12, self.rect.y + FLOOR_Y - 5 + FLOOR_OFFSET + 16),
        #                              (self.rect.x + FLOOR_X + FLOOR_OFFSET * 1.5 + 4, self.rect.y + FLOOR_Y - 5 + FLOOR_OFFSET + 20),]
        self._rover_floor_polygon = [(self.rect.x + FLOOR_X + FLOOR_OFFSET * 2.5, self.rect.y + FLOOR_Y - 5 + FLOOR_OFFSET),
                                     (self.rect.x + FLOOR_X + FLOOR_OFFSET * 2.5 - 10, self.rect.y + FLOOR_Y - 5 + FLOOR_OFFSET + 20),
                                     (self.rect.x + FLOOR_X + FLOOR_OFFSET * 2.5 + 10, self.rect.y + FLOOR_Y - 5 + FLOOR_OFFSET + 20)]

    def draw(self, surface) -> None:
        if self.distances_telemetry_controller is not None:
            self.details.set_value("gyro_bearing", self.distances_telemetry_controller.gyro_bearing)
            self.details.set_value("wall_angle", self.distances_telemetry_controller.wall_angle_avg)
            self.details.set_value("wall_distance", self.distances_telemetry_controller.wall_distance)
            self.details.set_value("wall_points", self.distances_telemetry_controller.wall_points)

            self.details.set_value("object_distance_1", self.distances_telemetry_controller.object_distance[0])
            self.details.set_value("object_distance_2", self.distances_telemetry_controller.object_distance[1])
            self.details.set_value("object_distance_3", self.distances_telemetry_controller.object_distance[2])
            self.details.set_value("object_distance_4", self.distances_telemetry_controller.object_distance[3])

            # self.details.set_value("floor_angle_1", math.degrees(self.distances_telemetry_controller.floor_angles[0]))
            self.details.set_value("floor_angle_1", self.distances_telemetry_controller.floor_angles[0])
            self.details.set_value("floor_points_1", self.distances_telemetry_controller.floor_points[0])

            # self.details.set_value("floor_angle_2", math.degrees(self.distances_telemetry_controller.floor_angles[1]))
            self.details.set_value("floor_angle_2", self.distances_telemetry_controller.floor_angles[1])
            self.details.set_value("floor_points_2", self.distances_telemetry_controller.floor_points[1])

            # self.details.set_value("floor_angle_3", math.degrees(self.distances_telemetry_controller.floor_angles[2]))
            self.details.set_value("floor_angle_3", self.distances_telemetry_controller.floor_angles[2])
            self.details.set_value("floor_points_3", self.distances_telemetry_controller.floor_points[2])

            # self.details.set_value("floor_angle_4", math.degrees(self.distances_telemetry_controller.floor_angles[3]))
            self.details.set_value("floor_angle_4", self.distances_telemetry_controller.floor_angles[3])
            self.details.set_value("floor_points_4", self.distances_telemetry_controller.floor_points[3])

            small_font = self.ui_factory.small_font

            for x in range(4):
                for y in range(4):
                    k = x + y * 4

                    d = self.distances_telemetry_controller.distances[k]
                    if d > 1500: d = 1500
                    v = d

                    if v > 750:
                        v = (1500 - v) * 255 / 750
                        colour = (v, 0, 0)
                        text_colour = (255, 255, 255)
                    else:
                        v = (750 - v) * 255 / 750
                        colour = (255, v, 0)
                        text_colour = (0, 0, 255)

                    pygame.draw.rect(surface, colour, (self.rect.x + MARGIN + x * WIDTH, self.rect.y + MARGIN + y * HEIGHT, WIDTH, HEIGHT))
                    distance_str = f"{d:.0f}"

                    text_size = small_font.size(distance_str)
                    surface.blit(small_font.render(distance_str, True, text_colour),
                                 (self.rect.x + MARGIN + x * WIDTH + WIDTH - 5 - text_size[0],
                                  self.rect.y + MARGIN + y * HEIGHT + (HEIGHT - text_size[1]) // 2))

            polygon = []

            for i in range(4):
                d = self.distances_telemetry_controller.distances[i]
                d = d * WALL_Y / 1500
                # angle = ANGLES[i]
                # x = d * math.cos(angle - math.pi/2) + 340
                # y = d * math.sin(angle - math.pi/2) + 200
                x = self.rect.x + (i - 2) * WALL_POINT_DISTANCE + WALL_X
                y = self.rect.y + WALL_Y - d
                pygame.draw.circle(surface, (0, 255, 0), (x, y), 3)
                polygon.append((x, y))

            pygame.draw.lines(surface, (190, 190, 190), False, polygon)
            pygame.draw.lines(surface, (255, 255, 80), True, self._rover_wall_polygon)

            for l in range(4):
                polygon = []
                for i in range(3):
                    d = self.distances_telemetry_controller.distances[(i + 1) * 4 + l]
                    d = d * 200 / 1500
                    # x = (i - 1) * 25 + 400 + l * 60
                    # y = 240 - d
                    # angle = ANGLES[i + 1] + ANGLE_90
                    # x = d * math.cos(angle - ANGLE_90) + 400
                    # y = d * math.sin(angle - ANGLE_90) + 200 + l * 100
                    angle = ANGLE_90 - FLOOR_ANGLES[i + 1]
                    x = self.rect.x + FLOOR_X + d * math.cos(angle) + l * FLOOR_OFFSET * 3
                    y = self.rect.y + FLOOR_Y - d * math.sin(angle) + l * FLOOR_OFFSET
                    pygame.draw.circle(surface, (0, 255, 0), (x, y), 3)
                    polygon.append((x, y))
                pygame.draw.lines(surface, (190, 190, 190), False, polygon)
            pygame.draw.lines(surface, (255, 255, 80), True, self._rover_floor_polygon)

        super().draw(surface)

    def setup_distance_telemetry_client(self, distances_telemetry_controller: CommonDistancesTelemetryController):
        self.distances_telemetry_controller = distances_telemetry_controller
