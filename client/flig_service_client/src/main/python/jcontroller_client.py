#!/usr/bin/env python3

#
# Copyright 2016-2017 Games Creators Club
#
# MIT License
#

import math
from typing import Optional

import pygame

import pyros

DEBUG_AXES = False
DEBUG_BUTTONS = False
DEBUG_JOYSTICK = True
MAX_PING_TIMEOUT = 1

EXPO = 0.5

MAX_STOPPING = 10

# These constants were borrowed from linux/input.h
axis_names = {
    0x00: 'x',
    0x01: 'y',
    0x02: 'z',
    0x03: 'rx',
    0x04: 'ry',
    0x05: 'rz',
    0x06: 'trottle',
    0x07: 'rudder',
    0x08: 'wheel',
    0x09: 'gas',
    0x0a: 'brake',
    0x10: 'hat0x',
    0x11: 'hat0y',
    0x12: 'hat1x',
    0x13: 'hat1y',
    0x14: 'hat2x',
    0x15: 'hat2y',
    0x16: 'hat3x',
    0x17: 'hat3y',
    0x18: 'pressure',
    0x19: 'distance',
    0x1a: 'tilt_x',
    0x1b: 'tilt_y',
    0x1c: 'tool_width',
    0x20: 'volume',
    0x28: 'misc',
}

button_names = {
    0x120: 'trigger',
    0x121: 'thumb',
    0x122: 'thumb2',
    0x123: 'top',
    0x124: 'top2',
    0x125: 'pinkie',
    0x126: 'base',
    0x127: 'base2',
    0x128: 'base3',
    0x129: 'base4',
    0x12a: 'base5',
    0x12b: 'base6',
    0x12f: 'dead',
    0x130: 'a',
    0x131: 'b',
    0x132: 'c',
    0x133: 'x',
    0x134: 'y',
    0x135: 'z',
    0x136: 'tl',
    0x137: 'tr',
    0x138: 'tl2',
    0x139: 'tr2',
    0x13a: 'select',
    0x13b: 'start',
    0x13c: 'mode',
    0x13d: 'thumbl',
    0x13e: 'thumbr',

    0x220: 'dpad_up',
    0x221: 'dpad_down',
    0x222: 'dpad_left',
    0x223: 'dpad_right',

    # XBo 360 controller uses these codes.
    0x2c0: 'dpad_left',
    0x2c1: 'dpad_right',
    0x2c2: 'dpad_up',
    0x2c3: 'dpad_down',
    0xff1: 'lup',
    0xff2: 'ldown',
    0xff3: 'lleft',
    0xff4: 'lright',
    0xff5: 'lbutton',
    0xff6: 'rbutton',
    0xff7: 'tl1',
    0xff8: 'tr1',
    0xff9: 'ba',
    0xffa: 'bb',
    0xffb: 'bx',
    0xffc: 'by',
}


class JoystickClient:
    def __init__(self):
        self.pingLastTime = 0

        self.axis_states = {
            "x": 0, "y": 0,
            "rx": 0, "ry": 0
        }
        self.button_states = {}

        self.axis_map = []
        self.button_map = []

        for axis_name in axis_names:
            self.axis_states[axis_names[axis_name]] = 0.0

        for btn_name in button_names:
            self.button_states[button_names[btn_name]] = 0

        self.topSpeed = 20

        self.rover_turning_distance = 0
        self.rover_speed = 0

        self.lastNoChange = 0
        self.lastX1 = 0
        self.lastY1 = 0
        self.lastX2 = 0
        self.lastY2 = 0
        self.lastX3 = 0
        self.lastY3 = 0
        self.lastSelect = False
        self.lastStart = False
        self.lastTL = False
        self.lastTL2 = False
        self.lastTR = False
        self.lastTR2 = False
        self.lastA = False
        self.lastB = False
        self.lastBX = False
        self.lastBY = False
        self.lastLButton = False
        self.lastRButton = False
        self.lastTopSpeed = self.topSpeed

        self.have_joystick_event = False

        self.wobble = False
        self.wobble_alpha = 0

        self.last_divider_L = 1
        self.last_divider_R = 1
        self.divider_L = 1
        self.divider_R = 1

        self.already_stopped = 0


class MQTTJoystickClient(JoystickClient):
    def __init__(self):
        super().__init__()

    def process_buttons(self) -> None:
        # print("Axis states: " + str(self.axis_states))

        # 4 ly up: "TopBtn2", lx r 5: "PinkieBtn", ly down 6: "BaseBtn", lx left 7:"BaseBtn2"
        # x3 = int(axis_states["hat0x"])
        # y3 = int(axis_states["hat0y"])

        try:
            lup = self.button_states["lup"]
            lright = self.button_states["lright"]
            ldown = self.button_states["ldown"]
            lleft = self.button_states["lleft"]

            x3 = 0
            y3 = 0
            if lup:
                y3 = -1
            if ldown:
                y3 = 1
            if lleft:
                x3 = -1
            if lright:
                x3 = 1

            tl = self.button_states["tl1"]
            tl2 = self.button_states["tl2"]
            tr = self.button_states["tr1"]
            tr2 = self.button_states["tr2"]
            a = self.button_states["ba"]
            bb = self.button_states["bb"]
            bx = self.button_states["bx"]
            by = self.button_states["by"]

            start = self.button_states["start"]
            select = self.button_states["select"]

            lbutton = self.button_states["lbutton"]
            rbutton = self.button_states["rbutton"]

            if y3 != self.lastY3:
                if y3 < 0:
                    if self.topSpeed >= 20:
                        self.topSpeed += 10
                        if self.topSpeed > 300:
                            self.topSpeed = 300
                    else:
                        self.topSpeed += 1
                elif y3 > 0:
                    if self.topSpeed <= 20:
                        self.topSpeed -= 1
                        if self.topSpeed < 1:
                            self.topSpeed = 1
                    else:
                        self.topSpeed -= 10

            if x3 != self.lastX3:
                if x3 > 0:
                    self.topSpeed += 100
                    if self.topSpeed > 300:
                        self.topSpeed = 300
                elif x3 < 0:
                    if self.topSpeed >= 100:
                        self.topSpeed -= 100
                        if self.topSpeed < 30:
                            self.topSpeed = 30
                    elif self.topSpeed > 50:
                        self.topSpeed = 50

            self.lastX3 = x3
            self.lastY3 = y3
            self.lastStart = start
            self.lastTL = tl
            self.lastTL2 = tl2
            self.lastTR = tr
            self.lastTR2 = tr2
            self.lastA = a
            self.lastB = bb
            self.lastBX = bx
            self.lastBY = by
            self.lastSelect = select
            self.lastLButton = lbutton
            self.lastRButton = rbutton

            if DEBUG_BUTTONS:
                print(f"OK Button states: {self.button_states}")
            if DEBUG_AXES:
                print(f"OK Axis states: {self.axis_states}")

        except Exception as e:
            if DEBUG_BUTTONS:
                print(f"ERR Button states: {self.button_states} {e}")
            if DEBUG_AXES:
                print(f"ERR Axis states  : {self.axis_states} {e}")

    def calc_rover_speed(self, speed: float) -> int:
        return int(speed * self.topSpeed)

    @staticmethod
    def calculate_expo(v: float, expo_percentage: float) -> float:
        if v >= 0:
            return v * v * expo_percentage + v * (1.0 - expo_percentage)
        else:
            return - v * v * expo_percentage + v * (1.0 - expo_percentage)

    @staticmethod
    def calc_rover_distance(distance: float) -> int:
        if distance >= 0:
            distance = abs(distance)
            distance = 1.0 - distance
            distance += 0.2
            distance *= 500
        else:
            distance = abs(distance)
            distance = 1.0 - distance
            distance += 0.2
            distance = - distance * 500

        return int(distance)

    def process_joysticks(self):
        self.wobble_alpha += 1
        if self.have_joystick_event:
            self.have_joystick_event = False

            lx = float(self.axis_states["x"])
            ly = float(self.axis_states["y"])

            rx = float(self.axis_states["rx"])
            ry = float(self.axis_states["ry"])
            if self.wobble:
                print(f"wobble")
                rx = float(math.sin(self.wobble_alpha * 0.9))

            ld = math.sqrt(lx * lx + ly * ly)
            rd = math.sqrt(rx * rx + ry * ry)
            ra = math.atan2(rx, -ry) * 180 / math.pi

            if ld < 0.1 < rd:
                distance = rd
                distance = self.calculate_expo(distance, EXPO)

                self.rover_speed = self.calc_rover_speed(distance)

                pyros.publish("move/drive", f"{round(ra, 1)} {int(self.rover_speed / self.divider_R)}")
                if DEBUG_JOYSTICK:
                    print(f"Driving a:{round(ra, 1)} s:{self.rover_speed} ld:{ld} rd:{rd}")

                self.already_stopped = 0

            elif ld > 0.1 and rd > 0.1:
                ory = ry
                olx = lx
                ry = self.calculate_expo(ry, EXPO)

                lx = self.calculate_expo(lx, EXPO)

                self.rover_speed = -self.calc_rover_speed(ry) * 1.3
                self.rover_turning_distance = self.calc_rover_distance(lx)
                pyros.publish("move/steer", f"{self.rover_turning_distance} {int(self.rover_speed / self.divider_R)}")
                if DEBUG_JOYSTICK:
                    print(f"Steering d:{self.rover_turning_distance} s:{self.rover_speed} ry: {ory} lx:{olx} ld:{ld} rd:{rd}")

                self.already_stopped = 0
            elif ld > 0.1:
                olx = lx
                lx = self.calculate_expo(lx, EXPO) / 2
                self.rover_speed = self.calc_rover_speed(lx)
                pyros.publish("move/rotate", int(self.rover_speed / self.divider_L))
                if DEBUG_JOYSTICK:
                    print(f"Rotate s: {self.rover_speed}, lx: {olx}, ld: {ld}, rd: {rd}")
                self.already_stopped = 0

            else:
                if self.already_stopped < MAX_STOPPING:
                    pyros.publish("move/stop", "0")
                    self.already_stopped += 1

                if DEBUG_JOYSTICK:
                    print(f"Rotate stop: ld: {ld}, rd: {rd}")


class JoystickKeyProcessor:
    def __init__(self, joystick_client: JoystickClient) -> None:
        self.joystick_client = joystick_client

    def on_key_down(self, key: str) -> bool:
        processed = False
        if key == pygame.K_a:
            self.joystick_client.axis_states['x'] = -1.0
            processed = True
        elif key == pygame.K_d:
            self.joystick_client.axis_states['x'] = 1.0
            processed = True
        elif key == pygame.K_w:
            self.joystick_client.axis_states['y'] = -1.0
            processed = True
        elif key == pygame.K_s:
            self.joystick_client.axis_states['y'] = 1.0
            processed = True
        elif key == pygame.K_LEFT:
            self.joystick_client.axis_states['rx'] = -1.0
            processed = True
        elif key == pygame.K_RIGHT:
            self.joystick_client.axis_states['rx'] = 1.0
            processed = True
        elif key == pygame.K_UP:
            self.joystick_client.axis_states['ry'] = -1.0
            processed = True
        elif key == pygame.K_DOWN:
            self.joystick_client.axis_states['ry'] = 1.0
            processed = True
        elif key == pygame.K_MINUS:
            if self.joystick_client.topSpeed <= 20:
                self.joystick_client.topSpeed -= 1
                if self.joystick_client.topSpeed < 1:
                    self.joystick_client.topSpeed = 1
            else:
                self.joystick_client.topSpeed -= 10
                processed = True
        elif key == pygame.K_EQUALS:
            if self.joystick_client.topSpeed >= 20:
                self.joystick_client.topSpeed += 10
                if self.joystick_client.topSpeed > 300:
                    self.joystick_client.topSpeed = 300
            else:
                self.joystick_client.topSpeed += 1
                processed = True
        elif key == pygame.K_LEFTBRACKET:
            if self.joystick_client.topSpeed >= 100:
                self.joystick_client.topSpeed -= 100
                if self.joystick_client.topSpeed < 30:
                    self.joystick_client.topSpeed = 30
            elif self.joystick_client.topSpeed > 50:
                self.joystick_client.topSpeed = 50
                processed = True
        elif key == pygame.K_RIGHTBRACKET:
            self.joystick_client.topSpeed += 100
            if self.joystick_client.topSpeed > 300:
                self.joystick_client.topSpeed = 300
            processed = True

        if processed:
            self.joystick_client.have_joystick_event = True
        return processed

    def on_key_up(self, key: str) -> bool:
        processed = False
        if key == pygame.K_a:
            self.joystick_client.axis_states['x'] = 0.0
            processed = True
        elif key == pygame.K_d:
            self.joystick_client.axis_states['x'] = 0.0
            processed = True
        elif key == pygame.K_w:
            self.joystick_client.axis_states['y'] = 0.0
            processed = True
        elif key == pygame.K_s:
            self.joystick_client.axis_states['y'] = 0.0
            processed = True
        elif key == pygame.K_LEFT:
            self.joystick_client.axis_states['rx'] = 0.0
            processed = True
        elif key == pygame.K_RIGHT:
            self.joystick_client.axis_states['rx'] = 0.0
            processed = True
        elif key == pygame.K_UP:
            self.joystick_client.axis_states['ry'] = 0.0
            processed = True
        elif key == pygame.K_DOWN:
            self.joystick_client.axis_states['ry'] = 0.0
            processed = True

        if processed:
            self.joystick_client.have_joystick_event = True

        return processed


class PygameJoystickProcessor:
    def __init__(self, joystick_client: JoystickClient) -> None:
        self.joystick_client = joystick_client

        self.pygame_joystick: Optional[pygame.joystick.Joystick] = None

        self.ljx = 0
        self.ljy = 0
        self.ljrx = 0
        self.ljry = 0

    def connect(self) -> bool:
        try:
            j = pygame.joystick.Joystick(0)  # create a joystick instance
            j.init()  # init instance
            print('Enabled joystick: ' + j.get_name())
            self.ljx = 0
            self.ljy = 0
            self.ljrx = 0
            self.ljry = 0
            return True
        except pygame.error:
            print('no joystick found.')
            return False

    def process_pygame_event(self, event) -> None:
        if event.type in [pygame.JOYAXISMOTION, pygame.JOYBALLMOTION, pygame.JOYHATMOTION, pygame.JOYBUTTONDOWN, pygame.JOYBUTTONUP]:
            if self.pygame_joystick is None:
                self.connect()
            if self.pygame_joystick is not None:
                if event.type == pygame.JOYAXISMOTION:  # 7
                    if self.ljx != self.pygame_joystick.get_axis(0):
                        self.ljx = self.pygame_joystick.get_axis(0)
                        self.joystick_client.axis_states['x'] = self.ljx
                        self.joystick_client.have_joystick_event = True
                    if self.ljy != self.pygame_joystick.get_axis(1):
                        self.ljy = self.pygame_joystick.get_axis(1)
                        self.joystick_client.axis_states['y'] = self.ljy
                        self.joystick_client.have_joystick_event = True
                    if self.ljrx != self.pygame_joystick.get_axis(3):
                        self.ljrx = self.pygame_joystick.get_axis(3)
                        self.joystick_client.axis_states['rx'] = self.ljx
                        self.joystick_client.have_joystick_event = True
                    if self.ljry != self.pygame_joystick.get_axis(4):
                        self.ljry = self.pygame_joystick.get_axis(4)
                        self.joystick_client.axis_states['ry'] = self.ljry
                        self.joystick_client.have_joystick_event = True

                    print(f"x: {self.joystick_client.axis_states['x']}"
                          f" y: {self.joystick_client.axis_states['y']}"
                          f" rx: {self.joystick_client.axis_states['rx']}"
                          f" ry: {self.joystick_client.axis_states['ry']}")
                elif event.type == pygame.JOYBALLMOTION:  # 8
                    pass
                elif event.type == pygame.JOYHATMOTION:  # 9
                    value = self.pygame_joystick.get_hat(0)
                    self.joystick_client.axis_states['rx'] = value[0]
                    self.joystick_client.axis_states['ry'] = value[1]
                    self.joystick_client.have_joystick_event = True
                    print(f"rh: {self.joystick_client.axis_states['rx']} lh: {self.joystick_client.axis_states['ry']}")
                elif event.type == pygame.JOYBUTTONDOWN:  # 10
                    pass
                elif event.type == pygame.JOYBUTTONUP:  # 11
                    pass

#
# while True:
#     for event in pygame.event.get():
#         if event.type == pygame.QUIT:
#             pygame.quit()
#             sys.exit()
#
#         if event.type == pygame.JOYAXISMOTION:    # 7
#             if ljx != j.get_axis(0):
#                 ljx = j.get_axis(0)
#                 axis_states['x'] = ljx
#             if ljy != j.get_axis(1):
#                 ljy = j.get_axis(1)
#                 axis_states['y'] = ljy
#             if ljrx != j.get_axis(3):
#                 ljrx = j.get_axis(3)
#                 axis_states['rx'] = ljx
#             if ljry != j.get_axis(4):
#                 ljry = j.get_axis(4)
#                 axis_states['ry'] = ljry
#
#             print("x: " + str(axis_states['x']) + " y: " + str(axis_states['y']) + " rx: " + str(axis_states['rx']) + " ry: " + str(axis_states['ry']))
#         elif event.type == pygame.JOYBALLMOTION:  # 8
#             pass
#         elif event.type == pygame.JOYHATMOTION:   # 9
#             axis_states['rx'] = j.get_hat(0)
#             axis_states['ry'] = j.get_hat(1)
#             print("rh: " + str(axis_states['rx']) + " lh: " + str(axis_states['ry']))
#         elif event.type == pygame.JOYBUTTONDOWN:  # 10
#             pass
#         elif event.type == pygame.JOYBUTTONUP:    # 11
#             pass
#
#     processButtons()
#     processJoysticks()
#
#     pyros.loop(0.03)
