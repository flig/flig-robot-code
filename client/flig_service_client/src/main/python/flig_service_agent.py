import threading
import time
import traceback

import pyroslib
from telemetry import SocketTelemetryServer

from servo_lib import INA3221, PCA9685

average_low = 0

next_time = time.time()


if __name__ == "__main__":
    try:
        print("Starting flig-service agent...")

        pyroslib.init("flig-service-agent", unique=True)

        print("Starting telemetry server...")
        server = SocketTelemetryServer(port=1862)

        timeout = time.time() + 10
        started = False

        while not started and time.time() < timeout:
            try:
                server.start()
                started = True
            except Exception as e:
                print(f"    failed to start socket server; {e}")
                time.sleep(1)

        server_socket = server.get_socket()
        server_socket.settimeout(10)

        servo_current_stream_logger = server.create_logger("servo_current_stream")
        servo_current_stream_logger.add_float('current_0')
        servo_current_stream_logger.add_float('current_1')
        servo_current_stream_logger.add_float('current_2')
        servo_current_stream_logger.add_float('current_3')
        servo_current_stream_logger.add_float('current_4')
        servo_current_stream_logger.add_float('current_5')
        servo_current_stream_logger.init()

        def server_loop():
            while True:
                try:
                    server.process_incoming_connections()
                except:
                    pass

        server_thread = threading.Thread(target=server_loop, daemon=True)
        server_thread.start()

        print(f"Started telemetry server on port {server.port}")

        ina1 = INA3221(i2c_addr=0x40)
        ina2 = INA3221(i2c_addr=0x41)
        pca9685 = PCA9685(i2c_addr=0x44)

        def drive_servo(servo_channel: int, position: int) -> None:
            print(f"Driving servo {servo_channel} to position {position}")
            pca9685.set_servo_position(int(servo_channel), int(position))

        def generate_data_loop():
            global next_time, mode, average_low

            currents = [ina1.get_current_mA(i + 1) for i in range(3)] + [ina2.get_current_mA(i + 1) for i in range(3)]
            servo_current_stream_logger.log(time.time(),
                                   currents[0],
                                   currents[1],
                                   currents[2],
                                   currents[3],
                                   currents[4],
                                   currents[5])

        def handle_servo_value(topic, message, _groups):
            topic_split = topic.split("/")
            servo_channel = int(topic_split[-1])
            position = 0
            try:
                position = int(message)
            except ValueError as _:
                if "." in message:
                    position = int(message[:message.index(".")])

            drive_servo(servo_channel, position)

        pyroslib.subscribe("servo/raw/#", handle_servo_value)

        pyroslib.forever(0.02, outer=generate_data_loop)

    except Exception as ex:
        print("ERROR: " + str(ex) + "\n" + ''.join(traceback.format_tb(ex.__traceback__)))
