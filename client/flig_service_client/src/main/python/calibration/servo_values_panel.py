from functools import partial
from typing import Optional, Callable, Any

from pygame import Rect
from telemetry import TelemetryClient

from calibration.calculate_speed_panel import CalculateSpeedPanel
from servo_lib.leg_servos import ServoDefinition, StorageValue, LegServos
from pyros_support_ui.components import Collection, TopDownLayout, Component, BaseUIFactory, LeftRightLayout, ALIGNMENT, UiHint
from ui.screen import Screen
from ui.smart_number_input import SmartNumberInputComponent


class ValuesPanel(Collection):
    def __init__(self, rect: Rect, ui_factory: BaseUIFactory, screen: Screen, calculate_speed_panel: CalculateSpeedPanel):
        super().__init__(rect, TopDownLayout(margin=5))
        self.screen = screen
        self._value_changed_callback: Optional[Callable[[str], None]] = None
        self.servo_definition: Optional[ServoDefinition] = None
        self.calculate_speed_panel = calculate_speed_panel

        self._getters_and_setters = {}

        button_height = 40
        width = 320

        input_component_rect = Rect(0, 0, width, button_height)

        def number_input_compoennt(path: str, title: str, minumum_value: int, maxumum_value: int, special_all_offset: bool = False):
            self.add_component(
                SmartNumberInputComponent(
                    input_component_rect.copy(),
                    ui_factory,
                    self.create_getter_and_setter(path, special_all_offset),
                    self.create_storage_reader(path),
                    minumum_value, maxumum_value,
                    title,
                    top_scale=2, bottom_scale=0, button_font=ui_factory.small_font))

        self.servo_path = None
        self.add_component(Component(Rect(0, 0, width, 50)))
        number_input_compoennt("servo_channel", 'ser #', 0, 15)
        number_input_compoennt("current_channel", 'cur #', 0, 5)
        self.add_component(Component(Rect(0, 0, width, 10)))
        number_input_compoennt("limit_upper", '+lim', 100, 3000)
        self.add_component(Component(Rect(0, 0, width, 10)))
        number_input_compoennt("plus_90", '+90º', 100, 3000)
        number_input_compoennt("plus_45", '+45º', 100, 3000)
        number_input_compoennt("middle", 'mid', 100, 3000)
        number_input_compoennt("middle", 'off', 100, 3000, special_all_offset=True)
        number_input_compoennt("minus_45", '-45º', 100, 3000)
        number_input_compoennt("minus_90", '-90º', 100, 3000)
        self.add_component(Component(Rect(0, 0, width, 10)))
        number_input_compoennt("limit_lower", '-lim', 100, 3000)
        self.add_component(Component(Rect(0, 0, width, 10)))
        number_input_compoennt("speed", 'spd', 100, 3000)

        self.add_component(Component(Rect(0, 0, width, 50)))
        self.add_component(
            Collection(Rect(0, 0, width, 30),
                       components=[
                           ui_factory.text_button(Rect(0, 0, 120, 30), "Reverse", on_click=self.reverse_values, hint=UiHint.LIGHT),
                           ui_factory.text_button(Rect(0, 0, 120, 30), "Undo", on_click=self.undo, hint=UiHint.LIGHT),
                           Component(Rect(0, 0, 30, 30))
                       ],
                       layout=LeftRightLayout(h_alignment=ALIGNMENT.RIGHT)))
        self.add_component(
            Collection(Rect(0, 0, width, 30),
                       components=[
                           ui_factory.text_button(Rect(0, 0, 120, 30), "Update", on_click=self.update_servo, hint=UiHint.LIGHT),
                           ui_factory.text_button(Rect(0, 0, 120, 30), "Stop", on_click=self.stop_servo, hint=UiHint.LIGHT),
                           Component(Rect(0, 0, 30, 30))
                       ],
                       layout=LeftRightLayout(h_alignment=ALIGNMENT.RIGHT)))
        self.add_component(Component(Rect(0, 0, width, 10)))
        # self.calculate_speed_button = TextToggleButton(
        #     Rect(0, 0, 240, 30),
        #     ui_factory,
        #     on_text="Calc Speed", on_click_on=self.start_calculating_speed,
        #     off_text="Stop Speed Calc", on_click_off=self.stop_calculating_speed,
        #     auto_toggle=True,
        #     hint=UiHint.LIGHT)
        self.calculate_speed_button = ui_factory.text_button(
            Rect(0, 0, 240, 20),
            "Calculate Speed",
            on_click=self.show_calculate_speed,
            hint=UiHint.LIGHT)
        self.add_component(
            Collection(Rect(0, 0, width, 30),
                       components=[self.calculate_speed_button, Component(Rect(0, 0, 30, 30))],
                       layout=LeftRightLayout(h_alignment=ALIGNMENT.RIGHT)))

    def draw(self, surface) -> None:
        # if self.calculate_speed_panel.is_running_calibration():
        #     if self.calculate_speed_button.is_toggle_on():
        #         self.calculate_speed_button.toggle_off()
        # else:
        #     if not self.calculate_speed_button.is_toggle_on():
        #         self.calculate_speed_button.toggle_on()
        super().draw(surface)

    @property
    def telemetry_client(self) -> Optional[TelemetryClient]:
        return self.calculate_speed_panel.telemetry

    @telemetry_client.setter
    def telemetry_client(self, telemetry_client: Optional[TelemetryClient]) -> None:
        self.calculate_speed_panel.telemetry = telemetry_client

    def register_callback(self, value_changed_callback: Callable[[str], None]) -> None:
        self._value_changed_callback = value_changed_callback

    def set_values(self, servo_definition: ServoDefinition, servo_path: str):
        print(f"Setting values for path {servo_path}: {servo_definition}")
        self.servo_definition = servo_definition
        self.servo_path = servo_path
        self.calculate_speed_panel.servo = self.servo_definition

    def redefine_rect(self, rect):
        super(ValuesPanel, self).redefine_rect(rect)

    def undo(self, *_args):
        if self.servo_definition is not None:
            for prop in LegServos.SERVO_DEFINITION_FIELDS:
                getattr(self.servo_definition, prop).undo()

    def stop_servo(self, *_args):
        if self.servo_definition is not None:
            self.servo_definition.stop()

    def update_servo(self, *_args):
        if self._value_changed_callback is not None:
            self._value_changed_callback("middle")

    def reverse_values(self, *_args):
        def swap(a: str, b: str):
            storage_value_a: StorageValue = getattr(self.servo_definition, a)
            storage_value_b: StorageValue = getattr(self.servo_definition, b)
            v = storage_value_a.value
            storage_value_a.value = storage_value_b.value
            storage_value_b.value = v

        if self.servo_definition is not None:
            swap("minus_90", "plus_90")
            swap("minus_45", "plus_45")
            swap("limit_lower", "limit_upper")

    def show_calculate_speed(self, *_args):
        self.screen.set_modal(self.calculate_speed_panel)
        self.calculate_speed_panel.start_calibration()

    def _change_offset(self, difference: int):
        def update_position(path: str):
            self._getters_and_setters[path][1](self._getters_and_setters[path][0]() + difference)

        update_position("plus_90")
        update_position("plus_45")
        update_position("minus_45")
        update_position("minus_90")

    def create_getter_and_setter(self, path: str, special_all_offset: bool):
        def read_value(name: str):
            return getattr(self.servo_definition, name).value if self.servo_definition is not None else None

        def write_value(name, value: Any):
            if self.servo_definition is not None:
                difference = 0
                if special_all_offset:
                    difference = value - getattr(self.servo_definition, name).value
                getattr(self.servo_definition, name).value = value
                if self._value_changed_callback is not None:
                    self._value_changed_callback(name)
                if special_all_offset:
                    self._change_offset(difference)

        getter_and_setter = partial(read_value, path), partial(write_value, path)
        self._getters_and_setters[path] = getter_and_setter
        return getter_and_setter

    def create_storage_reader(self, path: str):
        def read_storage_value(name: str):
            return getattr(self.servo_definition, name) if self.servo_definition is not None else None
        return partial(read_storage_value, path)
