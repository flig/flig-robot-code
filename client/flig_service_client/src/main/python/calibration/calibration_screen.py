import pyros

from pygame import Rect

from calibration.calculate_speed_panel import CalculateSpeedPanel
from pyros_support_ui.text_toggle_button import TextToggleButton
from servo_lib.leg_servos import LegServos, ExtraServos
from calibration.calibration_panel import CalibrationPanel
from pyros_support_ui import PyrosClientApp
from pyros_support_ui.components import Collection, LeftRightLayout, TopDownLayout, BaseUIFactory, Component, UiHint
from pyros_support_ui.drop_down import DropDown

from pyros_support_ui.graph_component import GraphController, Graph

from telemetry_utils.telemetry_controller import TelemetryController
from ui.screen import Screen
from calibration.servo_values_panel import ValuesPanel


class CalibrationScreen(Screen):

    CURRENT_SENSOR_PANEL_WIDTH = 400
    VALUES_PANEL_WIDTH = 250

    def __init__(self, main_app: PyrosClientApp, ui_factory: BaseUIFactory,
                 calibrations: LegServos,
                 extra_servos: ExtraServos,
                 telemetry_controller: TelemetryController) -> None:
        super().__init__(main_app)
        self.calibrations = calibrations
        self.extra_servos = extra_servos
        self.telemetry_controller = telemetry_controller
        self.graph_controller = GraphController()
        self._graph_data = {}

        self.commands_panel = ui_factory.panel(None, layout=LeftRightLayout(margin=10), hint=UiHint.NO_DECORATION)

        self.calculate_speed_panel = CalculateSpeedPanel(ui_factory, self, self.graph_controller, self._graph_data)

        self.values_panel = ValuesPanel(Rect(0, 0, 300, 800), ui_factory, self, self.calculate_speed_panel)
        self.calibration_panel = CalibrationPanel(ui_factory, self.values_panel, self.calibrations, self.extra_servos)

        self.add_component(self.calibration_panel)
        self.add_component(self.values_panel)

        self.graphs = [Graph(None,
                             ui_factory,
                             controller=self.graph_controller)
                       for _ in range(6)]

        self.sensors_graphs_panel = Collection(None, layout=TopDownLayout(margin=10))
        for graph in self.graphs:
            self.sensors_graphs_panel.add_component(graph)
        self.add_component(self.sensors_graphs_panel)

        def add_drop_down(drop_down: DropDown):
            self.commands_panel.add_component(drop_down)
            self.add_component(drop_down.menu)

        add_drop_down(
            DropDown(
                Rect(0, 0, 140, 20),
                ui_factory,
                select_method=self.calibration_panel.select_leg,
                values=[
                    ('fr', "Front Right"), ('fl', "Front Left"), ('br', "Back Right"), ('bl', "Back Left"),
                    ('s0', "Servo 0"), ('s1', "Servo 1"), ('s2', "Servo 2"), ('s3', "Servo 3")
                ],
                default_value="fr"
            )
        )
        add_drop_down(
            DropDown(
                Rect(0, 0, 100, 20),
                ui_factory,
                select_method=self.calibration_panel.select_servo,
                values=[('shoulder', "Shoulder"), ('thigh', "Thigh"), ('shin', "Shin")],
                default_value="thigh"
            )
        )
        add_drop_down(
            DropDown(
                Rect(0, 0, 120, 20),
                ui_factory,
                select_method=self.calibration_panel.select_servo_position,
                values=[
                    ('minus_90', "-90º"),
                    ('minus_45', "-45º"),
                    ('middle', "Middle 0º"),
                    ('plus_45', "+45º"),
                    ('plus_90', "+90º"),
                    ('custom', 'Custom'),
                    ('off', 'Off')
                ],
                default_value="middle"
            )
        )

        self.undo_button = ui_factory.text_button(Rect(0, 0, 110, 20), "Undo All", on_click=self.undo_changes)
        self.save_all_button = ui_factory.text_button(Rect(0, 0, 110, 20), "Save All", on_click=self.save_all)

        self.commands_panel.add_component(Component(Rect(0, 0, 10, 20)))
        self.commands_panel.add_component(self.undo_button)
        self.commands_panel.add_component(self.save_all_button)

        self.servos_buttons = TextToggleButton(Rect(0, 0, 160, 20),
                                               ui_factory,
                                               on_text="Start Servos", on_click_on=self.start_servos,
                                               off_text="Stop Servos", on_click_off=self.stop_servos)
        self.commands_panel.add_component(self.servos_buttons)

    def on_show(self) -> None:
        self.telemetry_controller.set_on_connection_callback(self._on_telemetry_connection)

    def on_hide(self) -> None:
        self.telemetry_controller.clear_on_connection_callback()

    def _on_telemetry_connection(self, connected: bool) -> None:
        if connected:
            self._setup_telemetry_client(self.telemetry_controller)

    def undo_changes(self, *_args) -> None:
        self.calibrations.undo_change()
        self.extra_servos.undo_change()

    def save_all(self, *_args) -> None:
        self.calibrations.save_calibration_details()
        self.extra_servos.save_calibration_details()

    def start_servos(self, *_args) -> None:
        self.calibration_panel.servos_stopped = False
        self.servos_buttons.toggle_off()

    def stop_servos(self, *_args) -> None:
        self.calibration_panel.servos_stopped = True
        self.servos_buttons.toggle_on()

    def set_contributing_panel(self, contributing_command_panel: Collection) -> None:
        contributing_command_panel.add_component(self.commands_panel)

    def _setup_telemetry_client(self, telemetry_controller: TelemetryController):
        self.values_panel.telemetry_client = telemetry_controller.telemetry_client

        for i in range(len(self.graphs)):
            self.graphs[i].graph_data = telemetry_controller.graph_data[f"current_{i}"]

        self.calculate_speed_panel.servo = self.calibration_panel.selected_servo_calibration

    def key_down(self, key) -> None:
        if self.main_app.key_down(key):
            pass

    def key_up(self, key) -> None:
        if self.main_app.key_up(key):
            pass

    def draw(self, surface) -> None:
        super().draw(surface)
        if self.save_all_button.enabled != (self.calibrations.modified or self.extra_servos.modified):
            self.save_all_button.enabled = (self.calibrations.modified or self.extra_servos.modified)
            self.undo_button.enabled = (self.calibrations.modified or self.extra_servos.modified)

    def redefine_rect(self, rect) -> None:
        self.rect = rect
        self.calibration_panel.redefine_rect(
            Rect(rect.x, rect.y, rect.width - CalibrationScreen.CURRENT_SENSOR_PANEL_WIDTH - CalibrationScreen.VALUES_PANEL_WIDTH - 5 * 2, rect.height))
        self.values_panel.redefine_rect(
            Rect(self.calibration_panel.rect.right + 5, rect.y, CalibrationScreen.VALUES_PANEL_WIDTH, rect.height))
        self.sensors_graphs_panel.redefine_rect(
            Rect(self.values_panel.rect.right + 5, rect.y, CalibrationScreen.CURRENT_SENSOR_PANEL_WIDTH, rect.height))

    @staticmethod
    def calibrate(*_args) -> None:
        pyros.publish("balancing/calibrate", "all")
