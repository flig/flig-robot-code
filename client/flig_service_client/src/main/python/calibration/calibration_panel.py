import threading

import time
from typing import Optional

import pyros
from math import sqrt, sin, cos, atan, radians

import pygame

from pygame import Rect

from servo_lib.leg_servos import LegServos, ServoDefinition, ExtraServos
from pyros_support_ui.components import Collection, BaseUIFactory, ALIGNMENT, CardsCollection
from pyros_support_ui.number_input import NumberInputComponent
from pyros_support_ui.pygamehelper import load_image
from calibration.servo_values_panel import ValuesPanel


class CalibrationPanel(Collection):
    IMAGE_SCALE = 3
    SHIN_LEN = 480 // IMAGE_SCALE

    SIDE_LEG_CENTRE = (90 // IMAGE_SCALE, 128 // IMAGE_SCALE)
    THIGH_CENTRE = (83 // IMAGE_SCALE, 112 // IMAGE_SCALE)
    THIGH_SHIN_CENTRE = (72 // IMAGE_SCALE, 585 // IMAGE_SCALE)
    SHIN_CENTRE = (132 // IMAGE_SCALE, 72 // IMAGE_SCALE)

    IMAGE_POSITION = (340, 300)

    def __init__(self,
                 ui_factory: BaseUIFactory,
                 values_panel: ValuesPanel,
                 calibrations: LegServos,
                 extra_servos: ExtraServos):
        super().__init__(None)

        self.values_panel = values_panel
        self.calibrations = calibrations
        self.extra_servos = extra_servos

        self.values_panel.register_callback(self.values_property_changed)

        self.selected_leg: Optional[str] = None
        self.selected_leg_calibration: Optional[dict] = None
        self.selected_servo: Optional[str] = None
        self.selected_servo_calibration: Optional[ServoDefinition] = None
        self.selected_servo_position = None

        self.thigh_image: Optional[pygame.Surface] = None
        self.shin_image: Optional[pygame.Surface] = None
        self.side_leg_image: Optional[pygame.Surface] = None
        self.thigh_image_rotated: Optional[pygame.Surface] = None
        self.shin_image_rotated: Optional[pygame.Surface] = None
        self.side_leg_image_rotated: Optional[pygame.Surface] = None

        self.resources_loaded = False

        def load_images() -> None:
            self.thigh_image = load_image("thigh.png")
            self.shin_image = load_image("shin.png")
            self.side_leg_image = load_image("side-leg.png")
            scale = CalibrationPanel.IMAGE_SCALE

            self.thigh_image = pygame.transform.scale(self.thigh_image, (self.thigh_image.get_width() // scale, self.thigh_image.get_height() // scale))
            self.thigh_image_rotated = self.thigh_image
            self.shin_image = pygame.transform.scale(self.shin_image, (self.shin_image.get_width() // scale, self.shin_image.get_height() // scale))
            self.shin_image_rotated = self.shin_image
            self.side_leg_image = pygame.transform.scale(self.side_leg_image, (self.side_leg_image.get_width() // scale, self.side_leg_image.get_height() // scale))
            self.side_leg_image_rotated = self.side_leg_image
            self.resources_loaded = True
            self.update_rotated_images()

        thread = threading.Thread(target=load_images, daemon=True)
        thread.start()

        self.x_image_offset = 0
        self.y_image_offset = 0
        self.x_image_offset_2 = 0
        self.y_image_offset_2 = 0
        self.x_image_offset_3 = 0
        self.y_image_offset_3 = 0

        self._angle: Optional[float] = None
        self._position = 0
        self._servos_stopped = False

        self.position_label = ui_factory.label(Rect(0, 0, 250, 30), "Position 0", h_alignment=ALIGNMENT.CENTER)
        self.add_component(self.position_label)

        self.angle_label = ui_factory.label(Rect(0, 0, 250, 30), "Angle º", h_alignment=ALIGNMENT.CENTER)
        self.angle_card = CardsCollection(Rect(0, 0, 250, 30))
        self.angle_value = ui_factory.label(Rect(0, 0, 250, 30), str(self._angle), h_alignment=ALIGNMENT.CENTER)
        self.angle_input = NumberInputComponent(
                    Rect(0, 0, 250, 30),
                    ui_factory,
                    (lambda: self._angle, self.set_angle),
                    "",
                    minimal_value=-180, maximum_value=180,
                    top_scale=2, bottom_scale=0, button_font=ui_factory.small_font)
        self.angle_input.layout.h_alignment = ALIGNMENT.CENTER
        self.add_component(self.angle_label)
        self.angle_card.add_card("display", self.angle_value)
        self.angle_card.add_card("edit", self.angle_input)
        self.angle_card.select_card("display")
        self.add_component(self.angle_card)

    @property
    def servos_stopped(self) -> bool: return self._servos_stopped

    @servos_stopped.setter
    def servos_stopped(self, servos_stopped):
        self._servos_stopped = servos_stopped
        if servos_stopped:
            for leg in LegServos.LEGS:
                for leg_servo in LegServos.LEG_SERVOS:
                    self.calibrations.servos[leg][leg_servo].stop()
            for group in ExtraServos.SERVO_GROUPS:
                for servo in ExtraServos.SERVOS:
                    self.calibrations.servos[group][servo].stop()
        else:
            self.switch_servo()

    def set_angle(self, angle: Optional[float], update_rotated_images: bool = True):
        if self._angle != angle:
            self._angle = angle
            if update_rotated_images:
                self.update_rotated_images()
        if angle is not None:
            self.angle_value.text = str(self._angle)
        else:
            self.angle_value.text = "off"
        self._send_servo_value()

    def _send_servo_value(self):
        self.switch_servo()

    def values_property_changed(self, _name: str) -> None:
        self._send_servo_value()

    def set_position(self, position):
        self.position_label.text = f"Position {position}"

    def redefine_rect(self, rect):
        self.rect = rect
        self.position_label.redefine_rect(Rect(rect.x, rect.y,
                                          self.position_label.rect.width,
                                          self.position_label.rect.height))
        self.angle_label.redefine_rect(Rect(rect.x + (rect.width - self.angle_label.rect.width) / 2,
                                            rect.y,
                                            self.angle_label.rect.width,
                                            self.angle_label.rect.height))
        self.angle_card.redefine_rect(Rect(rect.x + (rect.width - self.angle_card.rect.width) / 2,
                                           self.angle_label.rect.bottom + 5,
                                           self.angle_card.rect.width,
                                           self.angle_card.rect.height))

    def draw(self, surface):
        x_offset = CalibrationPanel.IMAGE_POSITION[0]
        y_offset = CalibrationPanel.IMAGE_POSITION[1]  # + self.commands_panel.rect.bottom + 5
        if self.selected_servo == 'shoulder':
            if self.resources_loaded:
                surface.blit(self.side_leg_image_rotated, (
                    self.rect.x + x_offset + self.x_image_offset,
                    self.rect.y + y_offset + self.y_image_offset
                ))
        elif self.selected_servo == 'thigh':
            if self.resources_loaded:
                surface.blit(self.thigh_image_rotated, (
                    self.rect.x + x_offset + self.x_image_offset,
                    self.rect.y + y_offset + self.y_image_offset
                ))

                surface.blit(self.shin_image_rotated, (
                    self.rect.x + x_offset + self.x_image_offset_2,
                    self.rect.y + y_offset + self.y_image_offset_2
                ))

            pygame.draw.circle(surface, (255, 255, 255), (
                self.rect.x + x_offset + self.x_image_offset_3,
                self.rect.y + y_offset + self.y_image_offset_3
            ), 4)
        elif self.selected_servo == 'shin':
            if self.resources_loaded:
                surface.blit(self.thigh_image_rotated, (
                    self.rect.x + x_offset - CalibrationPanel.THIGH_CENTRE[0],
                    self.rect.y + y_offset - CalibrationPanel.THIGH_CENTRE[1]
                ))
                surface.blit(self.shin_image_rotated, (
                    self.rect.x + x_offset + CalibrationPanel.THIGH_SHIN_CENTRE[0] - CalibrationPanel.THIGH_CENTRE[0] + self.x_image_offset,
                    self.rect.y + y_offset + CalibrationPanel.THIGH_SHIN_CENTRE[1] - CalibrationPanel.THIGH_CENTRE[1] + self.y_image_offset
                ))
            pygame.draw.circle(surface, (255, 255, 255), (
                self.rect.x + x_offset + CalibrationPanel.THIGH_SHIN_CENTRE[0] - CalibrationPanel.THIGH_CENTRE[0],
                self.rect.y + y_offset + CalibrationPanel.THIGH_SHIN_CENTRE[1] - CalibrationPanel.THIGH_CENTRE[1]
            ), 4)

        pygame.draw.circle(surface, (190, 190, 190), (self.rect.x + x_offset, self.rect.y + y_offset), 10)

        super().draw(surface)

    def select_leg(self, leg_id: str) -> None:
        if leg_id.startswith("s"):
            self.selected_leg = None
            self.selected_servo = leg_id[1:]
            self.selected_leg_calibration = self.extra_servos.servos["extra"]
            self.update_rotated_images()
        else:
            if self.selected_leg is None:
                self.selected_servo = None
            print(f"Selected leg with id {leg_id}")
            self.selected_leg = leg_id
            self.selected_leg_calibration = self.calibrations.servos[leg_id]
            self.switch_servo()
            self.update_rotated_images()

    def select_servo(self, servo_id) -> None:
        if self.selected_servo != servo_id:
            self.selected_servo = servo_id
            self.select_servo_position('off')
            self.update_rotated_images()

    def select_servo_position(self, position_id):
        if position_id == 'off':
            position_id = None
        if position_id == 'custom':
            self.angle_card.select_card('edit')
        elif position_id is None:
            self.angle_card.select_card('display')
        else:
            self.angle_card.select_card('display')

        if self.selected_servo_position != position_id:
            self.selected_servo_position = position_id
            self.switch_servo()
            self.update_rotated_images()

    def switch_servo(self):
        pyros.publish("servo/control", "stop")
        if self.selected_servo_calibration is not None:
            self.selected_servo_calibration.stop()
        time.sleep(0.2)
        pyros.publish("servo/control", "start")
        if self.selected_leg_calibration is not None and self.selected_servo is not None:
            self.selected_servo_calibration = self.selected_leg_calibration[self.selected_servo]
            if not self._servos_stopped:
                servo_position = self.selected_servo_calibration.calculate_position(self._angle)
                print(f"Driving servo {self.selected_servo_calibration.servo_channel} for angle {self._angle} to position {servo_position}")
                self.selected_servo_calibration.send_value(servo_position)

    def get_selected_angle(self):
        if self.selected_servo_position is None:
            angle = None
        elif self.selected_servo_position == 'minus_45':
            angle = -45
        elif self.selected_servo_position == 'minus_90':
            angle = -90
        elif self.selected_servo_position == 'plus_90':
            angle = 90
        elif self.selected_servo_position == 'plus_45':
            angle = 45
        elif self.selected_servo_position == 'middle':
            angle = 0
        else:
            angle = self._angle
        return angle

    def update_rotated_images(self):
        def calculate_offset(image, coordinates):
            if angle < 0:
                ix = sin(radians(angle)) * image.get_height()
                iy = 0
            elif angle > 0:
                ix = 0
                iy = -sin(radians(angle)) * image.get_width()
            else:
                ix = 0
                iy = 0

            theta = -atan(coordinates[1] / coordinates[0])
            z = sqrt(coordinates[0] * coordinates[0] + coordinates[1] * coordinates[1])

            return (
                -cos(theta + radians(angle)) * z + ix,
                sin(theta + radians(angle)) * z + iy
            )

        if self.selected_leg is not None and self.selected_servo is not None:
            print(f"-> going to set values for path {self.selected_leg}/{self.selected_servo}")
            selected_servo_values = self.calibrations.servos[self.selected_leg][self.selected_servo]
            self.values_panel.set_values(
                selected_servo_values,
                f"{self.selected_leg}/{self.selected_servo}"
            )
        elif self.selected_leg is None and self.selected_servo is not None:
            print(f"-> going to set values for path extra/{self.selected_servo}")
            selected_servo_values = self.extra_servos.servos["extra"][self.selected_servo]
            self.values_panel.set_values(
                selected_servo_values,
                f"extra/{self.selected_servo}"
            )

        angle = self.get_selected_angle()
        if angle != self._angle:
            self.set_angle(angle, update_rotated_images=False)

        if angle is None:
            angle = 0

        if angle > 90:
            angle = 90
        elif angle < -90:
            angle = -90

        if self.resources_loaded:
            if self.selected_servo == 'shoulder':
                self.side_leg_image_rotated = pygame.transform.rotate(self.side_leg_image, angle)

                self.x_image_offset, self.y_image_offset = calculate_offset(self.side_leg_image, CalibrationPanel.SIDE_LEG_CENTRE)

            elif self.selected_servo == 'thigh':
                self.thigh_image_rotated = pygame.transform.rotate(self.thigh_image, angle)
                self.shin_image_rotated = pygame.transform.rotate(self.shin_image, angle)

                self.x_image_offset, self.y_image_offset = calculate_offset(self.thigh_image, CalibrationPanel.THIGH_CENTRE)
                self.x_image_offset_2, self.y_image_offset_2 = calculate_offset(self.shin_image, CalibrationPanel.SHIN_CENTRE)

                coords = [CalibrationPanel.THIGH_SHIN_CENTRE[0] - CalibrationPanel.THIGH_CENTRE[0], CalibrationPanel.THIGH_SHIN_CENTRE[1] - CalibrationPanel.THIGH_CENTRE[1]]
                z_2 = sqrt(coords[0] * coords[0] + coords[1] * coords[1])

                new_x_2 = sin(radians(angle)) * z_2
                new_y_2 = cos(radians(angle)) * z_2

                self.x_image_offset_3 = new_x_2
                self.y_image_offset_3 = new_y_2

                self.x_image_offset_2 += new_x_2
                self.y_image_offset_2 += new_y_2

            elif self.selected_servo == 'shin':
                self.thigh_image_rotated = self.thigh_image

                self.shin_image_rotated = pygame.transform.rotate(self.shin_image, angle)
                self.x_image_offset, self.y_image_offset = calculate_offset(self.shin_image, CalibrationPanel.SHIN_CENTRE)
