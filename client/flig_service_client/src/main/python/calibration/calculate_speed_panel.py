import threading
import time
import traceback
from typing import Optional

from pygame import Rect
from telemetry import TelemetryClient

from pyros_support_ui.components import Collection, TopDownLayout, LeftRightLayout, ALIGNMENT, BaseUIFactory, Panel, UiHint, Component
from pyros_support_ui.graph_component import Graph, GraphController, AutoScalingData, DataType, GraphData
from pyros_support_ui.text_toggle_button import TextToggleButton
from servo_lib.leg_servos import ServoDefinition
from ui.screen import Screen


class ChangeGraphDataWarpper(AutoScalingData):
    def __init__(self, graph_data: GraphData):
        super().__init__(100, -100)
        self.graph_data = graph_data
        self._last_from_timestamp = 0.0
        self._last_to_timestamp = 0.0
        self._last_data = []

    def fetch(self, from_timestamp: float, to_timestamp: float) -> DataType:
        if from_timestamp == self._last_from_timestamp and to_timestamp == self._last_to_timestamp:
            return self._last_data

        self._last_from_timestamp = from_timestamp
        self._last_to_timestamp = to_timestamp
        data = self.graph_data.fetch(from_timestamp, to_timestamp)
        prev_value = data[0][1]
        for i in range(len(data)):
            if i == 0:
                data[i] = (data[i][0], 0)
            else:
                value = data[i][1]
                data[i] = (data[i][0], data[i][1] - prev_value)
                prev_value = value

        self._last_data = data

        return data


class CalculateSpeedPanel(Panel):
    CALC_SPEED_PANEL_WIDTH = 500
    CALC_SPEED_PANEL_HEIGHT = 395

    def __init__(self,
                 ui_factory: BaseUIFactory,
                 screen: Screen,
                 graph_controller: GraphController,
                 graph_data: dict) -> None:
        super().__init__(None,
                         background_colour=ui_factory.background_colour,
                         layout=TopDownLayout(margin=5))
        template_panel = ui_factory.panel(None)
        self.decoration = template_panel.decoration
        self.horizontal_decoration_margin = template_panel.horizontal_decoration_margin
        self.vertical_decoration_margin = template_panel.vertical_decoration_margin
        self.screen = screen
        self.graph_controller = graph_controller
        self.graph_data = graph_data
        self._telemetry_client: Optional[TelemetryClient] = None
        self._servo: Optional[ServoDefinition] = None
        self._start_calibration = False
        self._stop_calibration = False
        self._running_calibration = False
        self._data = None
        self._stream = None

        self.servo_current_graph = Graph(
            Rect(0, 0, 0, 140),
            ui_factory,
            controller=self.graph_controller)

        self.servo_current_change_graph = Graph(
            Rect(0, 0, 0, 140),
            ui_factory,
            controller=self.graph_controller)

        self.add_component(self.servo_current_graph)
        self.add_component(self.servo_current_change_graph)

        self.add_component(Component(Rect(0, 0, 0, 5)))
        last_panel = Collection(Rect(0, 0, 0, 80), layout=LeftRightLayout(margin=5))
        self.add_component(last_panel)

        results_panel = ui_factory.panel(Rect(0, 0, 90, 85), layout=TopDownLayout())
        results_panel.horizontal_decoration_margin += 15
        last_panel.add_component(results_panel)
        self.results_label = [ui_factory.label(Rect(0, 0, 200, 20), "-", h_alignment=ALIGNMENT.RIGHT),
                              ui_factory.label(Rect(0, 0, 200, 20), "-", h_alignment=ALIGNMENT.RIGHT),
                              ui_factory.label(Rect(0, 0, 200, 20), "-", h_alignment=ALIGNMENT.RIGHT)]

        results_panel.add_component(self.results_label[0])
        results_panel.add_component(self.results_label[1])
        results_panel.add_component(self.results_label[2])

        remaining_panel = Collection(Rect(0, 0, CalculateSpeedPanel.CALC_SPEED_PANEL_WIDTH - results_panel.rect.width - 15, 80), layout=TopDownLayout(margin=5))
        last_panel.add_component(remaining_panel)

        self.feedback_label = ui_factory.label(Rect(0, 0, 0, 20), "Not working")
        remaining_panel.add_component(self.feedback_label)

        remaining_panel.add_component(Component(Rect(0, 0, 0, 5)))

        bottom_panel = Collection(Rect(0, 0, 0, 55), layout=LeftRightLayout())
        remaining_panel.add_component(bottom_panel)

        self.start_stop_button = TextToggleButton(
            Rect(0, 0, 170, 25),
            ui_factory,
            on_text="Start", on_click_on=self.start_calibration,
            off_text="Stop", on_click_off=self.stop_calibration)

        # self.stop_button = ui_factory.text_button(
        #     Rect(0, 0, 170, 25), "Stop", on_click=self.stop_calibration, hint=UiHint.LIGHT)
        self.finish_button = ui_factory.text_button(
            Rect(0, 0, 170, 25), "Close", on_click=self.finish_button_click, hint=UiHint.LIGHT)
        self.iteration_label = ui_factory.label(
            Rect(0, 0, CalculateSpeedPanel.CALC_SPEED_PANEL_WIDTH - results_panel.rect.width - self.finish_button.rect.width - 15, 20),
            "i = -",
            h_alignment=ALIGNMENT.CENTER,
            v_alignment=ALIGNMENT.MIDDLE,
        )
        bottom_panel.add_component(self.iteration_label)
        buttons_panel = Collection(Rect(0, 0, 170, 55), layout=TopDownLayout(margin=5))
        buttons_panel.add_component(self.start_stop_button)
        buttons_panel.add_component(self.finish_button)
        bottom_panel.add_component(buttons_panel)

        self._calculator_thread = threading.Thread(target=self._running_calculations, daemon=True)
        self._calculator_thread.start()

    def redefine_rect(self, rect) -> None:
        super().redefine_rect(Rect((rect.width - CalculateSpeedPanel.CALC_SPEED_PANEL_WIDTH) / 2, 200,
                                   CalculateSpeedPanel.CALC_SPEED_PANEL_WIDTH, CalculateSpeedPanel.CALC_SPEED_PANEL_HEIGHT))

    def draw(self, surface) -> None:
        super().draw(surface)
        if self.is_running_calibration():
            if self.start_stop_button.is_toggle_on():
                self.start_stop_button.toggle_off()
        else:
            if not self.start_stop_button.is_toggle_on():
                self.start_stop_button.toggle_on()

    @property
    def telemetry(self) -> TelemetryClient:
        return self._telemetry_client

    @telemetry.setter
    def telemetry(self, telemetry_client: TelemetryClient) -> None:
        def retrieve_stream(stream) -> None:
            self._stream = stream
        self._telemetry_client = telemetry_client
        telemetry_client.get_stream_definition('servo_current_stream', retrieve_stream)

    @property
    def servo(self) -> Optional[ServoDefinition]:
        return self._servo

    @servo.setter
    def servo(self, servo: Optional[ServoDefinition]) -> None:
        self._servo = servo

    def finish_button_click(self, *_args) -> None:
        self.stop_calibration()
        self.screen.clear_modal()

    def start_calibration(self, *_args) -> None:
        if self.servo is not None:
            self._start_calibration = True
            current_graph_name = f"current_{self.servo.current_channel.value}"
            if current_graph_name in self.graph_data:

                graph_data = self.graph_data[current_graph_name]
                change_graph_data = ChangeGraphDataWarpper(graph_data)

                self.servo_current_graph.graph_data = graph_data
                self.servo_current_change_graph.graph_data = change_graph_data

                self.feedback(f"Using current channel {self.servo.current_channel.value}")

    def stop_calibration(self, *_args) -> None:
        self._stop_calibration = True

    def is_running_calibration(self) -> bool:
        return self._running_calibration

    def feedback(self, text: str) -> None:
        self.feedback_label.text = text

    def _running_calculations(self) -> None:

        def get_data(timeout=1) -> None:
            self._data = None

            def fetch_data(records):
                self._data = records

            now = time.time()
            self._telemetry_client.retrieve(self._stream, now - 3, now, fetch_data)

            while not self._stop_calibration and self._data is None and now + timeout > time.time():
                time.sleep(0.01)

            if self._data is None:
                self.feedback("Cannot fetch data")
                self._stop_calibration = True

        clear_exception = True
        while True:
            self._stop_calibration = False
            self._running_calibration = False
            try:

                while not self._start_calibration:
                    time.sleep(1)
                self._start_calibration = False
                self._stop_calibration = False

                for label in self.results_label:
                    label.text = "-"
                self.feedback("")

                if self._servo is not None and self._telemetry_client is not None:
                    self._running_calibration = True

                    position_angle = -45

                    self._servo.send_value(self._servo.calculate_position(position_angle))
                    time.sleep(2)
                    servo_speeds = []
                    iteration = 0
                    while not self._stop_calibration and len(servo_speeds) < 3 and iteration < 6:
                        iteration += 1
                        position_angle = -position_angle
                        self.iteration_label.text = f"i = {iteration}, a={position_angle}º"
                        self.servo.send_value(self.servo.calculate_position(position_angle))
                        time.sleep(2)

                        get_data()

                        if not self._stop_calibration and self._data is not None:
                            servo_speed = self.retrieve_speed(self._data)
                            if servo_speed is not None:
                                servo_speeds.append(servo_speed)
                                if len(servo_speeds) < 4:
                                    self.results_label[len(servo_speeds) - 1].text = f"{servo_speed:.1f}"
                        elif not self._stop_calibration:
                            self.feedback("Timeout on data")
                        else:
                            self.feedback("Terminated")

                    if not self._stop_calibration and len(servo_speeds) >= 3:
                        average_speed = sum(servo_speeds) / len(servo_speeds)
                        self.servo.speed.value = int(average_speed)
                        self.feedback(f"Average speed {self.servo.speed.value}")
                    self._stop_calibration = False
                else:
                    self.feedback("No servo set")

                clear_exception = True
            except Exception as e:
                if clear_exception:
                    print(f"ERROR: {e}\n{''.join(traceback.format_tb(e.__traceback__))}")
                    clear_exception = False
                time.sleep(1)

    def retrieve_speed(self, records) -> Optional[float]:
        records = [r[self.servo.current_channel.value + 1] for r in records]

        min_value = min(records)
        max_value = max(records)
        range = max_value - min_value

        if max_value - min_value < 100:
            self._stop_calibration = True
            self.feedback(f"Cur. too small: min {min_value:.0f} / max {max_value:.0f}, l={len(records)}")
            return None

        last_pointer = -1
        first_pointer = -1

        p = len(records) - 1
        while p > 0 and last_pointer == -1:
            if (records[p] - min_value) / range > 0.1:
                last_pointer = p
            p -= 1

        if p > 0:
            while p > 0 and first_pointer == -1:
                if (records[p] - min_value) / range <= 0.1:
                    first_pointer = p
                p -= 1

        while 0 < first_pointer < len(records) and (records[first_pointer] - min_value / range) < 0.5:
            first_pointer += 1

        while 0 < last_pointer < len(records) and (records[last_pointer] - min_value / range) < 0.5:
            last_pointer -= 1

        if last_pointer > 0 and first_pointer > 0:
            return (last_pointer - first_pointer) * 0.02 * 4 * 1000.0

        self.feedback(f"Failed f={first_pointer},l={last_pointer}")
        return None
