import pygame
from pygame import Rect
from pygame.font import Font
from pyros_support_ui.components import Component


BLACK = pygame.color.THECOLORS['black']


class LoadingScreen(Component):

    def __init__(self, rect: Rect, font: Font) -> None:
        super().__init__(rect)
        self.font = font
        self.colour = (128, 255, 128)

    def draw(self, surface) -> None:
        surface.fill(BLACK)
        surface.blit(self.font.render("Loading...", True, self.colour), (500, 380))
