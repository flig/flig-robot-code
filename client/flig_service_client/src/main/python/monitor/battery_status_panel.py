import time
from typing import Optional

import pygame
import pyros
from PIL import Image

from monitor.battery_status import BatteryStatus
from pyros_support_ui.components import Collection, TopDownLayout


class BatteryStatusPanel(Collection):
    def __init__(self, rect):
        super().__init__(rect, layout=TopDownLayout(margin=10))

        self.battery_status = BatteryStatus()

        pyros.subscribe("shutdown/announce", self.battery_status.shutdown_announce_handler)
        pyros.subscribe("power/state", self.battery_status.power_state_handler)

        # self.surface = self.pil_image_to_surface(self.battery_status.image)

        self.battery_image_surface: Optional[pygame.Surface] = None
        self.current_image_surface: Optional[pygame.Surface] = None
        self.last_time = 0

    @staticmethod
    def pil_image_to_surface(pil_image: Image):
        return pygame.image.fromstring(
            pil_image.tobytes(), pil_image.size, pil_image.mode).convert()

    def draw(self, surface) -> None:
        now = time.time()
        if self.battery_status.resources_loaded and self.last_time + 0.1 <= now:
            self.battery_image_surface = self.pil_image_to_surface(self.battery_status.draw_battery())
            self.current_image_surface = self.pil_image_to_surface(self.battery_status.draw_current())
            self.last_time = now
        if self.battery_image_surface is not None:
            surface.blit(self.battery_image_surface, (self.rect.x + ((self.rect.width - 240) // 2), self.rect.y))
        if self.battery_image_surface is not None:
            surface.blit(self.current_image_surface, (self.rect.x + ((self.rect.width - 240) // 2), self.rect.y + 250))
        # surface.blit(image_surface, (self.rect.x, self.rect.y))
