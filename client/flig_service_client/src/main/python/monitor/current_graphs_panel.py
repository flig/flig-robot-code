from typing import cast

from telemetry import TelemetryClient, CachingSocketTelemetryClient

from pyros_support_ui.components import Collection, BaseUIFactory, TopDownLayout
from pyros_support_ui.graph_component import GraphController, Graph, TelemetryGraphData
from telemetry_utils.telemetry_controller import TelemetryController


class GraphsPanel(Collection):
    def __init__(self, rect, ui_factory: BaseUIFactory):
        super().__init__(rect, layout=TopDownLayout(margin=10))

        self.graph_controller = GraphController()
        self._graph_data = {}

        self.graphs = [Graph(None,
                             ui_factory,
                             controller=self.graph_controller)
                       for _ in range(8)]

        for graph in self.graphs:
            self.add_component(graph)

    def setup_telemetry_client(self, telemetry_controller: TelemetryController):
        for i in range(len(self.graphs)):
            self.graphs[i].graph_data = telemetry_controller.graph_data[f"current_{i}"]
