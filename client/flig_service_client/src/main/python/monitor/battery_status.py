from pygame.font import Font
from typing import Optional, Tuple, List

import threading

from io import BytesIO

from PIL import ImageFont, ImageDraw, Image
from local_resources import Resource


class BatteryStatus:
    OUTER_RO = 120
    OUTER_RI = 90
    INNER_R = 70
    MIDDLE = 120
    SHUTDOWN_DISPLAY_TIMER = 10

    def __init__(self):
        self.image = Image.new('RGB', (240, 240))
        self.draw = ImageDraw.Draw(self.image)
        self.resources_loaded = False
        self.font_small: Optional[Font] = None
        self.font_medium: Optional[Font] = None
        self.font_big: Optional[Font] = None
        self.off_size: Optional[Tuple[float, float]] = None
        self.power_circle_image: Optional[Image] = None
        self.plug_image: Optional[Image] = None
        self.battery_image: Optional[Image] = None
        self.battey_circle: List[Image] = []

        def load_resources():
            print("            battery status loading fonts...")
            with Resource("Exo-Regular.otf") as f:
                self.font_small = ImageFont.truetype(BytesIO(f.read()), 14, encoding="unic")
            with Resource("Exo-Regular.otf") as f:
                self.font_medium = ImageFont.truetype(BytesIO(f.read()), 26, encoding="unic")
            with Resource("Exo-DemiBold.otf") as f:
                self.font_big = ImageFont.truetype(BytesIO(f.read()), 64, encoding="unic")
            print("            battery status loading fonts...")

            self.off_size = self.draw.textsize("OFF", font=self.font_big)

            print("            battery status loading images...")
            with Resource("power-circle.png") as f:
                self.power_circle_image = Image.open(BytesIO(f.read()))
            with Resource("plug.png") as f:
                self.plug_image = Image.open(BytesIO(f.read()))
            with Resource("battery.png") as f:
                self.battery_image = Image.open(BytesIO(f.read()))

            # self.battey_circle = []
            for i in range(1, 13):
                with Resource(f"battery-circle-{i}.png") as f:
                    self.battey_circle.append(Image.open(BytesIO(f.read())))

            print("            battery status loaded images.")
            self.resources_loaded = True

        thread = threading.Thread(target=load_resources, daemon=True)
        thread.start()

        self.voltage = 0.0
        self.on_battery = False
        self.battery_percentage = 0.0
        self.mAh = 0.0
        self.mAh_s = 0.0
        self.mAh_e = 0.0
        self.mA = 0.0
        self.mA_s = 0.0
        self.mA_e = 0.0

        self.max_current_mA = 3500

        self.had_error = False
        self.shutdown_counter = -1
        self.shutdown_timer = 0

        self.flashing = 0

    def shutdown_announce_handler(self, _topic: str, payload: str) -> None:
        # if DEBUG: print(f"Received shutdown announce '{payload}'")
        if payload == "-":
            self.shutdown_counter = -1
        elif payload == "now":
            self.shutdown_counter = 0
        else:
            # noinspection PyBroadException
            try:
                self.shutdown_counter = int(float(payload))
            except Exception as _:
                print(f"Failed to process {payload}")

    @staticmethod
    def _limit(value: float, min_value: float, max_value: float) -> float:
        if value < min_value:
            return min_value
        elif value > max_value:
            return max_value

        return value

    def power_state_handler(self, _topic: str, payload: str) -> None:
        # if DEBUG: print(f"Received powerstate '{payload}'")
        had_error = False
        for kv in payload.split(","):
            kvs = kv.split("=")

            if len(kvs) == 2:
                # noinspection PyBroadException
                try:
                    if kvs[0] == "V":
                        self.voltage = float(kvs[1])
                    elif kvs[0] == "B":
                        self.on_battery = kvs[1].lower() == "true"
                    elif kvs[0] == "B%":
                        self.battery_percentage = float(kvs[1])
                    elif kvs[0] == "mAh":
                        self.mAh = float(kvs[1])
                    elif kvs[0] == "mAh_s":
                        self.mAh_s = float(kvs[1])
                    elif kvs[0] == "mAh_e":
                        self.mAh_e = float(kvs[1])
                    elif kvs[0] == "mA":
                        self.mA = float(kvs[1])
                    elif kvs[0] == "mA_s":
                        self.mA_s = float(kvs[1])
                    elif kvs[0] == "mA_e":
                        self.mA_e = float(kvs[1])
                except Exception as _:
                    if not self.had_error:
                        print(f"Failed to process '{kv}'")
                        had_error = True
            else:
                had_error = True
        self.had_error = had_error

    def draw_two_fonts(self, big_text: str, small_text: str, top: bool):
        margin = 3
        size_medium = self.draw.textsize(big_text, font=self.font_medium)
        size_small = self.draw.textsize(small_text, font=self.font_small)
        whole_length = size_medium[0] + size_small[0] + margin
        # bias = size_small[0] // 2
        bias = 5

        if top:
            y = self.MIDDLE - size_medium[1] // 2 - 40
        else:
            y = self.MIDDLE + size_medium[1] // 2 + 12

        self.draw.text((
            self.MIDDLE - whole_length // 2 + bias,
            y
        ), big_text, fill=(224, 224, 224, 255), font=self.font_medium)
        self.draw.text((
            self.MIDDLE + whole_length // 2 - size_small[0] + margin + bias,
            y + 2
        ), small_text, fill=(224, 224, 224, 255), font=self.font_small)

    def draw_shutdown(self) -> Image:
        self.draw.rectangle((0, 0, 240, 240), fill=(0, 0, 0, 0))
        self.draw.ellipse((
            self.MIDDLE - self.INNER_R,
            self.MIDDLE - self.INNER_R,
            self.MIDDLE + self.INNER_R,
            self.MIDDLE + self.INNER_R,
        ), fill=(255 - abs(self.SHUTDOWN_DISPLAY_TIMER - self.shutdown_timer) * 4, 0, 0, 255))

        if self.shutdown_counter == 0:
            self.draw.text((
                self.MIDDLE - self.off_size[0] // 2,
                self.MIDDLE - self.off_size[1] // 2
            ), "OFF", fill=(255, 255, 255, 255), font=self.font_big)
        else:
            if self.shutdown_timer >= 0:
                self.draw.text((
                    self.MIDDLE - self.off_size[0] // 2,
                    self.MIDDLE - self.off_size[1] // 2
                ), "OFF", fill=(255, 255, 255, 255), font=self.font_big)
            t = str(self.shutdown_counter)
            s = self.draw.textsize(t, font=self.font_medium)
            self.draw.text((
                self.MIDDLE - s[0] // 2,
                self.MIDDLE + self.off_size[1] // 2
            ), t, fill=(224, 224, 224, 255), font=self.font_medium)

            self.shutdown_timer -= 1
            if self.shutdown_timer <= - self.SHUTDOWN_DISPLAY_TIMER:
                self.shutdown_timer = self.SHUTDOWN_DISPLAY_TIMER

        return self.image

    def _draw_middle_battery(self) -> None:
        self.image.paste(self.battery_image, (80, 80, 160, 160), None)

        c = int(self._limit(self.battery_percentage, 0.0, 100.0) * 56 / 100.0)
        self.draw.rectangle((88, 108, 88 + c, 130), fill=(224, 224, 224, 255))

    # noinspection PyPep8Naming
    def _draw_mA_and_mAh_text(self) -> None:
        mA = self.mA
        unit = "A"
        if mA < 10000:
            t = f"{mA:.0f}"
            unit = "mA"
        elif mA < 10000:
            t = f"{mA / 1000:.3f}"
        else:
            t = f"{mA / 1000:.1f}"

        self.draw_two_fonts(t, unit, True)
        self.draw_two_fonts(f"{self.mAh:.0f}" if self.mAh <= 9000 else ">9000", "mAh", False)

    def draw_current(self) -> Image:
        self.draw.rectangle((0, 0, 240, 240), fill=(0, 0, 0, 0))

        current = self._limit(self.mA_s / self.max_current_mA, 0, 1.0)
        do_draw = True

        if self.mA_s > self.max_current_mA * 0.85:
            if self.flashing == 0:
                self.flashing = 1
                do_draw = False
            else:
                self.flashing -= 1

        if do_draw:
            self.image.paste(self.power_circle_image, (0, 1, 240, 241), None)

            a = 135.0 + (1.0 - current) * 270.0
            self.draw.pieslice((-5, -5, 250, 250), 45, a, fill=(0, 0, 0, 255))

        if not self.on_battery:
            self.image.paste(self.plug_image, (80, 80, 160, 160), None)
        else:
            self._draw_middle_battery()

        self._draw_mA_and_mAh_text()

        return self.image

    def draw_battery(self) -> Image:
        self.draw.rectangle((0, 0, 240, 240), fill=(0, 0, 0, 0))

        do_draw = True

        if self.on_battery and self.battery_percentage <= 20.0:
            if self.flashing < 3:
                do_draw = False
            if self.flashing == 0:
                self.flashing = 8
            self.flashing -= 1

        if do_draw:
            if self.on_battery:
                i = int(self.battery_percentage // 8.3)
                if i > 11: i = 11
                self.image.paste(self.battey_circle[i], (0, 1, 240, 241), None)
            elif self.battery_percentage > 1:
                self.image.paste(self.battey_circle[11], (0, 1, 240, 241), None)

        if not self.on_battery:
            self.image.paste(self.plug_image, (80, 80, 160, 160), None)
        else:
            self._draw_middle_battery()

        self._draw_mA_and_mAh_text()

        return self.image

    def draw_display(self) -> Image:
        self.draw.rectangle((0, 0, 240, 240), fill=(0, 0, 0, 0))

        if self.shutdown_counter != -1:
            return self.draw_shutdown()
        elif self.mA_s > 500 and self.battery_percentage > 25:
            return self.draw_current()
        else:
            return self.draw_battery()
