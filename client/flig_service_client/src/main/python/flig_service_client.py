import threading

from typing import Optional

import sys
import pygame
import pyros

from pygame import Rect

from calibration import CalibrationScreen
from distances.distances_screen import DistancesScreen, DistanceControllerSelection
from jcontroller_client import MQTTJoystickClient, PygameJoystickProcessor
from loading_screen import LoadingScreen
from monitor.monitor_screen import MonitorScreen
from pyros_support_ui import PyrosClientApp, agent
from pyros_support_ui.components import UIAdapter, Collection, BaseUIFactory, LeftRightLayout, CardsCollection, UiHint
from pyros_support_ui.drop_down import DropDown
from pyros_support_ui.pygamehelper import load_font, load_image

from pyros_support_ui.box_blue_sf_theme import BoxBlueSFThemeFactory
from servo_lib.leg_servos import LegServos, ExtraServos
from telemetry_utils.distance_telemetry_controller import DistancesTelemetryController
from telemetry_utils.final_distance_telemetry_controller import FinalDistancesTelemetryController
from telemetry_utils.nine_dof_telemetry_controller import NineDoFTelemetryController
from telemetry_utils.telemetry_controller import TelemetryController
from ui.screen import Screen


print("Starting client app...")

ui_adapter = UIAdapter(screen_size=(1400, 848))

exo_regular_20 = load_font("Exo-Regular.otf", 20)
exo_regular_14 = load_font("Exo-Regular.otf", 14)
exo_regular_100 = load_font("Exo-Regular.otf", 100)

ui_factory = BoxBlueSFThemeFactory(ui_adapter, font=exo_regular_20, small_font=exo_regular_14, antialias=True)


pyros_client_app = PyrosClientApp(ui_factory,
                                  logo_image=load_image("flig_coloured_small.png"),
                                  logo_alt_image=load_image("flig_green_small.png"),
                                  connect_to_first=True,
                                  connect_to_only=False,
                                  rover_filter=lambda response, _: "ROVER" in response and response["ROVER"] == "FLIG")


pyros_client_app.pyros_init("flig-service-client-#")

connected_count_down = 0
loop_counter = 0


def _on_connected() -> None:
    global connected_count_down
    print(f"    connected: flagging that we can start requesting calibrations...")
    connected_count_down = 3


pyros_client_app.add_on_connected_subscriber(_on_connected)

if len(sys.argv) > 1:
    pyros_client_app.add_from_arguments(sys.argv[1], {"ROVER": "FLIG", "TYPE": "PYROS", "NAME": "FLIG"})

ui_adapter.top_component = pyros_client_app


def send_agent():
    # try:
    #     agent.init(None, "flig_service_agent.py", agent_id="flig_service_agent")
    # except Exception as e:
    #     print(f"Failed to upload agent; {e}")
    print("Not sending agent any more")


class MainScreen(Collection, DistanceControllerSelection):
    # DECORATION_OFFSET = 32
    DECORATION_OFFSET = 0

    def __init__(self, ui_factory: BaseUIFactory,
                 telemetry_controller: TelemetryController,
                 final_distances_telemetry_controller: FinalDistancesTelemetryController,
                 distances_telemetry_controller: DistancesTelemetryController,
                 nine_dof_telemetry_controller: NineDoFTelemetryController) -> None:
        super().__init__(None)
        self.telemetry_controller = telemetry_controller
        self.selected_distances_telemetry_controller = final_distances_telemetry_controller
        self.distances_telemetry_controller = distances_telemetry_controller
        self.final_distances_telemetry_controller = final_distances_telemetry_controller
        self.nine_dof_telemetry_controller = nine_dof_telemetry_controller
        self.commands_panel = ui_factory.panel(None, layout=LeftRightLayout(margin=10), hint=UiHint.NO_DECORATION)

        self.add_component(self.commands_panel)
        self.main_panel = CardsCollection(None)
        self.add_component(self.main_panel)

        self.main_menu_drop_down = DropDown(
            Rect(0, 0, 120, 30),
            ui_factory,
            select_method=self.select_screen,
            update_activation_button_text=False,
            text="Menu",
            menu_width=170
        )
        self.add_component(self.main_menu_drop_down.menu)
        self.commands_panel.add_component(self.main_menu_drop_down)

        self.telemetry_menu_drop_down = DropDown(
            Rect(0, 0, 120, 30),
            ui_factory,
            select_method=self.telemetry_actions,
            update_activation_button_text=False,
            text="Telemetry",
            menu_width=120
        )
        self.telemetry_menu_drop_down.add_item("start", "Start")
        self.telemetry_menu_drop_down.add_item("stop", "Stop")
        self.telemetry_menu_drop_down.add_item("save", "Save")
        self.telemetry_menu_drop_down.add_item("clear", "Clear")
        self.add_component(self.telemetry_menu_drop_down.menu)
        self.commands_panel.add_component(self.telemetry_menu_drop_down)

        self.distances_telemetry_menu_drop_down = DropDown(
            Rect(0, 0, 120, 30),
            ui_factory,
            select_method=self.pos_telemetry_actions,
            update_activation_button_text=False,
            text="Distances",
            menu_width=120
        )
        self.distances_telemetry_menu_drop_down.add_item("start-ps", "Start PS")
        self.distances_telemetry_menu_drop_down.add_item("start-d", "Start D")
        self.distances_telemetry_menu_drop_down.add_item("stop", "Stop")
        self.distances_telemetry_menu_drop_down.add_item("save", "Save")
        self.distances_telemetry_menu_drop_down.add_item("clear", "Clear")
        self.add_component(self.distances_telemetry_menu_drop_down.menu)
        self.commands_panel.add_component(self.distances_telemetry_menu_drop_down)

        self.contributing_panel = CardsCollection(None)
        self.commands_panel.add_component(self.contributing_panel)

    def redefine_rect(self, rect) -> None:
        self.rect = rect
        self.commands_panel.redefine_rect(pygame.Rect(rect.x + 5, rect.y - MainScreen.DECORATION_OFFSET, rect.width - 10, 30))
        self.main_panel.redefine_rect(pygame.Rect(rect.x + 5, self.commands_panel.rect.bottom + 5, rect.width - 10, rect.height - 5 - self.commands_panel.rect.height + MainScreen.DECORATION_OFFSET))

    def key_down(self, code: str) -> bool:
        return self.main_panel.key_down(code)

    def key_up(self, code: str) -> bool:
        return self.main_panel.key_up(code)

    def add_screen(self, screen_id: str, screen_name: str, component: Screen) -> None:
        self.main_panel.add_card(screen_id, component)
        self.main_menu_drop_down.add_item(screen_id, screen_name)
        contributing_command_panel = Collection(None)
        self.contributing_panel.add_card(screen_id, contributing_command_panel)
        component.set_contributing_panel(contributing_command_panel)

    def select_screen(self, screen_id: str) -> None:
        self.main_panel.select_card(screen_id)
        self.contributing_panel.select_card(screen_id)

    def telemetry_actions(self, action: str) -> None:
        print(f"Telemetry selected action {action}")
        if action == "start":
            self.telemetry_controller.start_connecting_telemetry()
        elif action == "stop":
            self.telemetry_controller.safely_close_telemetry_client()
        elif action == "save":
            self.telemetry_controller.save_telemetry()
        elif action == "clear":
            self.telemetry_controller.clear_telemetry()

    def pos_telemetry_actions(self, action: str) -> None:
        print(f"Distances selected action {action}")
        if action == "start-ps":
            self.selected_distances_telemetry_controller = self.final_distances_telemetry_controller
            self.selection_changed_callback()
            self.selected_distances_telemetry_controller.start_connecting_telemetry()
            self.nine_dof_telemetry_controller.start_connecting_telemetry()
        elif action == "start-d":
            self.selected_distances_telemetry_controller = self.distances_telemetry_controller
            self.selection_changed_callback()
            self.selected_distances_telemetry_controller.start_connecting_telemetry()
            self.nine_dof_telemetry_controller.start_connecting_telemetry()
        elif action == "stop":
            self.selected_distances_telemetry_controller.safely_close_telemetry_client()
            self.nine_dof_telemetry_controller.safely_close_telemetry_client()
        elif action == "save":
            self.selected_distances_telemetry_controller.save_telemetry()
            self.nine_dof_telemetry_controller.save_telemetry()
        elif action == "clear":
            self.selected_distances_telemetry_controller.clear_telemetry()
            self.nine_dof_telemetry_controller.clear_telemetry()


main_screen: Optional[MainScreen] = None
# pygame_joystick_processor: Optional[PygameJoystickProcessor] = None
# mqtt_joystick: Optional[MQTTJoystickClient] = None

print("    creating joystick client...")
mqtt_joystick = MQTTJoystickClient()
pygame_joystick_processor = PygameJoystickProcessor(mqtt_joystick)
print("    created joystick client.")

print("    creating telemetry controller...")
telemetry_controller = TelemetryController(pyros_client_app)
print("    created telemetry controller.")

loading = True

calibrations: Optional[LegServos] = None
extra_servos: Optional[ExtraServos] = None
nine_dof_telemetry_controller: Optional[NineDoFTelemetryController] = None


def load_client() -> None:
    global main_screen, loading
    global calibrations, extra_servos, nine_dof_telemetry_controller

    print("    creating distance telemetry controller...")
    final_distances_telemetry_controller = FinalDistancesTelemetryController(pyros_client_app)
    distances_telemetry_controller = DistancesTelemetryController(pyros_client_app)
    print("    created distance telemetry controller.")

    print("    creating 9dof telemetry controller...")
    nine_dof_telemetry_controller = NineDoFTelemetryController(pyros_client_app)
    print("    created 9dof telemetry controller.")

    print("    creating leg calibrations...")
    calibrations = LegServos()
    extra_servos = ExtraServos()
    print("    created leg calibrations.")

    print("    creating main screen...")
    main_screen = MainScreen(ui_factory, telemetry_controller, final_distances_telemetry_controller, distances_telemetry_controller, nine_dof_telemetry_controller)
    print("    created main screen.")

    print("    creating calibration screen...")
    calibration_screen = CalibrationScreen(pyros_client_app, ui_factory, calibrations, extra_servos, telemetry_controller)
    print("    created calibration screen.")

    print("    creating monitor screen...")
    monitor_screen = MonitorScreen(pyros_client_app, ui_factory, calibrations, telemetry_controller, mqtt_joystick)
    print("    created monitor screen.")

    print("    creating distances screen...")
    distances_screen = DistancesScreen(pyros_client_app, ui_factory, main_screen, nine_dof_telemetry_controller)
    print("    created distances screen.")

    main_screen.add_screen("monitor", "Monitor", monitor_screen)
    main_screen.add_screen("distances", "Distances", distances_screen)
    main_screen.add_screen("calibration", "Calibration", calibration_screen)
    main_screen.select_screen("distances")

    pyros_client_app.set_content(main_screen)

    print("    starting discovery in background...")
    pyros_client_app.start_discovery_in_background()
    print("    started discovery in background.")

    pyros_client_app.add_on_connected_subscriber(send_agent)

    print(f"Started client app.  @{loop_counter}")

    loading = False


thread = threading.Thread(target=load_client, daemon=True)
thread.start()


loading_screen = LoadingScreen(pyros_client_app.rect, exo_regular_100)


while True:
    loop_counter += 1
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
        if not loading:
            pygame_joystick_processor.process_pygame_event(event)

        if not loading:
            ui_adapter.process_event(event)
            if event.type == pygame.KEYDOWN:
                main_screen.key_down(event.key)
            if event.type == pygame.KEYUP:
                main_screen.key_up(event.key)

    if not loading:
        mqtt_joystick.process_buttons()
        mqtt_joystick.process_joysticks()

    if connected_count_down == 3:
        connected_count_down -= 1
        print(f"    connected - requesting leg calibration details... @{loop_counter}")
        calibrations.request_calibration_details()
        pyros.loop(0.03)
        print(f"    connected - requested leg calibration details. @{loop_counter}")

    elif connected_count_down == 2:
        connected_count_down -= 1
        print(f"    connected - requesting extra servo calibration details... @{loop_counter}")
        extra_servos.request_calibration_details()
        pyros.loop(0.03)
        print(f"    connected - requested extra servo calibration details. @{loop_counter}")
    elif connected_count_down == 1:
        connected_count_down -= 1
        print(f"    connected - requesting extra calibration details... @{loop_counter}")
        nine_dof_telemetry_controller.request_stored_calibration()
        pyros.loop(0.03)
        print(f"    connected - requested extra calibration details. @{loop_counter}")
    else:
        pyros.loop(0.03)

    agent.keep_agents()

    if loading:
        loading_screen.draw(ui_adapter.screen)
    else:
        ui_adapter.draw(ui_adapter.screen)

    ui_adapter.frame_end()
