################################################################################
# Copyright (C) 2020 Abstract Horizon
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Apache License v2.0
# which accompanies this distribution, and is available at
# https://www.apache.org/licenses/LICENSE-2.0
#
#  Contributors:
#    Daniel Sendula - initial API and implementation
#
#################################################################################

import sys
import pygame
import pyros
import threading
import traceback
import time

from functools import partial

from pygame import Rect

from pyros_support_ui import PyrosClientApp, agent
from pyros_support_ui.components import UIAdapter, Component, Collection, LeftRightLayout, UiHint, TopDownLayout
from pyros_support_ui.pygamehelper import load_font, load_image

from pyros_support_ui.box_blue_sf_theme import BoxBlueSFThemeFactory
# from pyros_support_ui.flat_theme import FlatThemeFactory

from pyros_support_ui.graph_component import Graph, GraphController, TelemetryGraphData
from pyros_support_ui.number_input import NumberInputComponent

from telemetry import CachingSocketTelemetryClient


ui_adapter = UIAdapter(screen_size=(1400, 848))
ui_factory = BoxBlueSFThemeFactory(ui_adapter, font=load_font("garuda.ttf", 20), small_font=load_font("garuda.ttf", 14))

_ui_factorysmall_font = ui_factory.small_font

pyros_client_app = PyrosClientApp(ui_factory,
                                  logo_image=load_image("GCC_coloured_small.png"),
                                  logo_alt_image=load_image("GCC_green_small.png"),
                                  connect_to_first=True,
                                  connect_to_only=False)

pyros_client_app.pyros_init("balancing-rover-#")
ui_adapter.top_component = pyros_client_app


class CommandsPanel(Collection):
    def __init__(self):
        super(CommandsPanel, self).__init__(None, layout=LeftRightLayout(margin=10))


class GraphsPanel(Collection):
    def __init__(self, rows, columns, graph_controller=None,
                 inner_colour=pygame.color.THECOLORS['white'],
                 background_colour=pygame.color.THECOLORS['black'],
                 label_colour=pygame.color.THECOLORS['grey']):
        super(GraphsPanel, self).__init__(None)
        self.rows = rows
        self.columns = columns
        self.main_graph = Graph(None, ui_factory, controller=graph_controller, inner_colour=inner_colour, background_colour=background_colour, label_colour=label_colour)
        self.add_component(self.main_graph)
        self.graphs = []

        def graph_selected(clicked_graph, _mouse_pos):
            self.main_graph.graph_data = clicked_graph.get_graph_data()

        for i in range(rows):
            self.graphs.append([])
            for _ in range(columns):
                graph = Graph(None,
                              ui_factory,
                              controller=graph_controller,
                              on_click=graph_selected,
                              inner_colour=inner_colour, background_colour=background_colour, label_colour=label_colour)
                self.graphs[i].append(graph)
                self.add_component(graph)

    def redefine_rect(self, rect):
        self.rect = rect

        margin = 4

        if self.rows > 2:
            self.main_graph.redefine_rect(Rect(rect.x, rect.y, rect.width, rect.height // 2 - margin))
        else:
            self.main_graph.redefine_rect(Rect(rect.x, rect.y, rect.width, rect.height * 2 // 3 - margin))

        x = rect.x
        y = self.main_graph.rect.bottom + margin
        width = (rect.width - margin * (self.columns - 1)) // self.columns
        height = (rect.bottom - y - margin * (self.rows - 1)) // self.rows
        for r in range(self.rows):
            for c in range(self.columns):
                self.graphs[r][c].redefine_rect(Rect(x + (width + margin) * c, y, width, height))

            y = y + height + margin


class ValuesPanel(Collection):
    def __init__(self, rect, _ui_factory):
        super(ValuesPanel, self).__init__(rect, TopDownLayout(margin=5))
        self.cached_values = {
            'pid_inner': {'p': 0.75, 'i': 0.2, 'd': 0.05, 'g': 1.0},
            'pid_outer': {'p': 0.75, 'i': 0.2, 'd': 0.05, 'g': 1.0},
            'gyro': {'filter': 0.3, 'freq': 200, 'bandwidth': 50},
            'accel': {'filter': 0.5, 'freq': 200},
            'combine_factor_gyro': 0.95,
            'expo': 0.2
        }
        self.manual = 0.01

        # pyros.subscribe("storage/write/balance/gyro/filter", self.gyro_filter_changed)
        # pyros.subscribe("storage/write/balance/accel/filter", self.accel_filter_changed)
        # pyros.subscribe("storage/write/balance/combine_factor_gyro", self.combine_factor_gyro_changed)
        # pyros.subscribe("storage/write/balance/expo", self.expo_changed)
        # pyros.subscribe("storage/write/balance/pid_inner/#", self.pid_inner_changed)
        # pyros.subscribe("storage/write/balance/pid_outer/#", self.pid_inner_changed)

        button_height = 40



        self.add_component(NumberInputComponent(Rect(0, 0, 300, button_height), _ui_factory, self.create_getter_and_setter("gyro/filter"), 'g-f', button_font=_ui_factorysmall_font))
        self.add_component(NumberInputComponent(Rect(0, 0, 300, button_height), _ui_factory, self.create_getter_and_setter("accel/filter"), 'a-f', button_font=_ui_factorysmall_font))
        self.add_component(NumberInputComponent(Rect(0, 0, 300, button_height), _ui_factory, self.create_getter_and_setter("combine_factor_gyro"), 'c-f', button_font=_ui_factorysmall_font))
        self.add_component(NumberInputComponent(Rect(0, 0, 300, button_height), _ui_factory, self.create_getter_and_setter("expo"), 'expo', button_font=_ui_factorysmall_font))
        self.add_component(Component(Rect(0, 0, 300, 10)))
        self.add_component(NumberInputComponent(Rect(0, 0, 300, button_height), _ui_factory, self.create_getter_and_setter("pid_inner/p"), 'pi-p', button_font=_ui_factorysmall_font))
        self.add_component(NumberInputComponent(Rect(0, 0, 300, button_height), _ui_factory, self.create_getter_and_setter("pid_inner/i"), 'pi-i', button_font=_ui_factorysmall_font))
        self.add_component(NumberInputComponent(Rect(0, 0, 300, button_height), _ui_factory, self.create_getter_and_setter("pid_inner/d"), 'pi-d', button_font=_ui_factorysmall_font))
        self.add_component(NumberInputComponent(Rect(0, 0, 300, button_height), _ui_factory, self.create_getter_and_setter("pid_inner/g"), 'pi-g', button_font=_ui_factorysmall_font))
        self.add_component(Component(Rect(0, 0, 300, 10)))
        self.add_component(
            NumberInputComponent(Rect(0, 0, 300, button_height), _ui_factory, (self.get_manual, self.set_manual), 'man', button_font=_ui_factorysmall_font, top_scale=2, bottom_scale=0))

        pyros_client_app.add_on_connected_subscriber(self.read_values)

    def read_values(self):
        def send_read_recursively(_map, path):
            for key in _map:
                if type(_map[key]) is dict:
                    send_read_recursively(_map[key], (path + "/" + key) if path != "" else key)
                else:
                    subscribe_topic = f"storage/read/balance/{path}/{key}"
                    print(f"Subscribing to {subscribe_topic}")
                    pyros.publish(subscribe_topic, "")

        send_read_recursively(self.cached_values, "")

    def redefine_rect(self, rect):
        super(ValuesPanel, self).redefine_rect(rect)

    def gyro_filter_changed(self, _topic, payload, _groups):
        print(f"Gyro filter {_topic} : {payload}")
        self.cached_values['gyro']['filter'] = float(payload)

    def accel_filter_changed(self, _topic, payload, _groups):
        print(f"Accel filter {_topic} : {payload}")
        self.cached_values['accel']['filter'] = float(payload)

    def combine_factor_gyro_changed(self, _topic, payload, _groups):
        print(f"Combine factor {_topic} : {payload}")
        self.cached_values['combine_factor_gyro'] = float(payload)

    def expo_changed(self, _topic, payload, _groups):
        print(f"Expo {_topic} : {payload}")
        self.cached_values['expo'] = float(payload)

    def pid_inner_changed(self, topic, payload, _groups):
        print(f"Received inner {topic} : {payload}")
        topic = topic[32:]
        # noinspection PyBroadException
        try:
            if "p" == topic:
                self.cached_values['pid_inner']['p'] = float(payload)
            if "i" == topic:
                self.cached_values['pid_inner']['i'] = float(payload)
            if "d" == topic:
                self.cached_values['pid_inner']['d'] = float(payload)
            if "g" == topic:
                self.cached_values['pid_inner']['g'] = float(payload)
        except Exception:
            pass

    def pid_outer_changed(self, topic, payload, _groups):
        print(f"Received outer {topic} : {payload}")
        topic = topic[32:]
        # noinspection PyBroadException
        try:
            if "p" == topic:
                self.cached_values['pid_inner']['p'] = float(payload)
            if "i" == topic:
                self.cached_values['pid_inner']['i'] = float(payload)
            if "d" == topic:
                self.cached_values['pid_inner']['d'] = float(payload)
            if "g" == topic:
                self.cached_values['pid_inner']['g'] = float(payload)
        except Exception:
            pass

    def create_getter_and_setter(self, path):
        def read_value(map_place, name):
            return map_place[name]

        def write_value(map_place, name, _path, value):
            map_place[name] = value

            if abs(value > 0.1):
                s = "{0:.2f}".format(value)
            elif abs(value > 0.01):
                s = "{0:.3f}".format(value)
            elif abs(value > 0.001):
                s = "{0:.4f}".format(value)
            elif abs(value > 0.0001):
                s = "{0:.5f}".format(value)
            else:
                s = "{0:.6f}".format(value)

            pyros.publish("storage/write/balance/" + _path, s)

        splt = path.split('/')
        m = self.cached_values
        for p in splt[:-1]:
            m = m[p]
        return partial(read_value, m, splt[-1]), partial(write_value, m, splt[-1], path)

    def set_manual(self, manual):
        self.manual = manual

    def get_manual(self):
        return self.manual


class BalancingRoverTelemetry(Collection):
    def __init__(self, _ui_factory, _pyros_client_app):
        super(BalancingRoverTelemetry, self).__init__(None)
        self.pyros_client_app = _pyros_client_app
        self.commands = CommandsPanel()
        self.add_component(self.commands)

        self.graph_controller = GraphController()

        self.sensors_graphs_panel = GraphsPanel(3, 3, graph_controller=self.graph_controller, inner_colour=(127, 255, 127))
        self.add_component(self.sensors_graphs_panel)

        self.values_panel = ValuesPanel(Rect(0, 0, 300, 800), _ui_factory)
        self.add_component(self.values_panel)

        self.start_stop_balancing_panel = Collection(Rect(0, 0, 100, 0))
        self.commands.add_component(self.start_stop_balancing_panel)
        self.start_stop_balancing_panel.add_component(_ui_factory.text_button(Rect(0, 0, 100, 0), "Start", on_click=self.start_servo))
        self.start_stop_balancing_panel.add_component(_ui_factory.text_button(Rect(0, 0, 100, 0), "Stop", on_click=self.stop_servo, hint=UiHint.WARNING))
        self.start_stop_balancing_panel.components[0].visible = False
        self.commands.add_component(_ui_factory.text_button(Rect(0, 0, 100, 0), "STOP", on_click=self.stop2_servo))
        self.commands.add_component(_ui_factory.text_button(Rect(0, 0, 100, 0), "Hold", on_click=self.hold_servo))
        self.commands.add_component(_ui_factory.text_button(Rect(0, 0, 100, 0), "TIMED", on_click=self.timed_servo))

        self.commands.add_component(_ui_factory.text_button(Rect(0, 0, 100, 0), "Calibrate", on_click=self.calibrate))

        self.start_stop_collecting_panel = Collection(Rect(0, 0, 200, 0))
        self.commands.add_component(self.start_stop_collecting_panel)
        self.start_stop_collecting_panel.add_component(_ui_factory.text_button(Rect(0, 0, 200, 0), "Stop Collecting", on_click=self.stop_collecting))
        self.start_stop_collecting_panel.add_component(_ui_factory.text_button(Rect(0, 0, 200, 0), "Continue Collecting", on_click=self.continue_collecting))
        self.start_stop_collecting_panel.components[0].visible = False

        self.sensors_pid_switch_panel = Collection(Rect(0, 0, 200, 0))
        self.commands.add_component(self.sensors_pid_switch_panel)

        self.pause_graph_panel = Collection(Rect(0, 0, 200, 0))
        self.commands.add_component(self.pause_graph_panel)
        self.pause_graph_panel.add_component(_ui_factory.text_button(Rect(0, 0, 200, 0), "Resume Graph", on_click=self.resume_graph))
        self.pause_graph_panel.add_component(_ui_factory.text_button(Rect(0, 0, 200, 0), "Pause Graph", on_click=self.pause_graph))
        self.pause_graph_panel.components[0].visible = False

        self.commands.add_component(_ui_factory.text_button(Rect(0, 0, 100, 0), "Clear", on_click=self.clear_graph))
        self.commands.add_component(_ui_factory.text_button(Rect(0, 0, 100, 0), "Save All", on_click=self.save_graph_all))
        self.commands.add_component(_ui_factory.text_button(Rect(0, 0, 100, 0), "Save ", on_click=self.save_graph))

        self.telemetry_client = None
        self._collect_data = True

        self._telemetry_receiving_thread = threading.Thread(target=self._receive_telemetry, name="telemetry_received", daemon=True)
        self._telemetry_receiving_thread.start()

        self._graph_data = {}

        self.do_connect = False

        pyros_client_app.add_on_connected_subscriber(self._connect_telemetry)

        def connect_telemetry_thread():
            while True:
                try:
                    while not self.do_connect:
                        time.sleep(0.5)

                    self.do_connect = False
                    agent.init(None, "current_reader_agent.py", agent_id="current_reader_agent")

                    host = ""
                    port = 1861
                    connected = False
                    timeout = time.time() + 7

                    while not connected and time.time() < timeout:
                        try:
                            host, _ = pyros.get_connection_details()
                            print(f"Connect thread: locally connecting to remote telemetry agent at {host}:{port} ...")
                            if self.telemetry_client is None or self.telemetry_client.host != host:
                                self._setup_telemetry_client(host, port)
                            self.start_stop_collecting_panel.components[0].visible = True
                            self.start_stop_collecting_panel.components[1].visible = False
                            connected = True
                            print(f"Connect thread: connected to remote telemetry agent at {host}:{port}.")
                        except Exception as e:
                            print(f"Connect thread: exception connecting to remote telemetry agent at {host}:{port}; {e}")
                            self._safely_close_telemetry_client()
                            time.sleep(1)
                    if not connected:
                        print(f"Connect thread: failed to connect to remote telemetry agent at {host}:{port}.")
                    self.connect_thread = None
                except Exception as _:
                    pass

        self.connect_thread = threading.Thread(target=connect_telemetry_thread, name="connect_telemetry", daemon=True)
        self.connect_thread.start()

    def _safely_close_telemetry_client(self):
        if self.telemetry_client is not None:
            try:
                self.telemetry_client.stop()
            except Exception as _:
                pass
        self.telemetry_client = None

    def _connect_telemetry(self):
        self.do_connect = True

    def _setup_telemetry_client(self, host, port):
        self.start_stop_collecting_panel.components[0].visible = True
        self.start_stop_collecting_panel.components[1].visible = False
        self.start_stop_balancing_panel.components[0].visible = False
        self.start_stop_balancing_panel.components[1].visible = True

        if self.telemetry_client is not None:
            self.telemetry_client.stop()

        print(f"... starting telemetry client at {host}:{port}...")
        self.telemetry_client = CachingSocketTelemetryClient(host=host, port=port)
        self.telemetry_client.start()
        print(f"... started telemetry client at {host}:{port}.")
        self.telemetry_client.socket.settimeout(2)
        self._graph_data["current_0"] = TelemetryGraphData(self.telemetry_client, self.telemetry_client.streams['current_reading_stream'], 'current_0', 32767, -32768, auto_scale=True)
        self._graph_data["current_1"] = TelemetryGraphData(self.telemetry_client, self.telemetry_client.streams['current_reading_stream'], 'current_1', 32767, -32768, auto_scale=True)
        self._graph_data["current_2"] = TelemetryGraphData(self.telemetry_client, self.telemetry_client.streams['current_reading_stream'], 'current_2', 32767, -32768, auto_scale=True)
        self._graph_data["current_3"] = TelemetryGraphData(self.telemetry_client, self.telemetry_client.streams['current_reading_stream'], 'current_3', 32767, -32768, auto_scale=True)
        self._graph_data["current_4"] = TelemetryGraphData(self.telemetry_client, self.telemetry_client.streams['current_reading_stream'], 'current_4', 32767, -32768, auto_scale=True)
        self._graph_data["current_5"] = TelemetryGraphData(self.telemetry_client, self.telemetry_client.streams['current_reading_stream'], 'current_5', 32767, -32768, auto_scale=True)
        # self._graph_data["angle"] = TelemetryGraphData(self.telemetry_client, self.telemetry_client.streams['current_reading_stream'], 'angle', 0, 360, auto_scale=True)

        self.sensors_graphs_panel.main_graph.graph_data = self._graph_data['current_0']
        self.sensors_graphs_panel.graphs[0][0].graph_data = self._graph_data['current_0']
        self.sensors_graphs_panel.graphs[0][1].graph_data = self._graph_data['current_1']
        self.sensors_graphs_panel.graphs[1][0].graph_data = self._graph_data['current_2']
        self.sensors_graphs_panel.graphs[1][1].graph_data = self._graph_data['current_3']
        self.sensors_graphs_panel.graphs[2][0].graph_data = self._graph_data['current_4']
        self.sensors_graphs_panel.graphs[2][1].graph_data = self._graph_data['current_5']
        # self.sensors_graphs_panel.graphs[3][0].set_graph_data(self._graph_data['angle'])

    def key_down(self, key):
        if self.pyros_client_app.key_down(key):
            pass
        # elif key == pygame.K_s:
        # else:
        #     pyros_support_ui.gcc.handle_connect_key_down(key)

    def key_up(self, key):
        if self.pyros_client_app.key_up(key):
            pass

    def redefine_rect(self, rect):
        self.rect = rect
        self.commands.redefine_rect(Rect(rect.x, rect.y, rect.width, 30))
        reminder_top = self.commands.rect.bottom + 5
        self.sensors_graphs_panel.redefine_rect(
            Rect(rect.x, reminder_top, rect.width - 300 - 5, rect.bottom - self.commands.rect.bottom - 5))
        self.values_panel.redefine_rect(
            Rect(self.sensors_graphs_panel.rect.right + 5, reminder_top, rect.right - self.sensors_graphs_panel.rect.right - 5, rect.bottom - self.commands.rect.bottom - 5))

    def start_servo(self, *_args):
        pyros.publish("test/command", "START")
        if pyros.is_connected():
            self.start_stop_balancing_panel.components[0].visible = False
            self.start_stop_balancing_panel.components[1].visible = True

    def stop_servo(self, *_args):
        pyros.publish("test/command", "STOP")
        if pyros.is_connected():
            self.start_stop_balancing_panel.components[0].visible = True
            self.start_stop_balancing_panel.components[1].visible = False

    def stop2_servo(self, *_args):
        pyros.publish("test/command", "STOP2")
        if pyros.is_connected():
            self.start_stop_balancing_panel.components[0].visible = True
            self.start_stop_balancing_panel.components[1].visible = False

    def hold_servo(self, *_args):
        pyros.publish("test/command", "HOLD")
        if pyros.is_connected():
            self.start_stop_balancing_panel.components[0].visible = True
            self.start_stop_balancing_panel.components[1].visible = False

    def timed_servo(self, *_args):
        pyros.publish("test/command", "TIMED")
        if pyros.is_connected():
            self.start_stop_balancing_panel.components[0].visible = True
            self.start_stop_balancing_panel.components[1].visible = False

    @staticmethod
    def calibrate(*_args):
        pyros.publish("balancing/calibrate", "all")

    def stop_collecting(self, *_args):
        self._collect_data = False
        self.telemetry_client.stop()
        self.start_stop_collecting_panel.components[0].visible = False
        self.start_stop_collecting_panel.components[1].visible = True

    def continue_collecting(self, *_args):
        if pyros.is_connected():
            if self.telemetry_client is None and pyros.is_connected():
                host, port = pyros.get_connection_details()
                self._setup_telemetry_client(host, 1861)
            else:
                self.telemetry_client.start()
            self._collect_data = True
            self.start_stop_collecting_panel.components[0].visible = True
            self.start_stop_collecting_panel.components[1].visible = False

    def pause_graph(self, *_args):
        self.pause_graph_panel.components[0].visible = True
        self.pause_graph_panel.components[1].visible = False
        self.graph_controller.pause()

    def resume_graph(self, *_args):
        self.pause_graph_panel.components[0].visible = False
        self.pause_graph_panel.components[1].visible = True
        self.graph_controller.resume()

    def clear_graph(self, *_args):
        def clear_data(stream):
            self.telemetry_client.trim(stream, time.time())

        self.telemetry_client.get_stream_definition("current_reading_stream", clear_data)

    def save_graph_all(self, *_args):

        with open("logs.csv", "wt") as file:
            def write_data(records):
                for record in records:
                    file.write(",".join([str(f) for f in record]) + "\n")

            def write_header(stream):
                file.write("timestamp," + ",".join(f.name for f in stream.fields) + "\n")
                self.telemetry_client.retrieve(stream, 0, time.time(), write_data)

            self.telemetry_client.get_stream_definition("current_reading_stream", write_header)

    def save_graph(self, *_args):

        fields_to_save = ["lw", "rw", "cy", "pi_p", "pi_i", "pi_d", "pi_pg", "pi_ig", "pi_dg", "pi_dt", "pi_o", "out"]
        field_indexes = [0]

        with open("logs.csv", "wt") as file:
            def write_data(records):
                for record in records:
                    file.write(",".join(str(record[i]) for i in field_indexes) + "\n")

            def write_header(stream):
                filtered_fields = [f.name for f in stream.fields if f.name in fields_to_save]
                field_indexes.extend(i + 1 for i in range(len(stream.fields)) if stream.fields[i].name in filtered_fields)
                print(f" fields {filtered_fields} and indexes {field_indexes}")
                file.write("timestamp," + ",".join(f for f in filtered_fields) + "\n")
                self.telemetry_client.retrieve(stream, 0, time.time(), write_data)

            self.telemetry_client.get_stream_definition("current_reading_stream", write_header)

    def _receive_telemetry(self):
        had_exception = False
        while True:
            try:
                if self.telemetry_client is not None and self._collect_data:
                    self.telemetry_client.process_incoming_data()
                had_exception = False
            except Exception as ex:
                self._safely_close_telemetry_client()
                if not had_exception:
                    print("ERROR: " + str(ex) + "\n" + ''.join(traceback.format_tb(ex.__traceback__)))
                    had_exception = True
                    if self._collect_data:
                        self._connect_telemetry()


balancing_rover_telemetry = BalancingRoverTelemetry(ui_factory, pyros_client_app)
pyros_client_app.set_content(balancing_rover_telemetry)
pyros_client_app.start_discovery_in_background()

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()

        ui_adapter.process_event(event)
        if event.type == pygame.KEYDOWN:
            balancing_rover_telemetry.key_down(event.key)
        if event.type == pygame.KEYUP:
            balancing_rover_telemetry.key_up(event.key)

    pyros.loop(0.03)
    agent.keep_agents()

    ui_adapter.draw(ui_adapter.screen)

    ui_adapter.frame_end()
