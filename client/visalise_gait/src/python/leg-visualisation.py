import pygame
import math

from body import Leg, LegID
from path import ArcPath, LinearPath
from servo_lib.leg_servos import ServoDefinition
from servo_lib.servo_driver import ServoDriver
from vector import Vector3

pygame.init()
pygame.font.init()
myfont = pygame.font.SysFont("Exo-DemiBold.otf", 30)


screen_width = 1280
screen_height = 600
y_offset = 10
x_offset = screen_width // 2
multiplier = 4
leg_1 = 47
leg_2 = 58

maximum_leg_1_angle = math.pi
minimum_leg_1_angle = -math.pi * 2

maximum_leg_2_angle = -math.pi / 8
minimum_leg_2_angle = -math.pi + math.pi / 4 + math.pi / 8

max_radius = math.sqrt(leg_1 * leg_1 + leg_2 * leg_2 - 2 * leg_1 * leg_2 * math.cos(minimum_leg_2_angle))

alpha_1 = 0
alpha_2 = math.pi / 16

# Set up the drawing window
screen = pygame.display.set_mode([screen_width, screen_height])

# Run until the user asks to quit
running = True


class LocalServoDriver(ServoDriver):

    def __init__(self):
        super().__init__()
        self.servo_positions = [0] * 32
        self.servo_angles = [0.0] * 32
        self.servo_angle_deltas = [0.0] * 32

    def get_current_mA(self, servo: ServoDefinition) -> float:
        return 0

    def set_position(self, servo: ServoDefinition, position: int, power: int = 100) -> None:
        self.servo_positions[servo.servo_channel.value] = position

    def set_angle(self, servo: ServoDefinition, angle: float, power: int = 100) -> None:
        super().set_angle(servo, angle, power=power)

        i = servo.servo_channel.value
        self.servo_angle_deltas[i] = angle - self.servo_angles[i]
        self.servo_angles[i] = angle


def calculate_angles_old(l1, l2, x, y):
    l3 = math.sqrt(x * x + y * y) if x != 0 else y

    t = (l3 * l3 + l1 * l1 - l2 * l2) / (2 * l3 * l1)

    alpha_prim = math.acos(t) if l3 <= l1 + l2 and t >= 0 else 0.0
    alpha_second = math.atan2(x, y)
    alpha = alpha_prim - alpha_second

    x1 = -l1 * math.sin(alpha)
    y1 = l1 * math.cos(alpha)

    beta_prim = math.atan2(x - x1, y - y1)
    beta = beta_prim + alpha - math.pi / 2.0

    alpha += math.pi / 2
    beta += math.pi / 2
    beta = - beta

    return alpha, beta


def screen_pos(alpha_1, alpha_2, x_offset=0, y_offset=0):
    if alpha_1 < minimum_leg_1_angle:
        alpha_1 = minimum_leg_1_angle
    if alpha_1 > maximum_leg_1_angle:
        alpha_1 = maximum_leg_1_angle
    if alpha_2 < minimum_leg_2_angle:
        alpha_2 = minimum_leg_2_angle
    if alpha_2 > maximum_leg_2_angle:
        alpha_2 = maximum_leg_2_angle
    x1 = leg_1 * math.cos(alpha_1) * multiplier + x_offset
    y1 = leg_1 * math.sin(alpha_1) * multiplier + y_offset
    x2 = leg_2 * math.cos(alpha_1 + alpha_2) * multiplier + x1
    y2 = leg_2 * math.sin(alpha_1 + alpha_2) * multiplier + y1

    return x1, y1, x2, y2


def in_range(a, b, given_range=1):
    return a - given_range <= b <= a + given_range


shoulder_servo_definition = ServoDefinition("shoulder", 0)
thigh_servo_definition = ServoDefinition("thigh", 1)
shin_servo_definition = ServoDefinition("shin", 2)

servo_driver = LocalServoDriver()
# noinspection PyTypeChecker
leg = Leg(LegID.FRONT_RIGHT_LEG, servo_driver,
          shoulder_servo_definition, thigh_servo_definition, shin_servo_definition,
          leg_1, leg_2)


def calculate_angles_new(x, y):
    theta, alpha, beta = leg.calculate_angles(x, 0, y)
    alpha = math.radians(alpha)
    beta = math.radians(beta)

    alpha = -alpha
    alpha += math.pi / 2
    beta += math.pi / 2
    beta = - beta

    # alpha = -alpha - math.pi / 2.0
    # beta = -beta - math.pi / 2.0
    # beta = - beta

    return alpha, beta


# old_a, old_b = calculate_angles_old(leg_1, leg_2, 2, 8)
# new_a, new_b = calculate_angles_new(2, 8)
#
# print(f"Calc -old {math.degrees(old_a), math.degrees(old_b)}")
# print(f"Calc -new {math.degrees(new_a), math.degrees(new_b)}")


def calculate_angles(x, y):
    # return calculate_angles_old(leg_1, leg_2, x, y)
    return calculate_angles_new(x, y)


positions = pygame.Surface((screen_width, screen_height))

x1, y1, x2, y2 = screen_pos(alpha_1, alpha_2, x_offset, y_offset)


px = -x_offset
py = 1

ix = 0
iy = 0

mx = 0
my = 0

alpha_1, alpha_2 = 0.0, 0.0

by_mouse = True
by_path = False
by_angle = False

paths = [
    (ArcPath(arc_height=15, start=Vector3(0, 0, 0), end=Vector3(15, 0, 0)), 1),
    (LinearPath(start=Vector3(15, 0, 0), end=Vector3(0, 0, 0)), 0.08)
]


current_time = 0
current_path = paths[0][0]
current_paths_index = 0

body_position = Vector3(0, 0, 85)
leg.set_path(current_path, current_time, paths[0][1])


def progress_path():
    global current_path, current_time, current_paths_index

    current_time += 0.02
    leg.drive_leg(current_time, body_position)
    if leg.at_destination(current_time):
        current_paths_index += 1
        if current_paths_index >= len(paths):
            current_paths_index = 0
            current_time = 0
        current_path = paths[current_paths_index][0]
        if by_path:
            duration = paths[current_paths_index][1]
            leg.set_path(current_path, current_time, duration)
        else:
            leg.set_path(current_path, current_time, -1.0)

    return current_time


def progress_path_back():
    global current_path, current_time, current_paths_index

    if current_time > current_path.requested_time:
        current_time -= 0.02
        leg.drive_leg(current_time, body_position)


finished_drawing = False
initial_position = Vector3()

while running:
    # Did the user click the window close button?
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.MOUSEMOTION and by_mouse:
            mx = event.pos[0] if 0 <= event.pos[0] < screen_width else x_offset
            my = event.pos[1] if y_offset < event.pos[1] < screen_height else y_offset

            ix = (mx - x_offset) / multiplier
            iy = (my - y_offset) / multiplier

            alpha_1, alpha_2 = calculate_angles(ix, iy)
            x1, y1, x2, y2 = screen_pos(alpha_1, alpha_2, x_offset, y_offset)

            if not in_range(mx, x2, 2) or not in_range(my, y2, 2):
                x = x2 - x_offset
                y = y2 - y_offset
                if math.sqrt(x * x + y * y) > max_radius * multiplier:
                    while my > y_offset and (not in_range(mx, x2, 2) or not in_range(my, y2, 2)):
                        my -= 1
                        iy = (my - y_offset) / multiplier
                        alpha_1, alpha_2 = calculate_angles(ix, iy)
                        x1, y1, x2, y2 = screen_pos(alpha_1, alpha_2, x_offset, y_offset)
                else:
                    while my < max_radius * multiplier and (not in_range(mx, x2, 2) or not in_range(my, y2, 2)):
                        my += 1
                        iy = (my - y_offset) / multiplier
                        alpha_1, alpha_2 = calculate_angles(ix, iy)
                        x1, y1, x2, y2 = screen_pos(alpha_1, alpha_2, x_offset, y_offset)
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_m and not by_mouse:
                by_mouse = True
                by_path = False
                by_angle = False
            elif event.key == pygame.K_p and not by_path:
                by_mouse = False
                by_path = True
                by_angle = False
                leg.set_path(current_path, current_time, paths[0][1])
            elif event.key == pygame.K_a and not by_angle:
                by_mouse = False
                by_path = False
                by_angle = True
                leg.set_path(current_path, current_time, -1.0)
            elif not by_mouse and event.key == pygame.K_RIGHT:
                progress_path()
            elif not by_mouse and event.key == pygame.K_LEFT:
                progress_path_back()

    if py < screen_height - y_offset:
        for i in range(1000):
            if py < screen_height - y_offset:
                palpha_1, palpha_2 = calculate_angles(px / multiplier, py / multiplier)

                px1, py1, px2, py2 = screen_pos(palpha_1, palpha_2)

                if in_range(px, px2, 2) and in_range(py, py2, 2):
                    positions.set_at((px + x_offset, py + y_offset), (0, 128, 0))
                else:
                    positions.set_at((px + x_offset, py + y_offset), (64, 0, 0))

                px += 1
                if px >= x_offset:
                    px = - x_offset
                    py += 1

    if not by_mouse:
        alpha_1 = servo_driver.servo_angles[thigh_servo_definition.servo_channel.value]
        alpha_2 = servo_driver.servo_angles[shin_servo_definition.servo_channel.value]

        alpha_1 = -math.radians(alpha_1 - 90)
        alpha_2 = -math.radians(90 + alpha_2)
        x1, y1, x2, y2 = screen_pos(alpha_1, alpha_2, x_offset, y_offset)

    screen.blit(positions, (0, 0))

    if not by_mouse:
        initial_position.clear()
        for path_duration in paths:
            path = path_duration[0]
            color = (255, 255, 0) if path == current_path else (200, 200, 200)

            if isinstance(path, ArcPath):
                px1 = initial_position.x + body_position.x
                px2 = path.end.x + body_position.x
                py1 = initial_position.z - path.arc_height + body_position.z
                py2 = initial_position.z + path.arc_height + body_position.z
                px1 = px1 * multiplier + x_offset
                px2 = px2 * multiplier + x_offset
                py1 = py1 * multiplier + y_offset
                py2 = py2 * multiplier + y_offset
                pygame.draw.arc(screen, color, pygame.Rect(px1, py1, px2 - px1, py2 - py1), 0, math.pi)
            elif isinstance(path, LinearPath):
                px1 = initial_position.x + body_position.x
                px2 = path.end.x + body_position.x
                py1 = initial_position.z + body_position.z
                py2 = initial_position.z + body_position.z
                px1 = px1 * multiplier + x_offset
                px2 = px2 * multiplier + x_offset
                py1 = py1 * multiplier + y_offset
                py2 = py2 * multiplier + y_offset
                pygame.draw.line(screen, color, (px1, py1), (px2, py2))

            initial_position.set(path.end)

    pygame.draw.line(screen, (0, 0, 255), (x_offset, y_offset), (x1, y1))
    pygame.draw.line(screen, (0, 0, 255), (x1, y1), (x2, y2))

    if by_mouse:
        screen.blit(myfont.render(f"M: (x, y)=({ix:.1f}, {iy:.1f}),  "
                                  f"height: {int((y2 - y_offset) / multiplier)}, "
                                  f"(a1, a2)=({math.degrees(alpha_1):.0f}, {math.degrees(alpha_2):.0f})", False, (255, 255, 255)), (0, 5))
    elif by_path:
        screen.blit(myfont.render(f"P: (x, y)=({leg.position.x:.1f}, {leg.position.z:.1f}), "
                                  f" height: {int((y2 - y_offset) / multiplier)}, (a1, a2)=({math.degrees(alpha_1):.0f}, {math.degrees(alpha_2):.0f}) ",
                                  False, (255, 255, 255)), (0, 5))
        screen.blit(myfont.render(f"∆˚: ({servo_driver.servo_angle_deltas[1]:.1f}, {servo_driver.servo_angle_deltas[2]:.1f}), "
                                  f"t: {current_time:0.01f}, "
                                  f"factor: {current_path.current_relative_position_in_path(current_time):0.01f}",
                                  False, (255, 255, 255)), (0, 30))
    elif by_angle:
        screen.blit(myfont.render(f"A: (x, y)=({leg.position.x:.1f}, {leg.position.z:.1f}), "
                                  f" height: {int((y2 - y_offset) / multiplier)}, (a1, a2)=({math.degrees(alpha_1):.0f}, {math.degrees(alpha_2):.0f}) ",
                                  False, (255, 255, 255)), (0, 5))
        screen.blit(myfont.render(f"∆˚: ({servo_driver.servo_angle_deltas[1]:.1f}, {servo_driver.servo_angle_deltas[2]:.1f}), "
                                  f"t: {current_time:0.01f}, "
                                  f"factor: {current_path.current_relative_position_in_path(current_time):0.01f} "
                                  f"max ˚ {7200 / thigh_servo_definition.speed.value}",
                                  False, (255, 255, 255)), (0, 30))

    # Flip the display
    pygame.display.flip()

# Done! Time to quit.
pygame.quit()
