import math
from typing import Tuple

from servo_lib.leg_servos import ServoDefinition


class Leg:
    def __init__(self,
                 thigh_len: float,
                 shin_len: float) -> None:

        self.thigh_len = thigh_len
        self.shin_len = shin_len

    def calculate_angles(self, x: float, y: float, z: float) -> Tuple[float, float, float]:
        l1 = self.thigh_len
        l2 = self.shin_len

        l3 = math.sqrt(x * x + z * z) if x != 0 else z

        t = (l3 * l3 + l1 * l1 - l2 * l2) / (2 * l3 * l1)

        alpha_prim = math.acos(t) if l3 <= l1 + l2 and t >= 0 else 0.0
        alpha_second = math.atan2(x, z)
        thigh_angle = alpha_prim - alpha_second

        x1 = -l1 * math.sin(thigh_angle)
        y1 = l1 * math.cos(thigh_angle)

        beta_prim = math.atan2(x - x1, z - y1)
        shin_angle = beta_prim + thigh_angle - math.pi / 2.0

        shoulder_angle = 0.0
        thigh_angle = -thigh_angle
        # thigh_angle -= math.pi / 2
        # shin_angle -= math.pi / 2

        return math.degrees(shoulder_angle), math.degrees(thigh_angle), math.degrees(shin_angle)

    def move(self, x: float, y: float, z: float) -> None:
        shoulder_angle, thigh_angle, shin_angle = self.calculate_angles(x, y, z)

        print(f" ({x:4.1f}, {y:4.1f}, {z:4.1f})={shoulder_angle:6.2f}, {thigh_angle:6.2f}, {shin_angle:6.2f}")


if __name__ == "__main__":

    leg = Leg(4.7, 5.8)

    print(f"Straight: ", end="")
    leg.move(0, 0, 5.8 + 4.7)

    print(f"Straight: ", end="")
    leg.move(0, 0, 5.8 + 4.6)

    x_len = 20
    step = 5

    print("X: ----------------------------")
    for x in range(-x_len, x_len+step, step):
        print(f"x:{x:5}: ", end="")
        leg.move(x / 10, 0, 5.0)

    y_len = 30
    print("Y: ----------------------------")
    for y in range(-y_len, y_len+step, step):
        ny = 7.5 + y / 10

        print(f"y:{ny:5}: ", end="")
        leg.move(0, 0, ny)
