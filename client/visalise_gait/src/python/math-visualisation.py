import pygame
import math

from body import Leg, LegID
from path import ArcPath, LinearPath
from servo_lib.leg_servos import ServoDefinition
from servo_lib.servo_driver import ServoDriver
from vector import Vector3

pygame.init()
pygame.font.init()
myfont = pygame.font.SysFont("Exo-DemiBold.otf", 30)


screen_width = 1280
screen_height = 600
y_offset = screen_height // 2
x_offset = screen_width // 2

multiplier = 4

leg_1 = 47
leg_2 = 58

maximum_leg_1_angle = math.pi
minimum_leg_1_angle = -math.pi * 2

maximum_leg_2_angle = -math.pi / 8
minimum_leg_2_angle = -math.pi + math.pi / 4 + math.pi / 8

max_radius = math.sqrt(leg_1 * leg_1 + leg_2 * leg_2 - 2 * leg_1 * leg_2 * math.cos(minimum_leg_2_angle))

alpha_1 = 0
alpha_2 = math.pi / 16

# Set up the drawing window
screen = pygame.display.set_mode([screen_width, screen_height])

# Run until the user asks to quit
running = True


class LocalServoDriver(ServoDriver):

    def __init__(self):
        super().__init__()
        self.servo_positions = [0] * 32
        self.servo_angles = [0.0] * 32
        self.servo_angle_deltas = [0.0] * 32

    def get_current_mA(self, servo: ServoDefinition) -> float:
        return 0

    def set_position(self, servo: ServoDefinition, position: int, power: int = 100) -> None:
        self.servo_positions[servo.servo_channel.value] = position

    def set_angle(self, servo: ServoDefinition, angle: float, power: int = 100) -> None:
        super().set_angle(servo, angle, power=power)

        i = servo.servo_channel.value
        self.servo_angle_deltas[i] = angle - self.servo_angles[i]
        self.servo_angles[i] = angle


def screen_pos(alpha_1, alpha_2, x_offset=0, y_offset=0):
    if alpha_1 < minimum_leg_1_angle:
        alpha_1 = minimum_leg_1_angle
    if alpha_1 > maximum_leg_1_angle:
        alpha_1 = maximum_leg_1_angle
    if alpha_2 < minimum_leg_2_angle:
        alpha_2 = minimum_leg_2_angle
    if alpha_2 > maximum_leg_2_angle:
        alpha_2 = maximum_leg_2_angle
    x1 = leg_1 * math.cos(alpha_1) * multiplier + x_offset
    y1 = leg_1 * math.sin(alpha_1) * multiplier + y_offset
    x2 = leg_2 * math.cos(alpha_1 + alpha_2) * multiplier + x1
    y2 = leg_2 * math.sin(alpha_1 + alpha_2) * multiplier + y1

    return x1, y1, x2, y2


def in_range(a, b, given_range=1):
    return a - given_range <= b <= a + given_range


shoulder_servo_definition = ServoDefinition("shoulder", 0)
thigh_servo_definition = ServoDefinition("thigh", 1)
shin_servo_definition = ServoDefinition("shin", 2)

servo_driver = LocalServoDriver()
# noinspection PyTypeChecker
leg = Leg(LegID.FRONT_RIGHT_LEG, servo_driver,
          shoulder_servo_definition, thigh_servo_definition, shin_servo_definition,
          leg_1, leg_2)


def calculate_angles_new(x, y):
    theta, alpha, beta = leg.calculate_angles(x, 0, y)
    alpha = math.radians(alpha)
    beta = math.radians(beta)

    alpha = -alpha - math.pi / 2.0
    beta = -beta - math.pi / 2.0

    return alpha, beta


def calculate_angles(x, y):
    # return calculate_angles_old(leg_1, leg_2, x, y)
    return calculate_angles_new(x, y)


# positions = pygame.Surface((screen_width, screen_height))

paths = [
    (ArcPath(arc_height=15, start=Vector3(0, 0, 0), end=Vector3(15, 0, 0)), -1),
    (LinearPath(start=Vector3(15, 0, 0), end=Vector3(0, 0, 0)), -0.08)
]


error1 = ""
error2 = ""


current_time = 0
current_path = paths[0][0]
current_paths_index = 0

body_position = Vector3(0, 0, 85)
leg.set_path(current_path, current_time, 1.0)

leg.drive_leg(current_time, body_position)

current_time += 0.02
try:
    leg.drive_leg(current_time, body_position)
except ValueError as e:
    error1 = str(e)

x0 = 7.5
y0 = 0

cc1_x = leg.cc_x
cc1_y = leg.cc_y
cc1_r = leg.cc_r


leg.requested_thigh_angle -= 5
try:
    leg.drive_leg(current_time, body_position)
except ValueError as e:
    error2 = str(e)

cc2_x = leg.cc_x
cc2_y = leg.cc_y
cc2_r = leg.cc_r

initial_position = Vector3()

while running:
    # Did the user click the window close button?
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

    # screen.blit(positions, (0, 0))
    screen.fill((0, 0, 0))

    pygame.draw.line(screen, (0, 128,0), (0, y_offset), (screen_width, y_offset))
    pygame.draw.line(screen, (0, 128, 0), (x_offset, 0), (x_offset, screen_height))

    initial_position.clear()
    for path_duration in paths:
        path = path_duration[0]
        color = (255, 255, 0) if path == current_path else (200, 200, 200)

        if isinstance(path, ArcPath):
            px1 = initial_position.x - x0 # + body_position.x
            px2 = path.end.x - x0  # + body_position.x
            py1 = initial_position.z - path.arc_height  # + body_position.z
            py2 = initial_position.z + path.arc_height  # + body_position.z
            px1 = px1 * multiplier + x_offset
            px2 = px2 * multiplier + x_offset
            py1 = py1 * multiplier + y_offset
            py2 = py2 * multiplier + y_offset
            pygame.draw.arc(screen, color, pygame.Rect(px1, py1, px2 - px1, py2 - py1), 0, math.pi)
        elif isinstance(path, LinearPath):
            px1 = initial_position.x - x0  # + body_position.x
            px2 = path.end.x - x0  # + body_position.x
            py1 = initial_position.z  # + body_position.z
            py2 = initial_position.z  # + body_position.z
            px1 = px1 * multiplier + x_offset
            px2 = px2 * multiplier + x_offset
            py1 = py1 * multiplier + y_offset
            py2 = py2 * multiplier + y_offset
            pygame.draw.line(screen, color, (px1, py1), (px2, py2))

        initial_position.set(path.end)

    pygame.draw.circle(screen, (128, 128, 128), (cc2_x * multiplier + x_offset, cc2_y * multiplier + y_offset), 2, width=1)
    pygame.draw.circle(screen, (128, 128, 128), (cc2_x * multiplier + x_offset, cc2_y * multiplier + y_offset), cc2_r * multiplier, width=1)
    pygame.draw.circle(screen, (255, 255, 255), (cc1_x * multiplier + x_offset, cc1_y * multiplier + y_offset), 2, width=1)
    pygame.draw.circle(screen, (255, 255, 255), (cc1_x * multiplier + x_offset, cc1_y * multiplier + y_offset), cc1_r * multiplier, width=1)

    screen.blit(myfont.render(f"E: {error1}", False, (255, 255, 255)), (0, 5))
    screen.blit(myfont.render(f"E: {error2}", False, (255, 255, 255)), (0, 35))

    # Flip the display
    pygame.display.flip()

# Done! Time to quit.
pygame.quit()
