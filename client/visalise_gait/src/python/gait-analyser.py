import time
from typing import Tuple, Optional

import math

import pygame

from body import Body, Leg, LegID
from gait.gait_manager_implementation import GaitManagerImpl
from gait_service_main import DEFAULT_THIGH_LEN, DEFAULT_SHIN_LEN, DEFAULT_DISTANCE_Y, DEFAULT_DISTANCE_X
from servo_lib.leg_servos import ServoDefinition, LegServos
from servo_lib.servo_driver import ServoDriver

pygame.init()
pygame.font.init()
myfont = pygame.font.SysFont("Exo-DemiBold.otf", 30)

NUMBER_OF_PREV_LEG_POSITIONS = 10

SPEEDS = {
    # 0: (None, 0.2),
    0.2: (None, 0.5),
    0.5: (0.2, 1),
    1: (0.5, 1.5),
    1.5: (1, 2),
    2: (1.5, 5), 5: (2, 10), 10: (5, 20), 20: (10, 30), 30: (20, 40), 40: (30, 50),
    50: (40, 75), 75: (50, 100), 100: (75, 150), 150: (100, 200), 200: (150, 250), 250: (200, 300), 300: (250, None)
}

DISTANCES = {
    -100000: (None, -1000), -1000: (-100000, -500), -500: (-1000, -250), -250: (-500, -150), -150: (-250, -100), -100: (-150, -50), -50: (-100, 0),
    0: (-50, 50),
    50: (0, 100), 100: (50, 150), 150: (100, 250), 250: (150, 500), 500: (250, 1000), 1000: (500, 100000), 100000: (1000, None)
}


class LocalServoDriver(ServoDriver):

    def __init__(self):
        super().__init__()
        self.servo_positions = [0] * 32
        self.servo_angles = [0.0] * 32
        self.servo_angle_deltas = [0.0] * 32

    def get_current_mA(self, servo: ServoDefinition) -> float:
        return 0

    def set_position(self, servo: ServoDefinition, position: int, power: int = 100) -> None:
        self.servo_positions[servo.servo_channel.value] = position

    def set_angle(self, servo: ServoDefinition, angle: float, power: int = 100) -> None:
        super().set_angle(servo, angle, power=power)

        i = servo.servo_channel.value
        self.servo_angle_deltas[i] = angle - self.servo_angles[i]
        self.servo_angles[i] = angle


class GaitAnalyser:
    def __init__(self) -> None:
        self.screen_width = 1280
        self.screen_height = 600
        self.y_offset = self.screen_height // 2
        self.x_offset = self.screen_width // 2

        self.ratio = 3
        self.minimum_x_body_offset = 10

        self.x_o: float = 0
        self.y_o: float = 0

        # Set up the drawing window
        self.screen = pygame.display.set_mode([self.screen_width, self.screen_height])

        # Run until the user asks to quit
        self.running = True
        self.current_time = 0
        self.do_progress = False

        self.leg_pos = {
            LegID.FRONT_LEFT_LEG: [],
            LegID.FRONT_RIGHT_LEG: [],
            LegID.BACK_RIGHT_LEG: [],
            LegID.BACK_LEFT_LEG: []
        }

        # self.shoulder_servo_definition = ServoDefinition("shoulder", 0)
        # self.thigh_servo_definition = ServoDefinition("thigh", 1)
        # self.shin_servo_definition = ServoDefinition("shin", 2)

        self.servo_driver = LocalServoDriver()
        # noinspection PyTypeChecker
        # leg = Leg(LegID.FRONT_RIGHT_LEG, servo_driver,
        #           shoulder_servo_definition, thigh_servo_definition, shin_servo_definition,
        #           leg_1, leg_2)

        self.leg_servos = LegServos()

        self.body = Body(self.leg_servos, self.servo_driver,
                         DEFAULT_THIGH_LEN, DEFAULT_SHIN_LEN,
                         DEFAULT_DISTANCE_X, DEFAULT_DISTANCE_Y)

        self.calc_legs()

        self.gait_manager = GaitManagerImpl(self.body)
        self.gait_manager.register_known_gaits()

        self.known_gaits_details = {
            "cat_walk_dp4": { "stride": 40, "speed": 2, "angle": 0.0, "distance": 100000, "tx": 0.4, "cx": 15, "cy": 10, "ox": 0, "cog_a": 0.9 },
            "cat_walk_sp6": { "stride": 30, "speed": 2, "angle": 0.0, "distance": 100000, "tx": 0.4 },
            "cat_walk_sp4": { "stride": 30, "speed": 2, "angle": 0.0, "distance": 100000 },
            "trot": { "stride": 50, "speed": 2, "angle": 0.0, "distance": 100000 },
        }
        self.known_gaits = [k for k in self.known_gaits_details]
        self.selected_gait_index = 0
        self.prepared_gait_index: Optional[int] = None
        self.reset_gait()

        self.show_trail = True
        self.playback_speed = .2
        self.last_played = 0
        self.play = False
        self.time_step = 0.1

        self.background = pygame.Surface((self.screen_width, self.screen_height))

    def reset_gait(self) -> None:
        self.play = False
        print(f"***** RESETING GAIT *****")
        self.body.body_position.x = 0
        self.body.body_position.y = 0
        self.body.body_position.z = 0
        self.body.body_position_path = None
        for leg in self.body:
            leg.position.x = 0
            leg.position.y = 0
            leg.position.z = 0
            leg.path = None
        self.gait_manager.reset()
        selected_gait_name = self.known_gaits[self.selected_gait_index]
        selected_gait_context = self.known_gaits_details[selected_gait_name]

        self.gait_manager.select_gait(selected_gait_name, 0, selected_gait_context)
        self.current_time = 0
        for leg_positions in self.leg_pos.values():
            del leg_positions[:]
        self.calc_legs()

    def adjust_x(self, x: float) -> float:
        return (x + self.body.body_position.x) * self.ratio + self.x_offset

    def adjust_y(self, y: float) -> float:
        return (y + self.body.body_position.y) * self.ratio + self.y_offset

    def draw_body(self) -> None:
        x = self.adjust_x(-self.body.body_position.x - self.body.distance_x // 2)
        y = self.adjust_y(-self.body.body_position.y - self.body.distance_y // 2)
        grey = (160, 160, 160)
        pygame.draw.circle(self.screen,
                           grey,
                           (x + (self.body.distance_x // 2) * self.ratio, y + (self.body.distance_y // 2) * self.ratio),
                           7, width=1)
        pygame.draw.rect(self.screen, grey, pygame.Rect(x, y, self.body.distance_x * self.ratio, self.body.distance_y * self.ratio), width=1)

        pygame.draw.line(self.screen, grey, (self.x_offset + (self.body.distance_x // 2 - 20) * self.ratio, self.y_offset), (self.x_offset + (self.body.distance_x // 2 + 20) * self.ratio, self.y_offset))
        pygame.draw.line(self.screen, grey, (self.x_offset + self.body.distance_x // 2 * self.ratio, self.y_offset + 10), (self.x_offset + (self.body.distance_x // 2 + 20) * self.ratio, self.y_offset))
        pygame.draw.line(self.screen, grey, (self.x_offset + self.body.distance_x // 2 * self.ratio, self.y_offset - 10), (self.x_offset + (self.body.distance_x // 2 + 20) * self.ratio, self.y_offset))

    def leg_to_abs(self, leg: Leg) -> Tuple[float, float, float]:
        x = leg.position.x + (self.body.distance_x // 2 if leg.leg_id.is_front else -self.body.distance_x // 2)
        y = leg.position.y + (self.body.distance_y // 2 if leg.leg_id.is_right else -self.body.distance_y // 2)
        z = leg.position.z + self.body.body_position.z
        return x, y, z

    def leg_to_screen(self, leg: Leg) -> Tuple[float, float, float]:
        x, y, z = self.leg_to_abs(leg)
        x = self.adjust_x(x)
        y = self.adjust_y(y)
        z = leg.position.z + self.body.body_position.z
        return x, y, z

    def leg_z_position(self, z: float) -> int:
        if math.isclose(z, self.body.body_position.z, rel_tol=0.1):
            return 0
        elif z > self.body.body_position.z:
            return 1
        return -1

    def calc_legs(self) -> None:
        for leg in self.body:
            x, y, z = self.leg_to_screen(leg)
            leg_positions = self.leg_pos[leg.leg_id]
            leg_positions.append((x, y, z))
            while len(leg_positions) > NUMBER_OF_PREV_LEG_POSITIONS:
                del leg_positions[0]

    def draw_legs(self) -> None:
        def lerp(max_value: int, min_value: int, steps: int, current_value: int) -> int:
            return int((max_value - min_value) * current_value / steps) + min_value

        for leg_positions in self.leg_pos.values():
            l = len(leg_positions)
            l = l if l > 1 else 2
            for i, leg_position in enumerate(leg_positions):
                last = (i == len(leg_positions) - 1)
                if self.show_trail or last:
                    x, y, z = leg_position
                    leg_z_pos = self.leg_z_position(z)

                    if last:
                        w = 255
                    else:
                        w = lerp(200, 0, l - 1, i)
                    if leg_z_pos == 0:
                        color = (w, w, w)
                    elif leg_z_pos > 0:
                        color = (w, 0, 0)
                    else:
                        color = (w, w, 0)
                    pygame.draw.circle(self.screen, color, (x, y), 10)

    def calculate_cog_colour(self) -> Tuple[int, int, int]:
        def sign(f: float) -> int:
            if math.isclose(f, 0.0, rel_tol=0.001):
                return 0
            elif f < 0.0:
                return -1
            return 1

        leg_in_air = [leg for leg in self.body if self.leg_z_position(leg.position.z + self.body.body_position.z) != 0][0]
        near_legs = [leg for leg in self.body
                     if leg.leg_id == leg_in_air.leg_id.same_side_leg()
                         or leg.leg_id == leg_in_air.leg_id.opposite_side_leg()]

        x1, y1, z1 = self.leg_to_abs(near_legs[0])
        x2, y2, z2 = self.leg_to_abs(near_legs[1])
        x3, y3, z3 = self.leg_to_abs(leg_in_air)

        self.x_o = - y1 * (x2 - x1) / (y2 - y1) + x1 + self.body.body_position.x
        self.y_o = self.body.body_position.y

        if abs(self.x_o) > self.minimum_x_body_offset and sign(self.x_o) == sign(x3):
            return (0, 64, 0)
        else:
            return (64, 0, 0)

    def draw_leg_poligon(self) -> None:
        poligon = []
        for leg_id in self.leg_pos:
            leg = self.body.legs[leg_id]
            x, y, z = self.leg_to_screen(leg)
            if self.leg_z_position(z) == 0:
                poligon.append((x, y))

        if len(poligon) < 3:
            color = (64, 0, 0)
            self.x_o = None
        elif len(poligon) == 3:
            color = self.calculate_cog_colour()
        else:
            self.x_o = None
            color = (0, 32, 0)
        if len(poligon) > 2:
            pygame.draw.polygon(self.screen, color, poligon)
            if len(poligon) == 3:
                cog_x = sum([x for x, y in poligon]) / 3
                cog_y = sum([y for x, y in poligon]) / 3
                pygame.draw.circle(self.screen, (160, 160, 0), (cog_x, cog_y), 4)

    def udpate_current_context(self, context: dict) -> None:
        self.gait_manager.update_current_gait_context(self.current_time, context)

    def handle_generic_key(self,
                           prop: str,
                           context: dict,
                           key: int,
                           up_key: int, down_key: int,
                           is_int: bool = False,
                           step: Optional[float] = None, min_value: Optional[float] = None, max_value: Optional[float] = None,
                           values: Optional[dict] = None) -> None:
        if prop in context:
            if key == up_key:
                value = context[prop]
                if step is not None and min_value is not None and max_value is not None:
                    value = (value + step) if value + step < max_value else max_value
                if values is not None:
                    next_value = values[value][1]
                    if next_value is not None:
                        value = next_value
                if is_int:
                    value = int(value)
                if context[prop] != value:
                    context[prop] = value
                    self.udpate_current_context(context)
            elif key == down_key:
                value = context[prop]
                if step is not None and min_value is not None and max_value is not None:
                    value = (value - step) if value - step > min_value else min_value
                if values is not None:
                    next_value = values[value][0]
                    if next_value is not None:
                        value = next_value
                if is_int:
                    value = int(value)
                if context[prop] != value:
                    context[prop] = value
                    self.udpate_current_context(context)

    def display_gait_name(self) -> None:
        if self.prepared_gait_index is not None:
            gait_name = self.known_gaits[self.prepared_gait_index]
            color = (255, 128, 0)
        else:
            gait_name = self.known_gaits[self.selected_gait_index]
            color = (255, 255, 255)
        gait_name = "G: " + gait_name
        text_width = myfont.size(gait_name)[0]
        self.screen.blit(myfont.render(gait_name, False, color), (self.screen_width - text_width - 5, 5))

    def progress(self) -> None:
        self.body.drive(self.current_time)
        self.gait_manager.process(self.current_time)
        self.calc_legs()

    def start(self) -> None:
        while self.running:
            now = time.time()

            # Did the user click the window close button?
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.running = False
                elif event.type == pygame.MOUSEMOTION:
                    mx = event.pos[0] if 0 <= event.pos[0] < self.screen_width else self.x_offset
                    my = event.pos[1] if self.y_offset < event.pos[1] < self.screen_height else self.y_offset

                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_r:
                        self.reset_gait()
                    if event.key == pygame.K_RIGHT:
                        self.current_time += self.time_step
                        self.do_progress = True
                    if event.key == pygame.K_LEFT:
                        self.current_time -= self.time_step
                        self.do_progress = True
                    if event.key == pygame.K_UP:
                        if self.prepared_gait_index is None: self.prepared_gait_index = self.selected_gait_index
                        self.prepared_gait_index -= 1
                        if self.prepared_gait_index < 0:
                            self.prepared_gait_index = len(self.known_gaits) - 1
                    if event.key == pygame.K_DOWN:
                        if self.prepared_gait_index is None: self.prepared_gait_index = self.selected_gait_index
                        self.prepared_gait_index += 1
                        if self.prepared_gait_index >= len(self.known_gaits):
                            self.prepared_gait_index = 0
                    if event.key == pygame.K_RETURN:
                        if self.prepared_gait_index is not None:
                            if self.selected_gait_index != self.prepared_gait_index:
                                self.selected_gait_index = self.prepared_gait_index
                                selected_gait_name = self.known_gaits[self.selected_gait_index]
                                selected_gait_context = self.known_gaits_details[selected_gait_name]
                                self.gait_manager.select_gait(selected_gait_name, 0, selected_gait_context)

                            self.prepared_gait_index = None

                    if event.key == pygame.K_t:
                        self.show_trail = not self.show_trail
                    if event.key == pygame.K_0:
                        self.playback_speed += 0.1
                    if event.key == pygame.K_9 and self.playback_speed >= 0.1:
                        self.playback_speed -= 0.1
                    if event.key == pygame.K_SPACE:
                        self.play = not self.play

                    current_gait_name = self.known_gaits[self.selected_gait_index]
                    current_gait_context = self.known_gaits_details[current_gait_name]
                    self.handle_generic_key("angle", current_gait_context, event.key, pygame.K_d, pygame.K_a, step=22.5, min_value=-180, max_value=180)
                    self.handle_generic_key("stride", current_gait_context, event.key, pygame.K_c, pygame.K_z, step=5, min_value=0, max_value=100)
                    self.handle_generic_key("speed", current_gait_context, event.key, pygame.K_w, pygame.K_s, values=SPEEDS)
                    self.handle_generic_key("distance", current_gait_context, event.key, pygame.K_e, pygame.K_q, values=DISTANCES)
                    self.handle_generic_key("tx", current_gait_context, event.key, pygame.K_2, pygame.K_1, step=0.1, min_value=0, max_value=1)

            if self.do_progress:
                self.progress()
                self.do_progress = False
            if self.play:
                if self.last_played + self.playback_speed < now:
                    self.last_played = now
                    self.current_time += self.time_step
                    self.progress()

            self.screen.blit(self.background, (0, 0))

            pygame.draw.line(self.screen, (0, 128,0), (0, self.y_offset), (self.screen_width, self.y_offset))
            pygame.draw.line(self.screen, (0, 128, 0), (self.x_offset, 0), (self.x_offset, self.screen_height))

            self.draw_leg_poligon()
            self.draw_legs()
            self.draw_body()

            current_gait_name = self.known_gaits[self.selected_gait_index]
            current_gait_context = self.known_gaits_details[current_gait_name]

            x_o_str = f"{self.x_o:.1f}" if self.x_o is not None else ""

            play_status = "PLAY > " if self.play else "STOP | "

            self.screen.blit(myfont.render(f"{play_status} "
                                           f", time: {self.current_time:.2f}"
                                           # f", ts: {self.time_step:.2f}"
                                           # f", ps: {self.playback_speed:.1f}"
                                           f", x_o: {x_o_str}",
                                           False, (255, 255, 255)), (0, 5))
            self.display_gait_name()

            speed = f"{current_gait_context['speed']:.1f}" if "speed" in current_gait_context else "-"
            angle = f"{current_gait_context['angle']:.1f}" if "angle" in current_gait_context else "-"
            stride = f"{current_gait_context['stride']:.0f}" if "stride" in current_gait_context else "-"
            height = f"{current_gait_context['height']:.0f}" if "height" in current_gait_context else "-"
            tx = f"{current_gait_context['tx']:.1f}" if "tx" in current_gait_context else "-"
            distance = "-"
            if "distance" in current_gait_context:
                distance = current_gait_context["distance"]
                if distance >= 8000:
                    distance = "inf"
                elif distance <= -8000:
                    distance = "-inf"
                else:
                    distance = str(int(distance))

            self.screen.blit(myfont.render(f"speed: {speed}"
                                           f" ,height: {height}"
                                           f" ,stride: {stride}"
                                           f", angle: {angle}"
                                           f", distance: {distance}"
                                           f", tx: {tx}"
                                           ,
                                           False, (255, 255, 255)), (0, 25))

            if self.x_o is not None:
                self.screen.blit(myfont.render(f"cog_x,y: {self.x_o:.2f}, {self.y_o:.2f}"
                                               ,
                                               False, (255, 255, 255)), (0, 45))

            # Flip the display
            pygame.display.flip()

        # Done! Time to quit.
        pygame.quit()


gait_analyser = GaitAnalyser()
gait_analyser.start()
