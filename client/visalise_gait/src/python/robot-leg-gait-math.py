
import time
import math


# a = driver.find_by_class..()
# now = time.time()
#
# while time.time() < now + 5 and a is None:
#     scroll_down...
#     a = driver.find_by_class..()

pi_half = math.pi / 2.0


def calculate_angles(l1, l2, x, y):
    def deg(a):
        return a * 180 / math.pi

    l3 = math.sqrt(x * x + y * y) if x != 0 else y

    t = (l3 * l3 + l1 * l1 - l2 * l2) / (2 * l3 * l1)

    alpha_prim = math.acos(t) if l3 < l1 + l2 else 0.0
    alpha_second = math.atan2(x, y)
    alpha = alpha_prim - alpha_second

    x1 = -l1 * math.sin(alpha)
    y1 = l1 * math.cos(alpha)

    beta_prim = math.atan2(x - x1, y - y1)
    beta = beta_prim + alpha - math.pi / 2.0

    return deg(alpha), deg(beta), x1, y1, deg(alpha_prim), deg(beta_prim)


# print(calculate_angles(4.7, 5.8, 0, 9))
# print(calculate_angles(6, 6, 0, 6 * math.sqrt(2)))
for i in range(60, 121):
    print(calculate_angles(6, 6, 0, i / 10.0))

# for x in range(-5, 5):
#     print(calculate_angles(4.7, 5.8, x, 9))

