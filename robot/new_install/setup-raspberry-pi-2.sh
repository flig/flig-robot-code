#!/bin/bash

RPI_HOST=$1

echo "Note: It is worth adding ssh keys this script can run without need for entering password each time it is needed."
echo ""

if [[ -z "$RPI_HOST" ]]; then
  echo "Please use: setup-raspberry-pi-2.sh <hostname-or-ipaddress>"
  echo ""
  echo "This script assumes user is 'pi'"
  exit 1
fi

echo "Using raspberry pi '$RPI_HOST'"


echo "*** Updating /boot/config.txt and /boot/cmdline.txt (speed of i2c and otg)"
ssh $RPI_HOST "sudo python3 <<EOF
import os

with open('/boot/config.txt', 'rt') as f: config = f.read()
if os.path.exists('/boot/config.bak'): os.remove('/boot/config.bak')
os.rename('/boot/config.txt', '/boot/config.bak')

has_i2c = False
has_otg = False
with open('/boot/config.txt', 'wt') as f:
    for line in config.split('\n'):
        if line.startswith('dtparam=i2c'):
            f.write('dtparam=i2c_arm=on,i2c_arm_baudrate=400000\n')
            has_i2c = True
        elif line.startswith('dtoverlay=dwc2'):
            f.write('dtoverlay=dwc2\n')
            has_otg = True
        else:
            f.write(line)
            f.write('\n')
    if not has_i2c: f.write('dtparam=i2c_arm=on,i2c_arm_baudrate=400000\n')
    if not has_otg: f.write('dtoverlay=dwc2\n')

with open('/boot/cmdline.txt', 'rt') as f: cmdline = f.read()
if os.path.exists('/boot/cmdline.bak'): os.remove('/boot/cmdline.bak')
os.rename('/boot/cmdline.txt', '/boot/cmdline.bak')

if not 'modules-load=dwc2' in cmdline:
    i = (cmdline.index('rootwait') + 8) if 'rootwait' in cmdline else len(cmdline) - 1
    otg = ' modules-load=dwc2,g_ether g_ether.host_addr=00:22:82:ff:ff:01 g_ether.dev_addr=00:22:82:ff:ff:11 '
    cmdline = cmdline[:i] + otg + cmdline[i:]

with open('/boot/cmdline.txt', 'wt') as f:
    f.write(cmdline)

EOF
"

echo "*** Updating /etc/dhcpcd.conf"
ssh $RPI_HOST "sudo cat /etc/dhcpcd.conf | ( ! grep -q -c usb0) && sudo echo >>/etc/dhcpcd.conf -e 'interface usb0\nstatic ip_address=10.0.11.2\n'"

echo "*** Installing system packages:"
ssh $RPI_HOST "sudo apt-get update && sudo apt-get install -y git mosquitto python3-venv python3-pip python3-smbus i2c-tools libopenjp2-7 libatlas-base-dev"


echo "*** Configuring mosquitto:"
scp local-anonymous.conf $RPI_HOST
ssh $RPI_HOST "sudo rm -rf /etc/mosquitto/conf.d/local-anonymous.conf && sudo mv /home/pi/local-anonymous.conf /etc/mosquitto/conf.d/local-anonymous.conf && sudo service mosquitto restart"

echo "*** Installing python packages:"
ssh $RPI_HOST "pip3 install smbus2 pillow numpy"


echo "*** Installing PyROS:"
ssh $RPI_HOST "mkdir -p /home/pi/temp && rm -rf /home/pi/temp/pyros /home/pi/temp/install_pyros.sh && cd /home/pi/temp && wget https://raw.githubusercontent.com/Abstract-Horizon/pyros/master/install_pyros.sh && bash install_pyros.sh"


echo "*** Installing PyROS daemon:"
ssh $RPI_HOST "mkdir -p /home/pi/pyrosdir && cd /home/pi/pyrosdir && sudo pyros install daemon -d /home/pi/pyrosdir -u pi -g pi && ln -s /home/pi/pyrosdir/code/pyroslib /home/pi/pyrosdir/code/pyros"

# TODO - upload telemetry!!!

echo "*** Stopping and disabling storage service on second PiZero"
pyros default service pi2:storage disable
pyros default stop pi2:storage

cd ../libs
echo "*** Installing Flig libraries for second Pi:"
./upload_libs-2.sh default

cd ../services

echo "*** Installing Flig services for second Pi:"
./upload_services-2.sh default

cd ../new-install


echo "*** Restarting PyROS:"
ssh $RPI_HOST "sudo service pyros stop && sudo service pyros start"

echo "Done."
