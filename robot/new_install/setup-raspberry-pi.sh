#!/bin/bash

RPI_HOST=$1

echo "Note: It is worth adding ssh keys this script can run without need for entering password each time it is needed."
echo ""

if [[ -z "$RPI_HOST" ]]; then
  echo "Please use: setup-raspberry-pi.sh <hostname-or-ipaddress>"
  echo ""
  echo "This script assumes user is 'pi'"
  exit 1
fi

echo "Using raspberry pi '$RPI_HOST'"


echo "*** Installing system packages:"
ssh $RPI_HOST "sudo apt-get update && sudo apt-get install -y git mosquitto python3-venv python3-pip python3-smbus i2c-tools libopenjp2-7 libatlas-base-dev iptables"


echo "*** Configuring mosquitto:"
scp local-anonymous.conf $RPI_HOST
ssh $RPI_HOST "sudo rm -rf /etc/mosquitto/conf.d/local-anonymous.conf && sudo mv /home/pi/local-anonymous.conf /etc/mosquitto/conf.d/local-anonymous.conf && sudo service mosquitto restart"

echo "*** Installing python packages:"
ssh $RPI_HOST "pip3 install pillow numpy"

#echo "*** Configuring OTG /etc/udev/rules.d/90-panocluster.rules"
#ssh $RPI_HOST "echo 'SUBSYSTEM==\"net\", ATTR{address}==\"00:22:82:ff:ff:01\", NAME=\"ethpi1\"' | sudo tee /etc/udev/rules.d/90-panocluster.rules"

#echo "*** Updating /etc/dhcpcd.conf"
#ssh $RPI_HOST "sudo cat /etc/dhcpcd.conf | ( ! grep -q -c ethpi1) && sudo echo >>/etc/dhcpcd.conf -e 'interface ethpi1\nstatic ip_address=10.0.11.1/24\n'"
echo "*** Updating /etc/dhcpcd.conf"
ssh $RPI_HOST "sudo cat /etc/dhcpcd.conf | ( ! grep -q -c usb0) && sudo echo >>/etc/dhcpcd.conf -e 'interface usb0\nstatic ip_address=10.0.11.1/24\n'"


echo "*** Installing PyROS:"
ssh $RPI_HOST "mkdir -p /home/pi/temp && rm -rf /home/pi/temp/pyros /home/pi/temp/install_pyros.sh && cd /home/pi/temp && wget https://raw.githubusercontent.com/Abstract-Horizon/pyros/master/install_pyros.sh && bash install_pyros.sh"


echo "*** Installing PyROS deamon:"
ssh $RPI_HOST "mkdir -p /home/pi/pyrosdir && cd /home/pi/pyrosdir && sudo pyros install daemon -d /home/pi/pyrosdir -u pi -g pi && ln -s /home/pi/pyrosdir/code/pyroslib /home/pi/pyrosdir/code/pyros && sudo service pyros start"

# TODO - upload telemetry!!!

cd ../libs
echo "*** Installing Flig libraries:"
./upload_libs.sh $RPI_HOST

cd ../services

echo "*** Installing Flig services:"
./upload_services.sh $RPI_HOST

cd ../new-install


echo "*** Restarting PyROS:"
ssh $RPI_HOST "sudo service pyros stop && sudo service pyros start"


echo "*** NOTE: Manually restore pyrosdir/data/storage.config"
echo "*** NOTE: Manually bind PS4 controller:"
echo "sudo bluetoothctl"
echo "> agent on"
echo "> discoverable on"
echo "> pairable on"
echo "> default-agent"
echo "> scan on"
echo ""
echo "*** long press 'share' + 'PS' buttons"
echo "when following appears"
echo "[NEW] Device A5:15:66:83:7F:0B Wireless Controller"
echo "> connect A5:15:66:83:7F:0B"
echo "> trust A5:15:66:83:7F:0B"
echo ""
echo "Done."