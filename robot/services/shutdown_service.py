#!/usr/bin/env python3

#
# Copyright 2016-2017 Games Creators Club
#
# MIT License
#

import traceback
import subprocess
import time
import pyroslib
from storage import storagelib
import RPi.GPIO as GPIO

#
# shutdown service
#
# This service is responsible shutting down Linux.
#

DEBUG = False

DEFAULT_SWITCH_PIN = 19
DEFAULT_LED_PIN = 26

switch_pin = None
led_pin = None

use_lights = True


def set_lights(state: bool) -> None:
    if state:
        GPIO.output(led_pin, 0)
    else:
        GPIO.output(led_pin, 1)


def prepare_to_shutdown():
    print("Preparing to shut down...")
    seconds = 0.0
    interval = 0.3
    state = True
    last_seconds = int(seconds)

    if switch_pin is not None:
        current_swtich = GPIO.input(switch_pin)
        previous_switch = current_swtich

        while seconds <= 6.0 and not (previous_switch == 0 and current_swtich == 1):
            time.sleep(interval)
            seconds += interval
            set_lights(state)
            state = not state
            if last_seconds != int(seconds):
                last_seconds = int(seconds)
                print("Preparing to shut down... " + str(last_seconds))
                pyroslib.publish("shutdown/announce", str(last_seconds))

            previous_switch = current_swtich
            current_swtich = GPIO.input(switch_pin)

        if not (previous_switch == 0 and current_swtich == 1):
            do_shutdown()
        else:
            pyroslib.publish("shutdown/announce", "-")
            set_lights(True)
    else:
        do_shutdown()


def do_shutdown():
    print("Shutting down now!")
    pyroslib.publish("shutdown/announce", "now")
    set_lights(True)
    pyroslib.loop(2.0)
    try:
        subprocess.call(["/usr/bin/sudo", "/sbin/shutdown", "-h", "now"])
    except Exception as exception:
        print("ERROR: Failed to shutdown; " + str(exception))


def check_if_secret_message(_topic, payload, _groups):
    if payload == "secret_message":
        prepare_to_shutdown()
    elif payload == "secret_message_now":
        do_shutdown()


def load_storage():
    global switch_pin, led_pin
    storagelib.subscribe_to_path("shutdown/pin")
    storagelib.subscribe_to_path("shutdown/led")
    storagelib.wait_for_data()
    try:
        storage_map = storagelib.storage_map["shutdown"]

        try:
            switch_pin = int(storage_map['pin'])
        except Exception as _:
            switch_pin = DEFAULT_SWITCH_PIN

        try:
            led_pin = int(storage_map['led'])
        except Exception as _:
            led_pin = DEFAULT_LED_PIN
    except Exception as _:
        switch_pin = DEFAULT_SWITCH_PIN
        led_pin = DEFAULT_LED_PIN

    print(f"  Switch pin is set to {switch_pin} and led {led_pin}")
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(switch_pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.setup(led_pin, GPIO.OUT)

    print("  Storage details loaded.")


if __name__ == "__main__":
    try:
        print("Starting shutdown service...")

        pyroslib.init("shutdown-service", unique=False)

        pyroslib.subscribe("system/shutdown", check_if_secret_message)
        print("  Loading storage details...")
        load_storage()

        if switch_pin is not None:
            if GPIO.input(switch_pin) == 0:
                while GPIO.input(switch_pin) == 0:
                    print("  Waiting to start shutdown-service - switch in wrong position...")
                    set_lights(True)
                    time.sleep(0.3)
                    set_lights(False)
                    i = 0
                    while GPIO.input(switch_pin) == 0 and i < 25:
                        time.sleep(0.2)
                        i += 1

        print("Started shutdown service.")

        def check_switch():
            if switch_pin is not None and GPIO.input(switch_pin) == 0:
                prepare_to_shutdown()

        pyroslib.forever(0.5, check_switch, priority=pyroslib.PRIORITY_LOW)

    except Exception as ex:
        print("ERROR: " + str(ex) + "\n" + ''.join(traceback.format_tb(ex.__traceback__)))
