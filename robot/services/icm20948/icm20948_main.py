#!/usr/bin/env python3

#
# Copyright 2016-2022 Games Creators Club
#
# MIT License
#

from threading import Thread
from typing import Optional, Any

import time
import traceback


import pyroslib as pyros
from telemetry import SocketTelemetryServer, TelemetryLogger

from icm20948 import ICM20948


class ICM20948Controller:
    def __init__(self) -> None:
        self.telemetry_server_port = 6401
        self._server: Optional[SocketTelemetryServer] = None
        self._server_socket: Optional[Any] = None
        self._current_stream_logger: Optional[TelemetryLogger] = None
        self.driver = ICM20948()
        self.driver.set_gyro_sample_rate(50)
        self.driver.set_accelerometer_sample_rate(50)
        self._gyro_x = 0
        self._gyro_y = 0
        self._gyro_z = 0
        self._accel_x = 0
        self._accel_y = 0
        self._accel_z = 0
        self._compass_x = 0
        self._compass_y = 0
        self._compass_z = 0

    def start_icm_20948(self) -> None:
        pass

    def start_telemetry_server(self, telemetry_server_port: int = 6402) -> None:
        self.telemetry_server_port = telemetry_server_port
        self._server = SocketTelemetryServer(port=self.telemetry_server_port)

        self.wait_for_server_start()
        self._server_socket = self._server.get_socket()
        self._server_socket.settimeout(10)

        self._current_stream_logger = self._define_stream(self._server)
        self._start_telemetry_server()

    def wait_for_server_start(self):
        timeout = time.time() + 10
        started = False

        while not started and time.time() < timeout:
            try:
                self._server.start()
                started = True
            except Exception as e:
                print(f"    failed to start socket server; {e}")
                time.sleep(1)

    @staticmethod
    def _define_stream(server: SocketTelemetryServer) -> TelemetryLogger:
        current_stream_logger = server.create_logger("icm20948")
        current_stream_logger.add_float("gyro_x")
        current_stream_logger.add_float("gyro_y")
        current_stream_logger.add_float("gyro_z")
        current_stream_logger.add_float("accel_x")
        current_stream_logger.add_float("accel_y")
        current_stream_logger.add_float("accel_z")
        current_stream_logger.add_float("compass_x")
        current_stream_logger.add_float("compass_y")
        current_stream_logger.add_float("compass_z")

        current_stream_logger.init()
        return current_stream_logger

    def _start_telemetry_server(self) -> None:
        def server_loop():
            while True:
                # noinspection PyBroadException
                try:
                    self._server.process_incoming_connections()
                except Exception as _:
                    pass

        server_thread = Thread(name="incoming-icm20948-telemetry", target=server_loop, daemon=True)
        server_thread.start()

    def loop(self) -> None:
        self._compass_x, self._compass_y, self._compass_z = self.driver.read_magnetometer_data()
        self._accel_x, self._accel_y, self._accel_z, self._gyro_x, self._gyro_y, self._gyro_z = self.driver.read_accelerometer_gyro_data()

        self._current_stream_logger.log(
            time.time(),
            self._gyro_x,
            self._gyro_y,
            self._gyro_z,
            self._accel_x,
            self._accel_y,
            self._accel_z,
            self._compass_x,
            self._compass_y,
            self._compass_z
        )


if __name__ == "__main__":
    try:
        print("Starting icm_20948 service...")

        pyros.init("icm_20948-service")

        from RPi import GPIO

        GPIO.setmode(GPIO.BCM)
        GPIO.setup(17, GPIO.OUT)
        GPIO.output(17, 1)

        icm_20948_controller = ICM20948Controller()
        icm_20948_controller.start_telemetry_server()
        icm_20948_controller.start_icm_20948()

        print("Started icm_20948 service.")

        pyros.forever(0.02, icm_20948_controller.loop)

    except Exception as ex:
        print("ERROR: " + str(ex) + "\n" + ''.join(traceback.format_tb(ex.__traceback__)))
