from enum import Enum, auto

from typing import Optional

from behaviour.behaviour import Behaviour, BehaviourManager, BehaviourProcessState
from behaviour.gait_behaviour import GaitBehaviour
from behaviour_controllers.positioning_service_controller import PositioningServiceController

DEBUG = True


class TippingPoints(Enum):
    Stand = auto()
    Tip = auto()
    Wait = auto()
    GetUp = auto()


class TipCattleFood(GaitBehaviour):
    def __init__(self, behaviour_manager: BehaviourManager) -> None:
        super().__init__(behaviour_manager, "tip-cattle-food", default_values={
            "sit_speed": 1,
            "wait": 4,
            "sit_height": 27,
            "fz": 70,
        })
        self.positioning_service: PositioningServiceController = behaviour_manager.positioning_service
        self.tipping_point = TippingPoints.Stand
        self.sit_speed = 1
        self.stand_height = 85
        self.wait = 4
        self.wait_started = 0

        self.direction = -1
        self.sit_height = 27
        self.fz = 70

        self.door_servo = 2

    def process_context(self, context: dict, update: bool = False) -> bool:
        changed = super().process_context(context, update)

        with self.context_processor(context, update) as c:
            self.sit_speed = c.process_float("sit_speed", self.sit_speed, self.default_values["sit_speed"])
            self.wait = c.process_float("wait", self.wait, self.default_values["wait"])
            self.sit_height = c.process_float("sit_height", self.sit_height, self.default_values["sit_height"])
            self.fz = c.process_float("fz", self.fz, self.default_values["fz"])

            return changed or c.is_changed()

    def start(self, current_time: float, context: dict) -> None:
        super().start(current_time, context)
        self.tipping_point = TippingPoints.Stand
        self.invoke_gait(current_time, "stand", self.make_stand_context())
        self.wait_started = current_time

    def stop(self, current_time: float, new_behaviour: Optional[Behaviour], new_context: dict, active: bool) -> None:
        super().stop(current_time, new_behaviour, new_context, active)
        self.invoke_gait(current_time, "stand", self.make_stand_context())

    def process(self, current_time: float, context: dict) -> BehaviourProcessState:
        """
        :param current_time: current time
        :param context: context it works in
        :return: True if hasn't finish otherwise False
        """

        if self.tipping_point == TippingPoints.Stand:
            if self.wait_started + self.sit_speed * 1.5 <= current_time:
                self.tipping_point = TippingPoints.Tip
                self.invoke_gait(current_time, "sit", {
                    "direction": -1,
                    "height": self.sit_height,
                    "fz": self.fz,
                    "cy": self.cy,
                })
                self.wait_started = current_time
                self.move_servo(current_time, 2, 0)

                if DEBUG: print("    started tipping")
        elif self.tipping_point == TippingPoints.Tip:
            if self.wait_started + self.sit_speed * 1.5 <= current_time:
                self.tipping_point = TippingPoints.Wait
                self.wait_started = current_time
                if DEBUG: print("    started waiting")
        elif self.tipping_point == TippingPoints.Wait:
            if self.wait_started + self.wait <= current_time:
                self.tipping_point = TippingPoints.GetUp
                self.invoke_gait(current_time, "stand", self.make_stand_context())
                self.move_servo(current_time, 2, -90)
                if DEBUG: print("    finished tipping - started getting up")
                self.wait_started = current_time
        elif self.tipping_point == TippingPoints.GetUp:
            if self.wait_started + self.sit_speed * 1.5 <= current_time:
                self.invoke_gait(current_time, "stand", self.make_stand_context())
                if DEBUG: print("    finished getting up")
                return BehaviourProcessState.RESTING
        else:
            print(f"    unknown state {self.tipping_point}")
            return BehaviourProcessState.RESTING

        return BehaviourProcessState.OPERATING
