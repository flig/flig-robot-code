from typing import Optional

from behaviour.behaviour import Behaviour, BehaviourManager, BehaviourProcessState
from behaviour.gait_behaviour import GaitBehaviour
from behaviour.turn_90_deg_of_wall import WALL_ANGLE_TOLERANCE


class ToDistance(GaitBehaviour):
    def __init__(self, behaviour_manager: BehaviourManager) -> None:
        super().__init__(behaviour_manager, "to-distance", default_values={
            "tolerance": 15,
            "distance": 100,
            "align": True,
            "precise": False
        })

        self.tolerance = 15
        self.distance = 100
        self.current_distance = 200
        self.align = True
        self.precise = False
        self.direction = "cw"
        self._has_angle = False
        self._final_orientation = False

    def process_context(self, context: dict, update: bool = False) -> bool:
        changed = super().process_context(context, update)

        self._has_angle = "angle" in context

        with self.context_processor(context, update) as c:
            self.tolerance = c.process_int("tolerance", self.tolerance, self.default_values["tolerance"])
            self.distance = c.process_int("distance", self.distance, self.default_values["distance"])
            self.align = c.process_bool("align", self.align, self.default_values["align"])
            self.precise = c.process_bool("precise", self.precise, self.default_values["precise"])

            return changed or c.is_changed()

    def start(self, current_time: float, context: dict) -> None:
        super().start(current_time, context)
        self._final_orientation = False

    def stop(self, current_time: float, new_behaviour: Optional[Behaviour], new_context: dict, active: bool) -> None:
        super().stop(current_time, new_behaviour, new_context, active)
        self.invoke_gait(current_time, "stand", self.make_stand_context())

    def process(self, current_time: float, context: dict) -> BehaviourProcessState:
        """
        :param current_time: current time
        :param context: context it works in
        :return: True if hasn't finish otherwise False
        """

        def get_wall_angle() -> int:
            wall_angle = int(self.positioning_service.wall_angle_avg)

            if wall_angle > 45:
                wall_angle = 90 - wall_angle
            if wall_angle < -45:
                wall_angle = 90 + wall_angle
            return wall_angle

        def calculate_turning_distance(wall_angle) -> int:
            new_turning_distance = 99999
            if -3 <= wall_angle <= 3:
                new_turning_distance = 99999
            elif wall_angle > 20:
                new_turning_distance = -150
            elif wall_angle > 10:
                new_turning_distance = -250
            elif wall_angle > 3:
                new_turning_distance = -450
            elif wall_angle < -20:
                new_turning_distance = 150
            elif wall_angle < -10:
                new_turning_distance = 250
            elif wall_angle < -3:
                new_turning_distance = 450

            return new_turning_distance

        if current_time < self._wait_for_sensor_timer:
            self._final_orientation = False
            return BehaviourProcessState.SETTING_UP

        else:
            if self._started:
                self._final_orientation = False
                self._started = False
                self.current_distance = self.positioning_service.wall_distance
                if self.current_distance < self.distance:
                    if self.stride > 0:
                        self.stride = -self.stride
                else:
                    if self.stride < 0:
                        self.stride = -self.stride

                if not self._has_angle:
                    self.angle = -self.sensor_angle

                self.invoke_gait(current_time, self.gait_name, self.make_walk_context())
                return BehaviourProcessState.SETTING_UP

            elif not self._final_orientation:
                changed = False
                reached_distance = False

                if self.sensor_angle >= 0:
                    self.current_distance = self.positioning_service.wall_distance
                else:
                    self.current_distance = self.positioning_service.distances[2]

                if abs(self.current_distance - self.distance) < self.tolerance:
                    print(f"Reached distance {self.current_distance} <= {self.distance}")
                    reached_distance = True

                elif self.precise:
                    if abs(self.current_distance - self.distance) < self.tolerance + 100 and abs(self.stride) > 15:
                        print(f"Slowing down abs({self.current_distance} - {self.distance}) < {self.tolerance + 100}")
                        self.leg_raise = int(self.default_values["leg_raise"])
                        self.walk_speed = float(self.default_values["walk_speed"])
                        if self.current_distance - self.distance < 0:
                            self.stride = -15
                        else:
                            self.stride = 15
                        changed = True
                    else:
                        pass
                        # print(f"P: Still waiting for distance {abs(self.current_distance - self.distance)} < {self.tolerance}, d={self.current_distance} ? {self.distance}")
                else:
                    if (self.stride > 0 and self.current_distance < self.distance)\
                            or (self.stride < 0 and self.current_distance > self.distance):
                        print(f"Overreached distance {self.distance} < {self.current_distance} (stride {self.stride})")
                        self.invoke_gait(current_time, "stand", self.make_stand_context())
                        return BehaviourProcessState.RESTING
                    else:
                        pass
                        # print(f"-: Still waiting for distance {self.current_distance} < {self.distance}")

                if self.sensor_angle >= 0 and self.align:
                    wall_angle = get_wall_angle()
                    new_turning_distance = calculate_turning_distance(wall_angle)
                else:
                    wall_angle = 0
                    new_turning_distance = 99999

                if reached_distance:
                    if abs(wall_angle) > WALL_ANGLE_TOLERANCE:
                        self._final_orientation = True
                        self.turning_distance = 0.1
                        if wall_angle > 0:
                            self.direction = "ccw"
                        else:
                            self.direction = "cw"
                        print(f"angle={wall_angle} -> Correcting {self.direction}")
                        self.invoke_gait(current_time, self.gait_name, self._make_rotate_context(self.direction))
                        return BehaviourProcessState.OPERATING
                    else:
                        self.invoke_gait(current_time, "stand", self.make_stand_context())
                        return BehaviourProcessState.RESTING

                if self.turning_distance != new_turning_distance:
                    print(f"Fixing angle for {wall_angle} and turning distance {new_turning_distance}, distance {self.current_distance} > {self.distance}")
                    self.turning_distance = new_turning_distance
                    changed = True

                if changed:
                    self.invoke_gait(current_time, self.gait_name, self.make_walk_context())

                return BehaviourProcessState.OPERATING
            else:
                wall_angle = get_wall_angle()
                if abs(wall_angle) <= WALL_ANGLE_TOLERANCE or\
                    (self.direction == "cw" and WALL_ANGLE_TOLERANCE < wall_angle < 25) or\
                    (self.direction == "ccw" and -25 < wall_angle < -WALL_ANGLE_TOLERANCE):
                        print(f"Reached angle {wall_angle} - stopping")
                        self.invoke_gait(current_time, "stand", self.make_stand_context())
                        return BehaviourProcessState.RESTING
                else:
                    return BehaviourProcessState.OPERATING

    def _make_rotate_context(self, direction: str) -> dict:
        stride = 10
        if direction == "ccw":
            if abs(self.turning_distance) < 1:
                return {"speed": self.walk_speed, "height": self.walk_height, "stride": -stride, "leg_raise": self.leg_raise, "angle": 180, "distance": self.turning_distance, "cy": self.cy}
            else:
                return {"speed": self.walk_speed, "height": self.walk_height, "stride": stride, "leg_raise": self.leg_raise, "angle": 0, "distance": self.turning_distance, "cy": self.cy}
        else:
            return {"speed": self.walk_speed, "height": self.walk_height, "stride": stride, "leg_raise": self.leg_raise, "angle": 0, "distance": self.turning_distance, "cy": self.cy}
