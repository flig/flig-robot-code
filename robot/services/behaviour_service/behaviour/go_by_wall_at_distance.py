import time
from enum import Enum, auto
from typing import Optional

from behaviour.behaviour import Behaviour, BehaviourManager, BehaviourProcessState
from behaviour.common_behaviour import CommonBehaviour
from behaviour_controllers.positioning_service_controller import PositioningServiceController


class SetupStage(Enum):
    WaitForServo = auto()
    Adjust = auto()
    Finished = auto()


class GoByWallAtDistance(CommonBehaviour):
    def __init__(self, behaviour_manager: BehaviourManager) -> None:
        super().__init__(behaviour_manager, "go-by-wall-at-distance", default_values={
            "distance": 100,
            "tolerance": 25,
            "wall": "right",
            "angle": 25,
            "cy": 10,
            "walk_gait": 85,
            "stand_gait": 85
        })

        self.positioning_service: PositioningServiceController = behaviour_manager.positioning_service
        self.gait = "trot"
        self.tolerance = 25
        self.distance = 100
        self.current_distance = 200
        self.speed = 0.5
        self.stand_height = 85
        self.walk_height = 85
        self.leg_raise = 3
        self.stride = 35
        self.turning_distance = 99999
        self.angle = 0
        self.wall = "right"
        self.stage = SetupStage.WaitForServo
        self.started_time = time.time()
        self.servo_time = 0.7
        self.cy = 10
        self._current_angle = 0
        self._last_distance = 0

    def process_context(self, context: dict, update: bool = False) -> bool:
        changed = super().process_context(context, update)

        with self.context_processor(context, update) as c:
            self.distance = c.process_int("distance", self.distance, self.default_values["distance"])
            self.tolerance = c.process_int("tolerance", self.tolerance, self.default_values["tolerance"])
            self.cy = c.process_int("cy", self.cy, self.default_values["cy"])
            self.angle = c.process_str("angle", self.angle, self.default_values["angle"])
            self.wall = c.process_str("wall", self.wall, self.default_values["wall"])

            return changed or c.is_changed()

    def start(self, current_time: float, context: dict) -> None:
        super().start(current_time, context)
        if self.wall == "right":
            self.move_servo(1, 90)
        else:
            self.move_servo(1, -90)
        self.stage = SetupStage.WaitForServo
        self.started_time = time.time()
        self._current_angle = 0

    def stop(self, current_time: float, new_behaviour: Optional[Behaviour], new_context: dict, active: bool) -> None:
        super().stop(current_time, new_behaviour, new_context, active)
        self.invoke_gait("stand", {"height": self.stand_height, "cy": self.cy})

    def process(self, current_time: float, context: dict) -> BehaviourProcessState:
        """
        :param current_time: current time
        :param context: context it works in
        :return: True if hasn't finish otherwise False
        """
        if self.stage == SetupStage.WaitForServo:
            if time.time() >= self.started_time + self.servo_time:
                self.invoke_gait(self.gait, self._make_context())
                self.stage = SetupStage.Adjust
                print("Servo turned - starting walk")
        elif self.stage == SetupStage.Adjust:
            changed = False

            self.current_distance = self.positioning_service.wall_distance

            if self.current_distance // 10 != self._last_distance // 10:
                self._last_distance = self.current_distance
                print(f"    distance {self.current_distance}, expected {self.distance}, tolerance {self.tolerance}, current a={self._current_angle}, angle={self.angle}")

            if self.current_distance <= self.distance - self.tolerance:
                if self._current_angle != self.angle:
                    self._current_angle = self.angle
                    changed = True
                    print("    going away of the wall")
            elif self.current_distance >= self.distance + self.tolerance:
                if self._current_angle != -self.angle:
                    self._current_angle = -self.angle
                    changed = True
                    print("    going to the wall")
            else:
                self._current_angle = 0
                self.stage = SetupStage.Finished
                return BehaviourProcessState.RESTING

            wall_angle = int(self.positioning_service.wall_angle_avg)

            if wall_angle > 5:
                if self.turning_distance > -150:
                    print(f"    distance={self.current_distance} angle={wall_angle}, td={self.turning_distance} <- -150")
                    self.turning_distance = -150
                    changed = True
            elif wall_angle < -5:
                if self.turning_distance <= 10 or self.turning_distance > 300:
                    print(f"    distance={self.current_distance} angle={wall_angle}, td={self.turning_distance} <- 150")
                    self.turning_distance = 150
                    changed = True
            elif self.turning_distance < 99999:
                print(f"    distance={self.current_distance} angle={wall_angle}, td={self.turning_distance} <- 99999")
                self.turning_distance = 99999
                changed = True

            if changed:
                self.invoke_gait(self.gait, self._make_context())

            return BehaviourProcessState.OPERATING
        else:
            print(f"    reached state {self.stage}")
            return BehaviourProcessState.RESTING

    def _make_context(self) -> dict:
        return {"speed": self.speed, "height": self.walk_height, "stride": self.stride, "leg_raise": self.leg_raise, "angle": self._current_angle, "distance": self.turning_distance, "cy": self.cy}
