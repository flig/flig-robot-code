import pyros
from typing import Optional, Dict, Any

from behaviour.behaviour import Behaviour, BehaviourManager
from behaviour.common_behaviour import CommonBehaviour
from behaviour_controllers.positioning_service_controller import PositioningServiceController
from context_helper import ContextProcessingHelper


class GaitBehaviour(CommonBehaviour):
    def __init__(self, behaviour_manager: BehaviourManager, name: str,
                 default_values: Optional[Dict[str, Any]] = None) -> None:
        super().__init__(behaviour_manager, name, default_values={
            "gait": "trot",
            "angle": 0,
            "leg_raise": 2,
            "stride": 25,
            "cy": 10,
            "stand_speed": 0.8,
            "stand_height": 85,
            "walk_speed": 0.5,
            "walk_height": 85,
            "sensor_angle": 0,
            **default_values
        })

        self.positioning_service: PositioningServiceController = behaviour_manager.positioning_service
        self.gait_name = "trot"
        self.angle = 0
        self.leg_raise = 3
        self.stride = 25
        self.cy = 10
        self.stand_speed = 0.5
        self.stand_height = 85
        self.walk_speed = 0.5
        self.walk_height = 85
        self.turning_distance = 99999
        self.sensor_angle = 0
        self._wait_for_sensor_time = 1
        self._wait_for_sensor_timer = 0
        self._started = False

    def process_context(self, context: dict, update: bool = False) -> bool:
        changed = super().process_context(context, update)

        with self.context_processor(context, update) as c:
            self.gait_name = c.process_str("gait", self.gait_name, self.default_values["gait"])
            self.angle = c.process_int("angle", self.angle, self.default_values["angle"])
            self.leg_raise = c.process_int("leg_raise", self.leg_raise, self.default_values["leg_raise"])
            self.stride = c.process_int("stride", self.stride, self.default_values["stride"])
            self.cy = c.process_int("cy", self.cy, self.default_values["cy"])
            self.stand_speed = c.process_float("stand_speed", self.stand_speed, self.default_values["stand_speed"])
            self.stand_height = c.process_int("stand_height", self.stand_height, self.default_values["stand_height"])
            self.walk_speed = c.process_float("walk_speed", self.walk_speed, self.default_values["walk_speed"])
            self.walk_height = c.process_int("walk_height", self.walk_height, self.default_values["walk_height"])
            self.sensor_angle = c.process_int("sensor_angle", self.sensor_angle, self.default_values["sensor_angle"])

            return changed or c.is_changed()

    @staticmethod
    def context_processor(context: dict, update: bool = False) -> ContextProcessingHelper:
        return ContextProcessingHelper(context, update)

    def start(self, current_time: float, context: dict) -> None:
        super().start(current_time, context)
        self.process_context(context)
        self.move_servo(current_time, 1, self.sensor_angle)
        self._started = True

    def cont(self, current_time: float, context: dict) -> None:
        super().cont(current_time, context)

    def update(self, current_time: float, context: dict) -> None:
        super().update(current_time, context)
        self.process_context(context)

    def stop(self, current_time: float, new_behaviour: Optional[Behaviour], new_context: dict, active: bool) -> None:
        super().stop(current_time, new_behaviour, new_context, active)

    def move_servo(self, current_time: float, servo: int, angle: int) -> None:
        current_servo_position = self.behaviour_manager.servo_positions[1]
        print(f"servo/extra/{servo}/angle <- {angle}")
        pyros.publish(f"servo/extra/{servo}/angle", f"{angle}")
        self._wait_for_sensor_timer = (current_time + self._wait_for_sensor_time) if current_servo_position != angle else 0
        self.behaviour_manager.servo_positions[1] = angle

    def make_stand_context(self) -> dict:
        return {"speed": self.stand_speed, "height": self.stand_height, "cy": self.cy}

    def make_walk_context(self) -> dict:
        return {
            "speed": self.walk_speed, "height": self.walk_height, "stride": self.stride, "leg_raise": self.leg_raise, "angle": self.angle, "distance": self.turning_distance, "cy": self.cy}
