import pyros
from typing import Optional, Dict, Any

from behaviour.behaviour import Behaviour, BehaviourManager
from context_helper import ContextProcessingHelper


class CommonBehaviour(Behaviour):
    def __init__(self, behaviour_manager: BehaviourManager, name: str,
                 default_values: Optional[Dict[str, Any]] = None) -> None:
        super().__init__(behaviour_manager, name)
        self.default_values = default_values if default_values is not None else {}
        self._wait_for_sensor_time = 1
        self._wait_for_sensor_timer = 0

    @staticmethod
    def process_context(context: dict, update: bool = False) -> bool:
        return False

    @staticmethod
    def context_processor(context: dict, update: bool = False) -> ContextProcessingHelper:
        return ContextProcessingHelper(context, update)

    def start(self, current_time: float, context: dict) -> None:
        super().start(current_time, context)
        self.process_context(context)

    def cont(self, current_time: float, context: dict) -> None:
        super().cont(current_time, context)

    def update(self, current_time: float, context: dict) -> None:
        super().update(current_time, context)
        self.process_context(context)

    def stop(self, current_time: float, new_behaviour: Optional[Behaviour], new_context: dict, active: bool) -> None:
        super().stop(current_time, new_behaviour, new_context, active)

    @staticmethod
    def invoke_gait(current_time: float, gait_name: str, context: dict) -> None:
        context_str = ",".join([f"{key}={context[key]}" for key in context])
        print(f"gait/invoke/{gait_name}, {context_str}")
        pyros.publish(f"gait/invoke/{gait_name}", context_str)

    def move_servo(self, current_time: float, servo: int, angle: int) -> None:
        current_servo_position = self.behaviour_manager.servo_positions[1]
        print(f"servo/extra/{servo}/angle <- {angle}")
        pyros.publish(f"servo/extra/{servo}/angle", f"{angle}")
        self._wait_for_sensor_timer = (current_time + self._wait_for_sensor_time) if current_servo_position != angle else 0
        self.behaviour_manager.servo_positions[1] = angle
