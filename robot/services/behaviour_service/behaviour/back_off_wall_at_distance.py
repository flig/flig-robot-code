from typing import Optional

from behaviour.behaviour import Behaviour, BehaviourManager, BehaviourProcessState
from behaviour.common_behaviour import CommonBehaviour
from behaviour_controllers.positioning_service_controller import PositioningServiceController


class BackOffWallAtDistance(CommonBehaviour):
    def __init__(self, behaviour_manager: BehaviourManager) -> None:
        super().__init__(behaviour_manager, "back-off-wall-at-distance", default_values={
            "distance": 100
        })

        self.positioning_service: PositioningServiceController = behaviour_manager.positioning_service
        self.gait = "trot"
        self.distance = 100
        self.current_distance = 200
        self.speed = 0.5
        self.height = 85
        self.leg_raise = 3
        self.stride = 25
        self.turning_distance = 99999

    def process_context(self, context: dict, update: bool = False) -> bool:
        changed = super().process_context(context, update)

        with self.context_processor(context, update) as c:
            self.distance = c.process_int("distance", self.distance, self.default_values["distance"])

            return changed or c.is_changed()

    def start(self, current_time: float, context: dict) -> None:
        super().start(current_time, context)
        self.invoke_gait(self.gait, self._make_context())

    def stop(self, current_time: float, new_behaviour: Optional[Behaviour], new_context: dict, active: bool) -> None:
        super().stop(current_time, new_behaviour, new_context, active)
        self.invoke_gait("stand", {"height": 95})

    def process(self, current_time: float, context: dict) -> BehaviourProcessState:
        """
        :param current_time: current time
        :param context: context it works in
        :return: True if hasn't finish otherwise False
        """
        self.current_distance = self.positioning_service.wall_distance
        if self.current_distance >= self.distance:
            self.invoke_gait("stand", {"height": 95})
            return BehaviourProcessState.RESTING

        wall_angle = int(self.positioning_service.wall_angle_avg)

        changed = False
        if wall_angle > 5:
            if self.turning_distance > -150:
                print(f"    distance={self.current_distance} angle={wall_angle}, td={self.turning_distance} <- -150")
                self.turning_distance = -150
                changed = True
        elif wall_angle < -5:
            if self.turning_distance <= 10 or self.turning_distance > 300:
                print(f"    distance={self.current_distance} angle={wall_angle}, td={self.turning_distance} <- 150")
                self.turning_distance = 150
                changed = True
        elif self.turning_distance < 99999:
            print(f"    distance={self.current_distance} angle={wall_angle}, td={self.turning_distance} <- 99999")
            self.turning_distance = 99999
            changed = True

        if changed:
            self.invoke_gait(self.gait, self._make_context())

        return BehaviourProcessState.OPERATING

    def _make_context(self) -> dict:
        return {"speed": self.speed, "height": self.height, "stride": -self.stride, "leg_raise": self.leg_raise, "angle": 0, "distance": self.turning_distance}
