from enum import Enum, auto

from typing import Optional

from behaviour.behaviour import Behaviour, BehaviourManager, BehaviourProcessState
from behaviour.common_behaviour import CommonBehaviour
from behaviour_controllers.positioning_service_controller import PositioningServiceController

DEBUG_ANGLE = True
SETTLE_TIME = 0.75


class Steps(Enum):
    GoOppositeWall = "to-corner", {
        "distance": "200", "sensor_angle": "0",
        "speed": "0.6", "stride": "50", "leg_raise": 4
    }, auto()

    HalfTurn = "turn-to-wall", {
        "direction": "cw",
        "speed": "1", "stride": "15", "leg_raise": 2
    }, auto()

    Ypos = "to-distance", {
        "distance": "240", "sensor_angle": "-90",
        "precise": "true",
        "speed": "0.7", "stride": "15", "leg_raise": 2
    }, auto()

    GoClose = "to-distance", {
        "distance": "100", "sensor_angle": "0",
        "precise": "true"
    }, auto()

    Tip = "tip-cattle-food", {}, auto()

    BackOff = "to-distance", {
        "distance": "200", "sensor_angle": "0",
        "speed": "0.7", "stride": "30", "leg_raise": 3
    }, auto()

    Turn = "turn-90-deg-of-wall", {
        "direction": "cw", "turning_distance": "0.1"
    }, auto()

    TurnToCorner = "turn-to-corner", {
        "direction": "cw"
    }, auto()

    Home = "to-corner", {
        "distance": "200", "sensor_angle": "0",
        "speed": "0.6", "stride": "50", "leg_raise": 4
    }, auto()


class Trough3(CommonBehaviour):
    def __init__(self, behaviour_manager: BehaviourManager) -> None:
        super().__init__(behaviour_manager, "trough3", default_values={})
        self.positioning_service: PositioningServiceController = behaviour_manager.positioning_service
        self.step = list(Steps)[0]

        self._last_angle = 0
        self._settle_started_at = 0

    def start(self, current_time: float, context: dict) -> None:
        super().start(current_time, context)
        self._set_step(Steps.GoOppositeWall, current_time)

    def stop(self, current_time: float, new_behaviour: Optional[Behaviour], new_context: dict, active: bool) -> None:
        super().stop(current_time, new_behaviour, new_context, active)

    def process(self, current_time: float, context: dict) -> BehaviourProcessState:
        """
        :param current_time: current time
        :param context: context it works in
        :return: True if hasn't finish otherwise False
        """
        if self.step == Steps.GoOppositeWall: self._set_step(Steps.HalfTurn, current_time)
        elif self.step == Steps.HalfTurn: self._set_step(Steps.Ypos, current_time)
        elif self.step == Steps.Ypos: self._set_step(Steps.GoClose, current_time)
        elif self.step == Steps.GoClose: self._set_step(Steps.Tip, current_time)
        elif self.step == Steps.Tip: self._set_step(Steps.BackOff, current_time)
        elif self.step == Steps.BackOff: self._set_step(Steps.Turn, current_time)
        elif self.step == Steps.Turn: self._set_step(Steps.TurnToCorner, current_time)
        elif self.step == Steps.TurnToCorner: self._set_step(Steps.Home, current_time)
        elif self.step == Steps.Home:
            print(f"Finished.")
            return BehaviourProcessState.RESTING
        else:
            return BehaviourProcessState.RESTING

        return BehaviourProcessState.OPERATING

    def _set_step(self, step: Steps, current_time: float) -> None:
        print(f"{self.step.name} -> {step.name}")

        self.step = step
        self.behaviour_manager.push_gait(step.value[0], current_time, step.value[1])
