from typing import Optional, List

from behaviour.back_off_wall_at_distance import BackOffWallAtDistance
from behaviour.behaviour import BehaviourManager, Behaviour, BehaviourProcessState
from behaviour.go_by_wall_at_distance import GoByWallAtDistance
from behaviour.idle import Idle
from behaviour.tip_cattle_food import TipCattleFood
from behaviour.to_corner import ToCorner
from behaviour.trough1 import Trough1
from behaviour.trough2 import Trough2
from behaviour.trough3 import Trough3
from behaviour.turn_90_deg_of_wall import Turn90DegOfWall
from behaviour.turn_to_cormer import TurnToCorner
from behaviour.turn_to_wall import TurnToWall
from behaviour_controllers.positioning_service_controller import PositioningServiceController

from behaviour.to_distance import ToDistance

DEBUG = True


class BehaviourStackElement:
    def __init__(self, behaviour: Behaviour, context: dict):
        self.behaviour = behaviour
        self.context = context


class BehaviourManagerImpl(BehaviourManager):
    def __init__(self, positioning_service: PositioningServiceController):
        super().__init__(positioning_service)
        self._behaviour_stack: List[BehaviourStackElement] = []
        # self._current_behaviour: Optional[Behaviour] = None
        # self._current_behaviour_context: Optional[dict] = None

    @property
    def current_behaviour(self) -> Optional[Behaviour]:
        if len(self._behaviour_stack) > 0:
            return self._behaviour_stack[-1].behaviour
        return None

    @property
    def current_behaviour_context(self) -> Optional[dict]:
        if len(self._behaviour_stack) > 0:
            return self._behaviour_stack[-1].context
        return None

    def register_known_behaviours(self) -> None:
        self.register_behaviour(Idle(self))
        self.register_behaviour(ToDistance(self))
        self.register_behaviour(ToCorner(self))
        self.register_behaviour(GoByWallAtDistance(self))
        self.register_behaviour(BackOffWallAtDistance(self))
        self.register_behaviour(Turn90DegOfWall(self))
        self.register_behaviour(TurnToWall(self))
        self.register_behaviour(TurnToCorner(self))
        self.register_behaviour(TipCattleFood(self))
        self.register_behaviour(Trough1(self))
        self.register_behaviour(Trough2(self))
        self.register_behaviour(Trough3(self))

    def select_behaviour(self, name: str, current_time: float, new_context: Optional[dict] = None) -> Optional[Behaviour]:
        if DEBUG: print(f"    *** behaviour manager: selecting behaviour {name}")
        if name not in self._known_behaviours:
            if DEBUG: print(f"    *** behaviour manager: name not in {self._known_behaviours}")
            return None

        new_behaviour = self._known_behaviours[name]
        while len(self._behaviour_stack) > 0:
            behaviour_stack_entry = self._behaviour_stack[-1]
            del self._behaviour_stack[-1]
            behaviour_stack_entry.behaviour.stop(current_time, new_behaviour, new_context, False)

        behaviour_stack_entry = BehaviourStackElement(new_behaviour, new_context)
        self._behaviour_stack.append(behaviour_stack_entry)
        new_behaviour.start(current_time, new_context)

        return new_behaviour

    def push_gait(self, name, current_time: float, new_context: Optional[dict] = None) -> Optional[Behaviour]:
        if DEBUG: print(f"    *** behaviour manager: pushing behaviour {name}")
        if name not in self._known_behaviours:
            if DEBUG: print(f"    *** behaviour manager: name not in {self._known_behaviours}")
            return None

        new_behaviour = self._known_behaviours[name]
        behaviour_stack_entry = BehaviourStackElement(new_behaviour, new_context)
        self._behaviour_stack.append(behaviour_stack_entry)
        new_behaviour.start(current_time, new_context)

        return new_behaviour

    def all_stop(self, current_time: float) -> None:
        current_behaviour = self.current_behaviour
        if current_behaviour is not None:
            current_behaviour.stop(current_time, None, {}, False)

    def process(self, current_time: float) -> None:
        if len(self._behaviour_stack) > 0:
            behaviour_stack_entry = self._behaviour_stack[-1]

            state = behaviour_stack_entry.behaviour.process(current_time, behaviour_stack_entry.context)

            if state == BehaviourProcessState.RESTING:
                if DEBUG: print(f"    *** behaviour manager: current behaviour {behaviour_stack_entry.behaviour.name} is at RESTING")
                behaviour_stack_entry.behaviour.stop(current_time, None, {}, False)

                del self._behaviour_stack[-1]
                if len(self._behaviour_stack) > 0:
                    behaviour_stack_entry = self._behaviour_stack[-1]
                    behaviour_stack_entry.behaviour.cont(current_time, behaviour_stack_entry.context)
