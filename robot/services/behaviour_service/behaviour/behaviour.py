from enum import Enum, auto

from typing import Optional

from abc import ABC, abstractmethod

from controllers.positioning_service_controller import PositioningServiceController


class BehaviourProcessState(Enum):
    SETTING_UP = auto(),
    OPERATING = auto(),
    RESTING = auto()


class Behaviour(ABC):
    def __init__(self, behaviour_manager: 'BehaviourManager', name: str) -> None:
        self.started_time = 0
        self.behaviour_manager = behaviour_manager
        self.name = name

    def start(self, current_time: float, context: dict) -> None:
        self.started_time = current_time

    def cont(self, current_time: float, _context: dict) -> None:
        pass

    def update(self, current_time: float, _context: dict) -> None:
        # self.started_time = current_time
        pass

    def stop(self, current_time: float, new_behaviour: Optional['Behaviour'], new_context: dict, active: bool) -> None:
        pass

    def process(self, current_time: float, context: dict) -> BehaviourProcessState:
        """
        :param current_time: current time
        :param context: context it works in
        :return: True if hasn't finish otherwise False
        """
        return BehaviourProcessState.RESTING


class BehaviourManager(ABC):
    def __init__(self, positioning_service: PositioningServiceController) -> None:
        self.positioning_service: PositioningServiceController = positioning_service
        self._known_behaviours = {}
        self.servo_positions = {1: 0, 2: 0, 3: 0}

    @property
    @abstractmethod
    def current_behaviour(self) -> Optional[Behaviour]:
        pass

    @property
    def current_behaviour_name(self) -> Optional[str]:
        return self.current_behaviour.name if self.current_behaviour is not None else None

    @property
    @abstractmethod
    def current_behaviour_context(self) -> Optional[dict]:
        pass

    def register_behaviour(self, behaviour: Behaviour) -> None:
        self._known_behaviours[behaviour.name] = behaviour

    @abstractmethod
    def select_behaviour(self, name: str, current_time: float, context: dict) -> Optional[Behaviour]:
        pass

    @abstractmethod
    def push_gait(self, name, current_time: float, new_context: Optional[dict] = None) -> Optional[Behaviour]:
        pass

    @abstractmethod
    def all_stop(self, current_time: float) -> None:
        pass

    @abstractmethod
    def process(self, current_time: float) -> None:
        pass
