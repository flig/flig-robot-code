import pyros
from typing import Optional

from behaviour.behaviour import Behaviour, BehaviourManager, BehaviourProcessState
from behaviour.common_behaviour import CommonBehaviour
from behaviour_controllers.positioning_service_controller import PositioningServiceController


class Idle(CommonBehaviour):
    def __init__(self, behaviour_manager: BehaviourManager) -> None:
        super().__init__(behaviour_manager, "idle")

    def process(self, current_time: float, context: dict) -> BehaviourProcessState:
        """
        :param current_time: current time
        :param context: context it works in
        :return: True if hasn't finish otherwise False
        """
        return BehaviourProcessState.RESTING
