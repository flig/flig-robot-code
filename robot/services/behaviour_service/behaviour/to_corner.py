from typing import Optional

from behaviour.behaviour import Behaviour, BehaviourManager, BehaviourProcessState
from behaviour.gait_behaviour import GaitBehaviour


class ToCorner(GaitBehaviour):
    def __init__(self, behaviour_manager: BehaviourManager) -> None:
        super().__init__(behaviour_manager, "to-corner", default_values={
            "tolerance": 15,
            "distance": 100,
            "corner": "left"
        })

        self.tolerance = 15
        self.distance = 100
        self.corner = "left"
        self.current_distance = 200
        self._has_angle = False

    def process_context(self, context: dict, update: bool = False) -> bool:
        changed = super().process_context(context, update)

        self._has_angle = "angle" in context

        with self.context_processor(context, update) as c:
            self.tolerance = c.process_int("tolerance", self.tolerance, self.default_values["tolerance"])
            self.distance = c.process_int("distance", self.distance, self.default_values["distance"])
            self.corner = c.process_str("corner", self.corner, self.default_values["corner"])

            return changed or c.is_changed()

    def start(self, current_time: float, context: dict) -> None:
        self.started_time = current_time
        self.process_context(context)
        self._started = True

    def stop(self, current_time: float, new_behaviour: Optional[Behaviour], new_context: dict, active: bool) -> None:
        super().stop(current_time, new_behaviour, new_context, active)
        self.invoke_gait(current_time, "stand", self.make_stand_context())

    def process(self, current_time: float, context: dict) -> BehaviourProcessState:
        """
        :param current_time: current time
        :param context: context it works in
        :return: True if hasn't finish otherwise False
        """

        def get_wall_angle() -> int:
            wall_angle = int(self.positioning_service.wall_angle_avg)

            if wall_angle > 45:
                wall_angle = 90 - wall_angle
            if wall_angle < -45:
                wall_angle = 90 + wall_angle
            return wall_angle

        def calculate_turning_distance(wall_angle) -> int:
            new_turning_distance = 99999
            if self.corner == "left":
                if wall_angle < -40:
                    new_turning_distance = 99999
                elif wall_angle < -30:
                    new_turning_distance = -450
                elif wall_angle < -20:
                    new_turning_distance = -300
                elif wall_angle <= 15:
                    new_turning_distance = -150
                elif wall_angle > 40:
                    new_turning_distance = 99999
                elif wall_angle > 30:
                    new_turning_distance = 500
                elif wall_angle > 20:
                    new_turning_distance = 350
                elif wall_angle > 15:
                    new_turning_distance = 200
            else:
                if wall_angle > 40:
                    new_turning_distance = 99999
                elif wall_angle > 30:
                    new_turning_distance = 500
                elif wall_angle > 20:
                    new_turning_distance = 350
                elif wall_angle >= -15:
                    new_turning_distance = 200
                elif wall_angle < -40:
                    new_turning_distance = 99999
                elif wall_angle < -30:
                    new_turning_distance = -450
                elif wall_angle < -20:
                    new_turning_distance = -300
                elif wall_angle < -15:
                    new_turning_distance = -150

            return new_turning_distance

        def calculate_turning_distance2(d0: int, d3: int) -> int:
            delta = d0 - d3
            new_turning_distance = 99999
            if abs(delta) < 30:
                new_turning_distance = 99999
            elif delta < -30:
                new_turning_distance = 500
            elif delta < -50:
                new_turning_distance = 350
            elif delta < -100:
                new_turning_distance = 250
            elif delta > 30:
                new_turning_distance = -500
            elif delta > 50:
                new_turning_distance = -350
            elif delta > 100:
                new_turning_distance = -250

            return new_turning_distance

        if current_time < self._wait_for_sensor_timer:
            return BehaviourProcessState.SETTING_UP

        else:
            if self._started:
                self._started = False
                if not self._has_angle:
                    self.angle = -self.sensor_angle

                self.invoke_gait(current_time, self.gait_name, self.make_walk_context())
                return BehaviourProcessState.SETTING_UP

            else:
                changed = False

                if self.positioning_service.wall_angle_avg < 0:
                    if self.positioning_service.wall_points == 3:
                        self.current_distance = self.positioning_service.distances[0]
                    else:
                        self.current_distance = self.positioning_service.distances[1]
                else:
                    if self.positioning_service.wall_points == 3:
                        self.current_distance = self.positioning_service.distances[3]
                    else:
                        self.current_distance = self.positioning_service.distances[2]

                if self.current_distance < self.distance + self.tolerance:
                    print(f"Reached distance {self.current_distance} <= {self.distance}")
                    self.invoke_gait(current_time, "stand", self.make_stand_context())
                    return BehaviourProcessState.RESTING
                #
                # elif abs(self.current_distance - self.distance) < self.tolerance + 100 and self.stride > 15:
                #     print(f"Slowing down abs({self.current_distance} - {self.distance}) < {self.tolerance + 100}")
                #     self.leg_raise = int(self.default_values["leg_raise"])
                #     self.walk_speed = float(self.default_values["walk_speed"])
                #     if self.current_distance - self.distance < 0:
                #         self.stride = -15
                #     else:
                #         self.stride = 15
                #     changed = True

                wall_angle = get_wall_angle()
                # new_turning_distance = calculate_turning_distance(wall_angle)
                d0 = self.positioning_service.distances[0]
                d3 = self.positioning_service.distances[3]
                new_turning_distance = calculate_turning_distance2(d0, d3)

                if self.turning_distance != new_turning_distance:
                    # print(f"Fixing angle for {wall_angle} and turning distance {new_turning_distance}, distance {self.current_distance} > {self.distance}")
                    print(f"Fixing angle for {wall_angle} and turning distance {new_turning_distance}, d0={d0} d3={d3} delta={d0 - d3}")
                    self.turning_distance = new_turning_distance
                    changed = True

                if changed:
                    self.invoke_gait(current_time, self.gait_name, self.make_walk_context())

                return BehaviourProcessState.OPERATING
