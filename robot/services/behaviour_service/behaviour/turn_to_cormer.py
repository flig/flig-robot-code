from enum import Enum, auto

from typing import Optional

from behaviour.behaviour import Behaviour, BehaviourManager, BehaviourProcessState
from behaviour.gait_behaviour import GaitBehaviour
from behaviour_controllers.positioning_service_controller import PositioningServiceController

DEBUG_ANGLE = True
SETTLE_TIME = 0.75
WALL_ANGLE_TOLERANCE = 4


class TurnToCorner(GaitBehaviour):
    def __init__(self, behaviour_manager: BehaviourManager) -> None:
        super().__init__(behaviour_manager, "turn-to-corner", default_values={
            "direction": "ccw",
            "turning_distance": 0.1,
        })
        self.positioning_service: PositioningServiceController = behaviour_manager.positioning_service
        self.direction = "ccw"

        self._last_angle = 0
        self._settle_started_at = 0

    def process_context(self, context: dict, update: bool = False) -> bool:
        changed = super().process_context(context, update)

        with self.context_processor(context, update) as c:
            self.direction = c.process_str("direction", self.direction, self.default_values["direction"])
            self.turning_distance = c.process_float("turning_distance", self.turning_distance, self.default_values["turning_distance"])

            return changed or c.is_changed()

    def start(self, current_time: float, context: dict) -> None:
        super().start(current_time, context)

    def stop(self, current_time: float, new_behaviour: Optional[Behaviour], new_context: dict, active: bool) -> None:
        super().stop(current_time, new_behaviour, new_context, active)
        self.invoke_gait(current_time, "stand", self.make_stand_context())

    def process(self, current_time: float, context: dict) -> BehaviourProcessState:
        """
        :param current_time: current time
        :param context: context it works in
        :return: True if hasn't finish otherwise False
        """
        if current_time < self._wait_for_sensor_timer:
            return BehaviourProcessState.SETTING_UP

        else:
            if self._started:
                self._started = False
                self.invoke_gait(current_time, self.gait_name, self._make_context())
                return BehaviourProcessState.SETTING_UP
            else:
                wall_angle = int(self.positioning_service.wall_angle_avg)

                if wall_angle > 45:
                    wall_angle = 90 - wall_angle
                if wall_angle < -45:
                    wall_angle = 90 + wall_angle

                    if abs(wall_angle) > 30:
                        print(f"angle={wall_angle} -> Finished")
                        self._settle_started_at = current_time + SETTLE_TIME
                        self.invoke_gait(current_time, "stand", self.make_stand_context())
                        return BehaviourProcessState.RESTING

                if self._last_angle != wall_angle:
                    self._last_angle = wall_angle
                    if DEBUG_ANGLE: print(f"angle={wall_angle}")

    def _make_context(self, slow_down: bool = False) -> dict:
        stride = self.stride
        if slow_down and stride > 20:
            stride = 15

        if self.direction == "ccw":
            if abs(self.turning_distance) < 1:
                return {"speed": self.walk_speed, "height": self.walk_height, "stride": -stride, "leg_raise": self.leg_raise, "angle": 180, "distance": self.turning_distance, "cy": self.cy}
            else:
                return {"speed": self.walk_speed, "height": self.walk_height, "stride": stride, "leg_raise": self.leg_raise, "angle": 0, "distance": self.turning_distance, "cy": self.cy}
        else:
            return {"speed": self.walk_speed, "height": self.walk_height, "stride": stride, "leg_raise": self.leg_raise, "angle": 0, "distance": self.turning_distance, "cy": self.cy}
