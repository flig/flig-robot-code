from typing import Optional, Union


class ContextProcessingHelper:
    def __init__(self, context: dict, update: bool) -> None:
        self.context = context
        self.update = update
        self.changed = False

    def process_float(self, context_key: str, old_value: float, default_value: Optional[float]) -> float:
        new_value = self.context[context_key] if self.context is not None and context_key in self.context else old_value if self.update else default_value
        new_value = float(new_value) if new_value is not None else new_value
        self.changed = self.changed or new_value != old_value
        return new_value

    def process_int(self, context_key: str, old_value: int, default_value: Optional[int]) -> int:
        new_value = self.context[context_key] if self.context is not None and context_key in self.context else old_value if self.update else default_value
        new_value = int(new_value) if new_value is not None else new_value
        self.changed = self.changed or new_value != old_value
        return new_value

    def process_str(self, context_key: str, old_value: str, default_value: Optional[str]) -> str:
        new_value = self.context[context_key] if self.context is not None and context_key in self.context else old_value if self.update else default_value
        new_value = new_value if new_value is not None else new_value
        self.changed = self.changed or new_value != old_value
        return new_value

    def process_bool(self, context_key: str, old_value: bool, default_value: Optional[bool]) -> bool:
        def to_bool(s: Union[str, bool]) -> bool:
            if s == True or s == False:
                return s

            if s.lower() == "true" or s.lower() == "yes" or s.lower() == "1":
                return True
            try:
                i = int(s)
                return i != 0
            except:
                pass
            return False
        new_value = to_bool(self.context[context_key]) if self.context is not None and context_key in self.context else old_value if self.update else default_value
        # new_value = to_bool(new_value) if new_value is not None else new_value
        self.changed = self.changed or new_value != old_value
        return new_value

    def is_changed(self) -> bool:
        return self.changed

    def __enter__(self) -> 'ContextProcessingHelper':
        return self

    def __exit__(self, exc_type, exc_val, exc_tb) -> bool:
        return False
