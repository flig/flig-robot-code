
import time
import traceback
from threading import Thread
from typing import Optional, Any, List

import pyros

from behaviour.behaviour_manager_implementation import BehaviourManagerImpl

from behaviour_controllers.positioning_service_controller import PositioningServiceController

DEBUG = False


class BehaviourServiceController:
    def __init__(self, positioning_service: PositioningServiceController) -> None:
        self.positioning_service = positioning_service
        self.behaviour_manager = BehaviourManagerImpl(self.positioning_service)
        pyros.subscribe("behaviour/invoke/+", self._handle_behaviour_invoke)

    def _handle_behaviour_invoke(self, _topic: str, payload: str, groups: List[str]) -> None:
        try:
            behaviour_name = groups[0]
            print(f"Got invoke behaviour {behaviour_name} with '{payload}'")

            parameters = payload.split(',')
            context = {kv[0]: kv[1] for kv in [kv.split("=") for kv in parameters] if len(kv) > 1}
            # if len(parameters) > 0 and parameters[0] == "-":
            #     context = self.gait_orchestrator.gait_manager.current_gait_context
            # else:
            #     context = {kv[0]: kv[1] for kv in [kv.split("=") for kv in parameters] if len(kv) > 1}

            behaviour = self.behaviour_manager.select_behaviour(behaviour_name, time.time(), context)
            if behaviour is not None:
                print(f"  selected and started behaviour '{behaviour_name}' with context '{context}'")
            else:
                print(f"  ** ERROR - no behaviour {behaviour_name}")
        except Exception as e:
            print(f"Error processing behaviour {e}\n{''.join(traceback.format_tb(e.__traceback__))}")

    def main_loop(self) -> None:
        current_time = time.time()
        self.behaviour_manager.process(current_time)


if __name__ == "__main__":
    try:
        print("Starting behaviour service...")

        pyros.init("behaviour-service", unique=False)
        positioning_service = PositioningServiceController()
        positioning_service.start()

        behaviour_controller = BehaviourServiceController(positioning_service)
        behaviour_controller.behaviour_manager.register_known_behaviours()
        print("Started behaviour service.")

        pyros.forever(0.02, behaviour_controller.main_loop)

    except Exception as ex:
        print("ERROR: " + str(ex) + "\n" + ''.join(traceback.format_tb(ex.__traceback__)))
