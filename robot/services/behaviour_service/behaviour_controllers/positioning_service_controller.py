import math

import time

from threading import Thread
from typing import Optional, Any, Tuple

from telemetry import SocketTelemetryServer, TelemetryLogger, TelemetryStreamDefinition

from telemetry_receiver_adapter import TelemetryReceiverAdapter

DEBUG_CALIBRATION = False


ANGLE_90 = math.pi / 2
ANGLE_45 = math.pi / 4
ANGLE_22_5 = math.pi / 8
ANGLE_12_25 = math.pi / 16
ANGLE_9_5 = math.pi / 19
ANGLE_6_125 = math.pi / 32

FLOOR_OUTER_ANGLE = ANGLE_22_5
# FLOOR_INNER_ANGLE = ANGLE_9_5  # - ANGLE_12_25
FLOOR_INNER_ANGLE = ANGLE_12_25
WALL_OUTER_ANGLE = ANGLE_22_5 - ANGLE_6_125
WALL_INNER_ANGLE = ANGLE_12_25 - ANGLE_6_125

FLOOR_ANGLES = [-FLOOR_OUTER_ANGLE, -FLOOR_INNER_ANGLE, FLOOR_INNER_ANGLE, FLOOR_OUTER_ANGLE]
WALL_ANGLES = [ANGLE_90 - -WALL_OUTER_ANGLE, ANGLE_90 - -WALL_INNER_ANGLE, ANGLE_90 - WALL_INNER_ANGLE, ANGLE_90 - WALL_OUTER_ANGLE]


GYRO_BEARING_CALIBRATION_TIME = 2
MAXIMAL_ALLOWED_CHANGE_ANGLE = 20


class PositioningServiceController:
    def __init__(self) -> None:
        super().__init__()
        self.positioning_telemetry_server_port = 6403
        self._server: Optional[SocketTelemetryServer] = None
        self._server_socket: Optional[Any] = None
        self._positioning_stream_logger: Optional[TelemetryLogger] = None

        self.distance_telemetry_adapter = DistanceTelemetryAdapter(self)
        self.nine_dof_telemetry_adapter = NineDoFTelemetryAdapter(self)

        self.statuses = [0] * 16
        self.distances = [1500] * 16
        self.received_distances = [1500] * 16

        self.last_wall_angle = 0
        self.wall_angle = 0
        self.wall_angle_avg = 0
        self.wall_points = 0
        self.wall_distance = 0

        self.floor_angles = [0.0] * 4
        self.floor_points = [0] * 4
        self.object_distance = [0.0] * 4

        self.wall_angle_avg_k = 0.4

        self.gyro_x = 0
        self.gyro_y = 0
        self.gyro_z = 0
        self.accel_x = 0
        self.accel_y = 0
        self.accel_z = 0
        self.compass_x = 0
        self.compass_y = 0
        self.compass_z = 0
        self.max_mag_x = -999999
        self.min_mag_x = 999999

        self.max_mag_y = -999999
        self.min_mag_y = 999999

        self.max_mag_z = -999999
        self.min_mag_z = 999999

        self.pitch = 0.0
        self.roll = 0.0

        self.bearing = 0.0
        self.gyro_bearing = 0

        self.calibration_is_on = False
        self.gyro_bearing_calibration_is_on = True
        self.gyro_bearing_calibration_start_time = time.time()
        self.gyro_z_total_cal = 0
        self.gyro_z_total_cal_number = 0

        self.gyro_z_average = 0

    def start(self) -> None:
        self.gyro_bearing_calibration_start_time = time.time()
        self._start_positioning_telemetry_server()
        self._start_vl53l5cx_receiver()
        self._start_nine_dof_receiver()

    def stop(self) -> None:
        self.distance_telemetry_adapter.safely_close_telemetry_client()
        self.nine_dof_telemetry_adapter.safely_close_telemetry_client()

    def _start_vl53l5cx_receiver(self) -> None:
        self.distance_telemetry_adapter.do_connect_telemetry = True

    def _start_nine_dof_receiver(self) -> None:
        self.nine_dof_telemetry_adapter.do_connect_telemetry = True

    def _start_positioning_telemetry_server(self, positioning_telemetry_server_port: int = 6403) -> None:
        self.positioning_telemetry_server_port = positioning_telemetry_server_port
        self._server = SocketTelemetryServer(port=self.positioning_telemetry_server_port)

        self.wait_for_server_start()
        self._server_socket = self._server.get_socket()
        self._server_socket.settimeout(10)

        self._positioning_stream_logger = self._define_stream(self._server)
        self._start_telemetry_server()

    def wait_for_server_start(self):
        timeout = time.time() + 10
        started = False

        while not started and time.time() < timeout:
            try:
                self._server.start()
                started = True
            except Exception as e:
                print(f"    failed to start socket server; {e}")
                time.sleep(1)

    @staticmethod
    def _define_stream(server: SocketTelemetryServer) -> TelemetryLogger:
        # self.wall_angle = 0
        # self.wall_points = 0
        # self.wall_distance = 0
        #
        # self.floor_angles = [0.0] * 4
        # self.floor_points = [0] * 4
        # self.object_distance = [0.0] * 4

        positioning_stream_logger = server.create_logger("positioning_stream")
        positioning_stream_logger.add_float("gyro_bearing")
        positioning_stream_logger.add_word("wall_angle", True)
        positioning_stream_logger.add_word("wall_angle_avg", True)
        positioning_stream_logger.add_byte("wall_points", True)
        positioning_stream_logger.add_float("wall_distance")
        for i in range(4):
            positioning_stream_logger.add_float(f"object_distance_{i}")
        for i in range(4):
            positioning_stream_logger.add_word(f"floor_angle_{i}", True)
            positioning_stream_logger.add_byte(f"floor_points_{i}", True)
        positioning_stream_logger.init()
        return positioning_stream_logger

    def _start_telemetry_server(self) -> None:
        def server_loop():
            while True:
                # noinspection PyBroadException
                try:
                    self._server.process_incoming_connections()
                except Exception as _:
                    pass

        server_thread = Thread(name="incoming-vl53l5cx-telemetry", target=server_loop, daemon=True)
        server_thread.start()

    def log_data(self) -> None:
        self._positioning_stream_logger.log(
            time.time(),
            self.gyro_bearing,
            self.wall_angle,
            int(self.wall_angle_avg),
            self.wall_points,
            self.wall_distance,
            self.object_distance[0],
            self.object_distance[1],
            self.object_distance[2],
            self.object_distance[3],
            self.floor_angles[0],
            self.floor_points[0],
            self.floor_angles[1],
            self.floor_points[1],
            self.floor_angles[2],
            self.floor_points[2],
            self.floor_angles[3],
            self.floor_points[3],
        )

    def distance_process_response(self) -> None:
        def points_distance(p1: Tuple[float, float], p2: Tuple[float, float]) -> float:
            return math.sqrt((p1[0] - p2[0]) * (p1[0] - p2[0]) + (p1[1] - p2[1]) * (p1[1] - p2[1]))

        def is_close(d1: float, d2: float, tol: float = 0.998) -> bool:
            return ((d1 > d2 and d2 / d1 > tol)
                    or (d1 < d2 and d1 / d2 > tol))

        top_points = [(0.0, 0.0)] * 4

        wall_distance = 9999
        for i in range(4):
            d = self.distances[i]
            if d < wall_distance:
                wall_distance = d
            x = d
            y = d / math.tan(WALL_ANGLES[i])
            top_points[i] = (x, y)

        d12 = points_distance(top_points[0], top_points[1])
        d23 = points_distance(top_points[1], top_points[2])
        d34 = points_distance(top_points[2], top_points[3])
        d13 = points_distance(top_points[0], top_points[2])
        d24 = points_distance(top_points[1], top_points[3])

        top_wall_right = True
        if is_close(d12 + d23, d13, 0.975):
            self.wall_points = 3
            wall_distance = self.distances[2]

            if is_close(d23 + d34, d24):
                self.wall_points = 4
        elif is_close(d23 + d34, d24, 0.975):
            wall_distance = self.distances[1]
            top_wall_right = False
            self.wall_points = -3
        else:
            d2 = self.distances[1]
            d3 = self.distances[2]
            wall_distance = d2 if d2 < d3 else d3
            self.wall_points = 2

        if top_wall_right:
            a = top_points[0][0] - top_points[1][0]
            c = d12
        else:
            a = top_points[2][0] - top_points[3][0]
            c = d34

        self.last_wall_angle = self.wall_angle
        self.wall_angle = int(math.degrees(-math.asin(a / c)))
        if (self.last_wall_angle < 0 < self.wall_angle and abs(self.wall_angle - self.last_wall_angle) > MAXIMAL_ALLOWED_CHANGE_ANGLE) or \
                (self.last_wall_angle > 0 > self.wall_angle and abs(self.last_wall_angle - self.wall_angle) > MAXIMAL_ALLOWED_CHANGE_ANGLE):
            # print(f"*** big angle jump, reseting avg; a={self.wall_angle} la={self.last_wall_angle}")
            self.wall_angle_avg = self.wall_angle
        else:
            self.wall_angle_avg = self.wall_angle_avg * (1 - self.wall_angle_avg_k) + self.wall_angle * self.wall_angle_avg_k
        self.wall_distance = wall_distance

        expected_lower_distance = 100 / math.cos(math.radians(55))
        for l in range(4):
            ld = self.distances[3 * 4 + l]
            if ld != 0 and (expected_lower_distance / ld < 0.80 or expected_lower_distance / ld > 1.2):
                self.object_distance[l] = ld
                self.floor_points[l] = -1
                self.floor_angles[l] = 0
            elif ld > self.distances[2 * 4 + l]:
                self.object_distance[l] = self.distances[2 * 4 + l]
                self.floor_points[l] = -2
                self.floor_angles[l] = 0
            else:
                points = [(0.0, 0.0)] * 4
                for i in range(1, 4):
                    d = self.distances[i * 4 + l]
                    angle = FLOOR_ANGLES[i]
                    x = d * math.cos(angle)
                    y = d * math.sin(angle)
                    points[i - 1] = (x, y)

                d12 = points_distance(points[0], points[1])
                d23 = points_distance(points[1], points[2])
                d13 = points_distance(points[0], points[2])

                if is_close(d12 + d23, d13):
                    self.floor_points[l] = 3
                    self.object_distance[l] = 9999
                else:
                    self.floor_points[l] = 2
                    self.object_distance[l] = self.distances[1 * 4 + l]

                a = points[2][0] - points[1][0]
                c = d23
                try:
                    self.floor_angles[l] = int(math.degrees(ANGLE_90 + math.asin(a / c) - ANGLE_12_25))
                except ValueError:
                    print(f"Math domain error a={a:.2f}, c={c:.2f} a/c={a/c:.2f}")

                if self.floor_angles[l] > 12:
                    self.floor_points[l] = -3
                    self.object_distance[l] = self.distances[2 * 4 + l]

    def nine_dof_process_results(self) -> None:
        if self.calibration_is_on:
            if self.compass_x > self.max_mag_x:
                self.max_mag_x = self.compass_x
            if self.compass_x < self.min_mag_x:
                self.min_mag_x = self.compass_x

            if self.compass_y > self.max_mag_y:
                self.max_mag_y = self.compass_y
            if self.compass_y < self.min_mag_y:
                self.min_mag_y = self.compass_y

            if self.compass_z > self.max_mag_z:
                self.max_mag_z = self.compass_z
            if self.compass_z < self.min_mag_z:
                self.min_mag_z = self.compass_z

        mag_x = (self.compass_x - self.min_mag_x) / (self.max_mag_x - self.min_mag_x) if self.max_mag_x != self.min_mag_x else 0
        mag_y = (self.compass_y - self.min_mag_y) / (self.max_mag_y - self.min_mag_y) if self.max_mag_y != self.min_mag_y else 0
        mag_z = (self.compass_z - self.min_mag_z) / (self.max_mag_z - self.min_mag_z) if self.max_mag_z != self.min_mag_z else 0

        mag_x -= 0.5
        mag_y -= 0.5
        mag_z -= 0.5

        bearing = math.atan2(mag_x, mag_y)
        if bearing < 0:
            bearing += 2 * math.pi
        self.bearing = bearing

        self.pitch = math.atan2(self.accel_x, self.accel_z)
        self.roll = math.atan2(self.accel_y, self.accel_z)

        if self.gyro_bearing_calibration_is_on:
            if self.gyro_bearing_calibration_start_time + GYRO_BEARING_CALIBRATION_TIME > time.time():
                self.gyro_z_total_cal += self.gyro_z
                self.gyro_z_total_cal_number += 1
                if DEBUG_CALIBRATION: print(f"    calibrating gyro_z total: {self.gyro_z_total_cal}, #: {self.gyro_z_total_cal_number}")
            elif self.gyro_z_total_cal_number > 0:
                self.gyro_z_average = self.gyro_z_total_cal / self.gyro_z_total_cal_number
                print(f"    finished gyro calibration - middle for gyro_z is {self.gyro_z_average}")
                self.gyro_bearing_calibration_is_on = False
            else:
                print("Failed to do gyro calibration. Trying again...")
                self.gyro_bearing_calibration_start_time = time.time()
        else:
            self.gyro_bearing += (self.gyro_z - self.gyro_z_average)


class DistanceTelemetryAdapter(TelemetryReceiverAdapter):
    def __init__(self, positioning_service: PositioningServiceController) -> None:
        super().__init__("positioning", "vl53lcx", telemetry_host="10.0.11.2", telemetry_port=6401)
        self.positioning_service = positioning_service

        self.stream: Optional[TelemetryStreamDefinition] = None

    def on_telemetry_connection(self, connected: bool) -> None:
        if connected:
            self.stream = self.telemetry_client.streams[self.stream_name]
        else:
            self.stream = None

    def on_telemetry_received(self) -> None:
        current_time = time.time()

        def write_data(records):
            if len(records) > 0:
                for record in records:
                    for i in range(0, 32):
                        if i % 2 == 0:
                            self.positioning_service.statuses[i // 2] = record[i + 1]
                        else:
                            self.positioning_service.received_distances[i // 2] = record[i + 1]

                    for i in range(len(self.positioning_service.received_distances)):
                        if self.positioning_service.statuses[i] == 5:
                            self.positioning_service.distances[i] = self.positioning_service.received_distances[i]

                self.telemetry_client.trim(self.stream, current_time)
                self.positioning_service.distance_process_response()
                self.positioning_service.log_data()

        def fetch_stream(stream):
            self.stream = stream
            self.telemetry_client.retrieve(stream, 0, time.time(), write_data)

        if self.telemetry_client is not None and self.telemetry_client.socket is not None:
            self.telemetry_client.get_stream_definition(self.stream_name, fetch_stream)


class NineDoFTelemetryAdapter(TelemetryReceiverAdapter):
    def __init__(self, positioning_service: PositioningServiceController) -> None:
        super().__init__("9dof", "icm20948", telemetry_host="10.0.11.2", telemetry_port=6402)
        self.positioning_service = positioning_service

        self.stream: Optional[TelemetryStreamDefinition] = None

    def on_telemetry_connection(self, connected: bool) -> None:
        if connected:
            self.stream = self.telemetry_client.streams[self.stream_name]
        else:
            self.stream = None

    def on_telemetry_received(self) -> None:
        current_time = time.time()

        def write_data(records):
            if len(records) > 0:
                # print(f"    receiving {len(records)} record(s): ", end="")
                for record in records:
                    # print(f"    got 9dof record {record}")
                    self.positioning_service.gyro_x = record[1]
                    self.positioning_service.gyro_y = record[2]
                    self.positioning_service.gyro_z = record[3]
                    self.positioning_service.accel_x = record[4]
                    self.positioning_service.accel_y = record[5]
                    self.positioning_service.accel_z = record[6]
                    self.positioning_service.compass_x = record[7]
                    self.positioning_service.compass_y = record[8]
                    self.positioning_service.compass_z = record[9]

                    self.positioning_service.nine_dof_process_results()

                self.telemetry_client.trim(self.stream, current_time)

        def fetch_stream(stream):
            self.stream = stream
            self.telemetry_client.retrieve(stream, 0, time.time(), write_data)

        if self.telemetry_client is not None and self.telemetry_client.socket is not None:
            self.telemetry_client.get_stream_definition(self.stream_name, fetch_stream)
