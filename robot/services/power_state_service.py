import traceback

import pyros

DEBUG = False


previous_power_state = None

if __name__ == "__main__":
    try:
        print("Starting power state service...")

        pyros.init("power-state-service", unique=False)

        def request_previous_state_handler(topic, payload):
            if previous_power_state is not None:
                pyros.publish("power/previous-state", previous_power_state)

        def power_state_handler(topic, payload):
            global previous_power_state
            previous_power_state = payload

        pyros.subscribe("power/request-previous-state", request_previous_state_handler)
        pyros.subscribe("power/state", power_state_handler)

        print("Started power state service.")

        pyros.forever(1, priority=pyros.PRIORITY_LOW)

    except Exception as ex:
        print("ERROR: " + str(ex) + "\n" + ''.join(traceback.format_tb(ex.__traceback__)))
