#!/usr/bin/env python3

#
# Copyright 2016-2017 Games Creators Club
#
# MIT License
#

from threading import Thread
from typing import Optional

import array
import math
import socket
import struct
import time
import traceback
from abc import ABC
from enum import Enum
from fcntl import ioctl

import pyroslib as pyros


DEBUG_AXES = False
DEBUG_BUTTONS = False
DEBUG_BUTTON_CHANGES = True
DEBUG_JOYSTICK = False
DEBUG_UDP = False
DEBUG_SETUP = False
DEBUG_LOGIC = True

EXPO = 0.5
MAX_STOPPING = 2

JCONTROLLER_UDP_PORT = 1880


class Modes(Enum):
    NONE = 0
    NORMAL = 1
    MENU = 2


# These constants were borrowed from linux/input.h
axis_names = {
    0x00: 'x',
    0x01: 'y',
    0x02: 'z',
    0x03: 'rx',
    0x04: 'ry',
    0x05: 'rz',
    0x06: 'trottle',
    0x07: 'rudder',
    0x08: 'wheel',
    0x09: 'gas',
    0x0a: 'brake',
    0x10: 'hat0x',
    0x11: 'hat0y',
    0x12: 'hat1x',
    0x13: 'hat1y',
    0x14: 'hat2x',
    0x15: 'hat2y',
    0x16: 'hat3x',
    0x17: 'hat3y',
    0x18: 'pressure',
    0x19: 'distance',
    0x1a: 'tilt_x',
    0x1b: 'tilt_y',
    0x1c: 'tool_width',
    0x20: 'volume',
    0x28: 'misc',
}

button_names = {
    0x120: 'select',
    0x121: 'lbutton',
    0x122: 'rbutton',
    0x123: 'start',
    0x124: 'lup',
    0x125: 'lright',
    0x126: 'ldown',
    0x127: 'lleft',
    0x128: 'tl1',
    0x129: 'tr1',
    0x12a: 'tl2',
    0x12b: 'tr2',
    0x12c: 'by',
    0x12d: 'ba',
    0x12e: 'bb',
    0x12f: 'bx',
    0x130: 'cross',
    0x131: 'circle',
    0x132: 'c',
    0x133: 'triangle',
    0x134: 'square',
    0x135: 'z',
    0x136: 'tl',
    0x137: 'tr',
    0x138: 'tl2',
    0x139: 'tr2',
    0x13a: 'share',
    0x13b: 'options',
    0x13c: 'ps',
    0x13d: 'lfire',
    0x13e: 'rfire'
    # 0x13a: 'select',
    # 0x13b: 'start',
    # 0x13c: 'mode',
    # 0x13d: 'thumbl',
    # 0x13e: 'thumbr',

    # 0x220: 'dpad_up',
    # 0x221: 'dpad_down',
    # 0x222: 'dpad_left',
    # 0x223: 'dpad_right',

    # XBo 360 controller uses these codes.
    # 0x2c0: 'dpad_left',
    # 0x2c1: 'dpad_right',
    # 0x2c2: 'dpad_up',
    # 0x2c3: 'dpad_down',
}


class Joystick(ABC):
    def __init__(self, joystick: 'JoystickController') -> None:
        self.joystick = joystick


class ButtonState:
    def __init__(self, name: str, initial_value: Optional[int] = None):
        self.name = name
        self._value: Optional[int] = initial_value
        self._last_value: Optional[int] = initial_value
        self._changed = False
        self._timer = 0
        self._present = False

    @property
    def value(self) -> int:
        return self._value

    @value.setter
    def value(self, new_value: int) -> None:
        self._last_value = self._value
        self._value = new_value
        self._changed = self._value != self._last_value
        self._timer = 0

    @property
    def present(self) -> bool:
        return self._present

    @present.setter
    def present(self, new_present_value: bool) -> None:
        self._present = new_present_value

    @property
    def changed(self) -> bool:
        return self._changed

    def process(self) -> None:
        self._changed = False
        self._timer += 1

    @property
    def selected(self) -> bool:
        return self._value == 1 and self._value != self._last_value

    @property
    def released(self) -> bool:
        return self._value == 0 and self._value != self._last_value

    @property
    def long_released(self) -> bool:
        return self.released and self._timer > 10

    def __repr__(self) -> str:
        return f"ButtonState[{self._value}/{self._last_value}, {self._changed}, {self._timer}]"

    def __str__(self) -> str:
        return self.__repr__()


class JoystickController:
    def __init__(self) -> None:
        self.axis_states = {
            "x": 0.0, "y": 0.0,
            "rx": 0.0, "ry": 0.0
        }
        self.button_states = {
            "x3": ButtonState("x3", 0), "y3": ButtonState("x3", 0)
        }

        for button_name in button_names.values():
            self.button_states[button_name] = ButtonState(button_name, 0)

        self.last_axis_states = {k: v for k, v in self.axis_states.items()}

        self.speed_index = 8
        self.speeds = [5, 10, 15, 20, 25, 30, 40, 50, 60, 75, 100, 150, 300]
        self.top_speed = self.speeds[self.speed_index]

        self.already_stopped = 0

        self.rover_turning_distance = 0
        self.rover_speed = 0
        self.rover_height = 85

        self.mode = Modes.NORMAL

        self.have_joystick_event = False

        self.already_stopped = 0

    def process_buttons(self):

        if DEBUG_BUTTON_CHANGES:
            changes = {k: v for k, v in self.button_states.items() if v.changed}
            if len(changes) > 0:
                print(f"   Get changes {changes}")

        try:
            if "hat0x" in self.axis_states:
                x3 = int(self.axis_states["hat0x"])
                y3 = int(self.axis_states["hat0y"])
            elif "lup" in self.button_states:
                l_up = self.button_states["lup"].value
                l_right = self.button_states["lright"].value
                l_down = self.button_states["ldown"].value
                l_left = self.button_states["lleft"].value

                x3 = -1 if l_left else (1 if l_right else 0)
                y3 = 1 if l_up else (-1 if l_down else 0)
            else:
                x3 = 0
                y3 = 0

            self.button_states["x3"].value = x3
            self.button_states["y3"].value = y3

            if self.mode == Modes.NORMAL:
                if self.button_states["x3"].changed:
                    if x3 > 0:
                        if self.speed_index < len(self.speeds):
                            self.speed_index += 1
                            self.top_speed = self.speeds[self.speed_index]
                            if DEBUG_LOGIC: print(f"    changed top speed {self.top_speed}, {self.speed_index}")
                    elif x3 < 0:
                        if self.speed_index > 0:
                            self.speed_index -= 1
                            self.top_speed = self.speeds[self.speed_index]
                            if DEBUG_LOGIC: print(f"    changed top speed {self.top_speed}, {self.speed_index}")
                if self.button_states["y3"].changed:
                    if y3 > 0:
                        self.rover_height = (self.rover_height - 5) if self.rover_height >= 5 else 0
                        pyros.publish("move/height", f"{int(self.rover_height)}")
                    elif y3 < 0:
                        self.rover_height = (self.rover_height + 5) if self.rover_height < 100 else 100
                        pyros.publish("move/height", f"{int(self.rover_height)}")

                if self.button_states["options"].selected:
                    pyros.publish("menu/enter", "options")
                    self.mode = Modes.MENU

            elif self.mode == Modes.MENU:
                if self.button_states["x3"].changed:
                    if x3 < 0:
                        pyros.publish("menu/navigate", "left")
                    elif x3 > 0:
                        pyros.publish("menu/navigate", "right")
                if self.button_states["y3"].changed:
                    if y3 < 0:
                        pyros.publish("menu/navigate", "down")
                    elif y3 > 0:
                        pyros.publish("menu/navigate", "up")

                for select_button in ["triangle", "circle", "square", "cross"]:
                    if self.button_states[select_button].selected:
                        pyros.publish("menu/select", select_button)

                if self.button_states["options"].selected:
                    self.mode = Modes.NORMAL

            for button_name in self.button_states:
                self.button_states[button_name].process()

            if DEBUG_BUTTONS: print(f"OK Button states: {({k: v.value for k, v in self.button_states.items()})}")

        except Exception as ex:
            print(f"ERR Button states: {({k: v.value for k, v in self.button_states.items()})}; " + str(ex) + "\n" + ''.join(traceback.format_tb(ex.__traceback__)))

    def calc_rover_speed(self, speed: float) -> float:
        spd = int(speed * self.top_speed)
        spd = 300 if spd > 300 else (-300 if spd < -300 else spd)

        return spd

    @staticmethod
    def calculate_expo(v: float, expo_percentage: float) -> float:
        return v * v * expo_percentage + v * (1.0 - expo_percentage) if v >= 0 else - v * v * expo_percentage + v * (1.0 - expo_percentage)

    @staticmethod
    def calc_rover_distance(distance):
        return int((1.0 - abs(distance) + 0.2) * 500 * (1 if distance > 0 else -1))

    def process_joysticks(self):
        lx = float(self.axis_states["x"])
        ly = float(self.axis_states["y"])

        rx = float(self.axis_states["rx"])
        ry = float(self.axis_states["ry"])

        ld = math.sqrt(lx * lx + ly * ly)
        rd = math.sqrt(rx * rx + ry * ry)
        ra = math.atan2(rx, -ry) * 180 / math.pi

        if self.mode == Modes.NORMAL:
            if ld < 0.1 < rd:
                distance = self.calculate_expo(rd, EXPO)
                self.rover_speed = self.calc_rover_speed(distance)

                pyros.publish("move/drive", f"{round(ra, 1)} {int(self.rover_speed)}")
                if DEBUG_JOYSTICK: print(f"Driving a:{round(ra, 1)} s:{self.rover_speed} ld:{ld} rd:{rd}")

                self.already_stopped = 0
                if DEBUG_LOGIC: print(f"    reset already stopped; {self.already_stopped}")

            elif ld > 0.1 and rd > 0.1:
                ory = ry
                olx = lx
                ry = self.calculate_expo(ry, EXPO)
                lx = self.calculate_expo(lx, EXPO)

                self.rover_speed = -self.calc_rover_speed(ry) * 1.3
                self.rover_turning_distance = self.calc_rover_distance(lx)
                pyros.publish("move/steer", f"{self.rover_turning_distance} {int(self.rover_speed)}")
                if DEBUG_JOYSTICK: print(f"Steering d:{self.rover_turning_distance} s:{self.rover_speed} ry: {ory} lx:{olx} ld:{ld} rd:{rd}")

                self.already_stopped = 0
                if DEBUG_LOGIC: print(f"    reset already stopped; {self.already_stopped}")

            elif ld > 0.1:
                olx = lx
                lx = self.calculate_expo(lx, EXPO) / 2
                self.rover_speed = self.calc_rover_speed(lx)
                pyros.publish("move/rotate", int(self.rover_speed))
                if DEBUG_JOYSTICK: print(f"Rotate s: {self.rover_speed}, lx: {olx}, ld: {ld}, rd: {rd}")

                self.already_stopped = 0
                if DEBUG_LOGIC: print(f"    reset already stopped; {self.already_stopped}")

            else:
                if self.already_stopped < MAX_STOPPING:
                    pyros.publish("move/stop", "0")
                    self.already_stopped += 1
                    if DEBUG_LOGIC: print(f"    already stopped {self.already_stopped}")

                if DEBUG_JOYSTICK: print(f"Rotate stop: ld: {ld}, rd: {rd}")

        if DEBUG_AXES: print(f"OK Axis states: {self.axis_states}")


class EventLoopJoystick(Joystick):
    def __init__(self, joystick: JoystickController) -> None:
        super().__init__(joystick)

        self.fn = '/dev/input/js0'
        self.jsdev = None

        self.axis_map = []
        self.button_map = []

    def connect_to_joystick(self, print_error: bool) -> bool:
        try:
            if DEBUG_SETUP: print(f"   Opening {self.fn}...")
            self.jsdev = open(self.fn, 'rb')

            buf = array.array('b', [0] * 64)

            ioctl(self.jsdev, 0x80006a13 + (0x10000 * len(buf)), buf)  # JSIOCGNAME(len)
            js_name = buf.tobytes().decode("UTF-8")
            print(f"    Device name: {js_name}")

            # Get number of axes and buttons.
            buf = array.array('B', [0])
            ioctl(self.jsdev, 0x80016a11, buf)  # JSIOCGAXES
            num_axes = buf[0]

            buf = array.array('B', [0])
            ioctl(self.jsdev, 0x80016a12, buf)  # JSIOCGBUTTONS
            num_buttons = buf[0]

            # Get the axis map.
            buf = array.array('B', [0] * 0x40)
            ioctl(self.jsdev, 0x80406a32, buf)  # JSIOCGAXMAP

            for axis in buf[:num_axes]:
                axis_name = axis_names.get(axis, '0x%02x' % axis)
                self.axis_map.append(axis_name)
                self.joystick.axis_states[axis_name] = 0.0

            # Get the button map.
            buf = array.array('H', [0] * 200)
            ioctl(self.jsdev, 0x80406a34, buf)  # JSIOCGBTNMAP

            for btn in buf[:num_buttons]:
                btn_name = button_names.get(btn, '0x%03x' % btn)
                self.button_map.append(btn_name)
                if btn_name not in self.joystick.button_states:
                    self.joystick.button_states[btn_name] = ButtonState(btn_name, 0)
                self.joystick.button_states[btn_name].present = True

            print(f"    {num_axes} axes found: {', '.join(self.axis_map)}")
            print(f"    {num_buttons} buttons found: {', '.join(self.button_map)}")
            return True
        except Exception as ex:
            if print_error:
                print("ERROR: Failed to connect to joystick; " + str(ex) + "\n" + ''.join(traceback.format_tb(ex.__traceback__)))
            return False
        except BaseException as ex:
            if print_error:
                print("ERROR: Failed to connect to joystick - no exception given; " + str(ex) + "\n" + ''.join(traceback.format_tb(ex.__traceback__)))
            return False

    def read_events(self) -> None:
        reconnect = True
        no_error = True

        while True:
            if reconnect:
                connected = self.connect_to_joystick(no_error)
                if connected:
                    reconnect = False
                    no_error = True
                else:
                    no_error = False
                time.sleep(0.5)
            else:
                try:
                    evbuf = self.jsdev.read(8)
                    if evbuf:
                        time_of_event, value, event_type, number = struct.unpack('IhBB', evbuf)

                        if event_type & 0x01:
                            button_name = self.button_map[number]
                            if button_name:
                                self.joystick.button_states[button_name].value = value
                                self.joystick.have_joystick_event = True

                        if event_type & 0x02:
                            selected_axis = self.axis_map[number]
                            if selected_axis:
                                f_value = value / 32767.0
                                self.joystick.axis_states[selected_axis] = f_value
                                self.joystick.have_joystick_event = True

                except BaseException as ex:
                    print("ERROR: Failed to read joystick; " + str(ex) + "\n" + ''.join(traceback.format_tb(ex.__traceback__)))
                    reconnect = True
                    for button_state in self.joystick.button_states.values():
                        button_state.present = False
                    time.sleep(0.2)

    def start_read_thread(self) -> None:
        thread = Thread(name="reading-joystick-evdev-events", target=self.read_events, args=(), daemon=True)
        thread.daemon = True
        thread.start()


class UDPJoystick(Joystick):
    def __init__(self, joystick: JoystickController) -> None:
        super().__init__(joystick)
        self.s = None

    # noinspection PyPep8Naming
    def read_UDP_events(self) -> None:
        self.s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.s.bind(('', JCONTROLLER_UDP_PORT))

        self.s.settimeout(10)
        print("    Started receive thread...")
        while True:
            # noinspection PyBroadException
            try:
                data, addr = self.s.recvfrom(1024)
                p = str(data, 'utf-8')

                if p.startswith("J#"):
                    if DEBUG_UDP:
                        print(f"       received {p}")

                    kvps = p[2:].split(";")
                    for kvp in kvps:
                        kv = kvp.split("=")
                        if len(kv) == 2:
                            key = kv[0]
                            value = kv[1]

                            if key in self.joystick.axis_states:
                                self.joystick.axis_states[key] = float(value)
                                self.joystick.have_joystick_event = True
                            elif key in self.joystick.button_states:
                                self.joystick.button_states[key].value = int(value)
                                self.joystick.have_joystick_event = True
                            else:
                                self.joystick.button_states[key] = ButtonState(key, int(value))

            except Exception as _:
                pass

    def start_read_thread(self) -> None:
        thread = Thread(name="reading-joystick-UDP-events", target=self.read_UDP_events, args=(), daemon=True)
        thread.daemon = True
        thread.start()


if __name__ == "__main__":
    try:
        print("Starting jcontroller service...")

        pyros.init("jcontroller-service")

        joystick_controller = JoystickController()
        event_loop_joystick = EventLoopJoystick(joystick_controller)
        udp_joystick = UDPJoystick(joystick_controller)

        event_loop_joystick.start_read_thread()
        udp_joystick.start_read_thread()

        print("Started jcontroller service.")

        # Main event loop
        def loop():
            if joystick_controller.have_joystick_event:
                joystick_controller.have_joystick_event = False
                joystick_controller.process_joysticks()
            joystick_controller.process_buttons()


        pyros.forever(0.1, loop)

    except Exception as ex:
        print("ERROR: " + str(ex) + "\n" + ''.join(traceback.format_tb(ex.__traceback__)))
