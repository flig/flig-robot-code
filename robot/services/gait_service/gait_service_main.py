import time
import traceback
from typing import Optional, List

import RPi.GPIO as GPIO

import pyros

from discovery.interfaces import get_interfaces

from controllers.controller import Controller
from controllers.gait_controller import GaitController
from controllers.joystick_controller import JoystickController
from controllers.leg_controller import LegController
from controllers.move_controller import MoveController
from controllers.servo_conrtoller import ServoController
from gait.gait import GaitManager, GaitProcessState
from gait.gait_manager_implementation import GaitManagerImpl
from body import Body
from gait_orchestrator import GaitOrchestrator
from power_management import PowerManagement, CurrentStatus
from servo_lib.leg_servos import LegServos, ExtraServos
from pcs9685ina3221_servo_lib import PCA9685
from pcs9685ina3221_servo_lib import PCA9685INA3221Servos

DEBUG = True


DEFAULT_DISTANCE_X = 145
DEFAULT_DISTANCE_Y = 60
DEFAULT_THIGH_LEN = 45
DEFAULT_SHIN_LEN = 60
DEFAULT_SERVO_POWER_PIN = 18
DEFAULT_ALL_STOP_PIN = 19
DEFAULT_ALl_STOP_INVERTED = 1


DEFAULT_PCA9685_I2C_ADDRESS = 0x44


class GaitService:
    def __init__(self):
        self.thigh_len = DEFAULT_THIGH_LEN
        self.shin_len = DEFAULT_SHIN_LEN
        self.distance_x = DEFAULT_DISTANCE_X
        self.distance_y = DEFAULT_DISTANCE_Y
        self.servo_power_pin = DEFAULT_SERVO_POWER_PIN
        self.all_stop_pin = DEFAULT_ALL_STOP_PIN
        self.all_stop_inverted = DEFAULT_ALl_STOP_INVERTED
        self.digital_servos = True

        self._expected_int_config_values = {
            "config/thigh_len", "config/shin_len",
            "config/distance_x", "config/distance_y",
            "config/servo_power_pin", "config/all_stop_pin", "config/all_stop_inverted",
            "config/digital_servos"
        }
        self._received_int_config_values = set()

        self.power_management = PowerManagement()

        print("  Starting current stream server...")
        # self.power_management.start_telemetry_server(find_free_port())
        self.power_management.start_telemetry_server(6400)
        pyros.subscribe("telemetry/streams-server/rollcall", self.streams_server_roll_call)
        print(f"  Started telemetry server on port {self.power_management.telemetry_server_port}")

        try:
            self.pca9685 = PCA9685(i2c_addr=DEFAULT_PCA9685_I2C_ADDRESS)
        except Exception as e:
            print(f"ERROR: Failed to setup PCA9685 at address {DEFAULT_PCA9685_I2C_ADDRESS}")
            raise e

        self.servo_driver: Optional[PCA9685INA3221Servos] = None

        self.leg_servos = LegServos()
        self.extra_servos = ExtraServos()
        self._calibration_requested = False
        self._calibration_received = False
        self._calibration_waiting_time = 20
        self._max_calibration_waiting_time = 40
        self.requested_calibration_details_time = 0

        self.previous_current_status: Optional[CurrentStatus] = None

        self.body: Optional[Body] = None  # Body(self.leg_servos, self.servo_driver, thigh_length, shin_length, distance_x, distance_y)

        self.gait_manager: Optional[GaitManager] = None  # GaitManager(self.body)
        self.gait_orchestrator = GaitOrchestrator()

        self.servo_controller = ServoController(self.gait_orchestrator, self.leg_servos, self.extra_servos)
        self.leg_controller = LegController(self.gait_orchestrator, self.servo_controller)
        self.gait_controller = GaitController(self.gait_orchestrator, self.servo_controller)
        self.move_controller = MoveController(self.gait_orchestrator)
        self.joystick_controller = JoystickController(self.gait_orchestrator, self.servo_controller)
        self.controllers: List[Controller] = [
            self.servo_controller,
            self.leg_controller,
            self.gait_controller,
            self.move_controller,
            self.joystick_controller
        ]

        pyros.subscribe("storage/write/config/#", self._handle_extra_configuration)

        pyros.init("gait-service")

    def streams_server_roll_call(self, _topic, _message) -> None:

        ifaces = get_interfaces()

        for iface_name in ifaces:
            iface = ifaces[iface_name]
            if 'inet' in iface.addrs and 'broadcast' in iface.addrs and iface.addrs['inet'] != '127.0.0.1':
                my_ip = iface.addrs['inet']
                pyros.publish("telemetry/streams-server/details", f"{my_ip}:{self.power_management.telemetry_server_port}")

    @staticmethod
    def _handle_servo_driver_global_off(global_off: bool) -> None:
        if global_off:
            pyros.publish("servo/status", "stopped")
        else:
            pyros.publish("servo/status", "started")

    @staticmethod
    def _handle_gait_manager_callback(gait_name: str, state: GaitProcessState) -> None:
        pyros.publish("gait/status", f"{gait_name} {state.name}")

    def _finish_setup(self) -> None:
        # self.streams_server_roll_call("telemetry/streams-server/rollcall", "")

        servo_power_pin: Optional[int] = None
        if self.servo_power_pin > 0:
            servo_power_pin = self.servo_power_pin

        digital_servos = False
        if self.digital_servos > 0:
            digital_servos = True

        self.servo_driver = PCA9685INA3221Servos(self.pca9685,
                                                 self.power_management.ina1_driver, self.power_management.ina2_driver,
                                                 all_off_pin=servo_power_pin,
                                                 digital_servo=digital_servos,
                                                 use_all_off_for_power=digital_servos)
        self.servo_driver.add_callback(self._handle_servo_driver_global_off)
        self.servo_driver.send_immediately = False
        print(f"    set up servo power pin {self.servo_power_pin}")

        self.body = Body(self.leg_servos, self.servo_driver,
                         self.thigh_len, self.shin_len,
                         self.distance_x, self.distance_y)
        self.leg_controller.drive_legs = True

        self.gait_manager = GaitManagerImpl(self.body)
        self.gait_orchestrator.drive_gait = True
        self.gait_orchestrator.debug_gait = False
        self.gait_manager.add_callback(self._handle_gait_manager_callback)

        self.gait_manager.register_known_gaits()
        self.gait_orchestrator.gait_manager = self.gait_manager

        self.servo_controller.servo_driver = self.servo_driver
        self.leg_controller.body = self.body

        GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BCM)
        print(f"    set up read all stop pin {self.all_stop_pin} {'as inverted' if self.all_stop_inverted else ''}")
        GPIO.setup(self.all_stop_pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)

    @staticmethod
    def _request_extra_configuration_details() -> None:
        pyros.publish("storage/read/config", "")

    def _handle_extra_configuration(self, topic, message) -> None:
        split_path = topic.split("/")
        del split_path[0]
        del split_path[0]

        topic = "/".join(split_path)
        if message != "" and topic in self._expected_int_config_values:
            self._received_int_config_values.add(topic)
            setattr(self, split_path[-1], int(message))
            print(f"    ... received {topic} '{message}'")

    def _extra_configuration_received(self) -> bool:
        if self._expected_int_config_values != self._received_int_config_values:
            return False
        return True

    def start_service(self) -> None:
        pyros.forever(0.02, outer=self.main_loop)

    def main_loop(self) -> None:
        current_time = time.time()

        if not self._calibration_requested:
            self._calibration_requested = True
            print(f"  Requested leg calibration data...")
            self.leg_servos.request_calibration_details()

            print(f"  Requested extra servo calibration data...")
            self.extra_servos.request_calibration_details()

            print(f"  Requested extra configuration data...")
            self._request_extra_configuration_details()

            self.requested_calibration_details_time = current_time

        if not self._calibration_received:
            legs_calibration_received = self.leg_servos.check_if_calibration_has_arrived()
            extra_servo_calibration_received = self.leg_servos.check_if_calibration_has_arrived()
            extra_configuration_received = self._extra_configuration_received()
            if legs_calibration_received and extra_configuration_received and extra_servo_calibration_received:
                print(f"Received calibration data. Starting legs...")
                self._calibration_received = True

                self._finish_setup()

                self.gait_manager.select_gait("unknown_leg_position", current_time, {})
                self.gait_manager.process(current_time)
            elif self.requested_calibration_details_time + self._calibration_waiting_time < current_time:
                self._calibration_waiting_time += 5
                if self._calibration_waiting_time > self._max_calibration_waiting_time:
                    self._calibration_waiting_time = self._max_calibration_waiting_time
                if not legs_calibration_received:
                    print(f"  Requested leg calibration data again ({self._calibration_waiting_time})...")
                    self.leg_servos.request_calibration_details()
                if not extra_servo_calibration_received:
                    print(f"  Requested extra servo calibration data again ({self._calibration_waiting_time})...")
                    self.extra_servos.request_calibration_details()
                if not extra_configuration_received:
                    print(f"  Requested extra configuration data again ({self._calibration_waiting_time})...")
                    self._request_extra_configuration_details()
                self.requested_calibration_details_time = current_time

        else:

            all_stop_pin_state = GPIO.input(self.all_stop_pin)
            if (not all_stop_pin_state and self.all_stop_inverted) \
                    or (all_stop_pin_state and not self.all_stop_inverted):
                self.servo_controller.all_stop()

            self.power_management.process_current_data(current_time)

            if self.power_management.high_current_overall_status != self.previous_current_status:
                if self.power_management.high_current_overall_status == CurrentStatus.CRITICAL:
                    print(f"ERROR:   Detected CRITICALLY HIGH current - stopping all!!!  {self.power_management.high_current_overall_status_message}")
                    self.servo_controller.all_stop()
                elif self.power_management.high_current_overall_status == CurrentStatus.WARNING:
                    print(f"WARNING: Detected high current! {self.power_management.high_current_overall_status_message}")
                elif self.previous_current_status is not None:
                    print(f"INFO:    Current returned nominal.")
                else:
                    print(f"INFO:    Got first current status - it is nominal.")
                self.previous_current_status = self.power_management.high_current_overall_status

            if not self.servo_driver.global_off:
                self.servo_driver.process(current_time)
            if self.leg_controller.drive_legs:
                if self.body is not None:
                    self.body.drive(current_time)
                else:
                    self.leg_controller.drive_legs = False
                    print(f"GaitManager: ERROR! 'drive_legs' was set to True without body being initialised!")
            if self.gait_orchestrator.drive_gait:
                if self.gait_manager is not None:
                    self.gait_manager.process(current_time)
                else:
                    self.gait_orchestrator.drive_gait = False
                    print(f"GaitManager: ERROR! 'drive_gait' was set to True without gait manager being initialised!")


if __name__ == "__main__":
    try:
        print("Starting gait service...")

        gait_service = GaitService()
        print("Started gait service.")

        gait_service.start_service()

    except Exception as ex:
        print("ERROR: " + str(ex) + "\n" + ''.join(traceback.format_tb(ex.__traceback__)))
