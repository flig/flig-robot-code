from enum import Enum, auto
from math import cos, sin
from typing import Dict, List, Optional

from gait.gait import GaitManager, GaitProcessState, Gait
from body import LegID, Leg
from gait.walk_gait import WalkGait
from path import Path, LinearPath, ArcPath
from vector import Vector3

DEBUG = True
DEBUG_PATHS = True


class TrotGaitStages(Enum):
    STOP = auto(),
    PREPARE = auto(),
    WALK = auto()


class TrotGait(WalkGait):
    def __init__(self, gait_manager: GaitManager, name: str = "trot"):
        super().__init__(gait_manager, name)
        self.gait_state = TrotGaitStages.STOP

        self.phase: int = 0
        self.leg_phases: Dict[LegID, int] = {
            LegID.FRONT_RIGHT_LEG: 0,
            LegID.BACK_LEFT_LEG: 0,
            LegID.FRONT_LEFT_LEG: 1,
            LegID.BACK_RIGHT_LEG: 1
        }

        self.leg_paths: Dict[LegID, List[Path]] = {
            LegID.FRONT_RIGHT_LEG: [],
            LegID.BACK_LEFT_LEG: [],
            LegID.FRONT_LEFT_LEG: [],
            LegID.BACK_RIGHT_LEG: []
        }

        self.phases_number = 2

        self.required_body_position = Vector3()
        self.height_position_path = LinearPath()

    def start(self, current_time: float, context: dict) -> None:
        if DEBUG: print(f"Trot: starting walk gait {context}")
        super().start(current_time, context)
        self.gait_state = TrotGaitStages.PREPARE
        self.phase: int = 0
        self.prepare(current_time, context)

    def cont(self, current_time: float, context: dict) -> None:
        if DEBUG: print(f"Trot: cont walk gait {context}")
        super().cont(current_time, context)

    def update(self, current_time: float, context: dict) -> None:
        if DEBUG: print(f"Trot: updating walk gait {context}")
        super().update(current_time, context)

    def prepare(self, current_time: float, context: dict) -> None:
        if DEBUG: print(f"Trot: preparing trot gait -"
                        f" height={self.height},"
                        f" angle={self.angle:.1f}, distance={(self.distance if self.distance else 0.0):.0f},"
                        f" arc={self.leg_raise}, stride={self.stride},"
                        f" ox={self.ox}, oy={self.oy}, cx={self.cx}, cy={self.cy},"
                        f" speed={self.speed}, cog_speed={self.cog_speed},"
                        f" power={self.power}")

        if DEBUG: print(f"Trot: setting up height to be {self.height} (1s)")

        self.required_body_position.set_values(0, 0, self.height)
        self.height_position_path.end.set(self.required_body_position)
        self.body.set_body_position_path(self.height_position_path, current_time, 1)

        self._create_paths_for_legs()

    def stop(self, current_time: float, new_gait: Optional[Gait], new_context: dict, active: bool) -> None:
        pass

    def process(self, current_time: float, context: dict) -> GaitProcessState:
        def next_phase(phase: int) -> int: return phase + 1 if phase < self.phases_number - 1 else 0

        if (self.gait_state != TrotGaitStages.WALK and self.body.is_all_at_destination(current_time)) \
                or (self.gait_state == TrotGaitStages.WALK and self.body.are_legs_at_destination(current_time)):
            if DEBUG: print(f"Trot: phase {self.phase}")
            self.gait_state = TrotGaitStages.WALK

            if self.updated:
                self.updated = False
                self._create_paths_for_legs()

            for leg_id in LegID:
                leg = self.body[leg_id]
                path = self.leg_paths[leg_id][self.phase]
                leg.set_path(path, current_time, self.speed, power=self.power)

            self.phase = next_phase(self.phase)
            return GaitProcessState.OPERATING_END_OF_PHASE

        return GaitProcessState.OPERATING

    def _create_paths_for_legs(self) -> None:
        furthest_distance = self.calculate_furthest_distance()
        for leg_id in LegID:
            leg = self.body[leg_id]
            self.leg_paths[leg_id] = []
            for i in range(2):
                self.leg_paths[leg_id].append(self.create_path_for_leg(leg, i, furthest_distance))

    def create_path_for_leg(self, leg: Leg, phase: int, furthest_distance: Optional[float]) -> Path:
        leg_id = leg.leg_id

        def fix_phase(_phase: int) -> int: return _phase - self.phases_number if _phase >= self.phases_number else _phase
        leg_phase = fix_phase(phase + self.leg_phases[leg_id])

        stride, angle, x_leg_sign, y_leg_sign = self.prepare_path_for_leg(leg_id, furthest_distance)
        x_adjust = self.cx * x_leg_sign + self.ox
        y_adjust = self.cy * y_leg_sign + self.oy

        if leg_phase == 0:
            d = -stride / 2.0
            x = x_adjust + d * cos(angle)
            y = y_adjust + d * sin(angle)

            if DEBUG: print(f"        Creating arc path for {leg.leg_id} to ({x:.2f}, {y:.2f}, {0:.2f}); phase={phase}; lph={leg_phase}, d={d}, x_adjust={x_adjust}, stride={stride}, angle={angle}")
            return ArcPath(-self.leg_raise).set_end_values(x, y, 0)
        elif leg_phase == 1:
            d = stride / 2.0
            x = x_adjust + d * cos(angle)
            y = y_adjust + d * sin(angle)
            if DEBUG: print(f"        Creating arc path for {leg.leg_id} to ({x:.2f}, {y:.2f}, {0:.2f}); phase={phase}; lph={leg_phase}, d={d}, x_adjust={x_adjust}, stride={stride}, angle={angle}")
            return ArcPath(self.leg_raise).set_end_values(x, y, 0)
        else:
            print(f"Trot: ERROR - leg_phase {leg_phase} for leg {leg.leg_id}")
            raise ValueError(f"Trot: ERROR - leg_phase {leg_phase} for leg {leg.leg_id}")
