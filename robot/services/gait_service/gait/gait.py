from abc import ABC, abstractmethod
from enum import Enum, auto
from typing import Optional, List, Callable

from body import Body
from utils import time_to_string

DEBUG = True

INITIAL_SETUP_GAIT_NAME = "initial_set_up"


class Gate:
    def __init__(self) -> None:
        self._triggered = False

    def trigger(self) -> bool:
        already_triggered = self._triggered
        self._triggered = True
        return already_triggered

    def proceed(self) -> bool:
        was_triggered = self._triggered
        self._triggered = False
        return was_triggered

    @property
    def is_triggered(self) -> bool:
        return self._triggered


class GaitProcessState(Enum):
    NEWLY_SELECTED = auto()
    SETTING_UP = auto()
    OPERATING = auto()
    OPERATING_END_OF_PHASE = auto()
    RESTING = auto()
    FINISHED = auto()


class ContextProcessingHelper:
    def __init__(self, context: dict, update: bool) -> None:
        self.context = context
        self.update = update
        self.changed = False

    def process_float(self, context_key: str, old_value: float, default_value: Optional[float]) -> float:
        new_value = self.context[context_key] if self.context is not None and context_key in self.context else old_value if self.update else default_value
        new_value = float(new_value) if new_value is not None else new_value
        self.changed = self.changed or new_value != old_value
        return new_value

    def process_int(self, context_key: str, old_value: int, default_value: Optional[int]) -> int:
        new_value = self.context[context_key] if self.context is not None and context_key in self.context else old_value if self.update else default_value
        new_value = int(new_value) if new_value is not None else new_value
        self.changed = self.changed or new_value != old_value
        return new_value

    def is_changed(self) -> bool:
        return self.changed

    def __enter__(self) -> 'ContextProcessingHelper':
        return self

    def __exit__(self, exc_type, exc_val, exc_tb) -> bool:
        return False


class Gait(ABC):
    def __init__(self, gait_manager: 'GaitManager', name: str) -> None:
        self.started_time = 0
        self.gait_manager = gait_manager
        self.body = gait_manager.body
        self.speed = 2
        self.power = 100
        self.name = name
        self.half_distance_x = gait_manager.body.distance_x // 2
        self.half_distance_y = gait_manager.body.distance_y // 2

    def __str__(self) -> str:
        return f"{self.name}[]"

    def __repr__(self) -> str:
        return f"{self.name}[]"

    def start(self, current_time: float, context: dict) -> None:
        self.started_time = current_time
        self.speed = float(context["speed"]) if context is not None and "speed" in context else 2
        self.power = int(context["power"]) if context is not None and "power" in context else 100

    def cont(self, current_time: float, context: dict) -> None:
        self.start(current_time, context)

    def update(self, current_time: float, _context: dict) -> None:
        # self.started_time = current_time
        pass

    def stop(self, current_time: float, new_gait: Optional['Gait'], new_context: dict, active: bool) -> None:
        pass

    def process(self, current_time: float, context: dict) -> GaitProcessState:
        """
        :param current_time: current time
        :param context: context it works in
        :return: True if hasn't finish otherwise False
        """
        return GaitProcessState.RESTING

    @staticmethod
    def context_processor(context: dict, update: bool) -> ContextProcessingHelper:
        return ContextProcessingHelper(context, update)

    def debug_print_position(self, with_paths: bool = False) -> None:
        if with_paths:
            print(f"    Body: ({self.body.body_position}) @ {time_to_string(self.started_time)}; {self.body.body_position_path}")
        else:
            print(f"    Body: ({self.body.body_position}) @ {time_to_string(self.started_time)}")
        for leg in self.body:
            if with_paths:
                print(f"    {leg.leg_id} ({leg.position.x:.1f}, {leg.position.y:.1f}, {leg.position.z:.1f}); {leg.path}")
            else:
                print(f"    {leg.leg_id} ({leg.position.x:.1f}, {leg.position.y:.1f}, {leg.position.z:.1f})")

    @staticmethod
    def calculate_speed(speed: float, distance: float) -> float:
        # speed = speed if speed >= 1.0 else 1.0
        return abs(distance * speed / 40.0)


class GaitManager(ABC):
    def __init__(self, body: Body) -> None:
        self.body = body
        self._known_gaits = {}
        self._gait_callbacks: List[Callable[[str, GaitProcessState], None]] = []
        self.stepping = False
        self.step_gate = Gate()

    @property
    @abstractmethod
    def current_gait(self) -> Optional[Gait]:
        pass

    @property
    def current_gait_name(self) -> Optional[str]:
        return self.current_gait.name if self.current_gait is not None else None

    @property
    @abstractmethod
    def current_gait_context(self) -> Optional[dict]:
        pass

    def register_gait(self, gait: Gait) -> None:
        self._known_gaits[gait.name] = gait

    @abstractmethod
    def select_gait(self, name: str, current_time: float, context: dict) -> Optional[Gait]:
        pass

    @abstractmethod
    def push_gait(self, name: str, current_time: float, context: dict) -> Optional[Gait]:
        pass

    @abstractmethod
    def update_current_gait_context(self, current_time: float, context: dict) -> None:
        pass

    @abstractmethod
    def all_stop(self, current_time: float) -> None:
        pass

    @abstractmethod
    def process(self, current_time: float) -> None:
        pass

    def add_callback(self, callback: Callable[[str, GaitProcessState], None]) -> None:
        if callback not in self._gait_callbacks:
            self._gait_callbacks.append(callback)

    def remove_callback(self, callback: Callable[[str, GaitProcessState], None]) -> None:
        if callback in self._gait_callbacks:
            del self._gait_callbacks[self._gait_callbacks.index(callback)]
