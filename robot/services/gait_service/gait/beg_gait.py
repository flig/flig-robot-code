from typing import Dict, List, Optional

from gait.gait import Gait, GaitManager, GaitProcessState
from body import LegID
from path import LinearPath, Path

DEBUG = True


class BegGait(Gait):
    def __init__(self, gait_manager: GaitManager, name: str = "beg"):
        super().__init__(gait_manager, name)
        self.speed = 1
        self.phase = 0
        self.finished = False
        self._finished_reported = False
        self.up = True
        self.leg_paths: Dict[LegID, List[Path]] = {
            LegID.FRONT_RIGHT_LEG: [LinearPath(), LinearPath(), LinearPath(), LinearPath()],
            LegID.FRONT_LEFT_LEG: [LinearPath(), LinearPath(), LinearPath(), LinearPath()],
            LegID.BACK_RIGHT_LEG: [LinearPath(), LinearPath(), LinearPath(), LinearPath()],
            LegID.BACK_LEFT_LEG: [LinearPath(), LinearPath(), LinearPath(), LinearPath()]
        }

        for p in range(4):
            for leg_id in self.leg_paths:
                if p == 0:
                    self.leg_paths[leg_id][p].end.set_values(x=35, y=0, z=0)
                elif p == 1:
                    if leg_id.is_front:
                        self.leg_paths[leg_id][p].end.set(self.leg_paths[leg_id][0].end)
                    else:
                        self.leg_paths[leg_id][p].end.set_values(x=70, y=0, z=0)
                elif p == 2:
                    if leg_id.is_front:
                        self.leg_paths[leg_id][p].end.set(self.leg_paths[leg_id][0].end)
                    else:
                        self.leg_paths[leg_id][p].end.set_values(x=40, y=0, z=50)
                elif p == 3:
                    if leg_id.is_front:
                        self.leg_paths[leg_id][p].end.set_values(x=9, y=0, z=-10)
                    else:
                        self.leg_paths[leg_id][p].end.set_values(x=-50, y=0, z=50)

    def start(self, current_time: float, context: dict) -> None:
        super().start(current_time, context)

        self.up = (context["up"].lower() in ["true", "t", "1", "y"]) if context is not None and "up" in context else self.phase == 0

        if self.up:
            self.phase = 0
        else:
            self.phase = 4

        if DEBUG: print(f"Beg: starting beg gait, up={self.up}, phase={self.phase}")

        self.body.all_on()

        self.finished = False
        self._finished_reported = False

        for leg in self.body:
            path = self.leg_paths[leg.leg_id][self.phase]
            leg.set_path(path, current_time, self.speed, power=self.power)

        if self.up:
            self.body.body_linear_move(0, 0, 45, current_time, self.speed)

    def stop(self, current_time: float, new_gait: Optional[Gait], new_context: dict, active: bool) -> None:
        pass

    def process(self, current_time: float, context: dict) -> GaitProcessState:
        if not self.finished:
            for leg in self.body:
                if not leg.at_destination(current_time):
                    # if DEBUG: print(f"Beg: Leg {leg.leg_id.name} not at position, yet...")
                    return GaitProcessState.OPERATING
            if not self.body.is_body_at_destination(current_time):
                # if DEBUG: print(f"Beg: Body (height) not at position, yet...")
                return GaitProcessState.OPERATING

            print(f"Beg: Legs arrived at required position")
            if self.up:
                if self.phase < 3:
                    self.phase += 1
                else:
                    self.finished = True
            else:
                if self.phase > 0:
                    self.phase -= 1
                else:
                    self.finished = True

            if not self.finished:
                for leg in self.body:
                    path = self.leg_paths[leg.leg_id][self.phase]
                    leg.set_path(path, current_time, self.speed, power=self.power)
            # else:
            #     for leg in self.legs:
            #         leg.turn_servos_off()
            #     self.legs.all_off()
        else:
            if not self._finished_reported:
                if DEBUG: print(f"Beg: Legs finished")
                self._finished_reported = True

        if self.finished:
            return GaitProcessState.RESTING
        return GaitProcessState.OPERATING
