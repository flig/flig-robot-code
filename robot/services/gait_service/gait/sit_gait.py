from typing import Optional

from gait.gait import Gait, GaitManager, GaitProcessState
from support.common_gait import CommonGait


DEBUG = True


DEFAULT_HEIGHT = 50
DEFAULT_BACK_BACK_X = 20
DEFAULT_BACK_FRONT_X = -20
DEFAULT_BACK_FRONT_Z = 40

DEFAULT_FRONT_FRONT_X = 45
DEFAULT_FRONT_BACK_X = 0
DEFAULT_FRONT_BACK_Z = 70

# "fx": 45,
# "fz": -70,
# "bx": 0,
# "bz": 10


class SitGait(CommonGait):
    def __init__(self, gait_manager: GaitManager, name: str = "sit"):
        super().__init__(gait_manager, name, default_values={
            "bbx": DEFAULT_BACK_BACK_X,
            "bfx": DEFAULT_BACK_FRONT_X,
            "bz": DEFAULT_BACK_FRONT_Z,
            "fbx": DEFAULT_FRONT_BACK_X,
            "ffx": DEFAULT_FRONT_FRONT_X,
            "fz": DEFAULT_FRONT_BACK_Z,
            "height": DEFAULT_HEIGHT,
            "speed": 1,
            "direction": 1
        })
        self.finished = True
        self.bbx = DEFAULT_BACK_BACK_X
        self.bfx = DEFAULT_BACK_FRONT_X
        self.bz = DEFAULT_BACK_FRONT_Z
        self.fbx = DEFAULT_FRONT_BACK_X
        self.ffx = DEFAULT_FRONT_FRONT_X
        self.fz = DEFAULT_FRONT_BACK_Z
        self.direction = 0

    def process_context(self, context: dict, update: bool = False) -> bool:
        changed = super().process_context(context, update)
        with self.context_processor(context, update) as c:
            self.bbx = c.process_float("bbx", self.bbx, self.default_values["bbx"])
            self.bfx = c.process_float("bfx", self.bfx, self.default_values["bfx"])
            self.bz = c.process_float("bz", self.bz, self.default_values["bz"])

            self.fbx = c.process_float("fbx", self.fbx, self.default_values["fbx"])
            self.ffx = c.process_float("ffx", self.ffx, self.default_values["ffx"])
            self.fz = c.process_float("fz", self.fz, self.default_values["fz"])

            self.direction = c.process_float("direction", self.direction, self.default_values["direction"])

            return changed or c.is_changed()

    def start(self, current_time: float, context: dict) -> None:
        if DEBUG: print(f"Sit: starting sit gait {context}")
        if DEBUG: self.debug_print_position()

        super().start(current_time, context)
        self.body.all_on()

        x_offset = self.body.body_position.x
        y_offset = self.body.body_position.y
        for leg in self.body:
            leg.position.x += x_offset
            leg.position.y += y_offset
        self.body.body_position.x = 0
        self.body.body_position.y = 0

        self.finished = False
        self.start_setting_legs(current_time)

    def update(self, current_time: float, context: dict) -> None:
        if DEBUG: print(f"Sit: got update {context}")
        self.start(current_time, context)

    def stop(self, current_time: float, new_gait: Optional[Gait], new_context: dict, active: bool) -> None:
        if DEBUG: print(f"Sit: stopping stand gait")

    def process(self, current_time: float, context: dict) -> GaitProcessState:
        if self.body.is_all_at_destination(current_time):
            if self.updated:
                self.start(current_time, context)
                return GaitProcessState.OPERATING

            if not self.finished:
                print(f"Sit: Legs arrived at required position")
                self.finished = True
            return GaitProcessState.RESTING
        else:
            return GaitProcessState.OPERATING

    def start_setting_legs(self, current_time: float) -> None:
        delta_height = abs(self.height - self.body.body_position.z)

        speed = self.calculate_speed(self.speed, delta_height)

        self.body.body_linear_move(0, 0, self.height, current_time, speed)
        if self.direction > 0:
            for leg in self.body.legs.values():
                sx, sy = leg.leg_id.leg_sign_xy()
                x_adjust = self.cx * sx + self.ox
                y_adjust = self.cy * sy + self.oy

                if leg.leg_id.is_back:
                    distance = leg.position.distance_values(self.bbx + x_adjust, y_adjust, 0)
                    speed = self.calculate_speed(self.speed, distance)
                    leg.linear_move(self.bbx + x_adjust, y_adjust, 0, current_time, speed)
                else:
                    distance = leg.position.distance_values(self.bfx + x_adjust, y_adjust, self.bz)
                    speed = self.calculate_speed(self.speed, distance)
                    leg.linear_move(self.bfx + x_adjust, y_adjust, self.bz, current_time, speed)
        else:
            for leg in self.body.legs.values():
                sx, sy = leg.leg_id.leg_sign_xy()
                x_adjust = self.cx * sx + self.ox
                y_adjust = self.cy * sy + self.oy

                if leg.leg_id.is_back:
                    distance = leg.position.distance_values(self.fbx + x_adjust, y_adjust, self.fz)
                    speed = self.calculate_speed(self.speed, distance)
                    leg.linear_move(self.fbx + x_adjust, y_adjust, self.fz, current_time, speed)
                else:
                    distance = leg.position.distance_values(self.ffx + x_adjust, y_adjust, 0)
                    speed = self.calculate_speed(self.speed, distance)
                    leg.linear_move(self.ffx + x_adjust, y_adjust, 0, current_time, speed)

        if DEBUG: print(f"    path for body {self.body.body_position} to {self.body.body_position_path.end}")
