from typing import Optional

from gait.gait import Gait, GaitManager, GaitProcessState
from support.common_gait import CommonGait


DEBUG = True


class LayGait(CommonGait):
    def __init__(self, gait_manager: GaitManager, name: str = "lay"):
        super().__init__(gait_manager, name, default_values={
            "height": 50,
            "speed": 1,
            "x": 37
        })
        self.finished = True
        self.height = 50
        self.x = 37

    def process_context(self, context: dict, update: bool = False) -> bool:
        changed = super().process_context(context, update)
        with self.context_processor(context, update) as c:
            self.x = c.process_float("x", self.x, self.default_values["x"])

            return changed or c.is_changed()

    def start(self, current_time: float, context: dict) -> None:
        if DEBUG: print(f"Lay: starting lay gait")
        if DEBUG: self.debug_print_position()

        super().start(current_time, context)
        self.body.all_on()

        x_offset = self.body.body_position.x
        y_offset = self.body.body_position.y
        for leg in self.body:
            leg.position.x += x_offset
            leg.position.y += y_offset
        self.body.body_position.x = 0
        self.body.body_position.y = 0

        self.finished = False
        self.start_setting_legs(current_time)

    def stop(self, current_time: float, new_gait: Optional[Gait], new_context: dict, active: bool) -> None:
        if DEBUG: print(f"Lay: stopping stand gait")

    def process(self, current_time: float, context: dict) -> GaitProcessState:
        if self.body.is_all_at_destination(current_time):
            print(f"Lay: Legs arrived at required position")
            self.finished = True
            return GaitProcessState.RESTING
        else:
            return GaitProcessState.OPERATING

    def start_setting_legs(self, current_time: float) -> None:
        max_height = 0

        for leg in self.body.legs.values():
            leg_height = leg.position.z + self.body.body_position.z
            max_height = leg_height if leg_height > max_height else max_height

        delta_height = abs(self.height - max_height)

        speed = self.calculate_speed(self.speed, delta_height)

        self.body.body_linear_move(0, 0, self.height, current_time, speed)
        for leg in self.body.legs.values():
            leg.linear_move(self.x, 0, 0, current_time, speed)

        if DEBUG: print(f"    path for body {self.body.body_position} to {self.body.body_position_path}")
        if DEBUG: self.debug_print_position(True)
