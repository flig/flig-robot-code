import math
from enum import auto, Enum

from gait.gait import GaitManager, GaitProcessState
from gait.stand_up_gait import CommonStandGait, StandGaitState

DEBUG = True
DEBUG_STATE = False


class RotateGaitState(Enum):
    MoveFLLeg = auto()
    MoveFRLeg = auto()
    MoveBLLeg = auto()
    MoveBRLeg = auto()


class RotateGait(CommonStandGait):
    def __init__(self, gait_manager: GaitManager, name: str = "rotate") -> None:
        super().__init__(gait_manager, name, False, default_values={"rotate": 0})
        self.rotate = 0

    def process_context(self, context: dict, update: bool = False) -> bool:
        changed = super().process_context(context, update)
        with self.context_processor(context, update) as c:
            self.rotate = c.process_float("rotate", self.rotate, self.default_values["rotate"])

            return changed or c.is_changed()

    def start(self, current_time: float, context: dict) -> None:
        super().start(current_time, context)

    def update(self, current_time: float, context: dict) -> None:
        super().start(current_time, context)

    def process(self, current_time: float, context: dict) -> GaitProcessState:
        if DEBUG:
            self.counter += 1
            if self.counter == 50:
                self.counter = 0
                if DEBUG_STATE:
                    print(f"Stand: current state {self.state.name}")

        if self.state != StandGaitState.STANDING:
            if self.state == StandGaitState.RAISING_TO_MINIMUM:
                if self.body.is_all_at_destination(current_time):
                    self.prepare_after_minimal_height(current_time)
            elif self.state == StandGaitState.LEVELING_LEGS:
                if self.body.is_all_at_destination(current_time):
                    self.start_repositioning_legs(current_time)
                elif DEBUG and self.counter == 0:
                    print(f"Stand: waiting for all body to get to destination at LEVELLING_LEGS")
                    print(f"       legs@dest: {self.body.are_legs_at_destination(current_time)} body@dest: {self.body.is_body_at_destination(current_time)}")

            elif self.state == StandGaitState.REPOSITIONING_LEGS:
                if not self.repositioning_legs(current_time):
                    self.start_twisting(current_time)
            elif self.state == StandGaitState.TWIST:
                if self.body.is_all_at_destination(current_time):
                    if DEBUG: print(f"Stand: twist finished; rotate={self.rotate}, twist={self.twist}")
                    if math.isclose(abs(self.twist), 0.0, abs_tol=0.5):
                        self.set_state(StandGaitState.STANDING)
                    else:
                        self.start_repositioning_legs(current_time)

        if self.state == StandGaitState.STANDING:
            if self._final:
                return GaitProcessState.RESTING
            else:
                return GaitProcessState.FINISHED
        return GaitProcessState.OPERATING

    def start_twisting(self, current_time: float) -> None:
        self.process_rotate()

        super().start_twisting(current_time)

    def process_rotate(self) -> None:
        if self.rotate > 15:
            self.twist = 15
        elif self.rotate < -15:
            self.twist = -15
        elif not math.isclose(abs(self.rotate), 0.5, abs_tol=0.1):
            self.twist = self.rotate
        self.rotate -= self.twist
