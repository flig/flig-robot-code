import traceback

import math
from enum import Enum, auto
from math import sin, cos, degrees
from typing import Dict, List, cast, Optional

import math_util
from gait.gait import GaitManager, GaitProcessState, Gait
from gait.utils import absolute_tolerance, NEW_LINE
from gait.walk_gait import WalkGait
from body import LegID, Leg
from path import Path, LinearPath, ArcPath

DEBUG = True
DEBUG_PATHS = True


HALF_PI = math.pi / 2.0
TWO_PI = math.pi * 2.0


class Cat2WalkGaitStages(Enum):
    STOP = auto()
    PREPARE = auto()
    ALL_FORWARD = auto()
    FIRST_STEP = auto()
    ALL_BACK = auto()
    SECOND_STEP = auto()
    WALK = auto()


class CatWalkSP6Gait(WalkGait):
    def __init__(self, gait_manager: GaitManager, name: str = "cat_walk_sp6"):
        super().__init__(gait_manager, name)
        self.gait_state = Cat2WalkGaitStages.STOP

        self.leg_phases: Dict[LegID, int] = {
            LegID.FRONT_RIGHT_LEG: 0,
            LegID.BACK_RIGHT_LEG: 1,
            LegID.FRONT_LEFT_LEG: 2,
            LegID.BACK_LEFT_LEG: 3,
        }
        self.phase: int = 0

        self.leg_paths: Dict[LegID, List[Path]] = {
            LegID.FRONT_RIGHT_LEG: [],
            LegID.BACK_LEFT_LEG: [],
            LegID.FRONT_LEFT_LEG: [],
            LegID.BACK_RIGHT_LEG: []
        }

        self.max_stride = 150

        self.all_forward_paths = {i: LinearPath() for i in LegID}
        self.first_step_path = ArcPath(self.leg_raise)
        self.all_back_paths = {i: LinearPath() for i in LegID}
        self.second_step_path = ArcPath(self.leg_raise)

        self.phases_number = 6

    def start(self, current_time: float, context: dict) -> None:
        if DEBUG: print(f"Pos: starting walk gait - preparing leg positions")
        super().start(current_time, context)
        if DEBUG: self.debug_print_position()

        self.gait_state = Cat2WalkGaitStages.PREPARE

        if DEBUG: print(f"Walk: starting walk gait -"
                        f" height={self.height},"
                        f" angle={self.angle:.1f}, distance={(self.distance if self.distance else 0.0):.0f},"
                        f" arc={self.leg_raise}, stride={self.stride},"
                        f" ox={self.ox}, oy={self.oy}, cx={self.cx}, cy={self.cy},"
                        f" speed={self.speed}, cog_speed={self.cog_speed},"
                        f" power={self.power}")

        # TODO do this smartly!!! Move it to Legs and introduce feedback which legs should 'bear weight' at which point!

        self.phase: int = 0

        self._create_paths_for_legs()

        height_difference = abs(self.height - self.body.body_position.z)

        if height_difference > absolute_tolerance:
            height_speed = min(0.5, height_difference / 20.0)
            self.gait_manager.push_gait("stand_up", current_time, {**context, "speed": height_speed})
        else:
            self.required_body_position.set_values(0, 0, self.height)
            self.height_position_path.end.set(self.required_body_position)

            self.body.set_body_position_path(self.height_position_path, current_time, 1)
            self.gait_state = Cat2WalkGaitStages.PREPARE

    def cont(self, current_time: float, _context: dict) -> None:
        self.started_time = current_time
        self.body.body_linear_move(0, 0, self.height, current_time, self.speed)

        self._create_paths_for_legs()

    def update(self, current_time: float, context: dict) -> None:
        self.started_time = current_time
        changed = self.process_context(context, True)
        if changed:
            self.required_body_position.set_values(0, 0, self.height)
            self.height_position_path.end.set(self.required_body_position)
            self.body.set_body_position_path(self.height_position_path, current_time, 1)
            self.gait_state = Cat2WalkGaitStages.PREPARE

            self._create_paths_for_legs()
            # self._update_body_position_details()

    def stop(self, current_time: float, new_gait: Optional[Gait], new_context: dict, active: bool) -> None:
        if DEBUG: self.debug_print_position()
        pass

    def process(self, current_time: float, context: dict) -> GaitProcessState:
        def next_phase(phase: int) -> int: return phase + 1 if phase + 1 < self.phases_number else 0

        if self.body.is_all_at_destination(current_time):
            if DEBUG: print(f"Walk: phase {self.phase}")

            if self.gait_state == Cat2WalkGaitStages.PREPARE:
                if DEBUG_PATHS: print("    leaning forward")
                for leg_id in LegID:
                    leg = self.body[leg_id]
                    path = self.all_forward_paths[leg_id]
                    path.end.x = max(-self.stride / 2.0, -self.max_stride / 2.0)
                    leg.set_path(path, current_time, self.speed, power=self.power)
                    if DEBUG_PATHS:
                        print(f"    {leg_id}  : {path}")

                self.gait_state = Cat2WalkGaitStages.ALL_FORWARD
            elif self.gait_state == Cat2WalkGaitStages.ALL_FORWARD:
                leg = self.body[LegID.BACK_RIGHT_LEG]
                self.first_step_path.end.x = 0
                leg.set_path(self.first_step_path, current_time, self.speed, power=self.power)
                self.gait_state = Cat2WalkGaitStages.FIRST_STEP
                if DEBUG_PATHS: print(f"    first step {leg.leg_id}  : {self.first_step_path}")
            elif self.gait_state == Cat2WalkGaitStages.FIRST_STEP:
                if DEBUG_PATHS: print("    leaning back")
                for leg_id in LegID:
                    leg = self.body[leg_id]
                    path = self.all_back_paths[leg_id]
                    path.end.x = leg.position.x + self.stride / 2.0
                    leg.set_path(path, current_time, self.speed, power=self.power)
                    if DEBUG_PATHS:
                        print(f"    {leg_id}  : {path}")

                self.gait_state = Cat2WalkGaitStages.ALL_BACK
            elif self.gait_state == Cat2WalkGaitStages.ALL_BACK:
                leg = self.body[LegID.FRONT_RIGHT_LEG]
                self.second_step_path.end.x = self.stride / 2.0
                leg.set_path(self.second_step_path, current_time, self.speed, power=self.power)
                if DEBUG_PATHS: print(f"    first step {leg.leg_id}  : {self.second_step_path}")
                self.gait_state = Cat2WalkGaitStages.SECOND_STEP
            else:
                # try:
                #     path = self.body_position_paths[self.phase]
                #     # path.prepare(self.legs.body_position, path.end, self.legs.body_position, current_time, self.speed)
                #     path.prepare(self.body.body_position, path.end, self.body.body_position, current_time + self.speed * (1 - self.cog_speed), self.speed * self.cog_speed)
                #     self.body.set_body_position_path(path)
                #     if DEBUG_PATHS: print(f"    Body: {path}")
                # except Exception as e:
                #     print(f"ERROR failed to set body path phase={self.phase}, len={len(self.body_position_paths)}, state={self.gait_state}")

                for leg_id in LegID:
                    try:
                        leg = self.body[leg_id]
                        path = self.leg_paths[leg_id][self.phase]
                        leg.set_path(path, current_time, self.speed, power=self.power)
                        if DEBUG_PATHS: print(f"    {leg_id}  : {path}")

                        # for leg_id in self.leg_phases:
                        #     self.leg_phases[leg_id] = next_phase(self.leg_phases[leg_id])
                    except Exception as e:
                        print(f"ERROR failed to set leg path phase={self.phase}, leg={leg_id} len={len(self.leg_paths[leg_id])}, state={self.gait_state}, "
                              f"e={str(e) + NEW_LINE + ''.join(traceback.format_tb(e.__traceback__))}")
                self.gait_state = Cat2WalkGaitStages.WALK

                self.phase = next_phase(self.phase)

        return GaitProcessState.OPERATING

    def _create_paths_for_legs(self) -> None:
        furthest_distance = self.calculate_furthest_distance()
        print(f"    Creating paths from {LegID}")
        for leg_id in LegID:
            leg = self.body[leg_id]
            self.leg_paths[leg_id] = []

            stride, angle, x_leg_sign, y_leg_sign = self.prepare_path_for_leg(leg_id, furthest_distance)
            x_adjust = self.cx * x_leg_sign + self.ox
            y_adjust = self.cy * y_leg_sign + self.oy

            for i in range(self.phases_number):
                path = self.create_path_for_leg(leg, i, stride, angle, x_adjust, y_adjust)
                if DEBUG: print(f"        Creating path {leg_id} as {path}; phase={i}, angle={degrees(angle):.2f}, stride={stride:.2f}")
                self.leg_paths[leg_id].append(path)

    def create_path_for_leg(self, leg: Leg, phase: int, stride: float, angle: float, x_adjust: float, y_adjust: float) -> Path:
        leg_id = leg.leg_id

        if phase == 0:
            if leg_id.is_right:
                return LinearPath().set_end_values(x_adjust, y_adjust, 0)
            else:
                d = -stride / 2.0
                x = x_adjust + d * cos(angle)
                y = y_adjust + d * sin(angle)
                return LinearPath().set_end_values(x, y, 0)
        elif phase == 1:
            if leg_id == LegID.BACK_LEFT_LEG:
                d = stride / 2.0
                x = x_adjust + d * cos(angle)
                y = y_adjust + d * sin(angle)
                self.temp_vector.set_values(x, y, 0)
                return ArcPath(self.leg_raise).set_end_values(x, y, 0)
            elif leg_id == LegID.FRONT_LEFT_LEG:
                d = -stride / 2.0
                x = x_adjust + d * cos(angle)
                y = y_adjust + d * sin(angle)
                self.temp_vector.set_values(x, y, 0)
                return LinearPath().set_end_values(x, y, 0)
            else:
                return LinearPath().set_end_values(x_adjust, y_adjust, 0)
        elif phase == 2:
            if leg_id == LegID.BACK_LEFT_LEG:
                d = stride / 2.0
                x = x_adjust + d * cos(angle)
                y = y_adjust + d * sin(angle)
                self.temp_vector.set_values(x, y, 0)
                return LinearPath().set_end_values(x, y, 0)
            elif leg_id == LegID.FRONT_LEFT_LEG:
                d = stride / 2.0
                x = x_adjust + d * cos(angle)
                y = y_adjust + d * sin(angle)
                self.temp_vector.set_values(x, y, 0)
                return ArcPath(self.leg_raise).set_end_values(x, y, 0)
            else:
                return LinearPath().set_end_values(x_adjust, y_adjust, 0)
        elif phase == 3:
            if leg_id.is_left:
                return LinearPath().set_end_values(x_adjust, y_adjust, 0)
            else:
                d = -stride / 2.0
                x = x_adjust + d * cos(angle)
                y = y_adjust + d * sin(angle)
                return LinearPath().set_end_values(x, y, 0)
        elif phase == 4:
            if leg_id == LegID.BACK_RIGHT_LEG:
                d = stride / 2.0
                x = x_adjust + d * cos(angle)
                y = y_adjust + d * sin(angle)
                self.temp_vector.set_values(x, y, 0)
                return ArcPath(self.leg_raise).set_end_values(x, y, 0)
            elif leg_id == LegID.FRONT_RIGHT_LEG:
                d = -stride / 2.0
                x = x_adjust + d * cos(angle)
                y = y_adjust + d * sin(angle)
                self.temp_vector.set_values(x, y, 0)
                return LinearPath().set_end_values(x, y, 0)
            else:
                return LinearPath().set_end_values(x_adjust, y_adjust, 0)
        elif phase == 5:
            if leg_id == LegID.BACK_RIGHT_LEG:
                d = stride / 2.0
                x = x_adjust + d * cos(angle)
                y = y_adjust + d * sin(angle)
                self.temp_vector.set_values(x, y, 0)
                return LinearPath().set_end_values(x, y, 0)
            elif leg_id == LegID.FRONT_RIGHT_LEG:
                d = stride / 2.0
                x = x_adjust + d * cos(angle)
                y = y_adjust + d * sin(angle)
                self.temp_vector.set_values(x, y, 0)
                return ArcPath(self.leg_raise).set_end_values(x, y, 0)
            else:
                return LinearPath().set_end_values(x_adjust, y_adjust, 0)

    def _update_body_position_details(self) -> None:
        def fix_phase(_phase: int) -> int: return _phase if _phase < 6 else _phase - 6

        leg_positions = [[0, 0], [0, 0], [0, 0]]

        while len(self.body_position_paths) < self.phases_number:
            self.body_position_paths.append(LinearPath())

        for phase, path in enumerate(self.body_position_paths):
            leg_id_for_phase = [leg_id for leg_id in self.leg_phases if fix_phase(self.leg_phases[leg_id] + phase) == 2][0]

            p = 0
            for leg_id in self.leg_paths:
                if leg_id != leg_id_for_phase:
                    position = self.leg_paths[leg_id][phase].end
                    leg_positions[p][0], leg_positions[p][1], _ = self.body.abs_leg_coordinates(leg_id, position)
                    # leg_positions[p][0] = position.x + (self.half_distance_x if leg_id.is_front else -self.half_distance_x)
                    # leg_positions[p][1] = position.y + (self.half_distance_y if leg_id.is_right else -self.half_distance_y)
                    p += 1

            xo, yo = math_util.inscribed_circle_centre(
                leg_positions[0][0], leg_positions[0][1],
                leg_positions[1][0], leg_positions[1][1],
                leg_positions[2][0], leg_positions[2][1])

            path = cast(LinearPath, path)
            path.end.set_values(
                -xo * self.tx,
                -yo * self.ty,
                self.height
            )
            if DEBUG: print(f"    updated path for phase {phase} with {path.end}")
