import math
from enum import Enum, auto
from math import sin, cos, degrees
from typing import Dict, List, cast, Optional

import math_util
from gait.gait import GaitManager, GaitProcessState, Gait
from gait.utils import absolute_tolerance
from gait.walk_gait import WalkGait
from body import LegID, Leg
from path import Path, LinearPath, ArcPath

DEBUG = True
DEBUG_PATHS = True


HALF_PI = math.pi / 2.0
TWO_PI = math.pi * 2.0


class CatWalkGaitStages(Enum):
    STOP = auto(),
    PREPARE = auto(),
    START_1 = auto(),
    START_2 = auto(),
    START_3 = auto(),
    WALK = auto()


class CatWalkSP4Gait(WalkGait):
    def __init__(self, gait_manager: GaitManager, name: str = "cat_walk_sp4"):
        super().__init__(gait_manager, name)
        self.gait_state = CatWalkGaitStages.STOP

        self.leg_phases: Dict[LegID, int] = {
            LegID.FRONT_RIGHT_LEG: 0,
            LegID.BACK_RIGHT_LEG: 1,
            LegID.FRONT_LEFT_LEG: 2,
            LegID.BACK_LEFT_LEG: 3,
        }
        self.phase: int = 0

        self.leg_paths: Dict[LegID, List[Path]] = {
            LegID.FRONT_RIGHT_LEG: [],
            LegID.BACK_LEFT_LEG: [],
            LegID.FRONT_LEFT_LEG: [],
            LegID.BACK_RIGHT_LEG: []
        }

        # self._create_paths_for_legs()

    def start(self, current_time: float, context: dict) -> None:
        if DEBUG: print(f"Pos: starting walk gait - preparing leg positions")
        super().start(current_time, context)
        if DEBUG: self.debug_print_position()

        self.gait_state = CatWalkGaitStages.PREPARE

        if DEBUG: print(f"Walk: starting walk gait -"
                        f" height={self.height},"
                        f" angle={self.angle:.1f}, distance={(self.distance if self.distance else 0.0):.0f},"
                        f" arc={self.leg_raise}, stride={self.stride},"
                        f" ox={self.ox}, oy={self.oy}, cx={self.cx}, cy={self.cy},"
                        f" speed={self.speed}, cog_speed={self.cog_speed},"
                        f" power={self.power}")

        # TODO do this smartly!!! Move it to Legs and introduce feedback which legs should 'bear weight' at which point!

        self._create_paths_for_legs()
        self._update_body_position_details()
        self.phase: int = 0

        height_difference = abs(self.height - self.body.body_position.z)

        if height_difference > absolute_tolerance:
            height_speed = min(0.5, height_difference / 20.0)
            self.gait_manager.push_gait("stand_up", current_time, {**context, "speed": height_speed})
        else:
            path = self.body_position_paths[3]
            self.body.set_body_position_path(path, current_time, self.speed)

    def cont(self, current_time: float, _context: dict) -> None:
        self.started_time = current_time
        path = self.body_position_paths[3]
        self.body.set_body_position_path(path, current_time, self.speed)

    def update(self, current_time: float, context: dict) -> None:
        self.started_time = current_time
        changed = self.process_context(context, True)
        if changed:
            self.required_body_position.set_values(0, 0, self.height)
            self.height_position_path.end.set(self.required_body_position)
            self.body.set_body_position_path(self.height_position_path, current_time, 1)

            self._create_paths_for_legs()
            self._update_body_position_details()

    def stop(self, current_time: float, new_gait: Optional[Gait], new_context: dict, active: bool) -> None:
        if DEBUG: self.debug_print_position()
        pass

    def process(self, current_time: float, context: dict) -> GaitProcessState:
        def next_phase(phase: int) -> int: return phase + 1 if phase < 3 else 0

        if self.body.is_all_at_destination(current_time):
            if DEBUG: print(f"Walk: phase {self.phase}")
            path = self.body_position_paths[self.phase]

            if self.speed < 0.0:
                raise ValueError("Cannot have movement of body at 'duration' while movement of legs at 'speed' of servos percentage...")

            self.body.set_body_position_path(path, current_time + self.speed * (1 - self.cog_speed), self.speed * self.cog_speed)
            if DEBUG_PATHS: print(f"    Body: {path}")

            for leg_id in LegID:
                leg = self.body[leg_id]
                path = self.leg_paths[leg_id][self.phase]
                leg.set_path(path, current_time, self.speed, power=self.power)
                if DEBUG_PATHS: print(f"    {leg_id}  : {path}")

            for leg_id in self.leg_phases:
                self.leg_phases[leg_id] = next_phase(self.leg_phases[leg_id])

            self.phase = next_phase(self.phase)

        return GaitProcessState.OPERATING

    def _create_paths_for_legs(self) -> None:
        furthest_distance = self.calculate_furthest_distance()
        for leg_id in LegID:
            leg = self.body[leg_id]
            self.leg_paths[leg_id] = []

            stride, angle, x_leg_sign, y_leg_sign = self.prepare_path_for_leg(leg_id, furthest_distance)
            x_adjust = self.cx * x_leg_sign + self.ox
            y_adjust = self.cy * y_leg_sign + self.oy

            for i in range(4):
                self.leg_paths[leg_id].append(self.create_path_for_leg(leg, i, stride, angle, x_adjust, y_adjust))

    def create_path_for_leg(self, leg: Leg, phase: int, stride: float, angle: float, x_adjust: float, y_adjust: float) -> Path:
        leg_id = leg.leg_id

        def fix_phase(_phase: int) -> int: return _phase - 4 if _phase > 3 else _phase

        leg_phase = fix_phase(phase + self.leg_phases[leg_id])
        if leg_phase != 3:
            if leg_phase == 0:
                d = stride / 6.0
            elif leg_phase == 1:
                d = -stride / 6.0
            else:
                d = -stride / 2.0
            x = x_adjust + d * cos(angle)
            y = y_adjust + d * sin(angle)
            if DEBUG: print(f"        Creating linear path for {leg_id} to ({x:.2f}, {y:.2f}, {0:.2f}); phase={phase}, angle={degrees(angle):.2f}, stride={stride:.2f}")
            return LinearPath().set_end_values(x, y, 0)
        elif leg_phase == 3:
            d = stride / 2.0
            x = x_adjust + d * cos(angle)
            y = y_adjust + d * sin(angle)
            if DEBUG: print(f"        Creating arc    path for {leg_id} to ({x:.2f}, {y:.2f}, {0:.2f}); phase={phase}, angle={degrees(angle):.2f}, stride={stride:.2f}")
            self.temp_vector.set_values(x, y, 0)
            return ArcPath(self.leg_raise).set_end_values(x, y, 0)
        else:
            print(f"Walk: ERROR - leg_phase {leg_phase} for leg {leg_id}")
            raise ValueError(f"Walk: ERROR - leg_phase {leg_phase} for leg {leg_id}")

    def _update_body_position_details(self) -> None:
        def fix_phase(_phase: int) -> int: return _phase if _phase < 4 else _phase - 4

        leg_positions = [[0, 0], [0, 0], [0, 0]]

        while len(self.body_position_paths) < self.phases_number:
            self.body_position_paths.append(LinearPath())

        for phase, path in enumerate(self.body_position_paths):
            leg_id_for_phase = [leg_id for leg_id in self.leg_phases if fix_phase(self.leg_phases[leg_id] + phase) == 2][0]

            p = 0
            for leg_id in self.leg_paths:
                if leg_id != leg_id_for_phase:
                    position = self.leg_paths[leg_id][phase].end
                    leg_positions[p][0], leg_positions[p][1], _ = self.body.abs_leg_coordinates(leg_id, position)
                    # leg_positions[p][0] = position.x + (self.half_distance_x if leg_id.is_front else -self.half_distance_x)
                    # leg_positions[p][1] = position.y + (self.half_distance_y if leg_id.is_right else -self.half_distance_y)
                    p += 1

            xo, yo = math_util.inscribed_circle_centre(
                leg_positions[0][0], leg_positions[0][1],
                leg_positions[1][0], leg_positions[1][1],
                leg_positions[2][0], leg_positions[2][1])

            path = cast(LinearPath, path)
            path.end.set_values(
                -xo * self.tx,
                -yo * self.ty,
                self.height
            )
            if DEBUG: print(f"    updated path for phase {phase} with {path.end}")
