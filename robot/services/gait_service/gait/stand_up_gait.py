import math
from enum import Enum, auto
from typing import Optional, Dict, Any

from body import ThreeLegDetails
from gait.gait import Gait, GaitManager, GaitProcessState
from support.common_gait import CommonGait
from utils import time_to_string

DEBUG = True
DEBUG_STATE = False

# DEFAULT_STABILITY_FACTOR = 0.85
DEFAULT_STABILITY_FACTOR = 0.3


class StandGaitState(Enum):
    RAISING_TO_MINIMUM = auto()
    LEVELING_LEGS = auto()
    REPOSITIONING_LEGS = auto()
    TWIST = auto()
    ADJUSTING_HEIGHT = auto()
    STANDING = auto()


class CommonStandGait(CommonGait):
    def __init__(self, gait_manager: GaitManager, name: str, final: bool, default_values: Optional[Dict[str, Any]] = None):
        super().__init__(gait_manager, name, default_values={
            "height": 92,
            "speed": 1,
            "arc": 5,
            "x": 0,
            "y": 0,
            "twist": 0,
            "cog_a": 0.8,
            "cog_speed": 0.2,
            "leg_raise": 15,
            **(default_values if default_values is not None else {})
        })
        self._final = final
        self.arc_height = 5
        self.x = 0
        self.y = 0
        self.twist = 0

        self.last_height = self.height
        self.last_x = self.x
        self.last_y = self.y
        self.last_cx = self.cx
        self.last_cy = self.cy
        self.last_ox = self.ox
        self.last_oy = self.oy
        self.last_twist = self.twist

        self.minimum_height = 65
        self.state = StandGaitState.STANDING

        # DEBUG
        self.counter = 0

    def _update_last_values(self) -> None:
        self.last_height = self.height
        self.last_x = self.x
        self.last_y = self.y
        self.last_cx = self.cx
        self.last_cy = self.cy
        self.last_ox = self.ox
        self.last_oy = self.oy
        self.last_twist = self.twist

    def set_state(self, state: StandGaitState) -> None:
        if DEBUG:
            print(f"Stand: {self.state.name} -> {state.name}")
        self.state = state

    def process_context(self, context: dict, update: bool = False) -> bool:
        changed = super().process_context(context, update)
        with self.context_processor(context, update) as c:
            self.arc_height = c.process_float("arc", self.arc_height, self.default_values["arc"])
            self.x = c.process_float("x", self.x, self.default_values["x"])
            self.y = c.process_float("y", self.y, self.default_values["y"])
            self.twist = c.process_float("twist", self.y, self.default_values["twist"])

            return changed or c.is_changed()

    def start(self, current_time: float, context: dict) -> None:
        super().start(current_time, context)
        self.body.all_on()

        if DEBUG: print(f"Stand: starting stand gait {context}")
        if DEBUG: self.debug_print_position()

        x_offset = self.body.body_position.x
        y_offset = self.body.body_position.y
        for leg in self.body:
            leg.position.x += x_offset
            leg.position.y += y_offset
        self.body.body_position.x = 0
        self.body.body_position.y = 0

        if self.body.body_position.z < self.minimum_height:
            self.start_raising_to_minimum_height(current_time)
        else:
            self.prepare_after_minimal_height(current_time)

    def update(self, current_time: float, context: dict) -> None:
        self._update_last_values()
        if DEBUG: print(f"Stand: updating stand gait {context}")
        super().update(current_time, context)

    def stop(self, current_time: float, new_gait: Optional[Gait], new_context: dict, active: bool) -> None:
        if DEBUG: print(f"Stand: stopping stand gait")
        if DEBUG: self.debug_print_position()

    def process(self, current_time: float, context: dict) -> GaitProcessState:
        if DEBUG:
            self.counter += 1
            if self.counter == 50:
                self.counter = 0
                if DEBUG_STATE:
                    print(f"Stand: current state {self.state.name}")

        if self.state != StandGaitState.STANDING:
            if self.state == StandGaitState.RAISING_TO_MINIMUM:
                if self.body.is_all_at_destination(current_time):
                    self.prepare_after_minimal_height(current_time)
                    return GaitProcessState.OPERATING_END_OF_PHASE
            elif self.state == StandGaitState.LEVELING_LEGS:
                if self.body.is_all_at_destination(current_time):
                    self.start_repositioning_legs(current_time)
                    return GaitProcessState.OPERATING_END_OF_PHASE
                elif DEBUG and self.counter == 0:
                    print(f"Stand: waiting for all body to get to destination at LEVELLING_LEGS")
                    print(f"       legs@dest: {self.body.are_legs_at_destination(current_time)} body@dest: {self.body.is_body_at_destination(current_time)}")

            elif self.state == StandGaitState.REPOSITIONING_LEGS:
                if not self.repositioning_legs(current_time):
                    print(f"Stand: not repositioning legs - starting twisting")
                    self.start_twisting(current_time)
                    return GaitProcessState.OPERATING_END_OF_PHASE
            elif self.state == StandGaitState.TWIST:
                if self.body.is_all_at_destination(current_time):
                    if self.body.body_position.z != self.height:
                        print(f"Stand: ADJUSTING_HEIGHT requested={self.height:.2f}, previous={self.body.body_position.z:.2f}")
                        self.state = StandGaitState.ADJUSTING_HEIGHT
                        speed = self.calculate_speed(self.speed, (self.body.body_position.z - self.height))
                        self.body.body_linear_move(self.body.body_position.x, self.body.body_position.y, self.height, current_time, speed)
                        return GaitProcessState.OPERATING_END_OF_PHASE
                    else:
                        self.set_state(StandGaitState.STANDING)
                        return GaitProcessState.OPERATING_END_OF_PHASE
            elif self.state == StandGaitState.ADJUSTING_HEIGHT:
                if self.body.is_all_at_destination(current_time):
                    self.set_state(StandGaitState.STANDING)
                    return GaitProcessState.OPERATING_END_OF_PHASE

        if self.state == StandGaitState.STANDING:
            if self.updated:
                self.updated = False
                if self.cx != self.last_cx or self.cy != self.last_cy or self.ox != self.last_ox or self.oy != self.last_oy:
                    self.prepare_after_minimal_height(current_time)
                elif self.x != self.last_x or self.y != self.last_y or self.height != self.last_height or self.twist != self.last_twist:
                    self.prepare_after_repositioning_legs(current_time)
                else:
                    # TODO is there something I've missed here?
                    # self.state = StandGaitState.STANDING
                    pass
            if self._final:
                return GaitProcessState.RESTING
            else:
                return GaitProcessState.FINISHED
        return GaitProcessState.SETTING_UP

    def prepare_after_minimal_height(self, current_time: float) -> None:
        if DEBUG: print(f"Stand: preparing for possible LEVELING_LEGS")
        if self.setup_requested_height(current_time, self.height):
            self.set_state(StandGaitState.LEVELING_LEGS)
            if DEBUG: print(f"Stand: starting LEVELING_LEGS")
        else:
            if DEBUG: print(f"Stand: no need for LEVELING_LEGS")
            self.start_repositioning_legs(current_time)

    def start_raising_to_minimum_height(self, current_time: float) -> None:
        if DEBUG: print(f"Stand: starting RAISING_TO_MINIMUM")
        x_average = sum([leg.position.x for leg in self.body]) / 4
        y_average = sum([leg.position.y for leg in self.body]) / 4
        for leg in self.body:
            distance = leg.position.distance_values(leg.position.x - x_average, leg.position.y - y_average, 0)
            speed = self.calculate_speed(self.speed, distance)

            leg.linear_move(leg.position.x - x_average, leg.position.y - y_average, 0, current_time, speed)
        self.set_state(StandGaitState.RAISING_TO_MINIMUM)

        distance = self.body.body_position.distance_values(0, 0, self.minimum_height)
        speed = self.calculate_speed(self.speed, distance)
        self.body.body_linear_move(0, 0, self.minimum_height, current_time, speed)
        if DEBUG: print(f"    path for body {self.body.body_position} to {self.body.body_position_path}")

    def start_repositioning_legs(self, current_time: float) -> None:
        if DEBUG: print(f"Stand: preparing for possible REPOSITIONING_LEGS")
        best_stability_three_leg_details = self._find_next_leg_to_reposition()
        if best_stability_three_leg_details is None:
            if DEBUG: print(f"Stand: no need for REPOSITIONING_LEGS")
            self.start_twisting(current_time)
        else:
            self.set_state(StandGaitState.REPOSITIONING_LEGS)
            if DEBUG: print(f"Stand: starting REPOSITIONING_LEGS")
            if DEBUG: print(f"       body at {self.body.body_position}")

            if not best_stability_three_leg_details.is_stable_for_this_leg(current_time, self.cog_a):
                self.setup_cog_without_leg(current_time, None, best_stability_three_leg_details.opposite_leg, self.speed * (1 - self.cog_speed))
            else:
                self.repositioning_legs(current_time)

    def repositioning_legs(self, current_time: float) -> bool:
        # Returns False when legs were repositioned
        if self.body.is_all_at_destination(current_time):

            cog_speed = self.speed * (1.0 - self.cog_speed)

            requested_move = False
            if DEBUG: print(f"       ... selecting next movement ({time_to_string(current_time)}); cog_speed={cog_speed:.1f}")
            best_stability_three_leg_details = self._find_next_leg_to_reposition()

            if best_stability_three_leg_details is not None:
                leg = best_stability_three_leg_details.opposite_leg
                stability_factor = best_stability_three_leg_details.triangle_stability_factor
                sx, sy = leg.leg_id.leg_sign_xy()
                x_adjust = self.x + self.cx * sx + self.ox
                y_adjust = self.y + self.cy * sy + self.oy

                if stability_factor > DEFAULT_STABILITY_FACTOR:
                    if DEBUG: print(f"       got bad stability ({stability_factor:.1f}) for main leg {best_stability_three_leg_details.main_leg.leg_id}")
                    distance = leg.position.distance_values(self.x, self.y, leg.position.z)

                    leg.linear_move(x_adjust, y_adjust, leg.position.z, current_time, self.calculate_speed(self.speed, distance))
                else:
                    if DEBUG: print(f"       got good stability ({stability_factor:.1f}) for main leg {best_stability_three_leg_details.main_leg.leg_id}")
                    leg.arc_move(self.arc_height, x_adjust, y_adjust, leg.position.z, current_time, self.speed)
                requested_move = True

                next_stability_three_leg_details = self._find_next_leg_to_reposition()

                if next_stability_three_leg_details is not None and \
                        not next_stability_three_leg_details.is_stable_for_this_leg(current_time + self.speed, self.cog_a) and \
                        next_stability_three_leg_details.triangle_stability_factor <= DEFAULT_STABILITY_FACTOR:
                    self.setup_cog_without_leg(current_time, None, next_stability_three_leg_details.opposite_leg, cog_speed)
            else:
                # No more legs to sort out
                if not math.isclose(self.body.body_position.x, 0, abs_tol=1) or not math.isclose(self.body.body_position.y, 0, abs_tol=1):
                    distance = self.body.body_position.distance_values(
                        0,
                        0,
                        self.body.body_position.z
                    )
                    speed = self.calculate_speed(self.speed, distance)
                    self.body.body_linear_move(0, 0, self.body.body_position.z, current_time, speed)

            if DEBUG:
                if not requested_move:
                    print(f"       ... no more movements ({time_to_string(current_time)})")
                else:
                    print(f"       ... selected movement ({time_to_string(current_time)})")
                for tld in self.body.three_leg_details.values():
                    print(f"       {tld}")
        else:
            requested_move = True

        return requested_move

    def _find_next_leg_to_reposition(self, current_time: Optional[float] = None) -> ThreeLegDetails:
        all_three_leg_details = self.body.get_all_three_leg_details(current_time)
        best_stability = 1
        best_stability_three_leg_details: Optional[ThreeLegDetails] = None
        for three_leg_detail in all_three_leg_details.values():
            leg = three_leg_detail.opposite_leg
            sx, sy = leg.leg_id.leg_sign_xy()
            x_adjust = self.x + self.cx * sx + self.ox
            y_adjust = self.y + self.cy * sy + self.oy

            if not math.isclose(leg.position.x, self.x + x_adjust, abs_tol=1) or not math.isclose(leg.position.y, self.y + y_adjust,  abs_tol=1):
                stability_factor = three_leg_detail.calculate_triangle_stability_factor()
                if best_stability_three_leg_details is None or stability_factor < best_stability:
                    best_stability_three_leg_details = three_leg_detail
                    best_stability = stability_factor
        return best_stability_three_leg_details

    def prepare_after_repositioning_legs(self, current_time: float) -> None:
        print(f"Stand: prepare after repositioning legs - starting twisting")
        self.start_twisting(current_time)

    def start_twisting(self, current_time: float) -> None:
        self.set_state(StandGaitState.TWIST)
        if DEBUG: print(f"Stand: starting twisting")

        radius = math.sqrt(self.body.half_distance_x * self.body.half_distance_x
                           + self.body.half_distance_y * self.body.half_distance_y)

        twist_rad = math.radians(self.twist)
        last_twist_rad = math.radians(self.last_twist)
        arc_length = radius * math.pi * (abs(last_twist_rad - twist_rad) / math.pi)

        # speed = self.calculate_speed(self.speed, arc_length)
        speed = self.speed / 3
        if DEBUG: print(f"    twist: r={radius}, arc_length={arc_length}")

        for leg in self.body.legs.values():
            sx, sy = leg.leg_id.leg_sign_xy()

            x_adjust = self.x + self.cx * sx + self.ox
            y_adjust = self.y + self.cy * sy + self.oy

            leg_angle = math.atan2(self.body.half_distance_y * sy, self.body.half_distance_x * sx)
            nx = math.cos(twist_rad + leg_angle) * radius - self.body.half_distance_x * sx + x_adjust
            ny = math.sin(twist_rad + leg_angle) * radius - self.body.half_distance_y * sy + y_adjust
            leg.linear_move(nx, ny, 0, current_time, speed)
            if DEBUG: print(f"    leg {leg.leg_id}: nx={nx}, ny={ny}, leg_angle={math.degrees(leg_angle)}")
        if DEBUG: print(f"    path for body {self.body.body_position} to {self.body.body_position_path}")


class StandUpGait(CommonStandGait):
    def __init__(self, gait_manager: GaitManager, name: str = "stand_up") -> None:
        super().__init__(gait_manager, name, False)


class StandGait(CommonStandGait):
    def __init__(self, gait_manager: GaitManager, name: str = "stand") -> None:
        super().__init__(gait_manager, name, True)


class CommonDelayStandGait(Gait):
    def __init__(self, gait_manager: GaitManager, name: str, next_gait: str) -> None:
        super().__init__(gait_manager, name)
        self.next_gait = next_gait
        self.delay = 3
        self.context: Optional[dict] = None

    def start(self, current_time: float, context: dict) -> None:
        super().start(current_time, context)
        self.context = context
        with self.context_processor(context, False) as c:
            self.delay = c.process_float("delay", self.delay, 3)
        if DEBUG: print(f"Delay stand gait: starting with delay {self.delay}")

    def cont(self, current_time: float, context: dict) -> None:
        super().cont(current_time, context)

    def update(self, current_time: float, context: dict) -> None:
        super().update(current_time, context)
        with self.context_processor(context, True) as c:
            self.delay = c.process_float("delay", self.delay, 3)

    def stop(self, current_time: float, new_gait: Optional[Gait], context: dict, active: bool) -> None:
        super().update(current_time, context)
        if new_gait.name != self.next_gait and self.started_time + self.delay < current_time:
            self.gait_manager.select_gait(self.next_gait, current_time, self.context)

    def process(self, current_time: float, context: dict) -> GaitProcessState:
        if self.started_time + self.delay < current_time:
            return GaitProcessState.FINISHED

        return GaitProcessState.RESTING


class DelayStandUpGait(CommonDelayStandGait):
    def __init__(self, gait_manager: GaitManager, name: str = "delayed_stand_up") -> None:
        super().__init__(gait_manager, name, "stand_up")


class DelayStandGait(CommonDelayStandGait):
    def __init__(self, gait_manager: GaitManager, name: str = "delayed_stand") -> None:
        super().__init__(gait_manager, name, "stand")
