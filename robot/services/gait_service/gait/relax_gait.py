from typing import Dict, Optional

from gait.gait import Gait, GaitManager, GaitProcessState
from body import LegID
from path import LinearPath, Path
from vector import Vector3

DEBUG = False


class RelaxGait(Gait):
    def __init__(self, gait_manager: GaitManager, name: str = "relax", power: int = 20):
        super().__init__(gait_manager, name)
        self.speed = 1
        self.default_power = power
        self.finished = False
        self._finished_reported = False
        self.end_position = Vector3(x=38, y=0, z=0)
        self.leg_paths: Dict[LegID, Path] = {
            LegID.FRONT_RIGHT_LEG: LinearPath(end=self.end_position),
            LegID.FRONT_LEFT_LEG: LinearPath(end=self.end_position),
            LegID.BACK_RIGHT_LEG: LinearPath(end=self.end_position),
            LegID.BACK_LEFT_LEG: LinearPath(end=self.end_position),
        }

    def start(self, current_time: float, context: dict) -> None:
        if DEBUG: print(f"Relax: starting relax gait")
        super().start(current_time, context)

        self.power = int(context["power"]) if context is not None and "power" in context else self.default_power
        self.body.all_on()
        self.finished = False
        self._finished_reported = False

        # Hack to start moving from some middle
        if self.body.body_position.z < 10:
            self.body.body_position.z = 50

        for leg in self.body:
            leg.set_path(self.leg_paths[leg.leg_id], current_time, self.speed, power=self.power)

        self.body.body_linear_move(0, 0, 30, current_time, self.speed)

    def stop(self, current_time: float, new_gait: Optional[Gait], new_context: dict, active: bool) -> None:
        pass

    def process(self, current_time: float, context: dict) -> GaitProcessState:
        if not self.finished:
            for leg in self.body:
                if not leg.at_destination(current_time):
                    if DEBUG: print(f"Relax: Leg {leg.leg_id.name} not at position, yet...")
                    return GaitProcessState.OPERATING
            if not self.body.is_body_at_destination(current_time):
                if DEBUG: print(f"Relax: Body (height) not at position, yet...")
                return GaitProcessState.OPERATING

            print(f"Relax: Legs arrived at required position")
            self.finished = True

            for leg in self.body:
                leg.turn_servos_off()
            self.body.all_off()
        else:
            if not self._finished_reported:
                if DEBUG: print(f"Relax: Legs finished")
                self._finished_reported = True

        if self.finished:
            return GaitProcessState.RESTING
        return GaitProcessState.OPERATING
