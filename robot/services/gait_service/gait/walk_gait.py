import math
from typing import Optional, Dict, Any

from gait.gait import Gait, GaitManager
from support.common_gait import CommonGait

DEBUG = True
DEBUG_PATHS = True


HALF_PI = math.pi / 2.0
TWO_PI = math.pi * 2.0


class WalkGait(CommonGait):
    def __init__(self, gait_manager: GaitManager,
                 name: str,
                 default_values: Optional[Dict[str, Any]] = None):
        super().__init__(gait_manager, name, default_values)

    def start(self, current_time: float, context: dict) -> None:
        super().start(current_time, context)

        self.process_context(context, False)

        # # TODO do this smartly!!! Move it to Legs and introduce feedback which legs should 'bear weight' at which point!
        # self.required_body_position.set_values(0, 0, self.height)
        # self.height_position_path.end.set(self.required_body_position)
        # self.body.set_body_position_path(self.height_position_path, current_time, 1)

    def stop(self, current_time: float, new_gait: Optional[Gait], new_context: dict, active: bool) -> None:
        pass
