from typing import Optional, List

from body import Body
from gait.beg_gait import BegGait
from gait.cat_walk_dp4_gait import CatWalkDP4Gait
from gait.cat_walk_sp6_gait import CatWalkSP6Gait
from gait.cat_walk_sp4_gait import CatWalkSP4Gait
from gait.do_nothing_gait import DoNothingGait
from gait.fast_arc_gait import FastArcGait
from gait.gait import Gait, GaitProcessState, GaitManager
from gait.initial_set_up_gait import InitialSetUpGait
from gait.lay_gait import LayGait
from gait.position_gait import PositionGait
from gait.relax_gait import RelaxGait
from gait.rotate_gait import RotateGait
from gait.sit_gait import SitGait
from gait.stand_up_gait import StandUpGait, DelayStandGait, StandGait, DelayStandUpGait
from gait.trot_gait import TrotGait
from gait.unknown_leg_position_gait import UnknownLegPositionGait

DEBUG = True


class GaitStackElement:
    def __init__(self, gait: Gait, context: dict):
        self.gait = gait
        self.context = context


class GaitManagerImpl(GaitManager):
    def __init__(self, body: Body):
        super().__init__(body)
        self._previous_state: Optional[GaitProcessState] = None
        self._gait_stack: List[GaitStackElement] = []

    def reset(self) -> None:
        del self._gait_stack[:]

    @property
    def current_gait(self) -> Optional[Gait]:
        return self._gait_stack[-1].gait if len(self._gait_stack) > 0 else None

    @property
    def current_gait_context(self) -> Optional[dict]:
        return self._gait_stack[-1].context if len(self._gait_stack) > 0 else None

    def register_known_gaits(self) -> None:
        self.register_gait(UnknownLegPositionGait(self))
        self.register_gait(InitialSetUpGait(self))
        self.register_gait(DoNothingGait(self))
        self.register_gait(RelaxGait(self, "relax", 30))
        self.register_gait(RelaxGait(self, "sit", 100))
        self.register_gait(StandUpGait(self))
        self.register_gait(StandGait(self))
        self.register_gait(LayGait(self))
        self.register_gait(SitGait(self))
        self.register_gait(DelayStandGait(self))
        self.register_gait(DelayStandUpGait(self))
        self.register_gait(PositionGait(self))
        self.register_gait(CatWalkDP4Gait(self))
        self.register_gait(CatWalkSP4Gait(self))
        self.register_gait(CatWalkSP6Gait(self))
        self.register_gait(TrotGait(self))
        self.register_gait(BegGait(self))
        self.register_gait(FastArcGait(self))
        self.register_gait(RotateGait(self))

    def _stack_to_string(self):
        return [ge.gait.name for ge in self._gait_stack]

    def _delete_gaits_on_stack_after(self, stack_index: int, current_time: float, new_gait: Gait, new_context: dict) -> None:
        i = len(self._gait_stack) - 1
        while i > stack_index:
            previous_gait_stack_elemnent = self._gait_stack[i]
            previous_gait = previous_gait_stack_elemnent.gait
            if DEBUG: print(f"    *** gait manager: stopping gait {previous_gait.name}, current={self._stack_to_string()}")
            previous_gait.stop(current_time, new_gait, new_context, False)
            previous_gait.started_time = 0
            del self._gait_stack[i]
            i -= 1

    def update_current_gait_context(self, current_time: float, context: dict) -> None:
        if len(self._gait_stack) > 0:
            gait_stack_element = self._gait_stack[-1]
            gait_stack_element.context = context
            gait_stack_element.gait.update(current_time, context)

    def select_gait(self, name: str, current_time: float, new_context: Optional[dict] = None) -> Optional[Gait]:
        if DEBUG: print(f"    *** gait manager: selecting gait {name}, current={self._stack_to_string()}")
        if name not in self._known_gaits:
            return None

        new_gait = self._known_gaits[name]
        if self.current_gait_name == name:
            top_gait = self.current_gait

            if DEBUG: print(f"    *** gait manager: updating gait {top_gait.name}, current={self._stack_to_string()}")
            top_gait.update(current_time, new_context)
        else:
            if len(self._gait_stack) > 0:
                old_gait_stack_element = self._gait_stack[-1]
                old_gait = old_gait_stack_element.gait
                del self._gait_stack[-1]

                i = 0
                while i < len(self._gait_stack) and self._gait_stack[i].gait != new_gait:
                    i += 1

                if i < len(self._gait_stack):
                    if DEBUG: print(f"        --- found gait on stack at index {i}, current={self._stack_to_string()}")
                    self._delete_gaits_on_stack_after(i, current_time, new_gait, new_context)
                else:
                    if len(self._gait_stack) > 0:
                        if DEBUG: print(f"        --- removing all gaits from the stack, current={self._stack_to_string()}")
                        self._delete_gaits_on_stack_after(-1, current_time, new_gait, new_context)

                    self._gait_stack.append(GaitStackElement(new_gait, new_context))
                    if DEBUG: print(f"        --- appended gait to the stack, current={self._stack_to_string()}")

                if DEBUG: print(f"    *** gait manager: stopping gait {old_gait.name}, current={self._stack_to_string()}")
                old_gait.stop(current_time, new_gait, new_context, True)
                old_gait.started_time = 0
            else:
                self._gait_stack.append(GaitStackElement(new_gait, new_context))
                if DEBUG: print(f"        --- appended gait to the stack, current={self._stack_to_string()}")

            top_gait_stack_element = self._gait_stack[-1]
            top_gait = top_gait_stack_element.gait
            if top_gait.started_time <= 0:
                if DEBUG: print(f"    *** gait manager: starting gait {top_gait.name}, current={self._stack_to_string()}")
                top_gait.start(current_time, top_gait_stack_element.context)
                [callback(top_gait.name, GaitProcessState.NEWLY_SELECTED) for callback in self._gait_callbacks]
            else:
                if DEBUG: print(f"    *** gait manager: continuing gait {top_gait.name}, current={self._stack_to_string()}")
                top_gait.cont(current_time, top_gait_stack_element.context)

        return top_gait

    def push_gait(self, name: str, current_time: float, new_context: Optional[dict] = None) -> Optional[Gait]:
        if DEBUG: print(f"    *** gait manager: pushing gait {name}, current={self._stack_to_string()}")
        if name not in self._known_gaits:
            return None

        new_gait = self._known_gaits[name]

        self._gait_stack.append(GaitStackElement(new_gait, new_context))

        top_gait_stack_element = self._gait_stack[-1]
        top_gait = top_gait_stack_element.gait
        if DEBUG: print(f"    *** gait manager: starting gait {top_gait.name}, current={self._stack_to_string()}")
        top_gait.start(current_time, top_gait_stack_element.context)
        [callback(top_gait.name, GaitProcessState.NEWLY_SELECTED) for callback in self._gait_callbacks]

        return top_gait

    def all_stop(self, current_time: float) -> None:
        del self._gait_stack[:]
        self.select_gait("unknown_leg_position", current_time, {})

    def process(self, current_time: float) -> None:
        if len(self._gait_stack) > 0:
            current_gait_stack_element = self._gait_stack[-1]
            current_gait = current_gait_stack_element.gait
            current_context = current_gait_stack_element.context

            if not self.stepping or (self.stepping and self.step_gate.is_triggered):
                self.step_gate.proceed()
                state = current_gait.process(current_time, current_context)

                if state != self._previous_state:
                    [callback(current_gait.name, state) for callback in self._gait_callbacks]
                    self._previous_state = state

                if state == GaitProcessState.FINISHED:
                    if DEBUG: print(f"    *** gait manager: current gait {current_gait.name} is at FINISHED, current={self._stack_to_string()}")
                    del self._gait_stack[-1]
                    if len(self._gait_stack) > 0:
                        previous_gait_stack_element = self._gait_stack[-1]
                        previous_gait = previous_gait_stack_element.gait
                        previous_context = previous_gait_stack_element.context

                        if DEBUG: print(f"    *** gait manager: stopping from resting gait {current_gait.name}, current={self._stack_to_string()}")
                        current_gait.stop(current_time, previous_gait, previous_context, True)
                        current_gait.started_time = 0

                        top_gait_stack_element = self._gait_stack[-1]
                        top_gait = top_gait_stack_element.gait
                        _top_context = top_gait_stack_element.context
                        if top_gait.started_time <= 0:
                            if DEBUG: print(f"    *** gait manager: starting from resting gait {top_gait.name}, current={self._stack_to_string()}")
                            top_gait.start(current_time, previous_context)
                            [callback(top_gait.name, GaitProcessState.NEWLY_SELECTED) for callback in self._gait_callbacks]
                        else:
                            if DEBUG: print(f"    *** gait manager: continuing from resting gait {top_gait.name}, current={self._stack_to_string()}")
                            top_gait.cont(current_time, previous_context)
                    else:
                        if DEBUG: print(f"    *** gait manager: stopping from resting gait {current_gait.name}, current={self._stack_to_string()}")
                        current_gait.stop(current_time, self._known_gaits["do_nothing"], {}, True)
                        current_gait.started_time = 0

                        self.select_gait("do_nothing", current_time, {})
                        # if DEBUG: print(f"    *** gait manager: stopping last gait {current_gait.name}, current={self._stack_to_string()}")
                        # current_gait.stop(current_time, None, {}, True)
                        # current_gait.started_time = 0
