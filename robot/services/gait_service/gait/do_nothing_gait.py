from typing import Optional

from gait.gait import Gait, GaitManager, GaitProcessState

DEBUG = False


class DoNothingGait(Gait):
    def __init__(self, gait_manager: GaitManager, name: str = "do_nothing"):
        super().__init__(gait_manager, name)
        self.finished = False

    def start(self, current_time: float, context: dict) -> None:
        if DEBUG: print(f"do_nothing: starting 'do nothing' gait")
        super().start(current_time, context)

    def stop(self, current_time: float, new_gait: Optional[Gait], new_context: dict, active: bool) -> None:
        pass

    def process(self, current_time: float, context: dict) -> GaitProcessState:
        return GaitProcessState.OPERATING
