import math
from enum import Enum, auto
from math import sin, cos, degrees
from typing import Dict, Optional, Tuple

from gait.gait import GaitManager, GaitProcessState, Gait
from gait.walk_gait import WalkGait
from body import LegID, Leg
from path import LinearPath, ArcPath


DEBUG = True
DEBUG_LEG_POSITION = True
DEBUG_PATHS = True


HALF_PI = math.pi / 2.0
TWO_PI = math.pi * 2.0

LINEAR_PATH = 0
ARC_PATH = 1


class CatWalkDP4GaitStages(Enum):
    STOP = auto(),
    SETTING_HEIGHT = auto(),
    COG_ADJUST = auto(),
    WALK = auto()


class CatWalkDP4Gait(WalkGait):
    def __init__(self, gait_manager: GaitManager,
                 name: str = "cat_walk_dp4"):
        super().__init__(gait_manager,
                         name)

        self.gait_state = CatWalkDP4GaitStages.STOP

        self.leg_in_air: Optional[Leg] = None
        self.selected_leg_to_be_lifted: Optional[Leg] = None
        self._leg_paths: Dict[LegID, Tuple[LinearPath, ArcPath]] = {}
        self._update_all_leg_paths = False

    # def process_context(self, context: dict, update: bool = False) -> bool:
    #     changed = super().process_context(context, update)
    #     with self.context_processor(context, update) as c:
    #         self.cog_a = c.process_float("cog_a", self.speed, self.default_values["cog_a"])
    #         return c.is_changed() or changed

    def start(self, current_time: float, context: dict) -> None:
        if DEBUG: print(f"Pos: starting walk gait - preparing leg positions")
        super().start(current_time, context)
        if DEBUG_LEG_POSITION: self.debug_print_position(True)

        if DEBUG: print(f"cat_walk_dp4: starting gait:"
                        f" height={self.height},"
                        f" angle={self.angle:.1f}, distance={(self.distance if self.distance else 0.0):.0f},"
                        f" arc={self.leg_raise}, stride={self.stride},"
                        f" ox={self.ox}, oy={self.oy}, cx={self.cx}, cy={self.cy},"
                        f" speed={self.speed}, cog_speed={self.cog_speed},"
                        f" power={self.power}")

        self.leg_in_air: Optional[Leg] = None

        self.cont(current_time, context)

    def cont(self, current_time: float, context: dict) -> None:
        self.started_time = current_time
        self.process_context(context, True)
        self._create_leg_paths()
        if DEBUG: print(f"cat_walk_dp4: continuing gait - checking if height is correct; {self.height}")
        started_setting_up_height = self.setup_requested_height(current_time, self.height)
        if started_setting_up_height:
            if DEBUG: print(f"cat_walk_dp4: continuing gait - setting up height")
            self.leg_in_air: Optional[Leg] = None
            self.selected_leg_to_be_lifted: Optional[Leg] = None
        else:
            if DEBUG: print(f"cat_walk_dp4: continuing gait - body on correct height")
            self.process(current_time, context)

    def update(self, current_time: float, context: dict) -> None:
        self.started_time = current_time
        _changed = self.process_context(context, True)
        self._create_leg_paths()
        self._update_all_leg_paths = True
        if DEBUG: print(f"cat_walk_dp4: updated gait:"
                        f" height={self.height},"
                        f" angle={self.angle:.1f}, distance={(self.distance if self.distance else 0.0):.0f},"
                        f" arc={self.leg_raise}, stride={self.stride}, leg_raise={self.leg_raise},"
                        f" ox={self.ox}, oy={self.oy}, cx={self.cx}, cy={self.cy},"
                        f" speed={self.speed}, cog_speed={self.cog_speed}, cog_a={self.cog_a},"
                        f" power={self.power}")

        # TODO - what here?
        # if changed:
        #     self.required_body_position.set_values(0, 0, self.height)
        #     self.height_position_path.end.set(self.required_body_position)
        #     self.body.set_body_position_path(self.height_position_path, current_time, 1)
        #
        #     self._create_paths_for_legs()
        #     self._update_body_position_details()

    def stop(self, current_time: float, new_gait: Optional[Gait], new_context: dict, active: bool) -> None:
        if DEBUG: print(f"cat_walk_dp4: stopped gait")
        if DEBUG: self.debug_print_position()
        self.gait_state = CatWalkDP4GaitStages.STOP

    def process(self, current_time: float, context: dict) -> GaitProcessState:
        if self.leg_in_air is None:
            # All legs are on the ground
            if not self.body.is_all_at_destination(current_time):
                # Still in motion from previous request
                return GaitProcessState.OPERATING

            if self.selected_leg_to_be_lifted is None:
                self.selected_leg_to_be_lifted = self._find_leg_to_lift(current_time)
                if DEBUG: print(f"cat_walk_dp4: no leg-in-air - selecting new leg "
                                f"{self.selected_leg_to_be_lifted.leg_id}")
                if self.is_stable_without_this_leg(current_time, self.selected_leg_to_be_lifted):
                    if DEBUG: print(f"cat_walk_dp4:     raising leg as other three are stable")
                    # It is OK to raise leg now!
                    self._raise_leg(current_time, self.selected_leg_to_be_lifted, True)
                else:
                    if DEBUG: print(f"cat_walk_dp4:     setting up CoG first")
                    # CoG is not correct for leg to be lifted
                    self.setup_cog_without_leg(current_time, None, self.selected_leg_to_be_lifted, self.speed)
            else:
                if DEBUG: print(f"cat_walk_dp4: no leg-in-air - raising previously selected leg "
                                f"{self.selected_leg_to_be_lifted.leg_id}")
                # We have selected leg to raise
                # We have updated CoG
                self._raise_leg(current_time, self.selected_leg_to_be_lifted, True)

            if DEBUG_LEG_POSITION: self.debug_print_position(True)
            return GaitProcessState.OPERATING

        else:
            # We already had one leg in air before
            if self.leg_in_air.path.at_destination(current_time):
                if DEBUG: print(f"cat_walk_dp4: leg-in-air {self.leg_in_air.leg_id} at destination")
                self.selected_leg_to_be_lifted = self._find_leg_to_lift(current_time)
                if self.is_stable_without_this_leg(current_time, self.selected_leg_to_be_lifted):
                    if DEBUG: print(f"cat_walk_dp4:     raising new selected leg {self.selected_leg_to_be_lifted.leg_id} as stable")
                    # It is OK to raise leg now!

                    # Leg has landed - and now needs to travel along the path on the ground
                    self.leg_in_air.set_path(self._leg_paths[self.leg_in_air.leg_id][LINEAR_PATH], current_time, self.speed * 3)
                    if DEBUG_LEG_POSITION: print(f"cat_walk_dp4:    set {self.leg_in_air.leg_id} path to {self.leg_in_air.path}")

                    self._raise_leg(current_time, self.selected_leg_to_be_lifted, False)
                    if abs(self.body.body_position.x) > 1 or abs(self.body.body_position.y) > 1:
                        if DEBUG_LEG_POSITION: print(f"cat_walk_dp4:    returning body to middle")
                        self.body.body_linear_move(0, 0, self.body.body_position.z, current_time, self.speed / 2)

                    if self._update_all_leg_paths:
                        if DEBUG: print(f"cat_walk_dp4:     other paths changes so updating them now:")
                        self._update_all_leg_paths = False

                        # Paths were changed in the meantime and we need to re-adjust them
                        # for other two legs - one at 2/3 and one of 1/2 through their paths

                        leg = self.body.legs[self._next_leg(self.leg_in_air.leg_id)]
                        leg.set_path(self._leg_paths[leg.leg_id][LINEAR_PATH], current_time, self.speed * 2.0)
                        if DEBUG_LEG_POSITION: print(f"cat_walk_dp4:    set {leg.leg_id} path to {leg.path}")

                        leg = self.body.legs[self._next_leg(leg.leg_id)]
                        leg.set_path(self._leg_paths[leg.leg_id][LINEAR_PATH], current_time, self.speed * 1.0)
                        if DEBUG_LEG_POSITION: print(f"cat_walk_dp4:    set {leg.leg_id} path to {leg.path}")

                else:
                    if DEBUG: print(f"cat_walk_dp4:     new selected leg {self.selected_leg_to_be_lifted.leg_id} is not stable. Setting CoG")
                    # Something went wrong and we had to stop all other leg movement
                    # and re-adjust CoG first. At least we have 'selected_leg_to_be_lifted' set for later
                    for leg in self.body:
                        leg.clear_path()
                        if DEBUG_LEG_POSITION: print(f"cat_walk_dp4:    stopped leg {leg.leg_id}")

                    self.leg_in_air = None
                    # CoG is not correct for leg to be lifted
                    self.setup_cog_without_leg(current_time, None, self.selected_leg_to_be_lifted, self.speed)

                if DEBUG_LEG_POSITION: self.debug_print_position(True)
                return GaitProcessState.OPERATING

            # One leg is in the air
            if self.selected_leg_to_be_lifted is None and self._check_leg_is_half_way_through(self.leg_in_air, current_time):
                if DEBUG: print(f"cat_walk_dp4: leg-in-air {self.leg_in_air.leg_id} half way through - setting up CoG for next raise")
                # Leg has passed or about to pass half way through
                # it is time to update CoG for next leg

                self.selected_leg_to_be_lifted = self._find_leg_to_lift(current_time)
                if DEBUG: print(f"cat_walk_dp4:     next selected leg {self.selected_leg_to_be_lifted.leg_id}")

                if not self.is_stable_without_this_leg(self.leg_in_air.path.requested_arrival, self.selected_leg_to_be_lifted):
                    if DEBUG: print(f"cat_walk_dp4:     after lifting {self.selected_leg_to_be_lifted.leg_id} we won't be stable, so setting up CoG now")
                    self.setup_cog_without_leg(current_time, self.leg_in_air.path.requested_arrival, self.selected_leg_to_be_lifted, self.speed)
                if DEBUG_LEG_POSITION: self.debug_print_position(True)
            else:
                # Leg is still lifting - no changes
                pass  # This is OK to stay as 'pass'. I've kept 'else' for completeness

        return GaitProcessState.OPERATING

    def _raise_leg(self, current_time: float, leg_to_lift: Leg, setup_other_legs: bool) -> None:
        # Raise Leg
        leg_to_lift.set_path(self._leg_paths[leg_to_lift.leg_id][ARC_PATH], current_time, self.speed)
        if DEBUG_LEG_POSITION: print(f"cat_walk_dp4:    set {leg_to_lift.leg_id} path to {leg_to_lift.path}")

        next_leg_id = leg_to_lift.leg_id
        if setup_other_legs:
            # Setup ground legs motion
            for i in range(3):
                next_leg_id = self._next_leg(next_leg_id)
                leg = self.body.legs[next_leg_id]

                speed = self.speed * (i + 1) / 3
                leg.set_path(self._leg_paths[leg.leg_id][LINEAR_PATH], current_time, speed * 3)
                if DEBUG_LEG_POSITION: print(f"cat_walk_dp4:    set {leg.leg_id} path to {leg.path}")

        self.leg_in_air = leg_to_lift
        self.selected_leg_to_be_lifted = None

    @staticmethod
    def _next_leg(leg_id: LegID) -> LegID:
        if leg_id == LegID.BACK_RIGHT_LEG:
            return LegID.FRONT_RIGHT_LEG
        elif leg_id == LegID.FRONT_RIGHT_LEG:
            return LegID.BACK_LEFT_LEG
        elif leg_id == LegID.BACK_LEFT_LEG:
            return LegID.FRONT_LEFT_LEG
        else:
            # elif leg_id == LegID.FRONT_LEFT_LEG:
            return LegID.BACK_RIGHT_LEG

    def _find_leg_to_lift(self, _current_time: float) -> Leg:
        if self.leg_in_air is not None:
            return self.body.legs[self._next_leg(self.leg_in_air.leg_id)]

        # We couldn't find previous leg_in_air - need to decide based on something else
        # We could select leg which is the closest to end of its ground, linear path
        selected_leg: Optional[Leg] = None
        current_distance = 1000000
        for leg in self.body:
            distance = leg.position.distance_vec(self._leg_paths[leg.leg_id][LINEAR_PATH].end)
            if distance < current_distance:
                selected_leg = leg
                current_distance = distance

        return selected_leg

    @staticmethod
    def _check_leg_is_half_way_through(leg: Leg, current_time: float, position: float = 0.5) -> bool:
        if isinstance(leg.path, ArcPath):
            return leg.path.current_relative_position_in_path(current_time) >= position

        # It is not arc path so all is fine - continue as if we moved halfway through
        return True

    def _create_leg_paths(self) -> None:
        furthest_distance = self.calculate_furthest_distance()
        for leg in self.body:
            leg_id = leg.leg_id
            stride, angle, x_leg_sign, y_leg_sign = self.prepare_path_for_leg(leg_id, furthest_distance)
            x_adjust = self.cx * x_leg_sign + self.ox
            y_adjust = self.cy * y_leg_sign + self.oy

            cos_angle = cos(angle)
            sin_angle = sin(angle)

            # cx and cy as minimal distances from centre to where leg must end
            # x_adjust = (self.cx + self.stride / 2.0 - self.body.half_distance_x) * abs(cos_angle) * x_leg_sign + self.ox
            # y_adjust = (self.cy + self.stride / 2.0 - self.body.half_distance_y) * abs(sin_angle) * y_leg_sign + self.oy
            # x_adjust = self.cx * abs(cos_angle) * x_leg_sign + self.ox
            # y_adjust = self.cy * abs(sin_angle) * y_leg_sign + self.oy
            # x_adjust = self.cx * abs(sin_angle) * x_leg_sign + self.ox
            # y_adjust = self.cy * abs(cos_angle) * y_leg_sign + self.oy

            d = stride / 2.0  # Last position of the leg
            x = d * cos_angle
            y = d * sin_angle

            x_s = x_adjust + x
            y_s = y_adjust + y
            x_e = x_adjust - x
            y_e = y_adjust - y
            z_e_adjust = 0 if leg.leg_id.is_front else -10
            if DEBUG: print(f"        Creating forward  path for {leg_id} to ({x_e:.2f}, {y_e:.2f}, {0:.2f}); angle={degrees(angle):.2f}, stride={stride:.2f}")
            forward_path = LinearPath()\
                .set_start_values(x_s, y_s, 0)\
                .set_end_values(x_e, y_e, z_e_adjust)

            # d = stride / 2.0
            # x = x_adjust + d * cos_angle
            # y = y_adjust + d * sin_angle
            if DEBUG: print(f"        Creating back/arc path for {leg_id} to ({x_s:.2f}, {y_s:.2f}, {0:.2f}); angle={degrees(angle):.2f}, stride={stride:.2f}")
            arc_path = ArcPath(self.leg_raise).set_end_values(x_s, y_s, 0)

            self._leg_paths[leg_id] = (forward_path, arc_path)
