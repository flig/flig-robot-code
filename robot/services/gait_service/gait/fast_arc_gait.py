from typing import Optional

from gait.gait import Gait, GaitManager, GaitProcessState
from body import LegID

DEBUG = True


class FastArcGait(Gait):
    def __init__(self, gait_manager: GaitManager, name: str = "fast_arc"):
        super().__init__(gait_manager, name)
        self.finished = False
        self.leg_raise = 20

    def _process_context(self, current_time: float, context: dict, update: bool = False) -> None:
        self.body.all_on()

        with self.context_processor(context, update) as c:
            self.leg_raise = c.process_float("leg_raise", self.leg_raise, 20)

        for leg_id in LegID:
            leg = self.body[leg_id]
            x = float(context[f"{leg_id.name}.x"]) if context is not None and f"{leg_id.name}.x" in context else leg.position.x
            y = float(context[f"{leg_id.name}.y"]) if context is not None and f"{leg_id.name}.y" in context else leg.position.y
            z = float(context[f"{leg_id.name}.z"]) if context is not None and f"{leg_id.name}.z" in context else leg.position.z

            if x != leg.position.x or y != leg.position.y or z != leg.position.z:
                if DEBUG: print(f"   {leg.leg_id}  : {self.leg_raise:.2f}, x={x:.2f}, y={y:.2f}, z={z:.2f}, ")
                leg.arc_move(self.leg_raise, x, y, z, current_time, 0, self.power)
                if DEBUG: print(f"   {leg.leg_id}  : {leg.path} @ {self.power:.2f}%")

    def start(self, current_time: float, context: dict) -> None:
        if DEBUG: print(f"Fast Arc: starting position gait")
        super().start(current_time, context)
        self._process_context(current_time, context)

    def cont(self, current_time: float, context: dict) -> None:
        super().cont(current_time, context)
        self.started_time = current_time

    def update(self, current_time: float, context: dict) -> None:
        self._process_context(current_time, context, True)

    def stop(self, current_time: float, new_gait: Optional[Gait], new_context: dict, active: bool) -> None:
        pass

    def process(self, current_time: float, context: dict) -> GaitProcessState:
        if not self.finished:
            if self.body.is_all_at_destination(current_time):
                print(f"Fast Arc: Legs arrived at required position")
                self.finished = True
                return GaitProcessState.RESTING

        return GaitProcessState.OPERATING
