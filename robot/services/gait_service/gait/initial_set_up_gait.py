import enum
from typing import Optional

from gait.gait import Gait, GaitManager, GaitProcessState, INITIAL_SETUP_GAIT_NAME
from support.common_gait import CommonGait

DEBUG = True


SHIN_ANGLE = 25
THIGH_ANGLE = 35


class InitialSetupStages(enum.Enum):
    INITIAL_WAIT = enum.auto()
    SHOULDERS_WAIT = enum.auto()
    SHOULDERS_WAIT2 = enum.auto()
    GOTO_TUCK_IN = enum.auto()


class InitialSetUpGait(CommonGait):
    def __init__(self, gait_manager: GaitManager, name: str = INITIAL_SETUP_GAIT_NAME):
        super().__init__(gait_manager, name)
        self.finished = False
        self.count_down = 5
        self.state: InitialSetupStages = InitialSetupStages.INITIAL_WAIT

    def start(self, current_time: float, context: dict) -> None:
        if DEBUG: print(f"Initial set up: starting initial set up gait")
        super().start(current_time, context)
        self.finished = False
        self.count_down = 5
        self.body.all_off()
        self.state: InitialSetupStages = InitialSetupStages.INITIAL_WAIT

    def stop(self, current_time: float, new_gait: Optional[Gait], new_context: dict, active: bool) -> None:
        if not self.finished:
            self.gait_manager.push_gait("initial_set_up", current_time, {})

    def process(self, current_time: float, context: dict) -> GaitProcessState:
        if not self.finished:
            if self.count_down > 0:
                self.count_down -= 1
                if self.state == InitialSetupStages.GOTO_TUCK_IN:
                    for leg in self.body.legs.values():
                        if leg.leg_id.is_left:
                            self.body.servo_driver.set_angle(leg.shoulder_servo_definition, -self.count_down)
                        else:
                            self.body.servo_driver.set_angle(leg.shoulder_servo_definition, self.count_down)
                        self.body.servo_driver.set_angle(leg.thigh_servo_definition, -THIGH_ANGLE)
                        self.body.servo_driver.set_angle(leg.shin_servo_definition, SHIN_ANGLE)

            else:
                if self.state == InitialSetupStages.INITIAL_WAIT:
                    if DEBUG: print("    Initial set up: INITIAL_WAIT - setting power to 50%, turning servos on")
                    self.body.servo_driver.global_power = 50
                    self.body.servo_driver.global_off = False

                    for leg in self.body.legs.values():
                        if leg.leg_id.is_left:
                            if DEBUG: print(f"                  : {leg.leg_id.name}, servo {leg.shoulder_servo_definition.servo_channel} angle to -45º")
                            self.body.servo_driver.set_angle(leg.shoulder_servo_definition, -45)
                        else:
                            if DEBUG: print(f"                  : {leg.leg_id.name}, servo {leg.shoulder_servo_definition.servo_channel} angle to 45º")
                            self.body.servo_driver.set_angle(leg.shoulder_servo_definition, 45)
                        self.body.servo_driver.set_angle(leg.thigh_servo_definition, -THIGH_ANGLE)
                        self.body.servo_driver.set_angle(leg.shin_servo_definition, SHIN_ANGLE)

                    if DEBUG: print(f"                  : count down to 50 for next SHOULDERS_WAIT")
                    self.count_down = 70
                    self.state = InitialSetupStages.SHOULDERS_WAIT
                elif self.state == InitialSetupStages.SHOULDERS_WAIT:
                    if DEBUG: print("    Initial set up: SHOULDERS_WAIT - setting power to 100%")
                    self.body.servo_driver.global_power = 100
                    self.count_down = 20
                    self.state = InitialSetupStages.SHOULDERS_WAIT2
                    if DEBUG: print(f"                  : count down to 20 for next SHOULDERS_WAIT2")

                elif self.state == InitialSetupStages.SHOULDERS_WAIT2:
                    if DEBUG: print("    Initial set up: SHOULDERS_WAIT2 - moving legs in")
                    self.count_down = 45
                    self.state = InitialSetupStages.GOTO_TUCK_IN
                    if DEBUG: print(f"                  : count down to 45 for next GOTO_TUCK_IN")
                elif self.state == InitialSetupStages.GOTO_TUCK_IN:
                    if DEBUG: print("    Initial set up: GOTO_TUCK_IN - done")
                    for leg in self.body.legs.values():
                        leg.position.x = 37
                        leg.position.y = 0
                        leg.position.z = 0
                    self.body.body_position.z = 50
                    self.finished = True

        if not self.finished:
            return GaitProcessState.RESTING
        else:
            return GaitProcessState.FINISHED
