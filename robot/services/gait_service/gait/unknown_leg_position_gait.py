from typing import Optional

from gait.gait import Gait, GaitManager, GaitProcessState

DEBUG = False


class UnknownLegPositionGait(Gait):
    def __init__(self, gait_manager: GaitManager, name: str = "unknown_leg_position"):
        super().__init__(gait_manager, name)
        self.finished = False

    def start(self, current_time: float, context: dict) -> None:
        if DEBUG: print(f"Unknown leg position: starting unknown leg position gait")
        super().start(current_time, context)
        self.body.all_off()

    def stop(self, current_time: float, new_gait: Optional[Gait], new_context: dict, active: bool) -> None:
        self.gait_manager.push_gait("initial_set_up", current_time, {})

    def process(self, current_time: float, context: dict) -> GaitProcessState:
        return GaitProcessState.OPERATING
