from typing import Optional

from gait.gait import Gait, GaitManager, GaitProcessState
from body import LegID

DEBUG = True


class PositionGait(Gait):
    def __init__(self, gait_manager: GaitManager, name: str = "position"):
        super().__init__(gait_manager, name)
        self.finished = False
        self.ox = 0
        self.oy = 0
        self.height = 0
        self.cx = 0
        self.cy = 0

    def _process_context(self, current_time: float, context: dict) -> None:
        self.body.all_on()

        self.ox = float(context["ox"]) if context is not None and "ox" in context else self.body.body_position.x
        self.oy = float(context["oy"]) if context is not None and "oy" in context else self.body.body_position.y
        self.height = float(context["height"]) if context is not None and "height" in context else self.body.body_position.z

        self.cx = float(context["cx"]) if context is not None and "cx" in context else 0
        self.cy = float(context["cy"]) if context is not None and "cy" in context else 0

        if "x" in context or "y" in context or "z" in context:
            x = float(context["x"]) if context is not None and "x" in context else 35
            y = float(context["y"]) if context is not None and "y" in context else 0
            z = float(context["z"]) if context is not None and "z" in context else self.height

            self.body.body_linear_move(self.ox, self.oy, z, current_time, self.speed)
            if DEBUG: print(f"   Body: {self.body.body_position_path}")

            for leg in self.body:
                x_adjust = (self.cx if leg.leg_id.is_front else - self.cx)
                y_adjust = (self.cy if leg.leg_id.is_right else - self.cy)

                leg.linear_move(x + x_adjust, y + y_adjust, 0, current_time, self.speed, self.power)
                if DEBUG: print(f"   {leg.leg_id}  : {leg.path} @ {self.power:.2f}%")

        else:
            self.body.body_linear_move(self.ox, self.oy, self.height, current_time, self.speed)
            if DEBUG: print(f"   Body: {self.body.body_position_path}")

            for leg_id in LegID:
                leg = self.body[leg_id]
                x = float(context[f"{leg_id.name}.x"]) if context is not None and f"{leg_id.name}.x" in context else leg.position.x
                y = float(context[f"{leg_id.name}.y"]) if context is not None and f"{leg_id.name}.y" in context else leg.position.y
                z = float(context[f"{leg_id.name}.z"]) if context is not None and f"{leg_id.name}.z" in context else leg.position.z

                # x_adjust = self.cx if leg.leg_id.is_front else - self.cx
                # y_adjust = self.cy if leg.leg_id.is_right else - self.cy
                #
                # x = x + x_adjust
                # y = y + y_adjust

                if x != leg.position.x or y != leg.position.y or z != leg.position.z:
                    leg.linear_move(x, y, z, current_time, self.speed, self.power)
                    if DEBUG: print(f"   {leg.leg_id}  : {leg.path} @ {self.power:.2f}%")

    def start(self, current_time: float, context: dict) -> None:
        if DEBUG: print(f"Pos: starting position gait")
        super().start(current_time, context)
        self._process_context(current_time, context)

    def cont(self, current_time: float, context: dict) -> None:
        super().cont(current_time, context)
        self.started_time = current_time
        self._process_context(current_time, context)

    def update(self, current_time: float, context: dict) -> None:
        self._process_context(current_time, context)

    def stop(self, current_time: float, new_gait: Optional[Gait], new_context: dict, active: bool) -> None:
        pass

    def process(self, current_time: float, context: dict) -> GaitProcessState:
        if not self.finished:
            if self.body.is_all_at_destination(current_time):
                print(f"Pos: Legs arrived at required position")
                self.finished = True
                return GaitProcessState.RESTING

        return GaitProcessState.OPERATING
