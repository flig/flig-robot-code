import math
from typing import Tuple


def circle_centre(x1: float, y1: float, x2: float, y2: float, x3: float, y3: float) -> Tuple[float, float]:
    x12 = x1 - x2
    x13 = x1 - x3

    y12 = y1 - y2
    y13 = y1 - y3

    y31 = y3 - y1
    y21 = y2 - y1

    x31 = x3 - x1
    x21 = x2 - x1

    sx13 = x1 * x1 - x3 * x3
    sy13 = y1 * y1 - y3 * y3
    sx21 = x2 * x2 - x1 * x1
    sy21 = y2 * y2 - y1 * y1

    x = (sx13 * y12 + sy13 * y12 + sx21 * y13 + sy21 * y13) / (2 * (x21 * y13 - x31 * y12))
    y = (sx13 * x12 + sy13 * x12 + sx21 * x13 + sy21 * x13) / (2 * (y21 * x13 - y31 * x12))

    return x, y


def inscribed_circle_centre(x1: float, y1: float, x2: float, y2: float, x3: float, y3: float) -> Tuple[float, float]:
    # a = math.sqrt((x2 - x3) * (x2 - x3) + (y2 - y3) * (y2 - y3))
    # b = math.sqrt((x1 - x3) * (x1 - x3) + (y1 - y3) * (y1 - y3))
    # c = math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2))
    #
    # d = a + b + c
    #
    # x = (a * x1 + b * x2 + c * x3) / d
    # y = (a * y1 + b * y2 + c * y3) / d

    x = (x1 + x2 + x3) / 3.0
    y = (y1 + y2 + y3) / 3.0

    return x, y


def is_close(value: float, close_to: float, abs_distance: float = 0.001) -> bool:
    return abs(value - close_to) < abs_distance
