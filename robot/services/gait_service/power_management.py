
import time
from enum import auto, Enum
from threading import Thread
from typing import Optional, Any

import pyros
from telemetry import SocketTelemetryServer, TelemetryLogger

from ina219 import INA219
from pcs9685ina3221_servo_lib import INA3221


class CurrentStatus(Enum):
    NOMINAL = auto()
    WARNING = auto()
    CRITICAL = auto()


class PowerManagement:
    def __init__(self,
                 ina219_shunt: float = 0.1,
                 ina219_address: int = 0x45,
                 ina3221_1_address: int = 0x40,
                 ina3221_2_address: int = 0x41) -> None:

        # system parameters
        self.telemetry_server_port = 0

        # configuration parameters
        self.servo_current_history_max_count = 50  # 1 second
        self.servo_current_warning_level = 600  # 1 second
        self.servo_current_warning_level_duration = 0.5  # 1 second
        self.servo_current_critical_level = 1000  # 1 second
        self.servo_current_critical_level_duration = 0.5  # 1 second
        self.total_battery_capacity_mAh = 1600
        self.lowest_battery_voltage = 3.75 * 2
        self.highest_battery_voltage = 4.2 * 2

        # shared, public parameters
        self.servo_currents = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        warning_count = int(self.servo_current_warning_level_duration / 0.02)  # 0.02 is 50 times/s
        critical_count = int(self.servo_current_critical_level_duration / 0.02)  # 0.02 is 50 times/s
        self.servo_currents_history = [[0.0, 0.0, 0.0, 0.0, 0.0, 0.0] for _ in range(max(warning_count, critical_count) * 2)]

        self.high_current_overall_status = CurrentStatus.NOMINAL
        self.high_current_overall_status_message: Optional[str] = None

        self.total_servo_current = 0
        self.electronics_current = 0
        self.peak_current = 0
        self.peak_servos_current = 0
        self.peak_electronic_current = 0

        self.servos_mAh = 0
        self.electronics_mAh = 0
        self.total_mAh = 0

        self.input_voltage = 0
        self.on_battery = False
        self.battery_percentage = 0

        # internal vars
        self.last_time = 0
        self.last_advertise_time = time.time() + 15  # Allow for not advertising it until power_state_service doesn't boot up

        try:
            self._ina = INA219(ina219_shunt, max_expected_amps=3.1, i2c_addr=ina219_address)
            self._ina.configure()
        except Exception as e:
            print(f"ERROR: Failed to set up INA219 at i2c address {ina219_address}")
            raise e

        try:
            self._ina1 = INA3221(i2c_addr=ina3221_1_address)
        except Exception as e:
            print(f"ERROR: Failed to set up first INA3221 at i2c address {ina3221_1_address}")
            raise e
        try:
            self._ina2 = INA3221(i2c_addr=ina3221_2_address)
        except Exception as e:
            print(f"ERROR: Failed to set up second INA3221 at i2c address {ina3221_2_address}")
            raise e

        self._server: Optional[SocketTelemetryServer] = None
        self._server_socket: Optional[Any] = None
        self._servos_current_stream_logger: Optional[TelemetryLogger] = None

        self._received_previous_state = False
        pyros.subscribe("power/previous-state", self._previous_state_details)

        self._had_exception = False

    @property
    def ina1_driver(self) -> INA3221: return self._ina1

    @property
    def ina2_driver(self) -> INA3221: return self._ina2

    def start_telemetry_server(self, telemetry_server_port: int = 1863) -> None:
        self.telemetry_server_port = telemetry_server_port
        self._server = SocketTelemetryServer(port=self.telemetry_server_port)

        self.wait_for_server_start()
        self._server_socket = self._server.get_socket()
        self._server_socket.settimeout(10)

        self._servos_current_stream_logger = self._define_stream(self._server)
        self._start_telemetry_server()

    def wait_for_server_start(self):
        timeout = time.time() + 10
        started = False

        while not started and time.time() < timeout:
            try:
                self._server.start()
                started = True
            except Exception as e:
                print(f"    failed to start socket server; {e}")
                time.sleep(1)

    @staticmethod
    def _define_stream(server: SocketTelemetryServer) -> TelemetryLogger:
        servos_current_stream_logger = server.create_logger("servo_current_stream")
        servos_current_stream_logger.add_float('current_0')
        servos_current_stream_logger.add_float('current_1')
        servos_current_stream_logger.add_float('current_2')
        servos_current_stream_logger.add_float('current_3')
        servos_current_stream_logger.add_float('current_4')
        servos_current_stream_logger.add_float('current_5')
        servos_current_stream_logger.add_float('electronics_current')
        servos_current_stream_logger.add_float('input_voltage')
        servos_current_stream_logger.init()
        return servos_current_stream_logger

    def _start_telemetry_server(self) -> None:
        def server_loop():
            while True:
                try:
                    self._server.process_incoming_connections()
                except Exception as _:
                    pass

        server_thread = Thread(name="reading-servo-current-stream", target=server_loop, daemon=True)
        server_thread.start()

    def process_current_data(self, current_time: float) -> None:
        self.servo_currents = [self._ina1.get_current_mA(i + 1) for i in range(3)] + [self._ina2.get_current_mA(i + 1) for i in range(3)]

        # currents = [self.servo_driver.get_current_mA(i + 1) for i in range(6)]
        # current_sum_1 = self.ina1.get_sum_current_mA()
        # current_sum_2 = self.ina2.get_sum_current_mA()
        self.total_servo_current = sum(self.servo_currents)

        self.input_voltage = self._ina.voltage()
        self.electronics_current = self._ina.current()

        self._servos_current_stream_logger.log(
            current_time,
            self.servo_currents[0],
            self.servo_currents[1],
            self.servo_currents[2],
            self.servo_currents[3],
            self.servo_currents[4],
            self.servo_currents[5],
            self.electronics_current,
            self.input_voltage)

        self.servo_currents_history.append(self.servo_currents_history[0])
        del self.servo_currents_history[0]
        top = self.servo_currents_history[-1]
        top[0] = self.servo_currents[0]
        top[1] = self.servo_currents[1]
        top[2] = self.servo_currents[2]
        top[3] = self.servo_currents[3]
        top[4] = self.servo_currents[4]
        top[5] = self.servo_currents[5]

        if self.last_time != 0:
            self._process_currents(current_time)

        else:
            self.on_battery = self.input_voltage <= self.highest_battery_voltage * 1.020
            time.sleep(0.1)
            print(f"    Requesting previous power state")
            pyros.publish("power/request-previous-state", "request")
            time.sleep(0.1)

        self.last_time = current_time

    def _previous_state_details(self, _topic, payload) -> None:
        if self._received_previous_state:
            print(f"    Received previous power state again - no action this time: {payload}")
        else:
            self._received_previous_state = True
            print(f"    Received previous power state: {payload}")
            for k, v in [(kv[0], kv[1]) for kv in [kv.split("=") for kv in payload.split(",")] if len(kv) > 1]:
                if k == "mAh_s":
                    self.servos_mAh += float(v)
                elif k == "mAh_e":
                    self.electronics_mAh += float(v)

    # noinspection PyPep8Naming
    def _process_currents(self, current_time: float) -> None:
        delta_time = current_time - self.last_time
        self.servos_mAh += self.total_servo_current * self.input_voltage / 5.0 * delta_time / 3600.0
        self.electronics_mAh += self.electronics_current * delta_time / 3600.0

        self.total_mAh = self.servos_mAh + self.electronics_mAh

        if not self.on_battery:
            # Mains
            self.battery_percentage = 100
        else:
            if self.total_mAh >= self.total_battery_capacity_mAh:
                battery_mAh_percentage = 0
            else:
                battery_mAh_percentage = 100.0 * (self.total_battery_capacity_mAh - self.total_mAh) / self.total_battery_capacity_mAh

            if self.input_voltage <= self.lowest_battery_voltage:
                battery_voltage_percentage = 0
            elif self.input_voltage >= self.highest_battery_voltage:
                battery_voltage_percentage = 100
            else:
                battery_voltage_percentage = 100.0 * (self.input_voltage - self.lowest_battery_voltage) / (self.highest_battery_voltage - self.lowest_battery_voltage)

            if battery_mAh_percentage < 0.1 or battery_voltage_percentage < 0.1:
                self.battery_percentage = min(battery_mAh_percentage, battery_voltage_percentage)
            else:
                self.battery_percentage = battery_mAh_percentage * 0.5 + battery_voltage_percentage * 0.5

        current_current = self.total_servo_current + self.electronics_current
        self.peak_current = current_current if current_current > self.peak_current else self.peak_current
        self.peak_servos_current = self.total_servo_current if self.total_servo_current > self.peak_servos_current else self.peak_servos_current
        self.peak_electronic_current = self.electronics_current if self.electronics_current > self.peak_electronic_current else self.peak_electronic_current

        if self.last_advertise_time < current_time:
            pyros.publish("power/state", f"V={self.input_voltage:.2f},"
                                         f"B={str(self.on_battery).lower()},"
                                         f"B%={self.battery_percentage:.1f},"
                                         f"mAh={self.servos_mAh + self.electronics_mAh:.0f},"
                                         f"mAh_s={self.servos_mAh:.0f},"
                                         f"mAh_e={self.electronics_mAh:.0f},"
                                         f"mA={self.peak_current:.0f},"
                                         f"mA_s={self.peak_servos_current:.0f},"
                                         f"mA_e={self.peak_electronic_current:.0f}")

            self.peak_current = current_current
            self.peak_servos_current = self.total_servo_current
            self.peak_electronic_current = self.electronics_current
            self.last_advertise_time = current_time + 2

        warning_count = int(self.servo_current_warning_level_duration / 0.02)  # 0.02 is 50 times/s
        critical_count = int(self.servo_current_critical_level_duration / 0.02)  # 0.02 is 50 times/s
        count = max(warning_count, critical_count) + 1

        i = 0
        try:
            checking_critical = True
            message = None
            status: Optional[CurrentStatus] = None
            while i < count and status is None:
                i += 1
                history_entry = self.servo_currents_history[-i]
                max_current = max(history_entry)
                max_current_index = history_entry.index(max_current)
                if max_current < self.servo_current_warning_level:
                    status = CurrentStatus.NOMINAL
                elif max_current < self.servo_current_critical_level:
                    if i > warning_count:
                        status = CurrentStatus.WARNING
                        message = f"{(message + ', ') if message is not None else ''}Warning in group {max_current_index}"
                    checking_critical = False
                elif checking_critical and i > critical_count:
                    status = CurrentStatus.CRITICAL
                    message = f"{(message + ', ') if message is not None else ''}CRITICAL in group {max_current_index}"

            self.high_current_overall_status = status
            self.high_current_overall_status_message = message
            self._had_exception = False
        except Exception as e:
            if not self._had_exception:
                self._had_exception = True
                print(f"warning_count={warning_count}, critical_count={critical_count}, count={count}, i={i}, len={len(self.servo_currents_history)}, e={e}")
