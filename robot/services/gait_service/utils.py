from datetime import datetime


def time_to_string(time: float) -> str:
    return datetime.fromtimestamp(time).strftime("%M:%S.%f")[:-3]
