import math
from abc import ABC, abstractmethod
from typing import Optional, Tuple

from utils import time_to_string
from vector import Vector3

DEBUG = False
ARC_PATH_DEBUG = False


class Path(ABC):
    def __init__(self, start: Optional[Vector3] = None, end: Optional[Vector3] = None) -> None:
        self.start = start if start is not None else Vector3()
        self.end = end if end is not None else Vector3()
        self.current: Optional[Vector3] = None
        self.requested_time: Optional[float] = None
        self.requested_arrival: Optional[float] = None
        self.current_position_in_path: Optional[float] = None
        self.requested_speed: Optional[float] = None
        self.temp = Vector3()

    def set_start_values(self, x: float, y: float, z: float) -> 'Path':
        self.start.set_values(x, y, z)
        return self

    def set_end_values(self, x: float, y: float, z: float) -> 'Path':
        self.end.set_values(x, y, z)
        return self

    def _prepare_with_duration(self, start: Vector3, end: Vector3, current: Vector3, requested_time: float, duration: float) -> 'Path':
        self.requested_time = requested_time
        self.requested_arrival = requested_time + duration
        self.current_position_in_path = 0.0
        self.requested_speed = None
        self.start.set(start)
        self.current = current
        self.current.set(self.start)
        self.end.set(end)
        return self

    def _prepare_with_speed(self, start: Vector3, end: Vector3, current: Vector3, requested_time: float, speed: float) -> 'Path':
        raise NotImplementedError("Cannot set for speed yet")
        # self.requested_time = requested_time
        # self.requested_arrival = None
        # self.requested_speed = speed
        # self.current_position_in_path = None
        # self.start.set(start)
        # self.current = current
        # self.current.set(self.start)
        # self.end.set(end)
        # return self

    def prepare(self, start: Vector3, end: Vector3, current: Vector3, requested_time: float, speed_or_duration: float) -> 'Path':
        if speed_or_duration >= 0.0:
            return self._prepare_with_duration(start, end, current, requested_time, speed_or_duration)
        else:
            return self._prepare_with_speed(start, end, current, requested_time, -speed_or_duration)

    def current_relative_position_in_path(self, current_time: float) -> float:
        if self.requested_arrival is not None and self.requested_time is not None:
            # return (current_time - self.requested_time) / (self.requested_arrival - self.requested_time)
            # if math.isclose(self.requested_arrival, self.requested_time, abs_tol=0.001):
            if self.requested_arrival <= self.requested_time:
                return 1
            else:
                return (current_time - self.requested_time) / (self.requested_arrival - self.requested_time)
        elif self.current_position_in_path is not None:
            return self.current_position_in_path
        else:
            return 0.0

    def at_destination(self, current_time: float) -> bool:
        if self.requested_arrival is not None:
            return current_time >= self.requested_arrival
        else:
            return self.current_position_in_path is not None and self.current_position_in_path >= 1.0

    def drive(self, current_time: float) -> Vector3:
        if self.requested_arrival is not None:
            if current_time >= self.requested_arrival:
                return self._drive(1)
            elif current_time <= self.requested_time:
                return self.start
            else:
                return self._drive(self.current_relative_position_in_path(current_time))
        else:
            raise NotImplementedError

    @abstractmethod
    def _drive(self, factor: float) -> Vector3:
        pass

    @abstractmethod
    def calculate_point_with_distance(self, x: float, y: float, z: float, l1: float) -> Tuple[float, float, float]:
        pass


class LinearPath(Path):
    def __init__(self, start: Optional[Vector3] = None, end: Optional[Vector3] = None) -> None:
        super().__init__(start, end)

    def __repr__(self) -> str:
        if self.requested_arrival is not None:
            return f"LinearPath[{self.start} -> {self.end}, {self.current}, {time_to_string(self.requested_time)}-{time_to_string(self.requested_arrival)}]"
        elif self.requested_speed is not None:
            return f"LinearPath[{self.start} -> {self.end}, {self.current}, {time_to_string(self.requested_time)} @ {self.requested_speed * 100:.1f}%]"
        else:
            return f"LinearPath[{self.start} -> {self.end}, {self.current}, unset]"

    def __str__(self) -> str:
        return self.__repr__()

    def _drive(self, factor: float) -> Vector3:
        if 0 < factor < 1.0:
            return self.current.lerp_in(self.start, self.end, factor)
        elif factor >= 1.0:
            self.current.set(self.end)
            return self.end
        else:
            self.current.set(self.start)
            return self.start

    def calculate_point_with_distance(self, x: float, y: float, z: float, l1: float) -> Tuple[float, float, float]:
        xc = self.current.x - x
        yc = self.current.z - z

        x1 = self.start.x - x
        y1 = self.start.z - z
        x2 = self.end.x - x
        y2 = self.end.z - z

        cc = (y2 - y1) / (x2 - x1)
        a = cc * cc + 1
        b = 2 * cc * x1 * (1 - cc)
        c = (y1 - cc * x1) * (y1 - cc * x1) - l1 * l1

        x_1 = (-b + math.sqrt(b * b - 4 * a * c)) / 2 * a
        x_2 = (-b - math.sqrt(b * b - 4 * a * c)) / 2 * a

        y_1 = cc * x_1 - cc * x1 + y1
        y_2 = cc * x_2 - cc * x1 + y1

        def distance(x1: float, y1: float, x2: float, y2: float) -> float:
            return math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2))

        current_position_distance = distance(xc, yc, x2, y2)
        first_point_distance = distance(x_1, y_1, x2, y2)
        second_point_distance = distance(x_2, y_2, x2, y2)

        if current_position_distance < first_point_distance:
            return x_2 + x, y, y_2 + z
        elif current_position_distance < second_point_distance:
            return x_1 + x, y, y_1 + z
        elif second_point_distance < second_point_distance:
            return x_2 + x, y, y_2 + z
        # elif first_point_distance < second_point_distance:

        return x_1 + x, y, y_1 + z


class ArcPath(Path):
    def __init__(self, arc_height: float = 0.0, start: Optional[Vector3] = None, end: Optional[Vector3] = None) -> None:
        super().__init__(start, end)
        self._arc_height = arc_height
        self.arc_half_width = abs((self.end.x - self.start.x) / 2.0)
        self.angle_direction = 1

    def __repr__(self) -> str:
        if self.requested_arrival is not None:
            return f"ArcPath[{self.start} -> {self.end}, {self.current}, {time_to_string(self.requested_time)}-{time_to_string(self.requested_arrival)}]," \
                   f"ah={self._arc_height},ahw={self.arc_half_width:.1f},dir={self.angle_direction}"
        elif self.requested_speed is not None:
            return f"ArcPath[{self.start} -> {self.end}, {self.current}, {time_to_string(self.requested_time)} @ {self.requested_speed * 100:.1f}%]," \
                   f"ah={self._arc_height},ahw={self.arc_half_width:.1f},dir={self.angle_direction}"
        else:
            return f"ArcPath[{self.start} -> {self.end}, {self.current}, unset]," \
                   f"ah={self._arc_height},ahw={self.arc_half_width:.1f},dir={self.angle_direction}"

    def __str__(self) -> str:
        return self.__repr__()

    @property
    def arc_height(self) -> float: return self._arc_height

    @arc_height.setter
    def arc_height(self, arc_height: float) -> None:
        self._arc_height = arc_height
        self.arc_half_width = abs((self.end.x - self.start.x) / 2.0)

    def prepare(self, start: Vector3, end: Vector3, current: Vector3, requested_time: float, duration_or_speed: float) -> 'Path':
        super().prepare(start, end, current, requested_time, duration_or_speed)
        self.arc_half_width = abs((self.end.x - self.start.x) / 2.0)
        # TODO make it in 3D adding 3rd dimension
        self.angle_direction = -1 if self.end.x < self.start.x else 1
        return self

    def _drive(self, factor: float) -> Vector3:
        angle = math.pi * factor
        return self._drive_internal(factor, angle)

    def _drive_internal(self, factor: float, angle: float) -> Vector3:
        if factor < 1.0:
            height_y = self.start.z + (self.end.z - self.start.z) * factor

            if self.angle_direction > 0:
                self.current.x = self.start.x + self.arc_half_width - self.arc_half_width * math.cos(angle)
                self.current.z = height_y - self.arc_height * math.sin(angle)
            else:
                self.current.x = self.start.x - self.arc_half_width + self.arc_half_width * math.cos(angle)
                self.current.z = height_y - self.arc_height * math.sin(angle)

            self.current.y = self.start.y + (self.end.y - self.start.y) * factor

            if DEBUG or ARC_PATH_DEBUG:
                print(f"... arc {self.start} -> {self.end.x}"
                      f" factor={factor:.2f}, angle={math.degrees(angle):.2f} to {self.current}, dir={self.angle_direction},"
                      f" h_a={self.arc_half_width:.2f}, h={self.arc_height:.2f}")
        else:
            self.current.set(self.end)
        return self.current

    def calculate_point_with_distance(self, x: float, y: float, z: float, l1: float) -> Tuple[float, float, float]:
        aa = self.arc_half_width
        bb = self._arc_height
        x0 = self.start.x + aa
        # x0 = self.start.x
        y0 = self.start.z

        print(f"    x, z = {x}, {z}, l1 = {l1}")

        print(f"    x0, y0 = {x0}, {y0}")

        x1 = x - x0
        y1 = z - y0

        self.x1 = x1
        self.y1 = y1

        print(f"    x1, y1 = {x1}, {y1}")
        print(f"    x1^2 + y2^2 = {x1 * x1 + y1 * y1} ?= l1^2 = {l1 * l1}")
        print(f"    aa={aa}, bb={bb}")

        # if True:
        #     return x1, y1, l1, x0, y0

        c1 = x1 * x1 + y1 * y1 - l1 * l1
        c2 = aa * aa / bb * bb
        c3 = 1 - c2
        c4 = c1 + aa * aa

        print(f"    c1, c2, c3 = {c1}, {c2}, {c3}")

        a = c3 * c3
        b = -4 * y1 * c3
        c = 2 * c3 * c4 + 4 * y1 * y1 + 4 * x1 * x1 * c2   #c1 + c1 * c3 + 4 * y1 * y1 + 4 * x1 * x1 * c2
        d = -4 * y1 * c4  # -4 * y1 * c1
        e = c4 * c4 - 4 * x1 * x1 * aa * aa   # c1 * c1 - 4 * a * a * x1 * x1 * l1 * l1

        print(f"    f^4(x) = {a} * x^4 + {b} * x ^ 3 + {c} * x ^ 2 + {d} * x + {e}")

        p = (8 * a * c - 3 * b * b) / (8 * a * a)
        q = (b * b * b - 4 * a * b * c - 8 * a * a * d) / (8 * a * a * a)

        d0 = c * c - 3 * b * d + 12 * a * e
        d1 = 2 * c * c * c - 9 * b * c * d + 27 * b * b * e + 27 * a * d * d - 72 * a * c * e

        dd = (4 * d0 * d0 * d0 - d1 * d1) / 27

        if dd > 0:
            P = 8 * a * c - 3 * b * b
            D = 64 * a * a * a * e - 16 * a * a * c * c - 16 * a * b * b * c - 16 * a * a * b * d - 3 * b * b * b * b
            print(f"    ∆={dd} P={P} D={D}")

            phi = math.acos(d1 / (2 * math.sqrt(d0 * d0 * d0)))
            doroot = math.sqrt(d0 * d0 * d0)
            ss = 0.5 * math.sqrt(-2 * p / 3 + 2 * doroot * math.cos(phi / 3) / (3 * a))
        else:
            # print(f"    ∆={dd}")

            qq = ((d1 + math.sqrt(d1 * d1 - 4 * d0 * d0 * d0)) / 2.0) ** (1.0 / 3.0)
            ss = math.sqrt(-2.0 * p / 3.0 + (qq + d0 / qq) / (3.0 * a)) / 2.0
            print(f"    ∆={dd}, ss={ss}, qq={qq}, d0={d0}, d1={d1}")

        def calculate_y(x: float) -> Optional[Tuple[Tuple[float, float, float], float, float]]:
            y_squared = bb * bb - bb * bb * x * x / aa * aa
            if y_squared >= 0:
                y = math.sqrt(y_squared)
                if math.isclose((x - x1) * (x - x1) + (y - y1) * (y - y1), l1 * l1, abs_tol=0.05):
                    angle = math.atan2(y, x)
                    factor = angle / math.pi

                    return self._drive_internal(factor, angle).as_tuple(), angle, factor

            return None

        def calculate_x(y: float) -> Optional[Tuple[Tuple[float, float, float], float, float]]:
            x_squared = aa * aa - aa * aa * y * y / bb * bb
            if x_squared >= 0:
                x = math.sqrt(x_squared)
                if math.isclose((x - x1) * (x - x1) + (y - y1) * (y - y1), l1 * l1, abs_tol=0.05):
                    angle = math.atan2(y, x)
                    factor = angle / math.pi

                    return self._drive_internal(factor, angle).as_tuple(), angle, factor

            return None

        b4a = - b / (4 * a)

        # dd_full = 256 * a * a * a * e * e * e - 192 * a * a * b * d * e * e - \
        #     128 * a * a * c * c * e * e + 144 * a * a * c * d * d * e - \
        #     27 * a * a * d * d * d * d + 144 * a * b * b * c * e * e - \
        #     6 * a * b * b * d * d * e - 80 * a * b * c * c * d * e + \
        #     18 * a * b * c * d * d * d + 16 * a * c * c * c * c * e - \
        #     4 * a * c * c * c * d * d - 27 * b * b * b * b * e * e + \
        #     18 * b * b * b * c * d * e - 4 * b * b * b * d * d * d - \
        #     4 * b * b * c * c * c * e + b * b * c * c * d * d
        # print(f"∆={dd}")
        # print(f"∆={dd_full}")

        print(f"    ss={ss} p={p} q/ss={q / ss}, dd={dd}, d0={d0}, d1={d1}")
        print(f"    -4*ss={-4* ss}  -2*p={-2 * p} q/ss={q / ss}, dd={dd}, d0={d0}, d1={d1}")
        s24m2pqovers1 = -4 * ss * ss - 2 * p + q / ss

        results = []
        if s24m2pqovers1 >= 0:
            results.extend([
                b4a - ss - math.sqrt(s24m2pqovers1) / 2,
                b4a - ss + math.sqrt(s24m2pqovers1) / 2,
            ])
        else:
            P = 8 * a * c - 3 * b * b
            print(f"    s24m2pqovers1={s24m2pqovers1}, P={P}, 8*a^2={8 * a * a}")

        s24m2pqovers2 = -4 * ss * ss - 2 * p - q / ss
        if s24m2pqovers2 >= 0:
            results.extend([
                b4a - ss - math.sqrt(s24m2pqovers2) / 2,
                b4a - ss + math.sqrt(s24m2pqovers2) / 2,
            ])
        else:
            P = 8 * a * c - 3 * b * b
            print(f"    s24m2pqovers2={s24m2pqovers2}, P={P}, 8*a^2={8 * a * a}")

        results = [r for r in [calculate_x(y) for y in results] if r is not None]
        results = [r for r in results if r[2] > self.current_position_in_path]

        if len(results) > 1:
            results = [r for r in results if (self.angle_direction < 0 and r[0][2] <= 0) or (self.angle_direction > 0 and r[0][2] >= 0)]

        if len(results) == 0:
            raise ValueError(f"We don't have real results")
        if len(results) != 1:
            raise ValueError(f"We have {len(results)} results ({results})")

        xr, yr, zr = results[0][0]
        self.current_position_in_path = results[0][2]
        return xr + x0, yr, zr + y0
