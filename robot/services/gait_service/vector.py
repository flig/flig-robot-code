import math
from typing import Tuple


class Vector3:
    def __init__(self, x: float = 0.0, y: float = 0.0, z: float = 0.0) -> None:
        self.x = x
        self.y = y
        self.z = z

    def __repr__(self) -> str:
        return f"Vector3[{self.x:.2f}, {self.y:.2f}, {self.z:.2f}]"

    def __str__(self) -> str:
        return f"({self.x:.2f}, {self.y:.2f}, {self.z:.2f})"

    def __iadd__(self, other) -> 'Vector3':
        if not isinstance(other, Vector3):
            raise TypeError(f"Other object is not Vector3; {type(other)}")
        self.x += other.x
        self.y += other.y
        self.z += other.z
        return self

    def __isub__(self, other) -> 'Vector3':
        if not isinstance(other, Vector3):
            raise TypeError(f"Other object is not Vector3; {type(other)}")
        self.x -= other.x
        self.y -= other.y
        self.z -= other.z
        return self

    def as_tuple(self) -> Tuple[float, float, float]:
        return self.x, self.y, self.z

    def clear(self) -> 'Vector3':
        self.x = 0
        self.y = 0
        self.z = 0
        return self

    def set(self, other: 'Vector3') -> 'Vector3':
        if other != self:
            self.x = other.x
            self.y = other.y
            self.z = other.z
        return self

    def set_values(self, x: float, y: float, z: float) -> 'Vector3':
        self.x = x
        self.y = y
        self.z = z
        return self

    def set_tuple(self, values: Tuple[float, float, float]) -> 'Vector3':
        self.x = values[0]
        self.y = values[1]
        self.z = values[2]
        return self

    def lerp_in(self, start: 'Vector3', end: 'Vector3', position: float) -> 'Vector3':
        self.x = start.x + (end.x - start.x) * position
        self.y = start.y + (end.y - start.y) * position
        self.z = start.z + (end.z - start.z) * position
        return self

    def lerp(self, end: 'Vector3', position: float) -> Tuple[float, float, float]:
        return self.x + (end.x - self.x) * position,\
               self.y + (end.y - self.y) * position,\
               self.z + (end.z - self.z) * position

    def distance_vec(self, other: 'Vector3') -> float:
        return math.sqrt((self.x - other.x) * (self.x - other.x) + (self.y - other.y) * (self.y - other.y) + (self.z - other.z) * (self.z - other.z))

    def distance_values(self, x: float, y: float, z: float) -> float:
        return math.sqrt((self.x - x) * (self.x - x) + (self.y - y) * (self.y - y) + (self.z - z) * (self.z - z))
