import math
from enum import Enum
from typing import Tuple, Optional, Dict, List, cast

from gait.utils import absolute_tolerance, math_pi_half
from path import LinearPath, ArcPath, Path

from servo_lib.leg_servos import LegServos, ServoDefinition
from servo_lib.servo_driver import ServoDriver
from vector import Vector3

INITIAL_DEBUG = False
DEBUG_PATHS = True
DEBUG_LEG_POSITIONS = False
DEBUG_SERVO_ANGLES = False


class LegID(Enum):
    FRONT_RIGHT_LEG = "fr", True, True
    FRONT_LEFT_LEG = "fl", True, False
    BACK_RIGHT_LEG = "br", False, True
    BACK_LEFT_LEG = "bl", False, False

    def __new__(cls, leg_id: str, front: bool, right: bool):
        value = len(cls.__members__)
        obj = object.__new__(cls)
        obj._value_ = value
        return obj

    def __init__(self, leg_id: str, front: bool, right: bool) -> None:
        self._leg_id = leg_id
        self._front = front
        self._right = right
        self._same_side_leg = None
        self._opposite_side_leg = None
        self._opposite_leg = None

    def __repr__(self) -> str:
        return f"Leg({self.name.upper()})"

    def __str__(self) -> str:
        return f"{self.name.upper()}"

    @property
    def name(self) -> str: return self._leg_id

    @property
    def is_front(self) -> bool: return self._front

    @property
    def is_back(self) -> bool: return not self._front

    @property
    def is_right(self) -> bool: return self._right

    @property
    def is_left(self) -> bool: return not self._right

    @staticmethod
    def from_name(name: str) -> Optional['LegID']:
        for leg in LegID:
            if leg.name == name:
                return leg
        return None

    def leg_sign_xy(self) -> Tuple[float, float]:
        x_leg_sign = 1 if self.is_front else -1
        y_leg_sign = 1 if self.is_right else -1
        return x_leg_sign, y_leg_sign

    def same_side_leg(self) -> 'LegID':
        return self._same_side_leg

    def opposite_side_leg(self) -> 'LegID':
        return self._opposite_side_leg

    def opposite_leg(self) -> 'LegID':
        return self._opposite_leg

    def update_values_internal(self):
        if self == LegID.FRONT_RIGHT_LEG:
            self._same_side_leg = LegID.BACK_RIGHT_LEG
            self._opposite_side_leg = LegID.FRONT_LEFT_LEG
            self._opposite_leg = LegID.BACK_LEFT_LEG
        elif self == LegID.FRONT_LEFT_LEG:
            self._same_side_leg = LegID.BACK_LEFT_LEG
            self._opposite_side_leg = LegID.FRONT_RIGHT_LEG
            self._opposite_leg = LegID.BACK_RIGHT_LEG
        elif self == LegID.BACK_LEFT_LEG:
            self._same_side_leg = LegID.FRONT_LEFT_LEG
            self._opposite_side_leg = LegID.BACK_RIGHT_LEG
            self._opposite_leg = LegID.FRONT_RIGHT_LEG
        elif self == LegID.BACK_RIGHT_LEG:
            self._same_side_leg = LegID.FRONT_RIGHT_LEG
            self._opposite_side_leg = LegID.BACK_LEFT_LEG
            self._opposite_leg = LegID.FRONT_LEFT_LEG

    @staticmethod
    def finish_setup():
        for _leg_id in LegID:
            _leg_id.update_values_internal()
        if INITIAL_DEBUG:
            for _leg_id in LegID:
                print(f"    LegID: {_leg_id} - same/opposite/opposite_side {_leg_id.same_side_leg()}/{_leg_id.opposite_leg()}/{_leg_id.opposite_side_leg()}")


LegID.finish_setup()
temp_vector = Vector3()


class Body:
    def __init__(self,
                 leg_servos: LegServos,
                 servo_driver: ServoDriver,
                 thigh_length: int,
                 shin_length: int,
                 distance_x: int,
                 distance_y: int):

        self.thigh_length = thigh_length
        self.shin_length = shin_length
        self.distance_x = distance_x
        self.distance_y = distance_y
        self.half_distance_x = distance_x // 2
        self.half_distance_y = distance_y // 2

        self.servo_driver = servo_driver

        self.legs: Dict[LegID, Leg] = {
            leg_id: Leg(leg_id,
                        servo_driver,
                        leg_servos.servos[leg_id.name]["shoulder"],
                        leg_servos.servos[leg_id.name]["thigh"],
                        leg_servos.servos[leg_id.name]["shin"],
                        thigh_length, shin_length)
            for leg_id in LegID
        }
        self.body_position_path: Optional[Path] = None
        self.body_position = Vector3(z=7)
        self.three_leg_details: Dict[LegID, ThreeLegDetails] = {
            leg_id: ThreeLegDetails(self,
                                    self.legs[leg_id],
                                    self.legs[leg_id.same_side_leg()],
                                    self.legs[leg_id.opposite_side_leg()],
                                    self.legs[leg_id.opposite_leg()]) for leg_id in LegID
        }

        self.temp_vector = Vector3()

        self.body_position_paths: Dict[str, Path] = {
            "linear": LinearPath(),
            "arc": ArcPath()
        }

    def __iter__(self):
        return iter(self.legs.values())

    def __getitem__(self, item) -> 'Leg':
        if isinstance(item, LegID):
            return self.legs[item]

        return self.legs[LegID.from_name(str(item))]

    def drive(self, current_time: float) -> None:
        if self.body_position_path is not None:
            self.body_position_path.drive(current_time)

        for leg_id in self.legs:
            leg = self.legs[leg_id]
            leg.drive_leg(current_time, self.body_position)

    def set_body_position_path(self, path: Path, current_time: float, delta_time_or_speed: float) -> None:
        path.prepare(self.body_position, path.end, self.body_position, current_time, delta_time_or_speed)
        self.body_position_path = path
        if DEBUG_PATHS:
            print(f"    body <- {path}")

    def all_off(self) -> None:
        for leg in self.legs.values():
            leg.on = False
        self.servo_driver.global_off = True

    def all_on(self) -> None:
        for leg in self.legs.values():
            leg.on = True
        self.servo_driver.global_off = False

    def are_legs_at_destination(self, current_time: float) -> bool:
        for leg in self.legs.values():
            if not leg.at_destination(current_time):
                return False

        return True

    def is_body_at_destination(self, current_time: float) -> bool:
        if self.body_position_path is not None:
            return self.body_position_path.at_destination(current_time)
        return True

    def is_all_at_destination(self, current_time: float) -> bool:
        return self.is_body_at_destination(current_time) and self.are_legs_at_destination(current_time)

    def are_legs_over_position(self, current_time: float, relative_position: float) -> bool:
        """
        :param current_time: current time
        :param relative_position: value from 0.0 to 1.0
        :return:
        """
        for leg in self.legs.values():
            if leg.current_relative_position_in_path(current_time) < relative_position:
                return False

        return True

    def is_body_over_position(self, current_time: float, relative_position: float) -> bool:
        """
        :param current_time: current time
        :param relative_position: value from 0.0 to 1.0
        :return:
        """
        if self.body_position_path is not None:
            return self.body_position_path.current_relative_position_in_path(current_time) < relative_position

        return True

    def find_higher_legs(self, tolerance=absolute_tolerance) -> Tuple[List['Leg'], List['Leg']]:
        """
        This method returns list of legs in 'air' and list of legs on the ground (levelled)
        while allowing lower legs to all be grouped by z within tolerance.
        :return: legs in air, legs on ground
        """
        # TODO all this works for levelled body - need to add body orientation to this method
        height = 0
        ground_legs = []
        higher_legs = []
        for leg in self.legs.values():
            if leg.position.z > height:
                higher_legs.extend(ground_legs)
                height = leg.position.z
                del ground_legs[:]
                ground_legs.append(leg)
            elif math.isclose(height, leg.position.z, abs_tol=tolerance):
                ground_legs.append(leg)
            else:
                higher_legs.append(leg)

        return higher_legs, ground_legs

    def legs_on_same_positions(self) -> bool:
        compare_leg: Optional[Leg] = None
        for leg in self.legs.values():
            if compare_leg is None:
                compare_leg = leg
            elif (not math.isclose(compare_leg.position.x, leg.position.x, abs_tol=absolute_tolerance)
                  or not math.isclose(compare_leg.position.y, leg.position.y, abs_tol=absolute_tolerance)
                  or not math.isclose(compare_leg.position.z, leg.position.z, abs_tol=absolute_tolerance)):
                return False
        return True

    def body_linear_move(self, end_x_offset: float, end_y_offset: float, end_z_height: float, current_time: float, delta_time_or_speed: float, _power: int = 100) -> None:
        # self.temp_vector.set_values(end_x_offset, end_y_offset, end_z_height)
        linear_path = cast(LinearPath, self.body_position_paths["linear"])
        linear_path.end.x = end_x_offset
        linear_path.end.y = end_y_offset
        linear_path.end.z = end_z_height
        # linear_path.prepare(self.body_position, self.temp_vector, self.body_position, current_time, delta_time_or_speed)
        self.set_body_position_path(linear_path, current_time, delta_time_or_speed)

    def abs_leg_coordinates(self, leg_id: LegID, leg_pos: Vector3) -> Tuple[float, float, float]:
        x_leg_sign, y_leg_sign = leg_id.leg_sign_xy()
        return \
            leg_pos.x + self.half_distance_x * x_leg_sign, \
            leg_pos.y + self.half_distance_y * y_leg_sign, \
            leg_pos.z

    def abs_leg_coordinates_to_vec(self, leg_id: LegID, leg_pos: Vector3, destination: Vector3) -> Vector3:
        x_leg_sign, y_leg_sign = leg_id.leg_sign_xy()
        destination.x = leg_pos.x + self.half_distance_x * x_leg_sign
        destination.y = leg_pos.y + self.half_distance_y * y_leg_sign
        destination.z = leg_pos.z

        return destination

    def get_three_leg_details_at(self, for_leg: LegID, time_to_lift: float) -> 'ThreeLegDetails':
        three_leg_details = self.three_leg_details[for_leg]
        three_leg_details.update_leg_positions(time_to_lift)
        return three_leg_details

    def get_all_three_leg_details(self, for_time: Optional[float] = None) -> Dict[LegID, 'ThreeLegDetails']:
        all_three_leg_details = self.three_leg_details
        for three_leg_details in all_three_leg_details.values():
            three_leg_details.update_leg_positions(for_time)

        return all_three_leg_details


class Leg:
    def __init__(self,
                 leg_id: LegID,
                 servo_driver: ServoDriver,
                 shoulder_servo_definition: ServoDefinition,
                 thigh_servo_definition: ServoDefinition,
                 shin_servo_definition: ServoDefinition,
                 thigh_len: int,
                 shin_len: int) -> None:

        self.leg_id = leg_id
        self.servo_driver = servo_driver
        self.shoulder_servo_definition = shoulder_servo_definition
        self.thigh_servo_definition = thigh_servo_definition
        self.shin_servo_definition = shin_servo_definition
        self.thigh_len = thigh_len
        self.shin_len = shin_len

        self.requested_shoulder_angle = 0
        self.requested_thigh_angle = 0
        self.requested_shin_angle = 0

        self.on = True

        self.paths: Dict[str, Path] = {
            "linear": LinearPath(),
            "arc": ArcPath()
        }

        self.path: Optional[Path] = None

        self.position = Vector3()
        self.temp_vector = Vector3()
        self.requested_power: int = 0

        self.cc_x = 0
        self.cc_y = 0
        self.cc_r = 0

    def clear_path(self) -> None:
        self.path = None

    def set_path(self, path: Path, current_time: float, duration_or_speed: float, power: int = 100) -> None:
        # path.prepare(self.position, path.end, self.position, current_time, duration_or_speed)
        path.prepare(self.position, path.end, self.position, current_time, duration_or_speed)
        self.path = path
        self.requested_power = power
        if DEBUG_PATHS:
            print(f"    {self.leg_id} <- {path} @ {power}")

    def turn_servos_off(self) -> None:
        self.servo_driver.set_position(self.shoulder_servo_definition, 0)
        self.servo_driver.set_position(self.thigh_servo_definition, 0)
        self.servo_driver.set_position(self.shin_servo_definition, 0)

    def linear_move(self, end_x: float, end_y: float, end_z: float, current_time: float, delta_time_or_speed: float, power: int = 100) -> None:
        # self.temp_vector.set_values(end_x, end_y, end_z)

        lineal_path = cast(LinearPath, self.paths["linear"])
        lineal_path.end.x = end_x
        lineal_path.end.y = end_y
        lineal_path.end.z = end_z

        # lineal_path.prepare(self.position, self.temp_vector, self.position, current_time, delta_time_or_speed)
        self.set_path(lineal_path, current_time, delta_time_or_speed, power)

    def arc_move(self, arc_height: float, end_x: float, end_y: float, end_z: float, current_time: float, delta_time_or_speed: float, power: int = 100) -> None:
        # self.temp_vector.set_values(end_x, end_y, end_z)

        arc_path = cast(ArcPath, self.paths["arc"])
        arc_path.end.x = end_x
        arc_path.end.y = end_y
        arc_path.end.z = end_z
        # arc_path.prepare(self.position, self.temp_vector, self.position, current_time, delta_time_or_speed)
        arc_path.arc_height = arc_height
        self.set_path(arc_path, current_time, delta_time_or_speed, power)

    def at_destination(self, current_time: float) -> bool:
        if self.path is not None:
            return self.path.at_destination(current_time)
        return True

    def current_relative_position_in_path(self, current_time: float) -> float:
        return self.path.current_relative_position_in_path(current_time)

    def is_close(self, x: float, y: float, z: float) -> bool:
        return (math.isclose(x, self.position.x, abs_tol=absolute_tolerance)
                and math.isclose(y, self.position.y, abs_tol=absolute_tolerance)
                and math.isclose(z, self.position.z, abs_tol=absolute_tolerance))

    def drive_leg(self, current_time: float, body_position: Vector3) -> None:
        if self.on:
            if self.path is not None:
                if self.path.requested_arrival is not None:
                    # driving by given amount of time
                    self.path.drive(current_time)

                    self.move(self.position.x + body_position.x,
                              self.position.y + body_position.y,
                              self.position.z + body_position.z,
                              self.requested_power)
                else:
                    # driving by given speed
                    # TODO NOT IMPLEMENTED YET!
                    if self.path.current_position_in_path is None:
                        self.move(self.position.x + body_position.x,
                                  self.position.y + body_position.y,
                                  self.position.z + body_position.z,
                                  self.requested_power)
                        self.path.current_position_in_path = 0.0
                    else:
                        l1 = self.thigh_len
                        l2 = self.shin_len
                        thigh_angle = math.radians(self.requested_thigh_angle)
                        shin_angle = -math.radians(self.requested_shin_angle) - math.pi / 2.0

                        # new_thigh_angle = thigh_angle + math.radians(7200 / self.thigh_servo_definition.speed.value)
                        new_thigh_angle = thigh_angle

                        x1 = l1 * math.sin(new_thigh_angle)
                        y1 = 0
                        z1 = l1 * math.cos(new_thigh_angle)

                        x2 = l2 * math.sin(new_thigh_angle - shin_angle) + x1
                        z2 = l2 * math.cos(new_thigh_angle - shin_angle) + z1

                        # self.cc_x = x1
                        # self.cc_y = z1
                        # self.cc_r = l2

                        try:
                            x, y, z = self.path.calculate_point_with_distance(
                                x1 - body_position.x,
                                y1 - body_position.y,
                                z1 - body_position.z, l2)
                        except ValueError as e:

                            self.cc_x = self.path.end.x1
                            self.cc_y = self.path.end.y1
                            self.cc_r = l2

                            raise e from e

                        # self.move(x + body_position.x,
                        #           y + body_position.y,
                        #           z + body_position.z,
                        #           self.requested_power)
            else:
                self.move(self.position.x + body_position.x,
                          self.position.y + body_position.y,
                          self.position.z + body_position.z,
                          self.requested_power)

    def calculate_angles(self, x: float, y: float, z: float) -> Tuple[float, float, float]:
        l1 = self.thigh_len
        l2 = self.shin_len

        if math.isclose(y, 0.0, abs_tol=0.001):
            leg_length = z
            shoulder_angle = 0.0
        else:
            leg_length = math.sqrt(y * y + z * z)
            shoulder_angle = math.atan2(y, z)

        l3 = math.sqrt(x * x + leg_length * leg_length) if x != 0 else leg_length

        # Bodge for l3 never ever to be zero!
        if l3 == 0:
            l3 = 1

        # print(f"l1={l1:.2f}, l2={l2:.2f}, l3={l3:.2f}, x={x:.2f}, y={y:.2f}, z={z:.2f}, leg_length={leg_length:.2f}")
        t = (l3 * l3 + l1 * l1 - l2 * l2) / (2 * l3 * l1)

        alpha_prim = math.acos(t) if l3 <= l1 + l2 and t >= 0 else 0.0
        alpha_second = math.atan2(x, leg_length)
        thigh_angle = alpha_prim - alpha_second

        x1 = -l1 * math.sin(thigh_angle)
        y1 = l1 * math.cos(thigh_angle)

        beta_prim = math.atan2(x - x1, leg_length - y1)
        shin_angle = beta_prim + thigh_angle - math.pi / 2.0

        thigh_angle = -thigh_angle

        return math.degrees(shoulder_angle), math.degrees(thigh_angle), math.degrees(shin_angle)

    def move(self, x: float, y: float, z: float, power: int = 100) -> None:
        if DEBUG_LEG_POSITIONS:
            print(f"Leg {self.leg_id}: Setting leg position ({x:.2f}, {y:.2f}, {z:.2f}) @ {power}")

        shoulder_angle, thigh_angle, shin_angle = self.calculate_angles(x, y, z)

        if DEBUG_SERVO_ANGLES:
            print(f"Leg {self.leg_id}: Setting servo angles:"
                  f" shoulder({self.shoulder_servo_definition.servo_channel.value:.2f})={shoulder_angle:.2f}"
                  f" thigh({self.thigh_servo_definition.servo_channel.value:.2f})={thigh_angle:.2f}"
                  f" shin({self.shin_servo_definition.servo_channel.value:.2f})={shin_angle:.2f}"
                  f" @ {power}")
        self.requested_shoulder_angle = shoulder_angle
        self.requested_thigh_angle = thigh_angle
        self.requested_shin_angle = shin_angle
        self.servo_driver.set_angle(self.shoulder_servo_definition, shoulder_angle, power)
        self.servo_driver.set_angle(self.thigh_servo_definition, thigh_angle, power)
        self.servo_driver.set_angle(self.shin_servo_definition, shin_angle, power)
        if DEBUG_LEG_POSITIONS:
            l1 = self.thigh_len
            l2 = self.shin_len
            thigh_angle = math.radians(self.requested_thigh_angle) + math.pi / 2.0
            shin_angle = -math.radians(self.requested_shin_angle) - math.pi / 2.0

            print(f"Leg {self.leg_id}: Setting leg position ({x:.2f}, {y:.2f}, {z:.2f}) @ {power}")
            x1 = l1 * math.cos(thigh_angle)
            y1 = 0
            z1 = l1 * math.sin(thigh_angle)

            x2 = l2 * math.cos(thigh_angle - shin_angle) + x1
            y2 = 0
            z2 = l2 * math.sin(thigh_angle - shin_angle) + z1
            print(f"Leg {self.leg_id}: Recalculated leg position ({x2:.2f}, {y2:.2f}, {z2:.2f}) @ {power}")
            pass

    def leg_ground_angle(self, x: float, _y: float, z: float) -> float:
        # TODO - add 3D, 3rd dimension
        angle = math.atan2(z - self.position.z, x - self.position.x)

        return math.pi - angle if angle > math_pi_half else angle


class ThreeLegDetails:
    def __init__(self,
                 body: Body,
                 main_leg: Leg,
                 same_side_leg: Leg,
                 opposite_side_leg: Leg,
                 opposite_leg: Leg) -> None:
        self.body = body
        self.main_leg: Leg = main_leg
        self.same_side_leg: Leg = same_side_leg
        self.opposite_side_leg: Leg = opposite_side_leg
        self.opposite_leg = opposite_leg
        self.main_leg_abs_position = Vector3()
        self.same_side_leg_abs_position = Vector3()
        self.opposite_side_leg_abs_position = Vector3()
        self.time_to_lift: float = 0
        self._legs: List[Leg] = [self.main_leg, self.same_side_leg, self.opposite_side_leg]
        self._leg_positions: List[Vector3] = [
            self.main_leg_abs_position, self.same_side_leg_abs_position, self.opposite_side_leg_abs_position
        ]
        self.cog_x: float = 0
        self.cog_y: float = 0
        self.cog_distance_to_line: float = 0
        self.body_centre_distance_to_cog: float = 0
        self.triangle_side_lens: List[float] = [0.0, 0.0, 0.0]
        self.triangle_stability_factor: float = 0

    def __repr__(self) -> str:
        return self.__str__()

    def __str__(self) -> str:
        return f"TLD({self.main_leg.leg_id}={self.main_leg_abs_position}, {self.same_side_leg.leg_id}={self.same_side_leg_abs_position}, {self.opposite_side_leg.leg_id}={self.opposite_side_leg_abs_position}, " \
               f"cog=({self.cog_x:.1f}, {self.cog_y:.1f}), cog_d={self.cog_distance_to_line:.1f}, b_d={self.body_centre_distance_to_cog:.1f}, " \
               f"tl=({self.triangle_side_lens[0]:.1f}, {self.triangle_side_lens[1]:.1f}, {self.triangle_side_lens[2]:.1f}), " \
               f"stab={self.triangle_stability_factor:.1f})"

    def update_leg_positions(self, time_of_leg_lift: Optional[float] = None) -> 'ThreeLegDetails':
        if time_of_leg_lift is not None and time_of_leg_lift == time_of_leg_lift:
            return self

        centre_of_inscribed_circle_x = 0
        centre_of_inscribed_circle_y = 0

        for i in range(len(self._legs)):
            leg = self._legs[i]
            leg_position = self._leg_positions[i]
            if time_of_leg_lift is not None and leg.path is not None:
                path_factor = leg.path.current_relative_position_in_path(time_of_leg_lift)
                temp_vector.lerp_in(leg.path.start, leg.path.end, path_factor)
                self.body.abs_leg_coordinates_to_vec(leg.leg_id, temp_vector, leg_position)
            else:
                self.body.abs_leg_coordinates_to_vec(leg.leg_id, leg.position, leg_position)

            centre_of_inscribed_circle_x += leg_position.x
            centre_of_inscribed_circle_y += leg_position.y

        self.cog_x = centre_of_inscribed_circle_x / 3.0
        self.cog_y = centre_of_inscribed_circle_y / 3.0

        bx = self.body.body_position.x
        by = self.body.body_position.y

        self.body_centre_distance_to_cog = math.sqrt((bx - self.cog_x) * (bx - self.cog_x) + (by - self.cog_y) * (by - self.cog_y))

        x1 = self.same_side_leg_abs_position.x
        y1 = self.same_side_leg_abs_position.y
        x2 = self.opposite_side_leg_abs_position.x
        y2 = self.opposite_side_leg_abs_position.y

        self.cog_distance_to_line = abs((x2 - x1) * (y1 - self.cog_y) - (y2 - y1) * (x1 - self.cog_x)) / math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1))

        self.time_to_lift = time_of_leg_lift
        return self

    def calculate_triangle_stability_factor(self) -> float:
        def side_len(p1: Vector3, p2: Vector3) -> float:
            return math.sqrt((p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y))

        leg_positions = self._leg_positions
        self.triangle_side_lens[0] = side_len(leg_positions[0], leg_positions[1])
        self.triangle_side_lens[1] = side_len(leg_positions[1], leg_positions[2])
        self.triangle_side_lens[2] = side_len(leg_positions[2], leg_positions[0])

        self.triangle_side_lens.sort()

        self.triangle_stability_factor = self.triangle_side_lens[2] / (self.triangle_side_lens[1] + self.triangle_side_lens[0])
        return self.triangle_stability_factor

    def is_stable_for_this_leg(self, time_of_leg_lift: float, cog_affinity: float) -> bool:
        three_leg_details = self.body.get_three_leg_details_at(self.opposite_leg.leg_id, time_of_leg_lift)

        return three_leg_details.cog_distance_to_line * cog_affinity > three_leg_details.body_centre_distance_to_cog
