import traceback

import time
from typing import List

import pyros

from controllers.controller import Controller
from gait_orchestrator import GaitOrchestrator

DEBUG = True


class MoveController(Controller):
    def __init__(self, gait_orchestrator: GaitOrchestrator):
        super().__init__(gait_orchestrator)
        pyros.subscribe("move/+", self._handle_move_command)

    def _handle_move_command(self, _topic, message, groups) -> None:
        # print(f"Received Move event on {_topic}, msg='{message}', for {groups}")
        try:
            command = groups[0]
            args = message.split(" ")
            if command == "drive":
                self._handle_drive(args)
            elif command == "rotate":
                self._handle_rotate(args)
            elif command == "steer":
                self._handle_steer(args)
            elif command == "stop":
                self._handle_stop(args)
            elif command == "height":
                self._handle_height(args)
            else:
                print("Received unknown command " + command)
        except Exception as exception:
            print("ERROR: " + str(exception) + "\n" + ''.join(traceback.format_tb(exception.__traceback__)))

    def _handle_stop(self, args: List[str]) -> None:
        current_time = time.time()

        if DEBUG: print(f"Got stop (with parameters {args})")
        self.gait_orchestrator.stop(current_time)

    def _handle_drive(self, args: List[str]) -> None:
        if self.gait_orchestrator.is_ready:
            current_time = time.time()
            print(f"Got drive with parameters {args}")
            if len(args) == 1:
                angle = 0.0
                speed = int(float(args[0]))
            else:
                angle = float(args[0])
                speed = int(float(args[1]))

            self.gait_orchestrator.drive(current_time, speed, angle, None)
        else:
            print(f"Cannot handle 'drive' as gait manager is not initialised!")

    def _handle_steer(self, args: List[str]) -> None:
        if self.gait_orchestrator.is_ready:
            current_time = time.time()
            if DEBUG: print(f"Got steer with parameters {args}")
            if len(args) < 2:
                distance = 100000
                speed = int(float(args[0])) // 3
            else:
                distance = float(args[0])
                speed = int(float(args[1])) // 3

            self.gait_orchestrator.drive(current_time, speed, 0.0, distance)

        else:
            print(f"Cannot handle 'steer' as gait manager is not initialised!")

    def _handle_rotate(self, args: List[str]) -> None:
        if self.gait_orchestrator.is_ready:
            speed = int(args[0])
            current_time = time.time()
            if DEBUG: print(f"Got rotate with speed {speed}")

            angle = 0.0  # if speed > 0 else 180.0
            distance = 0.01 if speed > 0 else -0.01
            self.gait_orchestrator.drive(current_time, speed, angle, distance)
        else:
            print(f"Cannot handle 'rotate' as gait manager is not initialised!")

    def _handle_height(self, args: List[str]) -> None:
        if self.gait_orchestrator.is_ready:
            height = int(args[0])
            current_time = time.time()
            if DEBUG: print(f"Got height with height {height}")
            self.gait_orchestrator.change_value(current_time, "height", height)
        else:
            print(f"Cannot handle 'rotate' as gait manager is not initialised!")
