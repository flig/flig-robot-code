import time
from typing import Optional, Tuple, List, Callable

import pyros

from controllers.controller import Controller
from gait_orchestrator import GaitOrchestrator
from pcs9685ina3221_servo_lib import PCA9685INA3221Servos
from servo_lib.leg_servos import LegServos, ExtraServos


class ServoController(Controller):
    def __init__(self, gait_orchestrator: GaitOrchestrator, leg_servos: LegServos, extra_servos: ExtraServos) -> None:
        super().__init__(gait_orchestrator)
        self.leg_servos = leg_servos
        self.extra_servos = extra_servos
        self.servo_driver: Optional[PCA9685INA3221Servos] = None
        self._all_stop_callbacks: List[Callable[[], None]] = []
        self._soft_start_callbacks: List[Callable[[], None]] = []

        pyros.subscribe("servo/control", self._handle_servo_control)

        pyros.subscribe("servo/+/+/angle", self._handle_leg_servo_angle_commands)
        pyros.subscribe("servo/+/angle", self._handle_servo_angle_commands)
        pyros.subscribe("servo/+/+/pos", self._handle_leg_servo_pos_commands)
        pyros.subscribe("servo/+/pos", self._handle_servo_pos_commands)
        pyros.subscribe("servo/+/data", self._handle_servo_data_commands)

    def soft_start(self) -> None:
        if self.servo_driver is not None:
            print(f"Gait Service: enabling servos")
            self.servo_driver.stop_all()
            self.servo_driver.global_off = False

    def all_stop(self) -> None:
        if self.servo_driver is not None:
            if not self.servo_driver.global_off:
                print(f"Gait Service: ALL STOP!")
                self.servo_driver.global_off = True
                self.servo_driver.stop_all()
                if self.gait_orchestrator.is_ready:
                    self.gait_orchestrator.all_stop(time.time())

    def _handle_servo_control(self, _topic, payload) -> None:
        print(f"Got servo control message {payload}")
        if payload == "stop":
            self.all_stop()
        elif payload == "start":
            self.soft_start()
        else:
            print(f"Unknown servo control message {payload}")

    @staticmethod
    def _read_angle_and_power(message) -> Tuple[float, int]:
        if ',' in message:
            split_payload = message.split(",")
            return float(split_payload[0]), int(split_payload[1])
        return float(message), 100

    @staticmethod
    def _read_position_and_power(message) -> Tuple[int, int]:
        if ',' in message:
            split_payload = message.split(",")
            return int(split_payload[0]), int(split_payload[1])
        return int(message), 100

    def _handle_leg_servo_angle_commands(self, _topic: str, payload: str, groups: List[str]) -> None:
        if self.servo_driver is not None:
            try:
                print(f"Got leg servo angle request for {groups} angle {payload}")
                angle, power = self._read_angle_and_power(payload)
                leg = groups[0]
                part = groups[1]
                if leg == "extra":
                    self.servo_driver.set_angle(self.extra_servos.servos[leg][part], angle, power)
                else:
                    self.servo_driver.set_angle(self.leg_servos.servos[leg][part], angle, power)
            except Exception as e:
                print(f"Failed leg servo angle request {e}")
        else:
            print(f"Cannot handle leg servo angle commands as servo driver is not initialised!")

    def _handle_leg_servo_pos_commands(self, _topic: str, payload: str, groups: List[str]) -> None:
        if self.servo_driver is not None:
            try:
                print(f"Got leg servo pos request for {groups} angle {payload}")
                servo_position, power = self._read_position_and_power(payload)
                leg = groups[0]
                part = groups[1]
                if leg == "extra":
                    self.servo_driver.set_position(self.extra_servos.servos[leg][part], servo_position, power)
                else:
                    self.servo_driver.set_position(self.leg_servos.servos[leg][part], servo_position, power)
            except Exception as e:
                print(f"Failed leg servo pos request {e}")
        else:
            print(f"Cannot handle leg servo position commands as servo driver is not initialised!")

    def _handle_servo_angle_commands(self, _topic: str, payload: str, groups: List[str]) -> None:
        if self.servo_driver is not None:
            try:
                print(f"Got servo angle request for {groups} angle {payload}")
                angle, power = self._read_angle_and_power(payload)
                servo_channel = int(groups[0])
                servo_definition = self.leg_servos.get_servo_definition_for_channel(servo_channel)
                if servo_definition is None:
                    servo_definition = self.extra_servos.get_servo_definition_for_channel(servo_channel)
                self.servo_driver.set_angle(servo_definition, angle, power)
            except Exception as e:
                print(f"Failed leg servo angle request {e}")
        else:
            print(f"Cannot handle servo angle commands as servo driver is not initialised!")

    def _handle_servo_pos_commands(self, _topic: str, payload: str, groups: List[str]) -> None:
        if self.servo_driver is not None:
            try:
                print(f"Got servo pos request for {groups} position {payload}")
                servo_position, power = self._read_position_and_power(payload)
                servo_channel = int(groups[0])
                servo_definition = self.leg_servos.get_servo_definition_for_channel(servo_channel)
                if servo_definition is None:
                    servo_definition = self.extra_servos.get_servo_definition_for_channel(servo_channel)
                self.servo_driver.set_position(servo_definition, servo_position, power)
            except Exception as e:
                print(f"Failed servo pos request {e}")
        else:
            print(f"Cannot handle servo position commands as servo driver is not initialised!")

    def _handle_servo_data_commands(self, _topic: str, _payload: str, groups: List[str]) -> None:
        try:
            print(f"Got servo data request for {groups}:")
            servo_channel = int(groups[0])
            servo_definition = self.leg_servos.get_servo_definition_for_channel(servo_channel)
            if servo_definition is not None:
                for prop in LegServos.SERVO_DEFINITION_FIELDS:
                    print(f"  {getattr(servo_definition, prop)}")
            else:
                servo_definition = self.extra_servos.get_servo_definition_for_channel(servo_channel)
                if servo_definition is not None:
                    for prop in ExtraServos.SERVO_DEFINITION_FIELDS:
                        print(f"  {getattr(servo_definition, prop)}")
                else:
                    print(f"  ERROR - cannot find servo for channel {servo_channel}")
        except Exception as e:
            print(f"Failed servo data request {e}")
