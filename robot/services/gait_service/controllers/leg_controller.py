import time
from typing import Optional, List

import pyros

from body import Body, LegID
from controllers.controller import Controller
from controllers.servo_conrtoller import ServoController
from gait_orchestrator import GaitOrchestrator


class LegController(Controller):
    def __init__(self, gait_orchestrator: GaitOrchestrator, servo_controller: ServoController):
        super().__init__(gait_orchestrator)
        self.servo_controller = servo_controller
        self.body: Optional[Body] = None
        self._drive_legs = False

        pyros.subscribe("leg/control", self._handle_leg_control)
        pyros.subscribe("leg/+", self._handle_leg_commands)

    @property
    def drive_legs(self) -> bool:
        return self._drive_legs

    @drive_legs.setter
    def drive_legs(self, new_drive_legs_value: bool) -> None:
        self._drive_legs = new_drive_legs_value

    def _handle_leg_control(self, _topic, payload) -> None:
        print(f"Got leg control message {payload}")
        if payload == "stop":
            self.servo_controller.all_stop()
            self.drive_legs = False
        elif payload == "start":
            self.servo_controller.soft_start()
            self.drive_legs = True
        elif payload == "display":
            if self.body is not None:
                for leg in self.body.legs.values():
                    print(f"    * leg {leg.leg_id.name} = {leg.position.x:.2f}, {leg.position.y:.2f}, {leg.position.z:.2f}")
        else:
            print(f"Unknown leg control message {payload}")

    def _handle_leg_commands(self, _topic: str, payload: str, groups: List[str]) -> None:
        if self.body is not None:
            try:
                print(f"Got leg request for {groups}: {payload}")

                split_payload = payload.split(",")
                x = float(split_payload[0])
                y = float(split_payload[1])
                z = float(split_payload[2])

                delta_time = float(split_payload[3]) if len(split_payload) > 3 else 0.0
                power = int(split_payload[4]) if len(split_payload) > 4 else 100

                print(f"    requested leg position ({x}, {y}, {z}) for {delta_time}s at {power}% power")
                leg_id_str = groups[0]
                if leg_id_str == "*":
                    for leg_id in self.body.legs:
                        leg = self.body.legs[leg_id]
                        leg.linear_move(x, y, z, time.time(), delta_time, power)
                else:
                    leg = self.body.legs[LegID.from_name(leg_id_str)]
                    leg.linear_move(x, y, z, time.time(), delta_time, power)

            except Exception as e:
                print(f"Failed leg request {e}")
        else:
            print(f"Failed leg request - body not initialised yet")
