from enum import Enum

import time

import math
from typing import Dict, Optional

import pyros

from controllers.controller import Controller
from controllers.input_state import InputState
from controllers.servo_conrtoller import ServoController
from gait_orchestrator import GaitOrchestrator

DEBUG_RECEIVED_DATA = False
DEBUG_BUTTON_STATES = False
DEBUG_LOGIC = False

DEFAULT_FLOAT_TOLERANCE = None
EXPO = 0.35
MAX_STOPPING = 2


button_names = [
    "select", "lbutton", "rbutton", "start",
    "lup", "lright", "ldown", "lleft",
    "tl1", "tr1", "tl2", "tr2",
    "bx", "by", "ba", "bb",
    "cross", "circle", "triangle", "square",
    "c",
    "tl", "tr", "tl2", "tr2",
    "share", "options", "ps",
    "lfire", "rfire"
]


class Modes(Enum):
    NONE = 0
    NORMAL = 1
    MENU = 2


class JoystickController(Controller):
    def __init__(self, gait_orchestrator: GaitOrchestrator, servo_controller: ServoController):
        super().__init__(gait_orchestrator)
        self.servo_controller = servo_controller

        self.axis_states: Dict[str, InputState[float]] = {
            "x": InputState[float]("x", 0.0, float_tolerance=DEFAULT_FLOAT_TOLERANCE),
            "y": InputState[float]("y", 0.0, float_tolerance=DEFAULT_FLOAT_TOLERANCE),
            "z": InputState[float]("z", 0.0, float_tolerance=DEFAULT_FLOAT_TOLERANCE),
            "rx": InputState[float]("rx", 0.0, float_tolerance=DEFAULT_FLOAT_TOLERANCE),
            "ry": InputState[float]("ry", 0.0, float_tolerance=DEFAULT_FLOAT_TOLERANCE),
            "rz": InputState[float]("rz", 0.0, float_tolerance=DEFAULT_FLOAT_TOLERANCE)
        }
        self.button_states: Dict[str, InputState[int]] = {
            "x3": InputState[int]("x3", 0), "y3": InputState[int]("y3", 0)
        }
        for button_name in button_names:
            self.button_states[button_name] = InputState[int](button_name, 0)

        pyros.subscribe("joystick/changes", self._handle_joystick_changes)

        self.mode = Modes.NORMAL

        self.speed_index = 8
        self.speeds = [5, 10, 15, 20, 25, 30, 40, 50, 60, 75, 100, 150, 300]
        self.top_speed = self.speeds[self.speed_index]

    def _handle_joystick_changes(self, _topic, payload) -> None:
        pairs = payload.split(",")
        axes_changed = False
        buttons_changed = False
        for pair in pairs:
            kv = pair.split(":")
            if len(kv) > 1:
                k, v = kv
                if k in self.axis_states:
                    self.axis_states[k].value = float(v)
                    axes_changed = True
                elif k in self.button_states:
                    if "." in v:
                        self.button_states[k].value = int(float(v))
                    else:
                        self.button_states[k].value = int(v)
                    buttons_changed = True
                elif "." in v:
                    self.axis_states[k] = InputState(k, float(v))
                    axes_changed = True
                else:
                    self.button_states[k] = InputState(k, v)
                    buttons_changed = True

        current_time = time.time()

        if axes_changed:
            self._process_axes(current_time)
        if buttons_changed:
            self._process_buttons(current_time)

        for axis in self.axis_states.values():
            axis.process()
        for button in self.button_states.values():
            button.process()

        if DEBUG_BUTTON_STATES:
            print(f"Axes {self.axis_states}")
            print(f"Buttons {self.button_states}")

    def calc_rover_speed(self, speed: float) -> float:
        spd = int(speed * self.top_speed)
        spd = 300 if spd > 300 else (-300 if spd < -300 else spd)

        return spd

    @staticmethod
    def calculate_expo(v: float, expo_percentage: float) -> float:
        return v * v * expo_percentage + v * (1.0 - expo_percentage) if v >= 0 else - v * v * expo_percentage + v * (1.0 - expo_percentage)

    @staticmethod
    def calc_rover_distance(distance):
        # Max: 1.2 * 500 = 600
        return int((1.0 - abs(distance) + 0.05) * 500 * (1 if distance > 0 else -1))

    def _process_axes(self, current_time: float) -> None:
        if DEBUG_RECEIVED_DATA:
            changed = [f"{state.name}:{state.value}" for state in self.axis_states.values() if state.changed]
            print(f"    Received new axes data {changed}")

        lx = float(self.axis_states["x"].value)
        ly = float(self.axis_states["y"].value)

        rx = float(self.axis_states["rx"].value)
        ry = float(self.axis_states["ry"].value)

        ld = math.sqrt(lx * lx + ly * ly)
        rd = math.sqrt(rx * rx + ry * ry)
        ra = math.degrees(math.atan2(rx, -ry))

        if ld < 0.1 < rd:
            distance = self.calculate_expo(rd, EXPO)
            self.rover_speed = self.calc_rover_speed(distance)

            if DEBUG_LOGIC: print(f"    driving a:{round(ra, 1)} s:{self.rover_speed} ld:{ld} rd:{rd}")
            self.gait_orchestrator.drive(current_time, int(self.rover_speed), ra, None, top_speed=self.top_speed)

        elif ld > 0.1 and rd > 0.1:
            ory = ry
            olx = lx
            ry = self.calculate_expo(ry, EXPO)
            lx = self.calculate_expo(lx, EXPO)

            self.rover_speed = -self.calc_rover_speed(ry) * 1.3
            self.rover_turning_distance = self.calc_rover_distance(lx)
            if DEBUG_LOGIC: print(f"    steering d:{self.rover_turning_distance} s:{self.rover_speed} ry: {ory} lx:{olx} ld:{ld} rd:{rd}")
            self.gait_orchestrator.drive(current_time, int(self.rover_speed), ra, self.rover_turning_distance, self.top_speed)

        elif ld > 0.1:
            olx = lx
            lx = self.calculate_expo(lx, EXPO) / 2
            self.rover_speed = self.calc_rover_speed(lx) * 1.3
            if DEBUG_LOGIC: print(f"    rotate s: {self.rover_speed}, lx: {olx}, ld: {ld}, rd: {rd}")
            angle = 0.0  # if self.rover_speed > 0 else 180.0
            distance = 0.01 if self.rover_speed > 0 else -0.01

            self.gait_orchestrator.drive(current_time, int(self.rover_speed), angle, distance, top_speed=self.top_speed)

        else:
            if not self.gait_orchestrator.already_stopped:
                self.gait_orchestrator.stop(current_time)
                if DEBUG_LOGIC: print(f"    stop: ld: {ld}, rd: {rd}")

        if self.axis_states["rz"].changed:
            z = self.axis_states["rz"].value
            angle = (z - 1) * 90 / 2
            self.servo_controller.servo_driver.set_angle(self.servo_controller.extra_servos.servos["extra"]["2"], angle)

    def _process_buttons(self, current_time: float) -> None:
        if DEBUG_RECEIVED_DATA:
            changed = [f"{state.name}:{state.value}" for state in self.button_states.values() if state.changed]
            print(f"    Received new button data {changed}")

        new_height: Optional[int] = None
        new_leg_raise: Optional[int] = None
        new_cy: Optional[int] = None
        new_ox: Optional[int] = None
        new_oy: Optional[int] = None
        new_speed: Optional[float] = None
        if self.button_states["x3"].changed:
            x3 = self.button_states["x3"].value
            if self.button_states["tl"].selected:
                if x3 > 0:
                    new_cy = 5
                    if DEBUG_LOGIC: print(f"    changed cy for {new_cy}")
                elif x3 < 0:
                    new_cy = -5
                    if DEBUG_LOGIC: print(f"    changed cy for {new_cy}")
            elif self.button_states["tr"].selected:
                # if x3 > 0:
                #     new_oy = -5
                #     if DEBUG_LOGIC: print(f"    changed oy for {new_oy}")
                # elif x3 < 0:
                #     new_oy = 5
                #     if DEBUG_LOGIC: print(f"    changed oy for {new_oy}")
                if x3 > 0:
                    new_speed = -0.1
                    if DEBUG_LOGIC: print(f"    changed speed for {new_speed}")
                elif x3 < 0:
                    new_speed = 0.1
                    if DEBUG_LOGIC: print(f"    changed speed for {new_speed}")
            else:
                if x3 > 0:
                    if self.speed_index < len(self.speeds):
                        self.speed_index += 1
                        self.top_speed = self.speeds[self.speed_index]
                        if DEBUG_LOGIC: print(f"    changed top speed {self.top_speed}, {self.speed_index}")
                elif x3 < 0:
                    if self.speed_index > 0:
                        self.speed_index -= 1
                        self.top_speed = self.speeds[self.speed_index]
                        if DEBUG_LOGIC: print(f"    changed top speed {self.top_speed}, {self.speed_index}")
        if self.button_states["y3"].changed:
            y3 = self.button_states["y3"].value
            if self.button_states["tl"].selected:
                if y3 > 0:
                    new_leg_raise = -1
                    if DEBUG_LOGIC: print(f"    changed leg_raise for {new_leg_raise}")
                elif y3 < 0:
                    new_leg_raise = 1
                    if DEBUG_LOGIC: print(f"    changed leg_raise for {new_leg_raise}")
            elif self.button_states["tr"].selected:
                if y3 > 0:
                    new_ox = 5
                    if DEBUG_LOGIC: print(f"    changed ox for {new_ox}")
                elif y3 < 0:
                    new_ox = -5
                    if DEBUG_LOGIC: print(f"    changed ox for {new_ox}")
            else:
                if y3 > 0:
                    new_height = -5
                    if DEBUG_LOGIC: print(f"    changed height for {new_height}")
                elif y3 < 0:
                    new_height = 5
                    if DEBUG_LOGIC: print(f"    changed height {new_height}")

        if self.button_states["tl"].selected:
            if self.button_states["cross"].just_released:
                self.gait_orchestrator.down_gait(current_time)
            elif self.button_states["triangle"].just_released:
                self.gait_orchestrator.up_gait(current_time)
            elif self.button_states["square"].just_released:
                self.gait_orchestrator.previous_gait(current_time)
            elif self.button_states["circle"].just_released:
                self.gait_orchestrator.next_gait(current_time)

        if new_height is not None:
            self.gait_orchestrator.change_value(current_time, "height", relative=new_height)

        if new_leg_raise is not None:
            self.gait_orchestrator.change_value(current_time, "leg_raise", relative=new_leg_raise)

        if new_cy is not None:
            self.gait_orchestrator.change_value(current_time, "cy", relative=new_cy)

        if new_ox is not None:
            self.gait_orchestrator.change_value(current_time, "ox", relative=new_ox)

        if new_oy is not None:
            self.gait_orchestrator.change_value(current_time, "oy", relative=new_oy)

        if new_speed is not None:
            self.gait_orchestrator.change_value(current_time, "speed", relative=new_speed)

        if self.button_states["ps"].just_selected:
            print("Got CHALLENGE button")
            pyros.publish("piwars/challenge", "start")
