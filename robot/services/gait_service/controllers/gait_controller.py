import traceback

import time
from typing import List

import pyros

from controllers.controller import Controller
from controllers.servo_conrtoller import ServoController
from gait_orchestrator import GaitOrchestrator


class GaitController(Controller):
    def __init__(self, gait_orchestrator: GaitOrchestrator, servo_controller: ServoController):
        super().__init__(gait_orchestrator)
        self.servo_controller = servo_controller
        pyros.subscribe("gait/control", self._handle_gait_control)
        pyros.subscribe("gait/invoke/+", self._handle_gait_invoke)

    def _handle_gait_control(self, _topic: str, payload: str) -> None:
        print(f"Got gait control message {payload}")
        if payload == "stop":
            self.servo_controller.all_stop()
            self.gait_orchestrator.drive_gait = False
        elif payload == "start":
            if self.gait_orchestrator.gait_manager.stepping:
                self.gait_orchestrator.gait_manager.stepping = False
            else:
                self.servo_controller.soft_start()
                self.gait_orchestrator.drive_gait = True
                self.gait_orchestrator.debug_gait = False
        elif payload == "continue":
            if self.gait_orchestrator.gait_manager.stepping:
                self.gait_orchestrator.gait_manager.stepping = False
            else:
                self.servo_controller.soft_start()
                self.gait_orchestrator.drive_gait = True
                self.gait_orchestrator.debug_gait = False
        elif payload == "debug":
            self.gait_orchestrator.gait_manager.stepping = False
            self.drive_gait = False
            self.gait_orchestrator.debug_gait = True
        elif payload == "step":
            if not self.gait_orchestrator.debug_gait and not self.gait_orchestrator.gait_manager.stepping:
                self.gait_orchestrator.gait_manager.stepping = True
            if self.gait_orchestrator.gait_manager.stepping:
                self.gait_orchestrator.gait_manager.step_gate.trigger()

            current_time = time.time()
            if self.gait_orchestrator.debug_gait:
                if self.gait_orchestrator.is_ready:
                    self.gait_orchestrator.gait_manager.process(current_time)
                else:
                    print(f"Cannot debug stepping through gait as gait manager is not initialised!")
        else:
            print(f"Unknown gait control message {payload}")

    def _handle_gait_invoke(self, _topic: str, payload: str, groups: List[str]) -> None:
        if self.gait_orchestrator.is_ready:
            try:
                gait_name = groups[0]
                print(f"Got invoke gait {gait_name} with '{payload}'")

                parameters = payload.split(',')
                if len(parameters) > 0 and parameters[0] == "-":
                    context = self.gait_orchestrator.gait_manager.current_gait_context
                else:
                    context = {kv[0]: kv[1] for kv in [kv.split("=") for kv in parameters] if len(kv) > 1}

                gait = self.gait_orchestrator.gait_manager.select_gait(gait_name, time.time(), context)
                if gait is not None:
                    print(f"  selected and started gait '{gait_name}' with context '{context}'")
                else:
                    print(f"  ** ERROR - no gait {gait_name}")
            except Exception as e:
                print(f"Error processing gait {e}\n{''.join(traceback.format_tb(e.__traceback__))}")
        else:
            print(f"Error processing gait - gait manager is not initialised!")
