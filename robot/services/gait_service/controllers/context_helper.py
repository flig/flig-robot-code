from typing import Optional


class ContextProcessingHelper:
    def __init__(self, context: dict, update: bool) -> None:
        self.context = context
        self.update = update
        self.changed = False

    def process_float(self, context_key: str, old_value: float, default_value: Optional[float]) -> float:
        new_value = self.context[context_key] if self.context is not None and context_key in self.context else old_value if self.update else default_value
        new_value = float(new_value) if new_value is not None else new_value
        self.changed = self.changed or new_value != old_value
        return new_value

    def process_int(self, context_key: str, old_value: int, default_value: Optional[int]) -> int:
        new_value = self.context[context_key] if self.context is not None and context_key in self.context else old_value if self.update else default_value
        new_value = int(new_value) if new_value is not None else new_value
        self.changed = self.changed or new_value != old_value
        return new_value

    def is_changed(self) -> bool:
        return self.changed

    def __enter__(self) -> 'ContextProcessingHelper':
        return self

    def __exit__(self, exc_type, exc_val, exc_tb) -> bool:
        return False
