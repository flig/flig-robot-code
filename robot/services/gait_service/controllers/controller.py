from abc import ABC
from typing import Optional

from gait_orchestrator import GaitOrchestrator


class Controller(ABC):
    def __init__(self, gait_orchestrator: GaitOrchestrator):
        self.gait_orchestrator = gait_orchestrator
