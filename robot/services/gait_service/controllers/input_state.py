import math
from typing import Optional, TypeVar, Generic

T = TypeVar('T')


class InputState(Generic[T]):
    def __init__(self, name: str, initial_value: Optional[T] = None, float_tolerance: Optional[float] = None):
        self.name = name
        self._value: Optional[T] = initial_value
        self._last_value: Optional[T] = initial_value
        self._changed = False
        self._timer = 0
        self._present = False
        self._float_tolerance = float_tolerance

    @property
    def value(self) -> T:
        return self._value

    @value.setter
    def value(self, new_value: T) -> None:
        if self._float_tolerance is not None:
            self._changed = not math.isclose(abs(new_value), abs(self._value), abs_tol=self._float_tolerance)
        else:
            self._changed = new_value != self._value

        if self._changed:
            self._last_value = self._value
            self._value = new_value
        self._timer = 0

    @property
    def present(self) -> bool:
        return self._present

    @present.setter
    def present(self, new_present_value: bool) -> None:
        self._present = new_present_value

    @property
    def changed(self) -> bool:
        return self._changed

    def process(self) -> None:
        self._changed = False
        self._last_value = self._value
        self._timer += 1

    @property
    def selected(self) -> bool:
        return self._value != 0

    @property
    def released(self) -> bool:
        return self._value == 0

    @property
    def just_selected(self) -> bool:
        return self._value == 1 and self._value != self._last_value

    @property
    def just_released(self) -> bool:
        return self._value == 0 and self._value != self._last_value

    @property
    def long_released(self) -> bool:
        return self.just_released and self._timer > 10

    def __repr__(self) -> str:
        return f"ButtonState[{self._value}/{self._last_value}, {self._changed}, {self._timer}]"

    def __str__(self) -> str:
        return self.__repr__()