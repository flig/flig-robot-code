import math
from enum import Enum, auto
from typing import Optional

from gait.gait import GaitManager, INITIAL_SETUP_GAIT_NAME

DEBUG_OPERATIONS = True

MAX_TWIST_ANGLE = math.pi / 8 + math.pi / 16
MAX_DISTANCE = 30


class GaitLayer(Enum):
    STAND = auto()
    WALK = auto()
    SIT = auto()
    LAY = auto()


class GaitOrchestrator:
    def __init__(self):
        self.gait_manager: Optional[GaitManager] = None

        self._drive_gait = False
        self._debug_gait = False

        self.already_stopped = True

        self.drive_gaits_contexts = {
            "trot": {
                "speed": 0.5,
                "height": 86,
                "stride": 10,
                "leg_raise": 2,
                "angle": 0,
                "distance": None,
                "cx": 0,
                "cy": 10,
                "ox": 0,
                "oy": 0
            },
            "cat_walk_dp4": {
                "ignore_stride": True,
                "speed": 1.6,
                "height": 80,
                "stride": 75,
                "leg_raise": 30,
                "angle": 0,
                "distance": None,
                "cog_a": 0.7,
                "cx": 0,
                "cy": 20,
                "ox": -15,
                "oy": 0
            },
            # "cat_walk_sp4": {
            #     "speed": 1,
            #     "height": 86,
            #     "stride": 60,
            #     "leg_raise": 15,
            #     "angle": 0,
            #     "distance": None,
            #     "tx": 0.5, "ty": 0.5,
            #     "cx": 0, "cy": 0, "ox": -15,
            #     "oy": 0
            # },
            # "cat_walk_sp6": {
            #     "speed": 1,
            #     "height": 86,
            #     "stride": 60,
            #     "leg_raise": 15,
            #     "angle": 0,
            #     "distance": None,
            #     "cx": 0, "cy": 0, "ox": 0,
            #     "oy": 0
            # },
            "stand": {
                "height": 95,
                "cx": 10,
                "cy": 10,
                "ox": 0,
                "oy": 0,
                "cog_a": 0.9,
                "cog_speed": -0.3
            },
            "stand_up": {
                "height": 95,
                "cx": 0,
                "cy": 10,
                "ox": 0,
                "oy": 0
            },
            "delayed_stand": {
                "delay": 3,
                "height": 95
            },
            "delayed_stand_up": {
                "delay": 3,
                "height": 95
            },
            "sit-back": {
                "override_name": "sit",
                "direction": 1,
                "cy": 10,
                "height": 50
            },
            "sit-forward": {
                "override_name": "sit",
                "direction": -1,
                "fz": 70,
                "height": 27,
                "cy": 10
            },
            "lay": {

            }
        }

        self._gait_lists = {
            GaitLayer.STAND: ["stand"],
            GaitLayer.WALK: ["trot", "cat_walk_dp4"],  #, "cat_walk_sp4", "cat_walk_sp6"],
            GaitLayer.SIT: ["sit-back", "sit-forward"],
            GaitLayer.LAY: ["lay"]
        }
        self._gait_list_indexes = {
            GaitLayer.STAND: 0,
            GaitLayer.WALK: 0,
            GaitLayer.SIT: 0,
            GaitLayer.LAY: 0
        }

        self.gait_layer_selected_gait = {GaitLayer.STAND: "stand", GaitLayer.WALK: "trot", GaitLayer.SIT: "sit-back", GaitLayer.LAY: "lay"}
        self.gait_layer = GaitLayer.WALK
        self.standing_gait = "stand"
        self.delayed_standing_gait = "delayed_stand"

        self.max_twist = math.degrees(MAX_TWIST_ANGLE)
        self.max_distance = MAX_DISTANCE

    @property
    def drive_gait(self) -> bool:
        return self._drive_gait

    @drive_gait.setter
    def drive_gait(self, new_drive_gait_value) -> None:
        self._drive_gait = new_drive_gait_value

    @property
    def debug_gait(self) -> bool:
        return self._debug_gait

    @debug_gait.setter
    def debug_gait(self, new_debug_gait_value) -> None:
        self._debug_gait = new_debug_gait_value

    @property
    def is_ready(self) -> bool:
        return self.gait_manager is not None

    @property
    def selected_gait(self) -> str:
        return self.gait_layer_selected_gait[self.gait_layer]

    @property
    def selected_gait_name(self) -> str:
        name = self.selected_gait
        if "override_name" in self.drive_gaits_contexts[name]:
            name = self.drive_gaits_contexts[name]["override_name"]
        return name

    @selected_gait.setter
    def selected_gait(self, new_selected_gait: str):
        if new_selected_gait in self.drive_gaits_contexts:
            self.gait_layer_selected_gait[self.gait_layer] = new_selected_gait
        else:
            raise ValueError(f"Unknown gait {new_selected_gait}")

    def all_stop(self, current_time: float) -> None:
        self.gait_manager.all_stop(current_time)

    def process(self, current_time: float) -> None:
        pass

    def drive(self, current_time: float, speed: int, angle: float, distance: Optional[float], top_speed: Optional[float] = None) -> None:
        def speed_to_deg(max_twist: float, spd: float) -> float:
            if spd > top_speed: spd = top_speed
            if spd < -top_speed: spd = -top_speed
            if spd > 0:
                return (top_speed - spd) * max_twist / top_speed - max_twist
            elif spd < 0:
                return max_twist - (top_speed + spd) * max_twist / top_speed
            else:
                return max_twist

        def distance_to_deg(max_twist: float, d: float) -> float:
            # return int((1.0 - abs(distance) + 0.2) * 500 * (1 if distance > 0 else -1))

            if d > 500: d = 500
            if d < -500: d = -500
            if d == 0:
                return max_twist
            elif d > 0:
                return max_twist - d * max_twist / 500

            return max_twist - d * max_twist / 500

        if self.gait_manager is not None:

            context = self.drive_gaits_contexts[self.selected_gait]
            if self.gait_layer == GaitLayer.STAND:
                if top_speed is not None:
                    if distance is not None:
                        if distance < 0.11:
                            context["twist"] = speed_to_deg(self.max_twist, speed)
                            context["angle"] = angle
                            context["distance"] = 0.01
                            context["stride"] = int(top_speed) // 3
                            context["x"] = 0
                            context["y"] = 0
                        else:
                            context["twist"] = distance_to_deg(self.max_twist, distance)
                            context["angle"] = angle
                            context["distance"] = 0.01
                            context["stride"] = int(top_speed) // 3

                            calculated_distance = self.max_distance * speed / top_speed
                            context["x"] = -calculated_distance * math.cos(math.radians(angle))
                            context["y"] = calculated_distance * math.sin(math.radians(angle))
                    else:
                        context["twist"] = 0
                        context["angle"] = 0
                        context["distance"] = None
                        context["stride"] = int(top_speed) // 3
                        calculated_distance = self.max_distance * speed / top_speed
                        context["x"] = -calculated_distance * math.cos(math.radians(-angle))
                        context["y"] = calculated_distance * math.sin(math.radians(-angle))
                else:
                    context["twist"] = 0
                    context["angle"] = angle
                    context["distance"] = distance
                    if "ignore_stride" not in context or not context["ignore_stride"]:
                        context["stride"] = int(speed) // 3

                    context["x"] = 0
                    context["y"] = 0
            else:
                context["angle"] = angle
                context["distance"] = distance
                if "ignore_stride" not in context or not context["ignore_stride"]:
                    context["stride"] = int(abs(speed)) // 3

            if self.gait_manager.current_gait_name != self.selected_gait_name:
                self.gait_manager.select_gait(self.selected_gait_name, current_time, context)
                if DEBUG_OPERATIONS: print(f"*** Drive: s={speed}, a={angle}, d={distance}, selected current_gait={self.gait_manager.current_gait_name}")
            else:
                current_gait = self.gait_manager.current_gait
                current_gait.update(current_time, context)
                if DEBUG_OPERATIONS: print(f"*** Drive: s={speed}, a={angle}, d={distance}, updated current_gait={self.gait_manager.current_gait_name}")

            self.already_stopped = False
        else:
            print(f"Cannot handle 'drive' as gait manager is not initialised!")

    def _stand(self, current_time: float, with_delay: bool = False) -> None:
        current_gait_name = self.gait_manager.current_gait_name
        if current_gait_name != self.standing_gait and current_gait_name != self.delayed_standing_gait:
            if current_gait_name == INITIAL_SETUP_GAIT_NAME or not with_delay:
                if DEBUG_OPERATIONS: print(f"    Stop: selecting gait={self.standing_gait}")
                self.gait_manager.select_gait(self.standing_gait, current_time, self.drive_gaits_contexts[self.standing_gait])
            else:
                if DEBUG_OPERATIONS: print(f"    Stop: pushing gait={self.standing_gait}")
                self.gait_manager.push_gait(self.delayed_standing_gait, current_time, self.drive_gaits_contexts[self.delayed_standing_gait])
        self.already_stopped = True

    def stop(self, current_time: float) -> None:
        if self.gait_manager is not None:
            if self.gait_layer == GaitLayer.STAND:
                context = self.drive_gaits_contexts[self.selected_gait]
                context["twist"] = 0
                context["angle"] = 0
                context["distance"] = 0.01
                context["x"] = 0
                context["y"] = 0
                current_gait = self.gait_manager.current_gait
                current_gait.update(current_time, context)
                if DEBUG_OPERATIONS: print(f"*** Stop: s={0}, a={0}, d={0.01}, updated current_gait={self.gait_manager.current_gait_name}")
            elif not self.already_stopped and self.gait_layer == GaitLayer.WALK:
                self._stand(current_time, with_delay=True)
                if DEBUG_OPERATIONS: print(f"*** Stop: current_gait={self.gait_manager.current_gait_name}")
            elif self.gait_layer != GaitLayer.WALK:
                if DEBUG_OPERATIONS: print(f"    Stop: not walking - won't do anything gait={self.gait_manager.current_gait_name}")
        else:
            print(f"Cannot handle 'stop' as gait manager is not initialised!")

    def change_value(self, current_time: float, key: str, absolute: Optional[float] = None, relative: Optional[float] = None) -> None:
        if self.gait_manager is not None:
            # context = self.gait_manager.current_gait_context
            context = self.drive_gaits_contexts[self.selected_gait]
            if context is not None and key in context:
                if absolute is not None:
                    context[key] = absolute
                elif relative is not None:
                    context[key] += relative
                else:
                    pass
                if context[key] < -30:
                    context[key] = -30
                new_value = context[key]
                if DEBUG_OPERATIONS: print(f"*** {key}: new_value={new_value}, updating gait={self.gait_manager.current_gait_name}")

                if self.gait_manager.current_gait_name != self.selected_gait_name:
                    self.gait_manager.select_gait(self.selected_gait_name, current_time, self.drive_gaits_contexts[self.selected_gait])
                else:
                    self.gait_manager.update_current_gait_context(current_time, context)
            else:
                print(f"*** {key}: Cannot handle 'change {key}' as there is no {key} in context; {context}")
        else:
            print(f"*** {key}: Cannot handle 'change {key}' as gait manager is not initialised!")

    def down_gait(self, current_time: float) -> None:
        if self.gait_manager is not None:
            if self.gait_layer == GaitLayer.STAND:
                self.gait_layer = GaitLayer.WALK
            elif self.gait_layer == GaitLayer.WALK:
                self.gait_layer = GaitLayer.SIT
            elif self.gait_layer == GaitLayer.SIT:
                self.gait_layer = GaitLayer.LAY
            else:
                self.gait_layer = GaitLayer.LAY
            if DEBUG_OPERATIONS: print(f"*** Down: layer {self.gait_layer}, new gait {self.selected_gait_name}, current={self.gait_manager.current_gait_name}")
            if self.gait_manager.current_gait_name != self.selected_gait_name:
                self.gait_manager.select_gait(self.selected_gait_name, current_time, self.drive_gaits_contexts[self.selected_gait])
                if self.gait_layer == GaitLayer.WALK:
                    self._stand(current_time, with_delay=False)
                if DEBUG_OPERATIONS: print(f"*** Down: selected current_gait={self.gait_manager.current_gait_name}")
        else:
            print(f"*** One Gait Down: Cannot handle 'one gait down' as gait manager is not initialised!")

    def up_gait(self, current_time: float) -> None:
        if self.gait_manager is not None:
            if self.gait_layer == GaitLayer.WALK:
                if DEBUG_OPERATIONS: print(f"*** Up: standing, current_gait={self.gait_manager.current_gait_name}")
                self.gait_layer = GaitLayer.STAND
                self.gait_manager.select_gait(self.selected_gait_name, current_time, self.drive_gaits_contexts[self.standing_gait])
            elif self.gait_layer == GaitLayer.SIT:
                if DEBUG_OPERATIONS: print(f"*** Up: ready to walk, current_gait={self.gait_manager.current_gait_name}")
                self.gait_layer = GaitLayer.WALK
                self._stand(current_time, with_delay=False)
            elif self.gait_layer == GaitLayer.LAY:
                self.gait_layer = GaitLayer.SIT
            if DEBUG_OPERATIONS: print(f"*** Up: layer {self.gait_layer}, new gait {self.selected_gait_name}, current={self.gait_manager.current_gait_name}")
            if self.gait_layer != GaitLayer.WALK and self.gait_manager.current_gait_name != self.selected_gait_name:
                self.gait_manager.select_gait(self.selected_gait_name, current_time, self.drive_gaits_contexts[self.selected_gait])
                if DEBUG_OPERATIONS: print(f"*** Up: sit, current_gait={self.gait_manager.current_gait_name}")
        else:
            print(f"*** One Gait Up: Cannot handle 'one gait up' as gait manager is not initialised!")

    def previous_gait(self, current_time: float) -> None:
        if self.gait_manager is not None:
            index = self._gait_list_indexes[self.gait_layer]
            index = index - 1 if index > 0 else len(self._gait_lists[self.gait_layer])
            self._gait_list_indexes[self.gait_layer] = index

            self.gait_layer_selected_gait[self.gait_layer] = self._gait_lists[self.gait_layer][self._gait_list_indexes[self.gait_layer]]

            if DEBUG_OPERATIONS: print(f"*** Previous: layer {self.gait_layer}, new gait {self.selected_gait_name}, current={self.gait_manager.current_gait_name}")
            if self.gait_manager.current_gait_name != self.selected_gait_name:
                self.gait_manager.select_gait(self.selected_gait_name, current_time, self.drive_gaits_contexts[self.selected_gait])
            else:
                self.gait_manager.update_current_gait_context(current_time, self.drive_gaits_contexts[self.selected_gait])
        else:
            print(f"*** Previous Gait: Cannot handle 'previous gait' as gait manager is not initialised!")

    def next_gait(self, current_time: float) -> None:
        if self.gait_manager is not None:
            index = self._gait_list_indexes[self.gait_layer]
            index = index + 1 if index < len(self._gait_lists[self.gait_layer]) - 1 else 0
            self._gait_list_indexes[self.gait_layer] = index

            self.gait_layer_selected_gait[self.gait_layer] = self._gait_lists[self.gait_layer][self._gait_list_indexes[self.gait_layer]]

            if DEBUG_OPERATIONS: print(f"*** Next: layer {self.gait_layer}, new gait {self.selected_gait_name}, current={self.gait_manager.current_gait_name}")
            if self.gait_manager.current_gait_name != self.selected_gait_name:
                self.gait_manager.select_gait(self.selected_gait_name, current_time, self.drive_gaits_contexts[self.selected_gait])
            else:
                self.gait_manager.update_current_gait_context(current_time, self.drive_gaits_contexts[self.selected_gait])
        else:
            print(f"*** Next Gait: Cannot handle 'next gait' as gait manager is not initialised!")
