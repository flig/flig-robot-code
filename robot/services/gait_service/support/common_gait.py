import math
from math import sin, cos, radians, degrees, atan2, isclose, sqrt
from typing import Optional, Tuple, List, Dict, Any

from gait.gait import Gait, GaitManager
from body import Leg, LegID
from path import LinearPath
from utils import time_to_string
from vector import Vector3

DEBUG = True
DEBUG_PATHS = True
DEBUG_LEG_POSITION = True


HALF_PI = math.pi / 2.0
TWO_PI = math.pi * 2.0


class CommonGait(Gait):
    def __init__(self, gait_manager: GaitManager,
                 name: str,
                 default_values: Optional[Dict[str, Any]] = None):
        super().__init__(gait_manager, name)

        self.updated = False

        self.temp_vector = Vector3()

        self.cog_a = 0.9  # CoG Affinity: 1.0 - border value, 0.5 half way between border and CoG (inscribed circle)
        self.minimum_x_body_offset = 10
        self.x_o = 0
        self.y_o = 0

        self.speed = 4
        self.power = 100
        self.height = 85
        self.leg_raise = 15
        self.stride = 18
        self.ox = 0  # absolute offset from body 0, 0
        self.oy = 0  # absolute offset from body 0, 0
        self.cx = 0  # relative offset from body 0, 0 - in to 0, 0
        self.cy = 0  # relative offset from body 0, 0 - in to 0, 0
        self.tx = 0.5  # percentage to movement to CoG
        self.ty = 0.5  # percentage to movement to CoG
        self.tz = 0.3  # currently not used!!! TODO

        self.cog_speed = 0.1  # CoG body adjustment speed: speed = speed * (1 - cog_speed)

        self.min_cog_speed = 0.05  # currently not used!!! TODO
        self.angle = 0.0
        self.distance: Optional[float] = None

        self.default_values = default_values if default_values is not None else {}

        def set_default(name: str, value: Any) -> None:
            if name not in self.default_values:
                self.default_values[name] = value

        set_default("speed", self.speed)
        set_default("power", self.power)
        set_default("height", self.height)
        set_default("leg_raise", self.leg_raise)
        set_default("stride", self.stride)
        set_default("ox", self.ox)
        set_default("oy", self.oy)
        set_default("cx", self.cx)
        set_default("cy", self.cy)
        set_default("tx", self.tx)
        set_default("ty", self.ty)
        set_default("tz", self.tz)
        set_default("cog_speed", self.cog_speed)
        set_default("min_cog_speed", self.min_cog_speed)
        set_default("angle", self.angle)
        set_default("distance", self.distance)
        set_default("cog_a", self.cog_a)

        self.phases_number = 4

        self.body_position_paths = []

        self.required_body_position = Vector3()
        self.height_position_path = LinearPath()

    def process_context(self, context: dict, update: bool = False) -> bool:
        with self.context_processor(context, update) as c:
            self.speed = c.process_float("speed", self.speed, self.default_values["speed"])
            self.power = c.process_int("power", int(self.power), self.default_values["power"])
            self.angle = c.process_float("angle", self.angle, self.default_values["angle"])
            self.distance = c.process_float("distance", self.distance, self.default_values["distance"])
            self.height = c.process_float("height", self.height, self.default_values["height"])
            self.leg_raise = c.process_float("leg_raise", self.leg_raise, self.default_values["leg_raise"])
            self.stride = c.process_float("stride", self.stride, self.default_values["stride"])
            self.cog_speed = c.process_float("cog_speed", self.cog_speed, self.default_values["cog_speed"])
            self.min_cog_speed = c.process_float("min_cog_speed", self.min_cog_speed, self.default_values["min_cog_speed"])
            self.tx = c.process_float("tx", self.tx, self.default_values["tx"])
            self.ty = c.process_float("ty", self.ty, self.default_values["ty"])
            self.tz = c.process_float("tz", self.tz, self.default_values["tz"])
            self.ox = c.process_float("ox", self.ox, self.default_values["ox"])
            self.oy = c.process_float("oy", self.oy, self.default_values["oy"])
            self.cx = c.process_float("cx", self.cx, self.default_values["cx"])
            self.cy = c.process_float("cy", self.cy, self.default_values["cy"])
            self.cog_a = c.process_float("cog_a", self.speed, self.default_values["cog_a"])

            return c.is_changed()

    def start(self, current_time: float, context: dict) -> None:
        self.updated = False
        super().start(current_time, context)

        self.process_context(context, False)

    def stop(self, current_time: float, new_gait: Optional[Gait], new_context: dict, active: bool) -> None:
        self.updated = False
        pass

    def update(self, current_time: float, context: dict) -> None:
        self.updated = True
        self.process_context(context)

    def setup_requested_height(self, current_time: float, height: float) -> bool:
        """
        Sets legs to common height
        :return: True if legs weren't at requested height and both legs and/or body needed adjusting
        """

        # Group legs per height
        leg_partitions = self.partition_legs_by_height()

        # Normalise so 'highest' leg is at '0' and all others have negative values
        z_positions = list(leg_partitions.keys())
        max_position = max(z_positions)
        if not math.isclose(max_position, 0, abs_tol=1):
            leg_partitions = {k - max_position: v for k, v in leg_partitions.items()}
            for leg in self.body:
                leg.position.z -= max_position
            self.body.body_position.z += max_position

        if DEBUG:
            partitions = {f"{k}:{[i.leg_id for i in v]}" for k, v in leg_partitions.items()}
            print(f"    Setting up height {height}; partitions={partitions} @ {time_to_string(current_time)}")
        if DEBUG_LEG_POSITION: self.debug_print_position(True)

        # If all legs are at the same level check if boxy is already on height or not
        if len(leg_partitions) == 1:
            if math.isclose(self.body.body_position.z, height, abs_tol=1):
                if DEBUG: print(f"        body {self.body.body_position.z} height close to {height}, no changes")
                return False

            speed = self.calculate_speed(self.speed, abs(self.body.body_position.z - height))
            self.height_position_path.end.set_values(self.body.body_position.x, self.body.body_position.y, height)
            self.body.set_body_position_path(self.height_position_path, current_time, speed)
            # self.body.body_linear_move(self.body.body_position.x, self.body.body_position.y, height, current_time, speed)
            if DEBUG: print(f"        all legs same height, body set to {height}")
            return True

        # Get all z positions of legs (difference between raised and 0) and same for
        # body height (difference between current and asked), and
        # calculate max speed given distance and currently requested speed
        z_positions = list(leg_partitions.keys())
        z_positions.append(-abs(height - self.body.body_position.z))
        min_position = min(z_positions)
        speed = self.calculate_speed(self.speed, abs(min_position))

        for p, legs in leg_partitions.items():
            if not math.isclose(p, 0, abs_tol=1):
                for leg in legs:
                    # leg.linear_move(leg.position.x, leg.position.y, 0, current_time, speed)
                    leg.linear_move(leg.position.x, leg.position.y, 0, current_time, speed)

        if not math.isclose(self.body.body_position.z, height, abs_tol=1):
            self.height_position_path.end.set_values(self.body.body_position.x, self.body.body_position.y, height)
            self.body.set_body_position_path(self.height_position_path, current_time, speed)
            # self.body.body_linear_move(self.body.body_position.x, self.body.body_position.y, height, current_time, speed)

        return True

    def calculate_furthest_distance(self) -> Optional[float]:
        if self.distance is None:
            return None

        if isclose(self.distance, 0.0, abs_tol=0.001):
            return None

        angle = radians(self.angle)

        xo = cos(angle) * self.distance
        yo = sin(angle) * self.distance

        xw = self.half_distance_x
        yw = self.half_distance_y

        xw = -xw if xo >= 0 else xw
        yw = -yw if yo >= 0 else yw

        xd = xo - xw
        yd = yo - yw

        return sqrt(xd * xd + yd * yd)

    def prepare_path_for_leg(self, leg_id: LegID, furthest_distance: Optional[float]) -> Tuple[float, float, float, float]:
        x_leg_sign, y_leg_sign = leg_id.leg_sign_xy()

        angle = radians(self.angle)
        stride = self.stride

        if self.distance is not None:
            xo = sin(angle) * self.distance
            yo = cos(angle) * self.distance

            xw = x_leg_sign * self.half_distance_x
            yw = y_leg_sign * self.half_distance_y

            xd = xo - xw
            yd = yo - yw

            leg_angle = atan2(yd, xd)
            angle = leg_angle + (HALF_PI if self.distance < 0 else -HALF_PI)
            if angle < -math.pi:
                angle += TWO_PI
            elif angle > math.pi:
                angle -= TWO_PI

            mr = math.sqrt(xd * xd + yd * yd)

            stride_factor = mr / furthest_distance

            stride = stride * stride_factor
            if DEBUG: print(f"    {leg_id}, xo,yo=({xo:.2f}, {yo:.2f}) xw,yw=({xw:.2f}, {yw:.2f}) xd,yd=({xd:.2f}, {yd:.2f}),"
                            f" stride={stride:.2f} angle={angle:.2f} angleº={degrees(angle):.0f} mr={mr:.2f}, furthest_distance={furthest_distance:.2f}")

        return stride, angle, x_leg_sign, y_leg_sign

    def partition_legs_by_height(self) -> Dict[float, List[Leg]]:
        leg_parititions: Dict[float, List[Leg]] = {}
        for leg in self.body:
            if len(leg_parititions) == 0:
                leg_parititions[leg.position.z] = [leg]
            else:
                partition: Optional[float] = None
                for p in leg_parititions:
                    if partition is None and math.isclose(p, leg.position.z, abs_tol=1):
                        partition = p
                if partition is None:
                    leg_parititions[leg.position.z] = [leg]
                else:
                    leg_parititions[partition].append(leg)
        return leg_parititions

    def setup_cog_without_leg(self, current_time: float, time_of_leg_lift: Optional[float], leg_to_be_lifted: Leg, speed: Optional[float]) -> None:
        three_leg_details = self.body.get_three_leg_details_at(leg_to_be_lifted.leg_id.opposite_leg(), time_of_leg_lift)

        # cog_x, cog_y, cog_distance_to_line, body_centre_distance_to_cog = self.calculate_distance_to_cog(time_of_leg_lift, leg_to_be_lifted)

        bx = self.body.body_position.x
        by = self.body.body_position.y

        needed_distance = three_leg_details.body_centre_distance_to_cog - three_leg_details.cog_distance_to_line * self.cog_a
        new_needed_cog_x = bx + (three_leg_details.cog_x - bx) * needed_distance / three_leg_details.body_centre_distance_to_cog
        new_needed_cog_y = by + (three_leg_details.cog_y - by) * needed_distance / three_leg_details.body_centre_distance_to_cog

        # max_x_cog = 20
        # max_y_cog = 10
        # if centre_of_inscribed_circle_x > max_x_cog:
        #     centre_of_inscribed_circle_x = max_x_cog
        # if centre_of_inscribed_circle_x < -max_x_cog:
        #     centre_of_inscribed_circle_x = -max_x_cog
        #
        # if centre_of_inscribed_circle_y > max_y_cog:
        #     centre_of_inscribed_circle_y = max_y_cog
        # if centre_of_inscribed_circle_y < -max_y_cog:
        #     centre_of_inscribed_circle_y = -max_y_cog

        if time_of_leg_lift is None:
            distance = self.body.body_position.distance_values(
                -new_needed_cog_x,
                -new_needed_cog_y,
                self.body.body_position.z
            )
            speed = self.calculate_speed(speed, distance)
        else:
            speed = time_of_leg_lift - current_time

        self.body.body_linear_move(
            -new_needed_cog_x,
            -new_needed_cog_y,
            self.body.body_position.z,
            current_time,
            speed
        )

        if DEBUG: print(f"common_walk :     setup body movement for CoG {self.body.body_position_path}")

    def is_stable_without_this_leg(self, time_of_leg_lift: float, leg_to_be_lifted: Leg) -> bool:
        three_leg_details = self.body.get_three_leg_details_at(leg_to_be_lifted.leg_id.opposite_leg(), time_of_leg_lift)
        return three_leg_details.is_stable_for_this_leg(time_of_leg_lift, self.cog_a)
