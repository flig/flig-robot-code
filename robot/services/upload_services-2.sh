#!/bin/bash

PYROS_HOST_AND_PORT=$1

if [[ ( -z "$PYROS_HOST_AND_PORT" ) ]]; then
  PYROS_HOST_AND_PORT="default"
fi

pyros $PYROS_HOST_AND_PORT upload -v -s -r pi2:shutdown_service shutdown_service_fast.py
#pyros $PYROS_HOST_AND_PORT upload -v -s -r power_state_service power_state_service.py
#pyros $PYROS_HOST_AND_PORT upload -v -s -r gait_service gait_service/gait_service_main.py -e gait_service/*
#pyros $PYROS_HOST_AND_PORT upload -v -s -r screen_service screen_service/screen_service_main.py -e screen_service/*
##pyros $PYROS_HOST_AND_PORT upload -v -s -r jcontroller_service jcontroller_service/jcontroller_service_main.py -e jcontroller_service/*
#pyros $PYROS_HOST_AND_PORT upload -v -s -r pi2:game_controller game_controller/game_controller_main.py -e game_controller/*
pyros $PYROS_HOST_AND_PORT upload -v -s -r pi2:vl53l5cx vl53l5cx/vl53l5cx_main.py -e vl53l5cx/*
pyros $PYROS_HOST_AND_PORT upload -v -s -r pi2:icm20948 icm20948/icm20948_main.py -e icm20948/*
