import traceback
from typing import Optional

import RPi.GPIO as GPIO
from PIL import Image, ImageDraw

from battery_status import BatteryStatus
from st7789 import ST7789

import pyroslib

DEBUG = False
TOUCH_ENABLED = True
TOUCH_PIN = 23


class TouchDriver:
    PRESSED_TIMER_LENGTH = 10
    RELEASED_TIMER_LENGTH = 6

    def __init__(self, pin: int) -> None:
        GPIO.setup(pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        self.pin = pin
        self.pressed_timer = -1
        self.released_timer = -1
        self.pressed = False
        self.click_detected = False
        self.double_click_detected = False
        self.long_click_detected = False

    def click(self):
        self.click_detected = True
        print(f"TOUCH: click")

    def long_click(self):
        self.long_click_detected = True
        print(f"TOUCH: long click")

    def double_click(self):
        self.double_click_detected = True
        print(f"TOUCH: double click")

    def process(self) -> None:
        if self.released_timer > 0: self.released_timer -= 1
        if self.pressed_timer > 0: self.pressed_timer -= 1
        if GPIO.input(self.pin):
            self.pressed = True
            # print(f"    pressed {self.pressed_timer:2d} / {self.released_timer:2d}")
            if self.pressed_timer < 0: self.pressed_timer = self.PRESSED_TIMER_LENGTH
            if 0 < self.released_timer < self.RELEASED_TIMER_LENGTH + 1:
                self.double_click()
                self.released_timer = 0
                self.pressed_timer = 0
            elif self.released_timer <= 0 and self.pressed_timer == 1:
                self.long_click()
                self.pressed_timer = 0
        else:
            self.pressed = False
            # print(f"    ------- {self.pressed_timer:2d} / {self.released_timer:2d}")
            if self.pressed_timer > 0:
                self.pressed_timer = -1
                self.released_timer = self.RELEASED_TIMER_LENGTH
            elif self.pressed_timer == 0:
                self.pressed_timer = -1
                self.released_timer = 0
            elif self.released_timer == 1:
                self.click()
                self.released_timer = -1


class ScreenService:
    SPI_MODE = 0b11
    DIVIDER = 2
    DETECTED_CLICK_TIME = 2

    def __init__(self, touch_driver: Optional[TouchDriver]):
        self.touch_driver = touch_driver
        self.display = ST7789(mode=self.SPI_MODE)

        self.display.begin()
        self.display.clear()

        self.image = Image.new('RGB', (240, 240))
        self.draw = ImageDraw.Draw(self.image)

        self.battery_status = BatteryStatus(self.image, self.draw)
        self.had_error = False
        self.divider = 0

        self.pressed_radius = 0
        self.detected_click = 0
        self.feedback_colour = (0, 0, 0)

    # noinspection PyPep8Naming
    def draw_display(self):
        if self.divider == 0:
            try:
                self.draw.rectangle((0, 0, 240, 240), fill=(0, 0, 0, 0))
                image = self.battery_status.draw_display()

                if self.touch_driver is not None:
                    if self.detected_click > 0:
                        m = ScreenService.DETECTED_CLICK_TIME * 2
                        i = 120.0 * (m - self.detected_click + 1) / m
                        self.detected_click -= 1
                        if self.touch_driver.pressed:
                            self.detected_click = 1

                        self.draw.ellipse(((120 - i, 120 - i), (120 + i, 120 + i)), fill=self.feedback_colour)
                    else:
                        if self.touch_driver.pressed:
                            self.pressed_radius = (self.pressed_radius + 10) if self.pressed_radius < 120 else 120
                            self.draw.ellipse((
                                (120 - self.pressed_radius, 120 - self.pressed_radius),
                                (120 + self.pressed_radius, 120 + self.pressed_radius)),
                                outline=(180, 180, 180),
                                width=10
                            )
                        else:
                            self.pressed_radius = 60
                    if self.touch_driver.click_detected:
                        self.touch_driver.click_detected = False
                        self.detected_click = ScreenService.DETECTED_CLICK_TIME
                        self.feedback_colour = (0, 255, 0)
                    if self.touch_driver.double_click_detected:
                        self.touch_driver.double_click_detected = False
                        self.detected_click = ScreenService.DETECTED_CLICK_TIME
                        self.feedback_colour = (255, 255, 0)
                    if self.touch_driver.long_click_detected:
                        self.detected_click = ScreenService.DETECTED_CLICK_TIME
                        self.touch_driver.long_click_detected = False
                        self.feedback_colour = (0, 255, 255)

                self.display.display(image)
                self.had_error = False
            except Exception as e:
                if not self.had_error:
                    self.had_error = True
                    print("ERROR: " + str(e) + "\n" + ''.join(traceback.format_tb(e.__traceback__)))
            self.divider = ScreenService.DIVIDER
        else:
            self.divider -= 1


if __name__ == "__main__":
    try:
        print("Starting display service...")

        if TOUCH_ENABLED:
            GPIO.setwarnings(False)
            GPIO.setmode(GPIO.BCM)
            touch_driver: Optional[TouchDriver] = TouchDriver(TOUCH_PIN)
        else:
            touch_driver: Optional[TouchDriver] = None

        screen_service = ScreenService(touch_driver)

        pyroslib.init("display-service", unique=False)

        def shutdown_announce_handler(topic, payload):
            screen_service.battery_status.shutdown_announce_handler(topic, payload)

        def power_state_handler(topic, payload):
            screen_service.battery_status.power_state_handler(topic, payload)

        pyroslib.subscribe("shutdown/announce", shutdown_announce_handler)
        pyroslib.subscribe("power/state", power_state_handler)

        print("Started display service.")

        def main() -> None:
            if touch_driver is not None:
                touch_driver.process()

            screen_service.draw_display()

        pyroslib.forever(0.05, main, priority=pyroslib.PRIORITY_LOW)

    except Exception as ex:
        print("ERROR: " + str(ex) + "\n" + ''.join(traceback.format_tb(ex.__traceback__)))
