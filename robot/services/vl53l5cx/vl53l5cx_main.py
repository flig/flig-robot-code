#!/usr/bin/env python3

#
# Copyright 2016-2022 Games Creators Club
#
# MIT License
#

from threading import Thread
from typing import Optional, Any

import time
import traceback

import pyroslib as pyros
from telemetry import SocketTelemetryServer, TelemetryLogger

from vl53l5cx import VL53L5CX


DEBUG = True

SERVO_NO = 1


class VL53L5CXController:
    def __init__(self) -> None:
        self.telemetry_server_port = 6401
        self._server: Optional[SocketTelemetryServer] = None
        self._server_socket: Optional[Any] = None
        self._current_stream_logger: Optional[TelemetryLogger] = None
        self.driver = VL53L5CX(
            disable_ambient_per_spad=True,
            disable_nb_spads_enabled=False,
            disable_nb_target_detected=True,
            disable_signal_per_spad=False,
            disable_range_sigma_mm=False,
            disable_distance_mm=False,
            disable_reflectance_percent=True,
            disable_target_status=False,
            disable_motion_indicator=True
        )
        self._statuses = [0] * 16
        self._distances = [0] * 16
        self.servo_no = SERVO_NO
        self.current_angle = 0

    def start_vl53l5cx(self) -> None:
        alive = self.driver.is_alive()
        if not alive:
            raise IOError("VL53L5CX Device is not alive")

        print("Initialising...")
        t = time.time()
        self.driver.init()
        print(f"Initialised ({time.time() - t:.1f}s)")

        self.driver.set_ranging_frequency_hz(5)

        # Ranging:
        self.driver.start_ranging()

        ranging_frequency_hz = self.driver.get_ranging_frequency_hz()
        print(f"Ranging frequency: {ranging_frequency_hz}hz")

        integration_time_ms = self.driver.get_integration_time_ms()
        print(f"Integration time:  {integration_time_ms}ms")

        sharpener_percent = self.driver.get_sharpener_percent()
        print(f"Sharpener percent: {sharpener_percent:.2f}%")

        target_order = self.driver.get_target_order()
        print(f"Target order:      {target_order}")

        ranging_mode = self.driver.get_ranging_mode()
        print(f"Ranging mode:      {ranging_mode}")

        pyros.subscribe("servo/status", self._handle_servo_status)
        pyros.subscribe("vl53l5cx/angle", self._handle_angle_request)

    def _handle_servo_status(self, _topic: str, payload: str) -> None:
        if payload == "started":
            pyros.publish(f"servo/extra/{self.servo_no}/angle", f"{self.current_angle}")

    def _handle_angle_request(self, _topic: str, payload: str) -> None:
        if DEBUG: print(f"vl53l5cx: got request for angle {payload}")
        try:
            self.current_angle = int(float(payload))
            pyros.publish(f"servo/extra/{self.servo_no}/angle", f"{self.current_angle}")
        except Exception as _:
            print(f"vl53l5cx: error angle value '{payload}'")

    def start_telemetry_server(self, telemetry_server_port: int = 6401) -> None:
        self.telemetry_server_port = telemetry_server_port
        self._server = SocketTelemetryServer(port=self.telemetry_server_port)

        self.wait_for_server_start()
        self._server_socket = self._server.get_socket()
        self._server_socket.settimeout(10)

        self._current_stream_logger = self._define_stream(self._server)
        self._start_telemetry_server()

    def wait_for_server_start(self):
        timeout = time.time() + 10
        started = False

        while not started and time.time() < timeout:
            try:
                self._server.start()
                started = True
            except Exception as e:
                print(f"    failed to start socket server; {e}")
                time.sleep(1)

    @staticmethod
    def _define_stream(server: SocketTelemetryServer) -> TelemetryLogger:
        current_stream_logger = server.create_logger("vl53lcx")
        for i in range(16):
            current_stream_logger.add_byte(f"status_{i}")
            current_stream_logger.add_word(f"distance_{i}")
        current_stream_logger.init()
        return current_stream_logger

    def _start_telemetry_server(self) -> None:
        def server_loop():
            while True:
                # noinspection PyBroadException
                try:
                    self._server.process_incoming_connections()
                except Exception as _:
                    pass

        server_thread = Thread(name="incoming-vl53lcx-connections", target=server_loop, daemon=True)
        server_thread.start()

    def loop(self) -> None:
        if self.driver.check_data_ready():
            ranging_data = self.driver.get_ranging_data()

            for i in range(16):
                self._statuses[i] = int(ranging_data.target_status[self.driver.nb_target_per_zone * i])
                self._distances[i] = int(ranging_data.distance_mm[self.driver.nb_target_per_zone * i])

            self._current_stream_logger.log(
                time.time(),
                self._statuses[0],
                self._distances[0],
                self._statuses[1],
                self._distances[1],
                self._statuses[2],
                self._distances[2],
                self._statuses[3],
                self._distances[3],
                self._statuses[4],
                self._distances[4],
                self._statuses[5],
                self._distances[5],
                self._statuses[6],
                self._distances[6],
                self._statuses[7],
                self._distances[7],
                self._statuses[8],
                self._distances[8],
                self._statuses[9],
                self._distances[9],
                self._statuses[10],
                self._distances[10],
                self._statuses[11],
                self._distances[11],
                self._statuses[12],
                self._distances[12],
                self._statuses[13],
                self._distances[13],
                self._statuses[14],
                self._distances[14],
                self._statuses[15],
                self._distances[15]
            )


if __name__ == "__main__":
    try:
        print("Starting vl53l5cx service...")

        pyros.init("vl53l5cx-service")

        from RPi import GPIO

        GPIO.setmode(GPIO.BCM)
        GPIO.setup(17, GPIO.OUT)
        GPIO.output(17, 1)

        vl53l5cx_controller = VL53L5CXController()
        vl53l5cx_controller.start_telemetry_server()
        vl53l5cx_controller.start_vl53l5cx()

        print("Started vl53l5cx service.")

        pyros.forever(0.1, vl53l5cx_controller.loop)

    except Exception as ex:
        print("ERROR: " + str(ex) + "\n" + ''.join(traceback.format_tb(ex.__traceback__)))
