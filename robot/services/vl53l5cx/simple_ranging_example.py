import time

from vl53l5cx.vl53l5cx import VL53L5CX
from RPi import GPIO

GPIO.setmode(GPIO.BCM)
GPIO.setup(17, GPIO.OUT)
GPIO.output(17, 1)

driver = VL53L5CX(
    disable_ambient_per_spad=True,
    disable_nb_spads_enabled=False,
    disable_nb_target_detected=True,
    disable_signal_per_spad=False,
    disable_range_sigma_mm=False,
    disable_distance_mm=False,
    disable_reflectance_percent=True,
    disable_target_status=False,
    disable_motion_indicator=True
)

alive = driver.is_alive()
if not alive:
    raise IOError("VL53L5CX Device is not alive")

print("Initialising...")
t = time.time()
driver.init()
print(f"Initialised ({time.time() - t:.1f}s)")

driver.set_ranging_frequency_hz(10)

# Ranging:
driver.start_ranging()

ranging_frequency_hz = driver.get_ranging_frequency_hz()
print(f"Ranging frequency: {ranging_frequency_hz}hz")

integration_time_ms = driver.get_integration_time_ms()
print(f"Integration time:  {integration_time_ms}ms")

sharpener_percent = driver.get_sharpener_percent()
print(f"Sharpener percent: {sharpener_percent:.2f}%")

target_order = driver.get_target_order()
print(f"Target order:      {target_order}")

ranging_mode = driver.get_ranging_mode()
print(f"Ranging mode:      {ranging_mode}")

print("")
print("")

previous_time = 0
loop = 0
while loop < 1000:
    if driver.check_data_ready():
        ranging_data = driver.get_ranging_data()

        # As the sensor is set in 4x4 mode by default, we have a total 
        # of 16 zones to print. For this example, only the data of first zone are 
        # print
        print(f"\033[2J\033[:H")
        now = time.time()
        if previous_time != 0:
            time_to_get_new_data = now - previous_time
            print(f"Print data no : {driver.streamcount: >3d} ({time_to_get_new_data * 1000:.1f}ms)")
        else:
            print(f"Print data no : {driver.streamcount: >3d}")

        for i in range(16):
            print(f"Zone : {i: >3d}, "
                  f"Status : {ranging_data.target_status[driver.nb_target_per_zone * i]: >3d}, "
                  f"Distance : {ranging_data.distance_mm[driver.nb_target_per_zone * i]: >4.0f} mm")

        print("")

        previous_time = now
        loop += 1

    time.sleep(0.005)
