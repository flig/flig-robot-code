#!/usr/bin/env python3

#
# Copyright 2016-2022 Games Creators Club
#
# MIT License
#
import os

from typing import List, Dict, Optional

import array
import socket
import struct
import threading
import time
import traceback
from abc import ABC
from enum import Enum
from fcntl import ioctl
from threading import Thread

import pyroslib as pyros

from input_state import InputState

DEBUG_AXES = False
DEBUG_BUTTONS = False
DEBUG_BUTTON_CHANGES = True
DEBUG_JOYSTICK = False
DEBUG_UDP = False
DEBUG_SETUP = False
DEBUG_OPERATIONAL = True

EXPO = 0.5
MAX_STOPPING = 2

JCONTROLLER_UDP_PORT = 1880
# DEFAULT_FLOAT_TOLERANCE = 0.05
DEFAULT_FLOAT_TOLERANCE = None


class Modes(Enum):
    NONE = 0
    NORMAL = 1
    MENU = 2


# These constants were borrowed from linux/input.h
axis_names = {
    0x00: "x",
    0x01: "y",
    0x02: "z",
    0x03: "rx",
    0x04: "ry",
    0x05: "rz",
    0x06: "trottle",
    0x07: "rudder",
    0x08: "wheel",
    0x09: "gas",
    0x0a: "brake",
    0x10: "hat0x",
    0x11: "hat0y",
    0x12: "hat1x",
    0x13: "hat1y",
    0x14: "hat2x",
    0x15: "hat2y",
    0x16: "hat3x",
    0x17: "hat3y",
    0x18: "pressure",
    0x19: "distance",
    0x1a: "tilt_x",
    0x1b: "tilt_y",
    0x1c: "tool_width",
    0x20: "volume",
    0x28: "misc",
}

button_names = {
    0x120: "select",
    0x121: "lbutton",
    0x122: "rbutton",
    0x123: "start",
    0x124: "lup",
    0x125: "lright",
    0x126: "ldown",
    0x127: "lleft",
    0x128: "tl1",
    0x129: "tr1",
    0x12a: "tl2",
    0x12b: "tr2",
    0x12c: "by",
    0x12d: "ba",
    0x12e: "bb",
    0x12f: "bx",
    0x130: "cross",
    0x131: "circle",
    0x132: "c",
    0x133: "triangle",
    0x134: "square",
    0x135: "z",
    0x136: "tl",
    0x137: "tr",
    0x138: "tl2",
    0x139: "tr2",
    0x13a: "share",
    0x13b: "options",
    0x13c: "ps",
    0x13d: "lfire",
    0x13e: "rfire"
}


class Joystick(ABC):
    def __init__(self, joystick: 'JoystickController') -> None:
        self.joystick = joystick


class JoystickController:
    def __init__(self) -> None:
        self.axis_states: Dict[str, InputState[float]] = {
            "x": InputState[float]("x", 0.0, float_tolerance=DEFAULT_FLOAT_TOLERANCE),
            "y": InputState[float]("y", 0.0, float_tolerance=DEFAULT_FLOAT_TOLERANCE),
            "rx": InputState[float]("rx", 0.0, float_tolerance=DEFAULT_FLOAT_TOLERANCE),
            "ry": InputState[float]("ry", 0.0, float_tolerance=DEFAULT_FLOAT_TOLERANCE)
        }
        self.button_states: Dict[str, InputState[int]] = {
            "x3": InputState[int]("x3", 0), "y3": InputState[int]("y3", 0)
        }

        for button_name in button_names.values():
            self.button_states[button_name] = InputState[int](button_name, 0)

        self.last_axis_states = {k: v for k, v in self.axis_states.items()}

        self.have_joystick_event = False

    def process_buttons(self, changes: dict) -> None:
        if DEBUG_BUTTON_CHANGES:
            debug_changes = {k: v for k, v in self.button_states.items() if v.changed}
            if len(debug_changes) > 0:
                print(f"   Got changes {debug_changes}")

        try:
            if "hat0x" in self.axis_states:
                x3 = int(self.axis_states["hat0x"].value)
                y3 = int(self.axis_states["hat0y"].value)
            elif "lup" in self.button_states:
                l_up = self.button_states["lup"].value
                l_right = self.button_states["lright"].value
                l_down = self.button_states["ldown"].value
                l_left = self.button_states["lleft"].value

                x3 = -1 if l_left else (1 if l_right else 0)
                y3 = -1 if l_up else (1 if l_down else 0)
            else:
                x3 = 0
                y3 = 0

            self.button_states["x3"].value = x3
            self.button_states["y3"].value = y3

            for button_state in self.button_states.values():
                if button_state.changed:
                    changes[button_state.name] = str(button_state.value)
                button_state.process()

            if DEBUG_BUTTONS: print(f"OK Button states: {({k: v.value for k, v in self.button_states.items()})}")

        except Exception as ex:
            print(f"ERR Button states: {({k: v.value for k, v in self.button_states.items()})}; " + str(ex) + "\n" + ''.join(traceback.format_tb(ex.__traceback__)))

    @staticmethod
    def calculate_expo(v: float, expo_percentage: float) -> float:
        return v * v * expo_percentage + v * (1.0 - expo_percentage) if v >= 0 else - v * v * expo_percentage + v * (1.0 - expo_percentage)

    def process_joysticks(self, changes: dict) -> None:
        for input_state in self.axis_states.values():
            if input_state.changed:
                changes[input_state.name] = f"{input_state.value:.2f}"
            input_state.process()

        if DEBUG_AXES: print(f"OK Axis states: {self.axis_states}")


class EventLoopJoystick(Joystick):
    def __init__(self, joystick: JoystickController) -> None:
        super().__init__(joystick)

        self.fn = '/dev/input/js0'
        self.jsdev = None

        self.axis_map: List[str] = []
        self.button_map: List[str] = []

    def connect_to_joystick(self, print_error: bool) -> bool:
        try:
            if DEBUG_SETUP: print(f"   Opening {self.fn}...")
            self.jsdev = open(self.fn, 'rb')

            buf = array.array('b', [0] * 64)

            ioctl(self.jsdev, 0x80006a13 + (0x10000 * len(buf)), buf)  # JSIOCGNAME(len)
            js_name = buf.tobytes().decode("UTF-8")
            print(f"    Device name: {js_name}")

            # Get number of axes and buttons.
            buf = array.array('B', [0])
            ioctl(self.jsdev, 0x80016a11, buf)  # JSIOCGAXES
            num_axes = buf[0]

            buf = array.array('B', [0])
            ioctl(self.jsdev, 0x80016a12, buf)  # JSIOCGBUTTONS
            num_buttons = buf[0]

            # Get the axis map.
            buf = array.array('B', [0] * 0x40)
            ioctl(self.jsdev, 0x80406a32, buf)  # JSIOCGAXMAP

            for axis in buf[:num_axes]:
                axis_name = axis_names.get(axis, '0x%02x' % axis)
                self.axis_map.append(axis_name)
                if axis_name not in self.joystick.axis_states:
                    self.joystick.axis_states[axis_name] = InputState[float](axis_name, 0.0, float_tolerance=DEFAULT_FLOAT_TOLERANCE)
                else:
                    self.joystick.axis_states[axis_name].value = 0.0

            # Get the button map.
            buf = array.array('H', [0] * 200)
            ioctl(self.jsdev, 0x80406a34, buf)  # JSIOCGBTNMAP

            for btn in buf[:num_buttons]:
                btn_name = button_names.get(btn, '0x%03x' % btn)
                self.button_map.append(btn_name)
                if btn_name not in self.joystick.button_states:
                    self.joystick.button_states[btn_name] = InputState[int](btn_name, 0)
                self.joystick.button_states[btn_name].present = True

            print(f"    {num_axes} axes found: {', '.join(self.axis_map)}")
            print(f"    {num_buttons} buttons found: {', '.join(self.button_map)}")
            return True
        except Exception as ex:
            if print_error:
                print("ERROR: Failed to connect to joystick; " + str(ex) + "\n" + ''.join(traceback.format_tb(ex.__traceback__)))
            return False
        except BaseException as ex:
            if print_error:
                print("ERROR: Failed to connect to joystick - no exception given; " + str(ex) + "\n" + ''.join(traceback.format_tb(ex.__traceback__)))
            return False

    def read_events(self) -> None:
        reconnect = True
        no_error = True

        while True:
            if reconnect:
                connected = self.connect_to_joystick(no_error)
                if connected:
                    reconnect = False
                    no_error = True
                else:
                    no_error = False
                time.sleep(0.5)
            else:
                try:
                    evbuf = self.jsdev.read(8)
                    if evbuf:
                        time_of_event, value, event_type, number = struct.unpack('IhBB', evbuf)

                        if event_type & 0x01:
                            button_name = self.button_map[number]
                            if button_name:
                                self.joystick.button_states[button_name].value = value
                                self.joystick.have_joystick_event = True

                        if event_type & 0x02:
                            selected_axis = self.axis_map[number]
                            if selected_axis:
                                f_value = value / 32767.0
                                self.joystick.axis_states[selected_axis].value = f_value
                                self.joystick.have_joystick_event = True

                except BaseException as ex:
                    print("ERROR: Failed to read joystick; " + str(ex) + "\n" + ''.join(traceback.format_tb(ex.__traceback__)))
                    reconnect = True
                    for button_state in self.joystick.button_states.values():
                        button_state.present = False
                    time.sleep(0.2)

    def start_read_thread(self) -> None:
        thread = Thread(name="reading-joystick-evdev-events", target=self.read_events, args=(), daemon=True)
        thread.daemon = True
        thread.start()


class UDPJoystick(Joystick):
    def __init__(self, joystick: JoystickController) -> None:
        super().__init__(joystick)
        self.s = None

    # noinspection PyPep8Naming
    def read_UDP_events(self) -> None:
        self.s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.s.bind(('', JCONTROLLER_UDP_PORT))

        self.s.settimeout(10)
        print("    Started receive thread...")
        while True:
            # noinspection PyBroadException
            try:
                data, addr = self.s.recvfrom(1024)
                p = str(data, 'utf-8')

                if p.startswith("J#"):
                    if DEBUG_UDP:
                        print(f"       received {p}")

                    kvps = p[2:].split(";")
                    for kvp in kvps:
                        kv = kvp.split("=")
                        if len(kv) == 2:
                            key = kv[0]
                            value = kv[1]

                            if key in self.joystick.axis_states:
                                self.joystick.axis_states[key].value = float(value)
                                self.joystick.have_joystick_event = True
                            elif key in self.joystick.button_states:
                                self.joystick.button_states[key].value = int(value)
                                self.joystick.have_joystick_event = True
                            else:
                                self.joystick.button_states[key] = InputState[int](key, int(value))

            except Exception as _:
                pass

    def start_read_thread(self) -> None:
        thread = Thread(name="reading-joystick-UDP-events", target=self.read_UDP_events, args=())
        thread.daemon = True
        thread.start()


class MainLoop:
    def __init__(self, joystick_controller: JoystickController, topics: List[str]) -> None:
        self.joystick_controller = joystick_controller
        self.topics = topics
        self.selected_topic = topics[0]
        self.share_button: Optional[InputState] = None
        self.option_button: Optional[InputState] = None

    @staticmethod
    def change_colour(r: int, g: int, b: int) -> None:
        leds = os.listdir("/sys/class/leds")
        for ledfile in leds:
            if ledfile.endswith(":red") and os.path.exists(f"/sys/class/leds/{ledfile}/brightness"):
                os.system(f"sudo bash -c \"echo {r} > /sys/class/leds/{ledfile}/brightness\"")
            if ledfile.endswith(":green") and os.path.exists(f"/sys/class/leds/{ledfile}/brightness"):
                os.system(f"sudo bash -c \"echo {g} > /sys/class/leds/{ledfile}/brightness\"")
            if ledfile.endswith(":blue") and os.path.exists(f"/sys/class/leds/{ledfile}/brightness"):
                os.system(f"sudo bash -c \"echo {b} > /sys/class/leds/{ledfile}/brightness\"")

    def loop(self) -> None:
        if self.joystick_controller.have_joystick_event:
            self.joystick_controller.have_joystick_event = False

            if self.share_button is None:
                if "share" in self.joystick_controller.button_states:
                    self.share_button = self.joystick_controller.button_states["share"]
                    if DEBUG_OPERATIONAL: print("Found 'share' button")
            else:
                if self.share_button.changed and not self.share_button.value:
                    self.selected_topic = self.topics[0]
                    if DEBUG_OPERATIONAL: print(f"Changed output to '{self.selected_topic}'")
                    self.change_colour(0, 128, 255)

            if self.option_button is None:
                if "options" in self.joystick_controller.button_states:
                    self.option_button = self.joystick_controller.button_states["options"]
                    if DEBUG_OPERATIONAL: print("Found 'options' button")
            else:
                if self.option_button.changed and not self.option_button.value:
                    self.selected_topic = self.topics[1]
                    if DEBUG_OPERATIONAL: print(f"Changed output to '{self.selected_topic}'")
                    self.change_colour(255, 128, 0)

            changes = {}
            self.joystick_controller.process_joysticks(changes)
            self.joystick_controller.process_buttons(changes)
            if len(changes) > 0:
                pyros.publish(self.selected_topic, ",".join(f"{k}:{v}" for k, v in changes.items()))


if __name__ == "__main__":
    try:
        print("Starting game controller service...")

        pyros.init("game-controller-service")

        joystick_controller = JoystickController()
        event_loop_joystick = EventLoopJoystick(joystick_controller)
        udp_joystick = UDPJoystick(joystick_controller)

        event_loop_joystick.start_read_thread()
        udp_joystick.start_read_thread()

        main_loop = MainLoop(joystick_controller, ["joystick/changes", "joystick/arm"])

        print("Started game controller service.")

        # # Main event loop
        # def loop():
        #     if joystick_controller.have_joystick_event:
        #         joystick_controller.have_joystick_event = False
        #         changes = {}
        #         joystick_controller.process_joysticks(changes)
        #         joystick_controller.process_buttons(changes)
        #         if len(changes) > 0:
        #             pyros.publish("joystick/changes", ",".join(f"{k}:{v}" for k, v in changes.items()))

        pyros.forever(0.1, main_loop.loop)

    except Exception as ex:
        print("ERROR: " + str(ex) + "\n" + ''.join(traceback.format_tb(ex.__traceback__)))
