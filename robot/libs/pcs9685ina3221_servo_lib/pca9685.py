import time
from typing import List

import smbus


class PCA9685:
    _DEFAULT_I2C_BUS = 1
    _DEFAULT_I2C_ADDRESS = 0x40

    _MODE1 = 0x00
    _MODE2 = 0x01
    _SUBADR1 = 0x02
    _SUBADR2 = 0x03
    _SUBADR3 = 0x04
    _PRESCALE = 0xFE
    _LED0_ON_L = 0x06
    _LED0_ON_H = 0x07
    _LED0_OFF_L = 0x08
    _LED0_OFF_H = 0x09
    _ALL_LED_ON_L = 0xFA
    _ALL_LED_ON_H = 0xFB
    _ALL_LED_OFF_L = 0xFC
    _ALL_LED_OFF_H = 0xFD

    _RESTART = 1 << 7
    _AI = 1 << 5
    _SLEEP = 1 << 4
    _ALLCALL = 1 << 0

    _OCH = 1 << 3
    _OUTDRV = 1 << 2

    def __init__(self,
                 i2c_bus: int = _DEFAULT_I2C_BUS,
                 i2c_addr: int = _DEFAULT_I2C_ADDRESS) -> None:
        self._i2c_bus = smbus.SMBus(i2c_bus)
        self._i2c_addr = i2c_addr

        self._i2c_bus.write_byte_data(self._i2c_addr, self._MODE1, self._AI | self._ALLCALL)
        self._i2c_bus.write_byte_data(self._i2c_addr, self._MODE2, self._OCH | self._OUTDRV)
        time.sleep(0.0005)

        mode = self._i2c_bus.read_byte_data(self._i2c_addr, self._MODE1)
        self._i2c_bus.write_byte_data(self._i2c_addr, self._MODE1, mode & ~self._SLEEP)

        time.sleep(0.0005)

        self._frequency = 50
        self._prescale = int(round(25000000.0 / (4096.0 * self._frequency)) - 1)

        if self._prescale < 3:
            self._prescale = 3
        elif self._prescale > 255:
            self._prescale = 255

        mode = self._i2c_bus.read_byte_data(self._i2c_addr, self._MODE1)

        self._i2c_bus.write_byte_data(self._i2c_addr, self._MODE1, (mode & ~self._SLEEP) | self._SLEEP)
        self._i2c_bus.write_byte_data(self._i2c_addr, self._PRESCALE, self._prescale)
        self._i2c_bus.write_byte_data(self._i2c_addr, self._MODE1, mode)

        time.sleep(0.0005)

        self._i2c_bus.write_byte_data(self._i2c_addr, self._MODE1, mode | self._RESTART)

        self.ns = 0.205
        # self.ns = 0.2

    def _set_duty_cycle(self, channel: int, steps: int) -> None:
        # steps = int(round(percent * (4096.0 / 100.0)))

        if steps < 0:
            on = 0
            off = 4096
        elif steps > 4095:
            on = 4096
            off = 0
        else:
            on = 0
            off = steps

        self._i2c_bus.write_i2c_block_data(self._i2c_addr, self._LED0_ON_L + 4 * channel,
                                           [on & 0xFF, on >> 8, off & 0xFF, off >> 8])

    def turn_off_all_servos(self) -> None:
        on = 0
        off = 4096

        self._i2c_bus.write_i2c_block_data(self._i2c_addr, self._ALL_LED_ON_L,
                                           [on & 0xFF, on >> 8, off & 0xFF, off >> 8])

    def set_servo_position(self, servo: int, value: int) -> None:
        value = int(value * self.ns)
        self._set_duty_cycle(servo, value)

    def set_all_servo_positions(self, positions: List[int]) -> None:
        def steps(position: int) -> int:
            steps = int(position * self.ns)
            return 4096 if steps < 0 else steps if steps < 4095 else 4095

        positions = [steps(position) for position in positions]
        data = []
        for position in positions:
            data.append(0)
            data.append(0)
            data.append(position & 0xFF)
            data.append(position >> 8)

        self._i2c_bus.write_i2c_block_data(self._i2c_addr, self._LED0_ON_L, data[:32])
        self._i2c_bus.write_i2c_block_data(self._i2c_addr, self._LED0_ON_L + 32, data[32:])
