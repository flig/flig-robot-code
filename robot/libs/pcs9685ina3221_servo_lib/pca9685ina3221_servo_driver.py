import logging
from typing import Optional

import RPi.GPIO as GPIO

from servo_lib.leg_servos import ServoDefinition
from pcs9685ina3221_servo_lib.ina3221 import INA3221
from pcs9685ina3221_servo_lib.pca9685 import PCA9685

from servo_lib.servo_driver import ServoDriver

logger = logging.getLogger(__name__)


DEBUG = True


class PCA9685INA3221Servos(ServoDriver):
    def __init__(self,
                 pca9685: PCA9685,
                 ina3221_1: INA3221, ina3221_2: INA3221,
                 digital_servo: bool = False,
                 all_off_pin: Optional[int] = None,
                 use_all_off_for_power: Optional[bool] = False) -> None:
        """

        :param pca9685:
        :param ina3221_1:
        :param ina3221_2:
        :param digital_servo: if set to True individual servo power won't be used
        :param all_off_pin: GPIO pin to be used for all off
        :param use_all_off_for_power: if set to True and digital is True and all_off_pin is present then
                                      all off pin will be used for power (0-50% half 2-2 pattern, >50% - full on)
        """
        super().__init__()
        self.pca9685 = pca9685
        self.ina3221_1 = ina3221_1
        self.ina3221_2 = ina3221_2
        self.digital_servo = digital_servo
        self.servo_positions = [0 for _ in range(16)]
        self.servo_power = [0 for _ in range(16)]
        self.servo_power_counter = [0 for _ in range(16)]
        self.servo_power_counter_reset = [0 for _ in range(16)]
        self.all_off_pin: Optional[int] = all_off_pin
        self.use_all_off_for_power = use_all_off_for_power
        self._temporary_off = 0
        if DEBUG:
            self._last_global_off = self._global_off
        if self.all_off_pin is not None:
            GPIO.setwarnings(False)
            GPIO.setmode(GPIO.BCM)
            GPIO.setup(self.all_off_pin, GPIO.OUT)

    def set_position(self, servo: ServoDefinition, servo_position: int, power: Optional[int] = None) -> None:
        channel = servo.servo_channel.value
        self.servo_positions[channel] = servo_position
        if not self.digital_servo:
            if power is None:
                power = self.global_power
            if self.servo_power[channel] != power:
                self.servo_power[channel] = power
                if power == 100:
                    self.servo_power_counter_reset[channel] = -1
                    self.servo_power_counter[channel] = -1
                elif power > 65:
                    self.servo_power_counter_reset[channel] = 3
                    self.servo_power_counter[channel] = 3
                elif power > 49:
                    self.servo_power_counter_reset[channel] = 2
                    self.servo_power_counter[channel] = 2
                elif power > 32:
                    self.servo_power_counter_reset[channel] = -3
                    self.servo_power_counter[channel] = -3
                elif power > 24:
                    self.servo_power_counter_reset[channel] = -4
                    self.servo_power_counter[channel] = -4
                elif power > 19:
                    self.servo_power_counter_reset[channel] = -5
                    self.servo_power_counter[channel] = -5
                else:
                    self.servo_power_counter_reset[channel] = -10
                    self.servo_power_counter[channel] = -10

        if self._send_immediately:
            self.pca9685.set_servo_position(channel, servo_position)

    def get_current_mA(self, servo: ServoDefinition) -> float:
        current_sensor_no = servo.current_channel.value
        if current_sensor_no < 3:
            return self.ina3221_1.get_current_mA(current_sensor_no)
        else:
            return self.ina3221_2.get_current_mA(current_sensor_no - 3)

    @ServoDriver.global_off.setter
    def global_off(self, global_off_state: bool) -> None:
        ServoDriver.global_off.fset(self, global_off_state)
        if self._global_off:
            GPIO.output(self.all_off_pin, 0)
            if DEBUG: print(f"### global OFF - setting pin to 0")  # ; global_off={self.global_off}, _global_off={self._global_off}; _temporary_off={self._temporary_off}")
        else:
            GPIO.output(self.all_off_pin, 1)
            if DEBUG: print(f"### global ON - setting pin to 1")  # ; global_off={self.global_off}, _global_off={self._global_off}; _temporary_off={self._temporary_off}")

    def stop_all(self):
        if self.all_off_pin is not None:
            GPIO.output(self.all_off_pin, 0)
            if not self.global_off:
                self._temporary_off = 1
        for i in range(16):
            self.pca9685.set_servo_position(i, 0)

    def process(self, current_time: float) -> None:
        if not self._global_off:
            if self._last_global_off != self._global_off:
                self._last_global_off = self._global_off
                if DEBUG: print(f"### global ON")  # ; global_off={self.global_off}, _global_off={self._global_off}; _temporary_off={self._temporary_off}")

            if self.all_off_pin is not None:
                if self._temporary_off > 0:
                    self._temporary_off -= 1
                    if self._temporary_off == 0:
                        if DEBUG: print(f"### setting pin to 1; _temporary_off={self._temporary_off}")
                        GPIO.output(self.all_off_pin, 1)

                elif self.use_all_off_for_power and self.digital_servo and self.global_power <= 50:
                    if self._temporary_off == -3:
                        if DEBUG: print(f"### setting pin to 0; _temporary_off={self._temporary_off}")
                        self._temporary_off = 2
                        GPIO.output(self.all_off_pin, 0)
                    else:
                        self._temporary_off -= 1

            def calculate_position(channel: int) -> int:
                if self.servo_power_counter_reset[channel] == 0:
                    return self.servo_positions[channel]
                elif self.servo_power_counter_reset[channel] > 0:
                    if self.servo_power_counter[channel] == 0:
                        self.servo_power_counter[channel] = self.servo_power_counter_reset[channel]
                    self.servo_power_counter[channel] -= 1
                    if self.servo_power_counter[channel] > 0:
                        return self.servo_positions[channel]
                    else:
                        return 0
                else:
                    if self.servo_power_counter[channel] == 0:
                        self.servo_power_counter[channel] = self.servo_power_counter_reset[channel]
                    self.servo_power_counter[channel] += 1
                    if self.servo_power_counter[channel] == 0:
                        return self.servo_positions[channel]
                    else:
                        return 0

            positions = [calculate_position(channel) for channel in range(16)]
            self.pca9685.set_all_servo_positions(positions)
        else:
            if self._last_global_off != self.global_off:
                self._last_global_off = self.global_off
                if DEBUG: print(f"### global OFF")  #; global_off={self.global_off}, _global_off={self._global_off}; _temporary_off={self._temporary_off}")
            self.pca9685.set_all_servo_positions([0 for _channel in range(16)])
