
import smbus


# noinspection PyPep8Naming
class INA3221:
    _DEFAULT_I2C_BUS = 1
    _DEFAULT_I2C_ADDRESS = 0x40

    _REG_CONFIG = 0x00
    _REG_SHUNTVOLTAGE_1 = 0x01
    _REG_BUSVOLTAGE_1 = 0x02
    _REG_SHUNSUMTVOLTAGE_1 = 0x0D

    _ENABLE_CHAN1 = 0x4000
    _ENABLE_CHAN2 = 0x2000
    _ENABLE_CHAN3 = 0x1000

    _AVG2 = 0x0800
    _AVG1 = 0x0400
    _AVG0 = 0x0200

    _VBUS_CT2 = 0x0100
    _VBUS_CT1 = 0x0080
    _VBUS_CT0 = 0x0040

    _VSH_CT2 = 0x0020
    _VSH_CT1 = 0x0010
    _VSH_CT0 = 0x0008

    _MODE_2 = 0x0004
    _MODE_1 = 0x0002
    _MODE_0 = 0x0001

    _DEFAULT_SHUNT_RESISTOR_VALUE = 0.1
    
    ###########################
    # INA3221 Code
    ###########################
    def __init__(self,
                 i2c_bus: int = _DEFAULT_I2C_BUS,
                 i2c_addr: int = _DEFAULT_I2C_ADDRESS,
                 shunt_resistor: int = _DEFAULT_SHUNT_RESISTOR_VALUE) -> None:
        self._i2c_bus = smbus.SMBus(i2c_bus)
        self._i2c_addr = i2c_addr
        self._shunt_resistor = shunt_resistor

        config = 0
        config |= INA3221._ENABLE_CHAN1
        config |= INA3221._ENABLE_CHAN2
        config |= INA3221._ENABLE_CHAN3
        # config |= INA3221.AVG0
        config |= INA3221._AVG1
        # config |= INA3221.AVG2
        # config |= INA3221.VBUS_CT0
        # config |= INA3221.VBUS_CT1
        config |= INA3221._VBUS_CT2
        # config |= INA3221.VSH_CT0
        # config |= INA3221.VSH_CT1
        config |= INA3221._VSH_CT2
        config |= INA3221._MODE_2
        # config |= INA3221.MODE_1
        config |= INA3221._MODE_0

        self._write_word(INA3221._REG_CONFIG, config)

    def _read_word(self, register: int) -> int:
        result = self._i2c_bus.read_word_data(self._i2c_addr, register) & 0xFFFF
        return ((result & 0xFF00) >> 8) + ((result & 0x00FF) << 8)

    def _write_word(self, register: int, data: int) -> None:
        data = data & 0xFFFF
        self._i2c_bus.write_word_data(self._i2c_addr, register, (data >> 8) + ((data & 0x00FF) << 8))

    def get_bus_voltage(self, channel: int) -> int:
        value = self._read_word(INA3221._REG_BUSVOLTAGE_1 + (channel - 1) * 2)
        return value - 65536 if value > 32767 else value

    def get_shunt_voltage(self, channel: int) -> int:
        value = self._read_word(INA3221._REG_SHUNTVOLTAGE_1 + (channel - 1) * 2)
        return value - 65536 if value > 32767 else value

    def get_bus_voltage_V(self, channel: int) -> float:
        # 32760 is 32.76V => 1 is 0.001V (32.76 / 32760 = 0.0001)
        return self.get_bus_voltage(channel) * 0.001

    def get_shunt_voltage_mV(self, channel: int) -> float:
        # 32760 is 163.8mV => 1 is 0.005 mV (163.8 / 32760 = 0.005)
        return self.get_shunt_voltage(channel) * 0.005

    def get_current_mA(self, channel: int) -> float:
        return self.get_shunt_voltage_mV(channel) / self._shunt_resistor

    def get_sum_shunt_voltage(self) -> int:
        value = self._read_word(INA3221._REG_SHUNSUMTVOLTAGE_1)
        return value - 65536 if value > 32767 else value

    def get_sum_shunt_voltage_mV(self) -> float:
        # 32760 is 163.8mV => 1 is 0.005 mV (163.8 / 32760 = 0.005)
        return self.get_sum_shunt_voltage() * 0.005

    def get_sum_current_mA(self) -> float:
        return self.get_sum_shunt_voltage_mV() / self._shunt_resistor
