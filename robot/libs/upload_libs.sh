#!/bin/bash

PYROS_HOST_AND_PORT=$1

if [[ ( -z "$PYROS_HOST_AND_PORT" ) ]]; then
  PYROS_HOST_AND_PORT="default"
fi

pyros $PYROS_HOST_AND_PORT upload -v servo_lib -e servo_lib/*
pyros $PYROS_HOST_AND_PORT upload -v ina219 -e ina219/*
pyros $PYROS_HOST_AND_PORT upload -v pcs9685ina3221_servo_lib -e pcs9685ina3221_servo_lib/*

pyros $PYROS_HOST_AND_PORT upload -v flig_definition  -e flig_definition_lib/DISCOVERY.py flig_definition_lib/__init__.py
