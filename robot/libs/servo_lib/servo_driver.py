import abc
from typing import Optional, Callable, List

from servo_lib.leg_servos import ServoDefinition

DEBUG = True


# noinspection PyPep8Naming
class ServoDriver(abc.ABC):
    def __init__(self, send_immediately: bool = True) -> None:
        self._send_immediately = send_immediately
        self._global_off = False
        self._global_power = 100
        self._global_on_off_callbacks: List[Callable[[bool], None]] = []

    def add_callback(self, callback: Callable[[bool], None]) -> bool:
        if callback not in self._global_on_off_callbacks:
            self._global_on_off_callbacks.append(callback)
            return True
        return False

    def remove_callback(self, callback: Callable[[bool], None]) -> bool:
        if callback in self._global_on_off_callbacks:
            del self._global_on_off_callbacks[self._global_on_off_callbacks.index(callback)]
            return True
        return False

    @property
    def global_off(self) -> bool:
        return self._global_off

    @global_off.setter
    def global_off(self, global_off_state: bool) -> None:
        if self._global_off != global_off_state:
            self._global_off = global_off_state
            [callback(global_off_state) for callback in self._global_on_off_callbacks]

    def stop_all(self):
        pass

    @property
    def global_power(self) -> int:
        return self._global_power

    @global_power.setter
    def global_power(self, global_power: int) -> None:
        self._global_power = global_power

    @property
    def send_immediately(self) -> bool: return self._send_immediately

    @send_immediately.setter
    def send_immediately(self, send_immediately: bool) -> None:
        self._send_immediately = send_immediately

    def process(self, current_time: float) -> None:
        pass

    @abc.abstractmethod
    def set_position(self, servo: ServoDefinition, position: int, power: Optional[int] = None) -> None: pass

    def set_angle(self, servo: ServoDefinition, angle: float, power: Optional[int] = None) -> None:
        servo_position = servo.calculate_position(angle)
        self.set_position(servo, servo_position, power)

    @abc.abstractmethod
    def get_current_mA(self, servo: ServoDefinition) -> float: pass
