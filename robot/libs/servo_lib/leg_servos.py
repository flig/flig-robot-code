from abc import ABC
from typing import Optional, Callable

import os

import pyros


DEBUG = False


class StorageValue:
    def __init__(self, path: str, default_value: any, is_integer: bool = True, modified_callback: Optional[Callable[[], None]] = None):
        self.modified_callback = modified_callback
        self.path = path
        self.is_integer = is_integer
        self._value: any = default_value
        self._old_value: any = default_value
        self._modified: bool = False
        self.received: bool = False

    @property
    def modified(self) -> bool: return self._modified

    @modified.setter
    def modified(self, new_modified: bool):
        if self._modified != new_modified:
            self._modified = new_modified
            if self.modified_callback is not None:
                self.modified_callback()

    @property
    def value(self) -> any: return self._value

    @value.setter
    def value(self, new_value):
        if self.is_integer:
            self._value = int(new_value)
        else:
            self._value = new_value
        if self.is_integer and '.' in str(self._value):
            raise ValueError(f"Got '.' for {self.path}, '{self._value}', old value '{new_value}")
        self.modified = new_value != self._old_value

    def __repr__(self):
        if not self.received:
            return f"@{self._value}"
        elif self.modified:
            return f"!{self._value}"
        else:
            return f"{self._value}"

    def receive_value(self, old_value):
        if old_value != "":
            if self.is_integer:
                self._value = int(old_value)
            else:
                self._value = old_value
            if self.is_integer and '.' in str(self._value):
                raise ValueError(f"Got '.' for {self.path}, '{self._value}', old value '{old_value}")
            self._old_value = self._value
            self.modified = False
            self.received = True
            if DEBUG: print(f"Got value '{old_value}' for path {self.path}")

    def undo(self):
        self._value = self._old_value
        self.modified = False

    def storage_callback(self, _topic, message, _group):
        try:
            int_value = int(message)
            self.receive_value(int_value)
        except ValueError:
            try:
                float_value = float(message)
                int_value = int(float_value)
                self.receive_value(int_value)
            except ValueError:
                self.receive_value(message)

    def subscribe(self):
        subscribe_read_topic = f"storage/read/legs/cal/{self.path}"
        subscribe_write_topic = f"storage/write/legs/cal/{self.path}"

        pyros.subscribe(subscribe_write_topic, self.storage_callback)

        if DEBUG: print(f"Subscribing to {subscribe_write_topic}")
        pyros.publish(subscribe_read_topic, "")

    def publish(self):
        try:
            value = self._value
            if not type(value) is int:
                if abs(value > 0.1):
                    value = "{0:.2f}".format(value)
                elif abs(value > 0.01):
                    value = "{0:.3f}".format(value)
                elif abs(value > 0.001):
                    value = "{0:.4f}".format(value)
                elif abs(value > 0.0001):
                    value = "{0:.5f}".format(value)
                else:
                    value = "{0:.6f}".format(value)
        except ValueError:
            value = self._value
        pyros.publish(f"storage/write/legs/cal/{self.path}", str(value))
        if DEBUG: print(f"Publishing on storage/write/legs/cal/{self.path} '{str(value)}'")


class CommonServos(ABC):
    SERVO_DEFINITION_FIELDS = [
        "servo_channel",
        "limit_lower",
        "minus_90", "minus_45", "middle", "plus_45", "plus_90",
        "limit_upper",
        "current_channel",
        "speed"
    ]

    def __init__(self):
        self.modified = False
        self.servos = {}

    def get_servo_definition_for_channel(self, channel: int) -> Optional['ServoDefinition']:
        for servo_group in self.servos:
            for servo_id in self.servos[servo_group]:
                if self.servos[servo_group][servo_id].servo_channel.value == channel:
                    return self.servos[servo_group][servo_id]
        return None

    def request_calibration_details(self):
        def send_read_recursively(_map):
            for key in _map:
                if type(_map[key]) is dict:
                    send_read_recursively(_map[key])
                elif not type(_map[key]) is ServoDefinition:
                    raise ValueError(f"Unknown type {type(_map[key])} for '{_map[key]}'")
                else:
                    for field in CommonServos.SERVO_DEFINITION_FIELDS:
                        getattr(_map[key], field).subscribe()

        # if "PYROS_DATA" in os.environ:
        #     pyros_data = os.environ["PYROS_DATA"]
        #     storage_filename = os.path.join(pyros_data, "storage.config")
        #     if os.path.exists(storage_filename):
        #         with open(storage_filename) as f:
        #             for line in f.readlines():
        #                 if line.startswith("legs/cal/"):
        #                     key, value = line.split("=")
        #                     key = key[9:]
        #                     path = key.split("/")
        #                     if path[0] in self.servos:
        #                         if path[1] in self.servos[path[0]]:
        #                             servo_def = self.servos[path[0]][path[1]]
        #
        #         s = f"legs/cal/{self.path}"

        send_read_recursively(self.servos)

    def check_if_calibration_has_arrived(self):
        def check_recursively(_map) -> bool:
            for key in _map:
                if type(_map[key]) is dict:
                    if not check_recursively(_map[key]):
                        return False
                elif not type(_map[key]) is ServoDefinition:
                    raise ValueError(f"Unknown type {type(_map[key])} for '{_map[key]}'")
                else:
                    for field in CommonServos.SERVO_DEFINITION_FIELDS:
                        if not getattr(_map[key], field).received:
                            return False
            return True

        return check_recursively(self.servos)

    def save_calibration_details(self, *_args):
        def send_write_recursively(_map):
            for key in _map:
                if type(_map[key]) is dict:
                    send_write_recursively(_map[key])
                elif not type(_map[key] is ServoDefinition):
                    raise ValueError(f"Unknown type {type(_map[key])} for '{_map[key]}'")
                else:
                    for field in CommonServos.SERVO_DEFINITION_FIELDS:
                        try:
                            getattr(_map[key], field).publish()
                        except Exception as e:
                            print(f"Cannot publish for key '{key}' object '{_map[key]}', error {e}")
                    # _map[key].publish()

        send_write_recursively(self.servos)
        self.modified = False

    def undo_change(self, *_args):
        def undo_recursively(_map):
            for key in _map:
                if type(_map[key]) is dict:
                    undo_recursively(_map[key])
                elif not type(_map[key] is ServoDefinition):
                    raise ValueError(f"Unknown type {type(_map[key])} for '{_map[key]}'")
                else:
                    for field in CommonServos.SERVO_DEFINITION_FIELDS:
                        try:
                            getattr(_map[key], field).undo()
                        except Exception as e:
                            print(f"Cannot publish for key '{key}' object '{_map[key]}', error {e}")

        undo_recursively(self.servos)

    def recalculate_modified(self):
        def recursively_find_modified(_map) -> bool:
            for key in _map:
                if type(_map[key]) is dict:
                    modified = recursively_find_modified(_map[key])
                    if modified:
                        return True
                elif not type(_map[key] is ServoDefinition):
                    raise ValueError(f"Unknown type {type(_map[key])} for '{_map[key]}'")
                else:
                    for field in CommonServos.SERVO_DEFINITION_FIELDS:
                        # noinspection PyBroadException
                        try:
                            if getattr(_map[key], field).modified:
                                return True
                        except Exception as _:
                            print(f"Cannot get '{field}' from {_map[key]}")
            return False

        self.modified = recursively_find_modified(self.servos)


class ExtraServos(CommonServos):
    SERVO_GROUPS = ["extra"]
    SERVOS = ["0", "1", "2", "3"]

    def __init__(self):
        super().__init__()
        servo_channel = 0
        for group in self.SERVO_GROUPS:
            self.servos[group] = {}
            for servo in self.SERVOS:
                self.servos[group][servo] = ServoDefinition(f"{group}/{servo}", servo_channel, modified_callback=self.recalculate_modified)
                servo_channel += 1


class LegServos(CommonServos):
    LEGS = ["br", "bl", "fr", "fl"]
    LEG_SERVOS = ["shoulder", "thigh", "shin"]

    def __init__(self):
        super().__init__()
        servo_channel = 0
        for leg in LegServos.LEGS:
            self.servos[leg] = {}
            for servo in LegServos.LEG_SERVOS:
                self.servos[leg][servo] = ServoDefinition(f"{leg}/{servo}", servo_channel, modified_callback=self.recalculate_modified)
                servo_channel += 1


class ServoDefinition:
    def __init__(self, path: str, servo_channel: int, modified_callback: Optional[Callable[[], None]] = None) -> None:
        self.servo_channel = StorageValue(f"{path}/servo_channel", servo_channel, modified_callback=modified_callback)
        self.current_channel = StorageValue(f"{path}/current_channel", 5 - (servo_channel // 2), modified_callback=modified_callback)
        self.limit_lower = StorageValue(f"{path}/limit_lower", 700, modified_callback=modified_callback)
        self.minus_90 = StorageValue(f"{path}/minus_90", 800, modified_callback=modified_callback)
        self.minus_45 = StorageValue(f"{path}/minus_45", 1175, modified_callback=modified_callback)
        self.middle = StorageValue(f"{path}/middle", 1550, modified_callback=modified_callback)
        self.plus_45 = StorageValue(f"{path}/plus_45", 1975, modified_callback=modified_callback)
        self.plus_90 = StorageValue(f"{path}/plus_90", 2300, modified_callback=modified_callback)
        self.limit_upper = StorageValue(f"{path}/limit_upper", 2400, modified_callback=modified_callback)
        self.speed = StorageValue(f"{path}/speed", 7200, modified_callback=modified_callback)
        self.max_angle = 0
        self.min_angle = 0
        self.update_values()

    def update_values(self) -> None:
        self.min_angle = (self.minus_90.value - self.limit_lower.value) * 45.0 / (self.minus_90.value - self.minus_45.value) - 90
        self.max_angle = (self.limit_upper.value - self.plus_90.value) * 45.0 / (self.plus_90.value - self.plus_45.value) + 90

    def calculate_position(self, angle: Optional[float]) -> int:
        if angle is None:
            position = 0
        elif angle < -45:
            position = (angle + 90.0) * (self.minus_45.value - self.minus_90.value) / 45.0 + self.minus_90.value
        elif angle < 0:
            position = (angle + 45.0) * (self.middle.value - self.minus_45.value) / 45.0 + self.minus_45.value
        elif angle > 45:
            position = (angle - 45.0) * (self.plus_90.value - self.plus_45.value) / 45.0 + self.plus_45.value
        elif angle > 0:
            position = angle * (self.plus_45.value - self.middle.value) / 45.0 + self.middle.value
        else:
            position = self.middle.value

        position = int(position)

        if position != 0:
            if self.minus_45.value < self.middle.value:
                if position < self.limit_lower.value:
                    position = self.limit_lower.value
            else:
                if position > self.limit_lower.value:
                    position = self.limit_lower.value
            if self.plus_45.value > self.middle.value:
                if position > self.limit_upper.value:
                    position = self.limit_upper.value
            else:
                if position < self.limit_upper.value:
                    position = self.limit_upper.value

        return position

    def stop(self):
        pyros.publish(f"servo/{self.servo_channel.value}/pos", "0")

    def send_value(self, position):
        pyros.publish(f"servo/{self.servo_channel.value}/pos", str(position))

    @staticmethod
    def max_angle(_channel) -> float:
        return 0

    @staticmethod
    def min_angle(_channel) -> float:
        return 0


if __name__ == "__main__":
    s = ServoDefinition("", 0)

    for a in range(-90, 90, 10):
        print(f"{s.calculate_position(a)}")
